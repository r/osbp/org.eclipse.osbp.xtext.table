/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.table.ui.labeling

import com.google.inject.Inject
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider
import org.eclipse.osbp.infogrid.model.gridsource.CxGridProperty
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropStyle
import org.eclipse.osbp.xtext.basic.ui.labeling.BasicDSLLabelProvider
import org.eclipse.osbp.xtext.table.Table
import org.eclipse.osbp.xtext.table.TableAggregation
import org.eclipse.osbp.xtext.table.TableAllColumns
import org.eclipse.osbp.xtext.table.TableAttribute
import org.eclipse.osbp.xtext.table.TableAxis
import org.eclipse.osbp.xtext.table.TableCellColor
import org.eclipse.osbp.xtext.table.TableColumn
import org.eclipse.osbp.xtext.table.TableDatamart
import org.eclipse.osbp.xtext.table.TableDateDayLookup
import org.eclipse.osbp.xtext.table.TableDerived
import org.eclipse.osbp.xtext.table.TableDtoDatasource
import org.eclipse.osbp.xtext.table.TableEvent
import org.eclipse.osbp.xtext.table.TableFormatter
import org.eclipse.osbp.xtext.table.TableGrid
import org.eclipse.osbp.xtext.table.TableHierarchy
import org.eclipse.osbp.xtext.table.TableIcon
import org.eclipse.osbp.xtext.table.TableInterval
import org.eclipse.osbp.xtext.table.TableLookup
import org.eclipse.osbp.xtext.table.TableMeasure
import org.eclipse.osbp.xtext.table.TableModel
import org.eclipse.osbp.xtext.table.TableNumberLookup
import org.eclipse.osbp.xtext.table.TableOrdinal
import org.eclipse.osbp.xtext.table.TablePackage
import org.eclipse.osbp.xtext.table.TablePreorder
import org.eclipse.osbp.xtext.table.TableStringLookup
import org.eclipse.osbp.xtext.table.TableTable
import org.eclipse.osbp.xtext.table.TableTask
import org.eclipse.osbp.xtext.table.TableTextColor
import org.eclipse.osbp.xtext.table.TableTooltip
import org.eclipse.osbp.xtext.table.TableTrend
import org.eclipse.osbp.xtext.table.TableValue

/**
 * Provides labels for a EObjects.
 *
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
class TableDSLLabelProvider extends BasicDSLLabelProvider {

	@Inject
	new(AdapterFactoryLabelProvider delegate) {
		super(delegate);
	}
	// Labels and icons can be computed like this:

	 override text ( Object o ) {
		switch (o) {
			TablePackage		: generateText( o, 'package'        , o.name )
			Table				: generateText( o, 'table model'    , o.name )
			TableTable			: generateText( o, 'table'          , o.source.datamartRef.name )
			TableGrid			: generateText( o, 'grid'           , o.source.dtoSource.name )
			TableAxis			: generateText( o, 'axis'           , o.axis.literal )
			TableDatamart		: generateText( o, 'datamart'       , o.datamartRef.name )
			TableEvent			: generateText( o, 'event'    )
			TableValue			: generateText( o, 'value'    )
			TableInterval		: generateText( o, 'interval' )
			TableFormatter		: generateText( o, 'formatter'      , o.format )
			TableTextColor		: generateText( o, '''textcolor («o.rgb»)''' )
			TableCellColor		: generateText( o, '''cellcolor («o.rgb»)''' )
			TableIcon			: generateText( o, 'icon'           , o.icon )
			TableTooltip		: generateText( o, 'tooltip'        , o.tooltip )
			TableTrend			: generateText( o, 'trend'          , o.icon.toString )

			TableAttribute		: generateText( o, 'table property' , o.valueRef.attributeRef.name )
			TableAggregation	: generateText( o, 'aggregation'    , o.valueRef.aggregation.toString )
			TableHierarchy		: generateText( o, 'level'          , o.valueRef.name )
			TableMeasure		: generateText( o, 'measure'        , o.valueRef.measureRef.name )
			TableDerived		: generateText( o, 'derived'        , o.valueRef.derivedRef.name )
			TableOrdinal		: generateText( o, 'ordinal'        , o.valueRef.toString )
			TableColumn			: generateText( o, 'column'         , o.valueRef.columnRef.toString )
			TableTask			: generateText( o, 'task'           )
			TableAllColumns		: generateText( o, 'all columns'    )
//			TableValueElement	: generateText( o, 'value element'  )

			TablePreorder		: generateText( o, 'pre order'      )
//			CxGridNestedField	: generateText( o, 'nested field'   )
			CxGridProperty		: generateText( o, 'grid property'  )
//			TableGridProperty	: generateText( o, 'grid property'  )
//			CxGridNestedField	: generateText( o, 'nested field'   )
//			CxGridPropTextStyle	: generateText( o, 'text style'     )
			CxGridPropStyle		: generateText( o, 'property style' )
//			TableDtoDatasource	: generateText( o, 'dto datasource' )
			TableDtoDatasource	: generateText( o, 'dto datasource' , o.dtoSource.name )
//			TableLookup			: generateText( o, 'lookup'         , o.toString )
			TableStringLookup	: generateText( o, 'string lookup'  , o.lookupValue )
			TableNumberLookup	: generateText( o, 'number lookup'  , o.lookupValue.toString )
			TableDateDayLookup	: generateText( o, 'date lookup'    , o.lookupValue.toString )
			default				: super.text( o )
		}
	}

	override image ( Object o ) {
		switch (o) {
			TableModel			: getInternalImage( 'model.png'        , class )
			TablePackage		: getInternalImage( 'package.gif'      , class )
			Table				: getInternalImage( 'dsl_table.png'    , class )
			TableTable			: getInternalImage( 'table-table.png'  , class )
			TableGrid			: getInternalImage( 'table-grid.png'   , class )
			TableAxis			: getInternalImage( 'axis.png'         , class )
			TableDatamart		: getInternalImage( 'dsl_datamart.png' , class )
			TableEvent			: getInternalImage( 'event.png'        , class )
			TableValue			: getInternalImage( 'value3.png'       , class )
			TableInterval		: getInternalImage( 'interval.png'     , class )
			TableFormatter		: getInternalImage( 'format.png'       , class )
			TableTextColor		: getColorOutlineImage( o.rgb, true )
			TableCellColor		: getColorOutlineImage( o.rgb, true )
			TableIcon			: getInternalImage( 'img.png'          , class )
			TableTooltip		: getInternalImage( 'tooltip.png'      , class )
			TableTrend			: getInternalImage( 'trend.png'        , class )
			TableAttribute		: getInternalImage( 'properties.png'   , class )
			TableAggregation	: getInternalImage( 'aggregate.png'    , class )
			TableAllColumns		: getInternalImage( 'column.png'       , class )
			TableHierarchy		: getInternalImage( 'level.png'        , class )
			TableMeasure		: getInternalImage( 'measure.png'      , class )
			TableDerived		: getInternalImage( 'derived.png'      , class )
			TableTask			: getInternalImage( 'task.png'         , class )
			TableColumn			: getInternalImage( 'column.png'       , class )
			TableLookup			: getInternalImage( 'lookup.png'       , class )
			TablePreorder		: getInternalImage( 'pre-order.png'    , class )
			CxGridProperty		: getInternalImage( 'properties.png'   , class )
			CxGridPropStyle		: getInternalImage( 'style.png'        , class )
			TableDtoDatasource	: getInternalImage( 'datasource.png'   , class )
			default				: super.image( o )

//			TableTrend has no own outline element - it is displayed within upper level element instead (e.g. interval)!
//			TableTrend			:	getInternalImage( 'trend_rising.png', class)
		}
	}
}
