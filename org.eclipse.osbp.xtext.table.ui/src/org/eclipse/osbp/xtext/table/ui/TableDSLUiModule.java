/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.table.ui;

import org.eclipse.osbp.xtext.basic.ui.BasicDSLUiModuleHelper;
import org.eclipse.osbp.xtext.oxtype.imports.IUnresolvedEObjectResolver;
import org.eclipse.osbp.xtext.oxtype.ui.contentassist.OXTypeReplacingAppendable;
import org.eclipse.osbp.xtext.oxtype.ui.contentassist.OXtypeProposalProvider;
import org.eclipse.osbp.xtext.oxtype.ui.imports.InteractiveUnresolvedEClassResolver;
import org.eclipse.osbp.xtext.oxtype.ui.quickfix.CustomJavaTypeQuickfixes;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.ui.editor.contentassist.AbstractJavaBasedContentProposalProvider;
import org.eclipse.xtext.ui.editor.hover.IEObjectHover;
import org.eclipse.xtext.ui.editor.hover.IEObjectHoverProvider;
import org.eclipse.xtext.ui.editor.hover.html.IEObjectHoverDocumentationProvider;
import org.eclipse.xtext.xbase.ui.contentassist.ReplacingAppendable;
import org.eclipse.xtext.xbase.ui.quickfix.JavaTypeQuickfixes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Use this class to register components to be used within the IDE.
 */
public class TableDSLUiModule extends
		org.eclipse.osbp.xtext.table.ui.AbstractTableDSLUiModule {

	private static Logger LOGGER = LoggerFactory
			.getLogger(TableDSLUiModule.class);

	public TableDSLUiModule(AbstractUIPlugin plugin) {
		super(plugin);
		BasicDSLUiModuleHelper.unitTestTemplates(plugin, LOGGER);
	}
	
	@Override
	public Class<? extends AbstractJavaBasedContentProposalProvider.ReferenceProposalCreator> bindAbstractJavaBasedContentProposalProvider$ReferenceProposalCreator() {
		return OXtypeProposalProvider.CustomReferenceProposalCreator.class;
	}

	@Override
	public Class<? extends IEObjectHover> bindIEObjectHover() {
		return TableDSLEObjectHover.class;
	}

	@Override
	public Class<? extends IEObjectHoverProvider> bindIEObjectHoverProvider() {
		return TableDSLEObjectHoverProvider.class;
	}

	@Override
	public Class<? extends IEObjectHoverDocumentationProvider> bindIEObjectHoverDocumentationProvider() {
		return TableDSLEObjectHoverDocumentationProvider.class;
	}

	@SuppressWarnings("restriction")
	public Class<? extends ReplacingAppendable.Factory> bindReplacingAppendable$Factory() {
		return OXTypeReplacingAppendable.Factory.class;
	}

	public Class<? extends IUnresolvedEObjectResolver> bindIUnresolvedEObjectResolver() {
		return InteractiveUnresolvedEClassResolver.class;
	}

	public Class<? extends JavaTypeQuickfixes> bindJavaTypeQuickfixes() {
		return CustomJavaTypeQuickfixes.class;
	}

}
