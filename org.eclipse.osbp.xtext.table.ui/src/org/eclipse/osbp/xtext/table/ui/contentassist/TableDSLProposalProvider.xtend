/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
package org.eclipse.osbp.xtext.table.ui.contentassist

import javax.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.jface.viewers.StyledString
import org.eclipse.osbp.dsl.common.datatypes.IDto
import org.eclipse.osbp.infogrid.model.gridsource.CxGridNestedField
import org.eclipse.osbp.infogrid.model.gridsource.CxGridNestedPath
import org.eclipse.osbp.infogrid.model.gridsource.CxGridProperty
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropBlobImageStyle
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropBooleanStyle
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropButtonStyle
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropDateStyle
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropHtmlStyle
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropImageStyle
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropNumberStyle
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropPriceStyle
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropProgressbarStyle
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropQuantityStyle
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropTextStyle
import org.eclipse.osbp.xtext.basic.ui.contentassist.BasicDSLProposalProviderHelper
import org.eclipse.osbp.xtext.datamartdsl.DatamartCube
import org.eclipse.osbp.xtext.datamartdsl.DatamartCubeAxis
import org.eclipse.osbp.xtext.datamartdsl.DatamartDerivedMeasure
import org.eclipse.osbp.xtext.datamartdsl.DatamartEntity
import org.eclipse.osbp.xtext.datamartdsl.DatamartHierarchy
import org.eclipse.osbp.xtext.datamartdsl.DatamartMeasure
import org.eclipse.osbp.xtext.datamartdsl.DatamartSetAggregation
import org.eclipse.osbp.xtext.datamartdsl.DatamartTask
import org.eclipse.osbp.xtext.datamartdsl.jvmmodel.DatamartDSLJvmModelInferrer
import org.eclipse.osbp.xtext.gridsource.scoping.TypeHelper
import org.eclipse.osbp.xtext.gridsource.ui.contentassist.GridSourceProposalProvider
import org.eclipse.osbp.xtext.table.Table
import org.eclipse.osbp.xtext.table.TableAxis
import org.eclipse.osbp.xtext.table.TableDatamart
import org.eclipse.osbp.xtext.table.TableEvent
import org.eclipse.osbp.xtext.table.TableInterval
import org.eclipse.osbp.xtext.table.TableLookup
import org.eclipse.osbp.xtext.table.TablePreorder
import org.eclipse.osbp.xtext.table.TableRangeElement
import org.eclipse.osbp.xtext.table.TableTable
import org.eclipse.osbp.xtext.table.TableValue
import org.eclipse.osbp.xtext.table.ui.TableDSLDocumentationTranslator
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.layout.FillLayout
import org.eclipse.swt.widgets.ColorDialog
import org.eclipse.swt.widgets.Shell
import org.eclipse.xtext.AbstractElement
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.ParserRule
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.XtextPackage
import org.eclipse.xtext.common.types.JvmDeclaredType
import org.eclipse.xtext.common.ui.contentassist.TerminalsProposalProvider
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.eclipse.xtext.ui.editor.contentassist.ReplacementTextApplier
import org.mihalis.opal.imageSelector.ImageSelectorDialog

class ImageFileNameTextApplier extends ReplacementTextApplier {
	var ContentAssistContext context
	var String[] extensions

	def setContext(ContentAssistContext context) {
		this.context = context
	}

	def setExtensions(String[] fileExtensions) {
		extensions = fileExtensions
	}

	// this will inject a file dialog when selecting the file picker proposal 
	override getActualReplacementString(ConfigurableCompletionProposal proposal) {
		var display = context.getViewer().getTextWidget().getDisplay();
		var shell = new Shell(display);
		shell.setLocation(new Point(display.activeShell.location.x + 30, display.activeShell.location.y + 30))
		shell.setLayout(new FillLayout());
		var imageSelectorDialog = new ImageSelectorDialog(shell, 16);
		imageSelectorDialog.setFilterExtensions(extensions)
		var imageFileName = imageSelectorDialog.open(true);
		return if (imageFileName !== null) "\"".concat(imageFileName).concat("\"") else "";
	}
}

class MyReplacementTextApplier extends ReplacementTextApplier {
	var ContentAssistContext context

	def setContext(ContentAssistContext context) {
		this.context = context
	}

	// this will inject a color picker when selecting the color picker proposal	
	override getActualReplacementString(ConfigurableCompletionProposal proposal) {
		var display = context.getViewer().getTextWidget().getDisplay();
		var colorDialog = new ColorDialog(display.getActiveShell());
		var newColor = colorDialog.open();
		return "\"".concat(newColor.red.toString).concat(",").concat(newColor.green.toString).concat(",").concat(
			newColor.blue.toString).concat("\"");
	}
}

class TableDSLProposalProvider extends AbstractTableDSLProposalProvider {

	@Inject extension TypeHelper
	@Inject TerminalsProposalProvider provider
	@Inject BasicDSLProposalProviderHelper providerHelper

	@Inject extension DatamartDSLJvmModelInferrer datamartInferrer
	var imageSelectionPrompt = "Select image file..."
	var colorSelectionPrompt = "Pick color..."
	@Inject GridSourceProposalProvider gridProposalProvider

	/**
	 * This override will enable 1 length non letter characters as keyword.
	 */
	override protected boolean isKeywordWorthyToPropose(Keyword keyword) {
		return true
	}

	override protected StyledString getKeywordDisplayString(Keyword keyword) {
		return BasicDSLProposalProviderHelper.getKeywordDisplayString(keyword,
			TableDSLDocumentationTranslator.instance())
	}

	/* imageFilePickerProposal(model, assignment, context, acceptor, ".jpg,.jpeg,.png") */
	def imageFilePickerProposal(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor, String fileExtensions) {
		var fileName = createCompletionProposal(imageSelectionPrompt, context) as ConfigurableCompletionProposal
		if (fileName !== null) {
			var applier = new ImageFileNameTextApplier()
			applier.setExtensions(fileExtensions.split(","))
			applier.setContext(context)
			fileName.setTextApplier = applier
		}
		acceptor.accept(fileName)
	}

	/**
	 * This method decides which proposals will be valid. 
	 */
	override protected boolean isValidProposal(String proposal, String prefix, ContentAssistContext context) {
		var result = super.isValidProposal(proposal, prefix, context)
		if (context.currentModel instanceof CxGridNestedField) {
			return isGridNestedFieldValidProposal(context, proposal, result)
		} else if (context.currentModel instanceof CxGridNestedPath) {
			return isGridNestedPathValidProposal(context, proposal, result)
		}
		if (context.currentModel instanceof TableAxis) {
			return isAxisValidProposal(context, proposal, result)
		}
		if (context.currentModel instanceof TableValue || context.getCurrentModel() instanceof TablePreorder ||
			context.getCurrentModel() instanceof TableEvent) {
			return isValueValidProposal(context, proposal, result)
		} else if (context.currentModel instanceof TableInterval || context.currentModel instanceof TableLookup) {
			return isIntervalValidProposal(context, proposal, result)
		}
		return result
	}

	override createProposals(ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		if (context.currentModel instanceof CxGridProperty && context.lastCompleteNodeIsStyle) {
			for (AbstractElement element : context.getFirstSetGrammarElements()) {
				if (element.eClass.classifierID == XtextPackage.KEYWORD) {
					var keyword = element as Keyword
					if (isKeywordWorthyToPropose(keyword, context)) {
						val gridProp = context.currentModel as CxGridProperty
						val nestedField = gridProp.path
						var nestedFieldType = nestedField.field.returnType.type
						if (nestedFieldType instanceof JvmDeclaredType){
							for (type : (nestedFieldType as JvmDeclaredType).superTypes){
								if (IDto.name.equals(type.qualifiedName)) {
//									if (nestedField!==null && nestedField.path!==null && nestedField.path.field!==null && nestedField.path.field.returnType!==null){
										nestedFieldType = nestedField?.path?.field?.returnType?.type
//									}
								}
							}
						}
						var eObject = element as EObject
						while (!(eObject instanceof ParserRule)) {
							eObject = eObject.eContainer
						}
						if (eObject !== null) {
							var rule = eObject as ParserRule

							// no dotted nested field attribute path but complete nested field
							if (nestedFieldType===null) {
								var ruleTypeClass = rule.type?.classifier.instanceClass
								if (typeof(CxGridPropTextStyle).equals(ruleTypeClass)) {
									context.setProposal(acceptor, keyword)
								}
							} else if (nestedFieldType.boolean && rule.booleanStyle) {
								context.setProposal(acceptor, keyword)
							} else if (nestedFieldType.string && rule.textStyle) {
								context.setProposal(acceptor, keyword)
							} else if (nestedFieldType.number) {
								if (nestedFieldType.isNumberWithDigits  && rule.progressStyle){
									context.setProposal(acceptor, keyword)
								} else {
									if (rule.numberStyle){
										context.setProposal(acceptor, keyword)
									}
								}
							} else if (nestedFieldType.date && rule.dateStyle) {
								context.setProposal(acceptor, keyword)
							}
						}
					}
				}
			}
		} else {
			super.createProposals(context, acceptor)
		}
	}
	
	def getLastCompleteNodeIsStyle(ContentAssistContext context) {
		val element = context.lastCompleteNode?.grammarElement
		if (element.eClass.classifierID == XtextPackage.KEYWORD) {
			var keyword = element as Keyword
			if ("style".equals(keyword.value)){
				return true
			}
		}
		return false
	}
	
	def boolean isBooleanStyle(ParserRule rule){
		var ruleTypeClass = rule.type?.classifier.instanceClass
		return	typeof(CxGridPropBooleanStyle).equals(ruleTypeClass)
	}

	def boolean isTextStyle(ParserRule rule){
		var ruleTypeClass = rule.type?.classifier.instanceClass
		return	(typeof(CxGridPropTextStyle).equals(ruleTypeClass) || typeof(CxGridPropHtmlStyle).equals(ruleTypeClass) || typeof(CxGridPropButtonStyle).equals(ruleTypeClass) || typeof(CxGridPropImageStyle).equals(ruleTypeClass) || typeof(CxGridPropBlobImageStyle).equals(ruleTypeClass))
	}

	def boolean isNumberStyle(ParserRule rule){
		var ruleTypeClass = rule.type?.classifier.instanceClass
		return	(typeof(CxGridPropNumberStyle).equals(ruleTypeClass) || typeof(CxGridPropPriceStyle).equals(ruleTypeClass) || typeof(CxGridPropQuantityStyle).equals(ruleTypeClass) || typeof(CxGridPropImageStyle).equals(ruleTypeClass))
	}

	def boolean isProgressStyle(ParserRule rule){
		var ruleTypeClass = rule.type?.classifier.instanceClass
		return	typeof(CxGridPropProgressbarStyle).equals(ruleTypeClass)
	}

	def boolean isDateStyle(ParserRule rule){
		var ruleTypeClass = rule.type?.classifier.instanceClass
		return	typeof(CxGridPropDateStyle).equals(ruleTypeClass)
	}

	def setProposal(ContentAssistContext context, ICompletionProposalAcceptor acceptor, Keyword keyword) {
		val proposal = createCompletionProposal(keyword.getValue(), getKeywordDisplayString(keyword), getImage(keyword),
			context);
		acceptor.accept(proposal)

	}

	override completeTableIcon_Icon(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		imageFilePickerProposal(model, assignment, context, acceptor, ".jpg,.jpeg,.png");
	}

	override completeTableValue_IconName(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		imageFilePickerProposal(model, assignment, context, acceptor, ".jpg,.jpeg,.png");
	}

	override completeTableFormatter_Format(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {

		// predefined formats
		acceptor.accept(createCompletionProposal("\"###,##0.00\" /* money */", "money", null, context));
		acceptor.accept(
			createCompletionProposal("\"###,##0.00 &curren;\" /* local national currency */", "local national currency",
				null, context));
		acceptor.accept(
			createCompletionProposal("\"###,##0.00 &curren;&curren;\" /* local international currency */",
				"local international currency", null, context));
		acceptor.accept(createCompletionProposal("\"#0.000\" /* quantity */", "quantity", null, context));
		acceptor.accept(
			createCompletionProposal("\"#0.00;(#0.00)\" /* negative in parentheses */", "negative in parentheses", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"##0.0 %\" /* ratio as percentage */", "ratio as percentage", null, context));
		acceptor.accept(
			createCompletionProposal("\"##0.0 %%\" /* ratio as per mill */", "ratio as per mill", null, context));
		acceptor.accept(createCompletionProposal("\"##0.0'%'\" /* percentage sign */", "percentage sign", null, context));
		acceptor.accept(createCompletionProposal("\"##0.0'?'\" /* per mill sign */", "per mill sign", null, context));
		acceptor.accept(createCompletionProposal("\"0.0000000000E00\" /* scientific */", "scientific", null, context));
		acceptor.accept(createCompletionProposal("\"SHORTDATE\" /* short date */", "short date", null, context));
		acceptor.accept(createCompletionProposal("\"MEDIUMDATE\" /* medium date */", "medium date", null, context));
		acceptor.accept(createCompletionProposal("\"LONGDATE\" /* long date */", "long date", null, context));
		acceptor.accept(createCompletionProposal("\"FULLDATE\" /* full date */", "full date", null, context));
		acceptor.accept(createCompletionProposal("\"SHORTTIME\" /* short time */", "short time", null, context));
		acceptor.accept(createCompletionProposal("\"MEDIUMTIME\" /* medium time */", "medium time", null, context));
		acceptor.accept(createCompletionProposal("\"LONGTIME\" /* long time */", "long time", null, context));
		acceptor.accept(createCompletionProposal("\"FULLTIME\" /* full time */", "full time", null, context));
		acceptor.accept(
			createCompletionProposal("\"SHORTDATESHORTTIME\" /* short date short time */", "short date short time", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"SHORTDATEMEDIUMTIME\" /* short date medium time */", "short date medium time",
				null, context));
		acceptor.accept(
			createCompletionProposal("\"SHORTDATELONGTIME\" /* short date long time */", "short date long time", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"SHORTDATEFULLTIME\" /* short date full time */", "short date full time", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"MEDIUMDATESHORTTIME\" /* medium date short time */", "medium date short time",
				null, context));
		acceptor.accept(
			createCompletionProposal("\"MEDIUMDATEMEDIUMTIME\" /* medium date medium time */", "medium date medium time",
				null, context));
		acceptor.accept(
			createCompletionProposal("\"MEDIUMDATELONGTIME\" /* medium date long time */", "medium date long time", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"MEDIUMDATEFULLTIME\" /* medium date full time */", "medium date full time", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"LONGDATESHORTTIME\" /* long date short time */", "long date short time", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"LONGDATEMEDIUMTIME\" /* long date medium time */", "long date medium time", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"LONGDATELONGTIME\" /* long date long time */", "long date long time", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"LONGDATEFULLTIME\" /* long date full time */", "long date full time", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"FULLDATESHORTTIME\" /* full date short time */", "full date short time", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"FULLDATEMEDIUMTIME\" /* full date medium time */", "full date medium time", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"FULLDATELONGTIME\" /* full date long time */", "full date long time", null,
				context));
		acceptor.accept(
			createCompletionProposal("\"FULLDATEFULLTIME\" /* full date full time */", "full date full time", null,
				context));
	}

	// we use this override to inject a color picker in the proposal provider as well as predefined color values
	override public void completeTableTextColor_Rgb(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		colorPickerProposal(model, assignment, context, acceptor)
	}

	override public void completeTableCellColor_Rgb(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		colorPickerProposal(model, assignment, context, acceptor)
	}

	def colorPickerProposal(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var pickColor = createCompletionProposal(colorSelectionPrompt, context) as ConfigurableCompletionProposal
		if (pickColor != null) {
			var applier = new MyReplacementTextApplier()
			applier.setContext(context)
			pickColor.setTextApplier = applier
		}
		acceptor.accept(pickColor)

		// predefined colors
		acceptor.accept(createCompletionProposal("\"230,128,64\" /* orange */", "orange", null, context));
		acceptor.accept(createCompletionProposal("\"255,82,82\" /* light red */", "light red", null, context));
		acceptor.accept(createCompletionProposal("\"255,0,0\" /* red */", "red", null, context));
		acceptor.accept(createCompletionProposal("\"204,102,102\" /* dark red */", "dark red", null, context));
		acceptor.accept(createCompletionProposal("\"255,255,128\" /*light yellow */", "light yellow", null, context));
		acceptor.accept(createCompletionProposal("\"255,255,0\" /* yellow */", "yellow", null, context));
		acceptor.accept(createCompletionProposal("\"128,128,0\" /* dark yellow */", "dark yellow", null, context));
		acceptor.accept(createCompletionProposal("\"102,204,102\" /* light green */", "light green", null, context));
		acceptor.accept(createCompletionProposal("\"0,255,0\" /* green */", "green", null, context));
		acceptor.accept(createCompletionProposal("\"147,183,95\" /* dark green */", "dark green", null, context));
		acceptor.accept(createCompletionProposal("\"102,102,204\" /* light blue */", "light blue", null, context));
		acceptor.accept(createCompletionProposal("\"0,0,255\" /* blue */", "blue", null, context));
		acceptor.accept(createCompletionProposal("\"147,95,183\" /* dark blue */", "dark blue", null, context));
		acceptor.accept(createCompletionProposal("\"0,0,0\" /* black */", "black", null, context));
		acceptor.accept(createCompletionProposal("\"255,255,255\" /* white */", "white", null, context));
		acceptor.accept(createCompletionProposal("\"0,0,255\" /* blue */", "blue", null, context));
		acceptor.accept(createCompletionProposal("\"0,0,128\" /* dark blue */", "dark blue", null, context));
		acceptor.accept(createCompletionProposal("\"82,182,255\" /* light blue */", "light blue", null, context));
	}

	def isValueValidProposal(ContentAssistContext context, String proposal, boolean result) {
		if (isGrammarToken(proposal)) {
			return true
		}
		var eObj = context.currentModel
		var requestedAxisName = ""
		while (!(eObj instanceof TableDatamart)) {
			eObj = eObj.eContainer
			if (eObj instanceof TableAxis) {
				requestedAxisName = (eObj as TableAxis).axis.literal
			}
		}
		if (eObj != null) {
			var datamart = (eObj as TableDatamart).datamartRef
			if (datamart != null && datamart.source != null) {
				if (datamart.source instanceof DatamartCube) {
					for (axis : (datamart.source as DatamartCube).axisslicer) {
						if (axis instanceof DatamartCubeAxis) {
							if ((axis as DatamartCubeAxis).axis.name.literal.equals(requestedAxisName)) {
								var hasMeasures = false
								var hasHierarchy = false
								var hasAggregation = false
								var isCrossjoined = false
								var isUndefined = false
								for (element : (axis as DatamartCubeAxis).elements) {
									if (element instanceof DatamartMeasure ||
										element instanceof DatamartDerivedMeasure) {
										hasMeasures = true
									}
								}
								for (element : (axis as DatamartCubeAxis).elements) {
									if (element instanceof DatamartHierarchy) {
										if (hasHierarchy) {
											isCrossjoined = true
										} else {
											hasHierarchy = true
										}

										// if hierarchy returns undefined columns, then use ordinal
										if ((element as DatamartHierarchy).allLevels) {
											isUndefined = true
										}
									}
									if (element instanceof DatamartSetAggregation) {
										if (hasAggregation) {
											isCrossjoined = true
										} else {
											hasAggregation = true
										}
										hasAggregation = true
									}
								}
								if (hasMeasures && (hasHierarchy || hasAggregation)) {
									isCrossjoined = true
								}
								if (isCrossjoined || isUndefined) {
									if ("ordinal".equals(proposal)) {
										return true
									}
									if ("allColumns".equals(proposal)) {
										return true
									}
									if ("all".equals(proposal)) {
										return true
									}
								}
								if (!isUndefined) {
									for (element : (axis as DatamartCubeAxis).elements) {
										if (element instanceof DatamartMeasure && "measure".equals(proposal)) {
											return true;
										}
										if (element instanceof DatamartDerivedMeasure && "derived".equals(proposal)) {
											return true;
										}
										if (element instanceof DatamartHierarchy /* && !hasMeasures*/ &&
											"level".equals(proposal)) {
											return true;
										}
										if (element instanceof DatamartSetAggregation /* && !hasMeasures*/ &&
											"aggregation".equals(proposal)) {
											return true;
										}
									}
								}
							}
						}
					}
				} else if (datamart.source instanceof DatamartEntity && "property".equals(proposal)) {
					return true
				} else if (datamart.source instanceof DatamartTask &&
					("column".equals(proposal) || "taskId".equals(proposal))) {
					return true
				}
			}
		}
		return false
	}

	def isGrammarToken(String proposal) {

		if (imageSelectionPrompt.equals(proposal)) {
			return true
		}
		if ("icon".equals(proposal)) {
			return true
		}
		if ("image".equals(proposal)) {
			return true
		}
		if ("using".equals(proposal)) {
			return true
		}
		if ("preorder".equals(proposal)) {
			return true
		}
		if ("intervals".equals(proposal)) {
			return true
		}
		if ("lookups".equals(proposal)) {
			return true
		}
		if ("formatter".equals(proposal)) {
			return true
		}
		if ("hidelabel".equals(proposal)) {
			return true
		}
		if ("collapse".equals(proposal)) {
			return true
		}
		if ("upToNumber".equals(proposal)) {
			return true
		}
		if ("upToInteger".equals(proposal)) {
			return true
		}
		if ("number".equals(proposal)) {
			return true
		}
		if ("integer".equals(proposal)) {
			return true
		}
		if ("string".equals(proposal)) {
			return true
		}
		if ("daysInPast".equals(proposal)) {
			return true
		}
		if ("to".equals(proposal)) {
			return true
		}
		if ("datamart".equals(proposal)) {
			return true
		}
		if ("{".equals(proposal)) {
			return true
		}
		if ("}".equals(proposal)) {
			return true
		}
		return false
	}

	def isGridNestedFieldValidProposal(ContentAssistContext context, String proposal, boolean b) {
		var model = context.currentModel
		if (model instanceof CxGridNestedField) {
			var nestedFieldType = (model as CxGridNestedField)?.field?.returnType?.type
			if (nestedFieldType instanceof JvmDeclaredType){
				for (type : (nestedFieldType as JvmDeclaredType).superTypes){
					if (IDto.name.equals(type.qualifiedName)) {
						return true
					}
				}
			}
		}
		return false
	}

	def isGridNestedPathValidProposal(ContentAssistContext context, String proposal, boolean b) {
		if (".".equals(proposal)){
			var model = context.currentModel
			if (model instanceof CxGridNestedPath) {
				var nestedPathType = (model as CxGridNestedPath)?.field?.returnType?.type
				if (nestedPathType instanceof JvmDeclaredType){
					for (type : (nestedPathType as JvmDeclaredType).superTypes){
						if (IDto.name.equals(type.qualifiedName)) {
							return true
						} else {
							return false
						}
					}
				}
			}
		}
		return b
	}

	def isIntervalValidProposal(ContentAssistContext context, String proposal, boolean b) {
		var node = context.currentNode?.parent?.semanticElement
		var container = context.currentModel.eContainer
		if (container instanceof TableValue) {
			if (node instanceof TableRangeElement) {
				if ("textcolor".equals(proposal) || "cellcolor".equals(proposal) || "icon".equals(proposal) ||
					"trend".equals(proposal) || "tooltip".equals(proposal)) {
					return true
				}
			} else {
				return true
			}
		}
		return false
	}

	def isAxisValidProposal(ContentAssistContext context, String proposal, boolean b) {
		var eObj = context.currentModel
		while (!(eObj instanceof Table)) {
			eObj = eObj.eContainer
		}
		if ((eObj instanceof Table) && !((eObj as Table).tabletype instanceof TableTable) &&
			"details".equals(proposal)) {
			return false
		}
		return true
	}

	override public void complete_QualifiedName(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this)
	}

	override public void complete_SignedNumber(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		providerHelper.createNumberProposal(context, acceptor, ruleCall, this);
	}

	override public void complete_Number(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		providerHelper.createNumberProposal(context, acceptor, ruleCall, this);
	}

	// ----------------- Delegates to grid ------------------------
	override void completePropertyButtonStyle_EventTopic(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.completePropertyButtonStyle_EventTopic(model, assignment, context, acceptor)

		gridProposalProvider.doCompletePropertyButtonStyle_EventTopic(acceptor, model, context)
	}

	override void completePropertyImageStyle_EventTopic(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.completePropertyImageStyle_EventTopic(model, assignment, context, acceptor)

		gridProposalProvider.doCompletePropertyImageStyle_EventTopic(acceptor, model, context)
	}

	override void completeStringToResourceStyleConfig_Value(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.completeStringToResourceStyleConfig_Value(model, assignment, context, acceptor)

		gridProposalProvider.doCompleteStringToResourceStyleConfig_Value(acceptor, model, context);
	}

	override void completeStringToResourceStyleConfig_ResourceThemePath(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.completeStringToResourceStyleConfig_ResourceThemePath(model, assignment, context, acceptor)

		gridProposalProvider.doCompleteStringToResourceStyleConfig_ResourceThemePath(acceptor, model, context);
	}

	override void completeNumericToResourceStyleConfig_ResourceThemePath(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.completeNumericToResourceStyleConfig_ResourceThemePath(model, assignment, context, acceptor)

		gridProposalProvider.doCompleteNumericToResourceStyleConfig_ResourceThemePath(acceptor, model, context);
	}

	override void completeNumericToResourceStyleConfig_Value(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.completeNumericToResourceStyleConfig_Value(model, assignment, context, acceptor)

		gridProposalProvider.doCompleteNumericToResourceStyleConfig_Value(acceptor, model, context);
	}

	override void completePropertyNumberStyle_NumberFormat(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.completePropertyNumberStyle_NumberFormat(model, assignment, context, acceptor)

		gridProposalProvider.doCompletePropertyNumberStyle_NumberFormat(acceptor, model, context);
	}

	override void completePropertyDateStyle_DateFormat(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.completePropertyDateStyle_DateFormat(model, assignment, context, acceptor)

		gridProposalProvider.doCompletePropertyDateStyle_DateFormat(acceptor, model, context)
	}

	override void completePropertyProgressbarStyle_MaxValue(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.completePropertyProgressbarStyle_MaxValue(model, assignment, context, acceptor)

		gridProposalProvider.doCompletePropertyProgressbarStyle_MaxValue(acceptor, model, context);
	}

	override completePropertyPriceStyle_HtmlPattern(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.completePropertyPriceStyle_HtmlPattern(model, assignment, context, acceptor)

		gridProposalProvider.doCompletePropertyPriceStyle_HtmlPattern(acceptor, model, context);
	}

	override void completePropertyPriceStyle_ValueNumberFormat(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.completePropertyPriceStyle_ValueNumberFormat(model, assignment, context, acceptor)

		gridProposalProvider.doCompletePropertyPriceStyle_ValueNumberFormat(acceptor, model, context);
	}

	override completePropertyQuantityStyle_HtmlPattern(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.completePropertyQuantityStyle_HtmlPattern(model, assignment, context, acceptor)

		gridProposalProvider.doCompletePropertyQuantityStyle_HtmlPattern(acceptor, model, context);
	}

	override void completePropertyQuantityStyle_ValueNumberFormat(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		super.completePropertyQuantityStyle_ValueNumberFormat(model, assignment, context, acceptor)

		gridProposalProvider.doCompletePropertyQuantityStyle_ValueNumberFormat(acceptor, model, context);
	}
	
//	override void completePropertyQuantityStyle_ValuePropertyPath(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
//		super.completePropertyQuantityStyle_ValuePropertyPath(model, assignment, context, acceptor);
//		
//		gridProposalProvider.doCompletePropertyQuantityStyle_ValuePropertyPath(model, assignment, context, acceptor);
//	}
//	
//	override void completePropertyQuantityStyle_UomPropertyPath(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
//		super.completePropertyQuantityStyle_ValuePropertyPath(model, assignment, context, acceptor);
//		
//		gridProposalProvider.doCompletePropertyQuantityStyle_UomPropertyPath(model, assignment, context, acceptor);
//	}
//
//	override void completePropertyPriceStyle_CurrencyPropertyPath(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
//		super.completePropertyQuantityStyle_ValuePropertyPath(model, assignment, context, acceptor);
//		
//		gridProposalProvider.doCompletePropertyPriceStyle_CurrencyPropertyPath(model, assignment, context, acceptor);
//	}
//	
//	override void completePropertyPriceStyle_ValuePropertyPath(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
//		super.completePropertyQuantityStyle_ValuePropertyPath(model, assignment, context, acceptor);
//		
//		gridProposalProvider.doCompletePropertyPriceStyle_ValuePropertyPath(model, assignment, context, acceptor);
//	}

	// ------------------------ delegates to TerminalsProposalProvider -----------------
	override public void complete_TRANSLATABLESTRING(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, ruleCall, context, acceptor)
	}

	override public void complete_ID(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_ID(model, ruleCall, context, acceptor)
	}

	override public void completeTableImage_ResizeString(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, (assignment.getTerminal() as RuleCall), context, acceptor)
	}

	override public void completeTableStringLookup_LookupValue(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, (assignment.getTerminal() as RuleCall), context, acceptor)
	}

}
