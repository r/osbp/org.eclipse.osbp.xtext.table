/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.table.ui.contentassist;

import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ReplacementTextApplier;

@SuppressWarnings("all")
public class MyReplacementTextApplier extends ReplacementTextApplier {
  private ContentAssistContext context;
  
  public ContentAssistContext setContext(final ContentAssistContext context) {
    return this.context = context;
  }
  
  @Override
  public String getActualReplacementString(final ConfigurableCompletionProposal proposal) {
    Display display = this.context.getViewer().getTextWidget().getDisplay();
    Shell _activeShell = display.getActiveShell();
    ColorDialog colorDialog = new ColorDialog(_activeShell);
    RGB newColor = colorDialog.open();
    return "\"".concat(Integer.valueOf(newColor.red).toString()).concat(",").concat(Integer.valueOf(newColor.green).toString()).concat(",").concat(
      Integer.valueOf(newColor.blue).toString()).concat("\"");
  }
}
