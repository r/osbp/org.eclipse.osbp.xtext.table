/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.table.ui.contentassist;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import javax.inject.Inject;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.infogrid.model.gridsource.CxGridNestedField;
import org.eclipse.osbp.infogrid.model.gridsource.CxGridNestedPath;
import org.eclipse.osbp.infogrid.model.gridsource.CxGridProperty;
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropBlobImageStyle;
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropBooleanStyle;
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropButtonStyle;
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropDateStyle;
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropHtmlStyle;
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropImageStyle;
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropNumberStyle;
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropPriceStyle;
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropProgressbarStyle;
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropQuantityStyle;
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropTextStyle;
import org.eclipse.osbp.xtext.basic.ui.contentassist.BasicDSLProposalProviderHelper;
import org.eclipse.osbp.xtext.datamartdsl.DatamartCube;
import org.eclipse.osbp.xtext.datamartdsl.DatamartCubeAxis;
import org.eclipse.osbp.xtext.datamartdsl.DatamartCubeElement;
import org.eclipse.osbp.xtext.datamartdsl.DatamartDefinition;
import org.eclipse.osbp.xtext.datamartdsl.DatamartDerivedMeasure;
import org.eclipse.osbp.xtext.datamartdsl.DatamartElement;
import org.eclipse.osbp.xtext.datamartdsl.DatamartEntity;
import org.eclipse.osbp.xtext.datamartdsl.DatamartHierarchy;
import org.eclipse.osbp.xtext.datamartdsl.DatamartMeasure;
import org.eclipse.osbp.xtext.datamartdsl.DatamartSetAggregation;
import org.eclipse.osbp.xtext.datamartdsl.DatamartSource;
import org.eclipse.osbp.xtext.datamartdsl.DatamartTask;
import org.eclipse.osbp.xtext.datamartdsl.jvmmodel.DatamartDSLJvmModelInferrer;
import org.eclipse.osbp.xtext.gridsource.scoping.TypeHelper;
import org.eclipse.osbp.xtext.gridsource.ui.contentassist.GridSourceProposalProvider;
import org.eclipse.osbp.xtext.table.Table;
import org.eclipse.osbp.xtext.table.TableAxis;
import org.eclipse.osbp.xtext.table.TableDatamart;
import org.eclipse.osbp.xtext.table.TableEvent;
import org.eclipse.osbp.xtext.table.TableInterval;
import org.eclipse.osbp.xtext.table.TableLookup;
import org.eclipse.osbp.xtext.table.TablePreorder;
import org.eclipse.osbp.xtext.table.TableRangeElement;
import org.eclipse.osbp.xtext.table.TableTable;
import org.eclipse.osbp.xtext.table.TableValue;
import org.eclipse.osbp.xtext.table.ui.TableDSLDocumentationTranslator;
import org.eclipse.osbp.xtext.table.ui.contentassist.AbstractTableDSLProposalProvider;
import org.eclipse.osbp.xtext.table.ui.contentassist.ImageFileNameTextApplier;
import org.eclipse.osbp.xtext.table.ui.contentassist.MyReplacementTextApplier;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.TypeRef;
import org.eclipse.xtext.XtextPackage;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.ui.contentassist.TerminalsProposalProvider;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("all")
public class TableDSLProposalProvider extends AbstractTableDSLProposalProvider {
  @Inject
  @Extension
  private TypeHelper _typeHelper;
  
  @Inject
  private TerminalsProposalProvider provider;
  
  @Inject
  private BasicDSLProposalProviderHelper providerHelper;
  
  @Inject
  @Extension
  private DatamartDSLJvmModelInferrer datamartInferrer;
  
  private String imageSelectionPrompt = "Select image file...";
  
  private String colorSelectionPrompt = "Pick color...";
  
  @Inject
  private GridSourceProposalProvider gridProposalProvider;
  
  /**
   * This override will enable 1 length non letter characters as keyword.
   */
  @Override
  protected boolean isKeywordWorthyToPropose(final Keyword keyword) {
    return true;
  }
  
  @Override
  protected StyledString getKeywordDisplayString(final Keyword keyword) {
    return BasicDSLProposalProviderHelper.getKeywordDisplayString(keyword, 
      TableDSLDocumentationTranslator.instance());
  }
  
  /**
   * imageFilePickerProposal(model, assignment, context, acceptor, ".jpg,.jpeg,.png")
   */
  public void imageFilePickerProposal(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor, final String fileExtensions) {
    ICompletionProposal _createCompletionProposal = this.createCompletionProposal(this.imageSelectionPrompt, context);
    ConfigurableCompletionProposal fileName = ((ConfigurableCompletionProposal) _createCompletionProposal);
    if ((fileName != null)) {
      ImageFileNameTextApplier applier = new ImageFileNameTextApplier();
      applier.setExtensions(fileExtensions.split(","));
      applier.setContext(context);
      fileName.setTextApplier(applier);
    }
    acceptor.accept(fileName);
  }
  
  /**
   * This method decides which proposals will be valid.
   */
  @Override
  protected boolean isValidProposal(final String proposal, final String prefix, final ContentAssistContext context) {
    boolean result = super.isValidProposal(proposal, prefix, context);
    EObject _currentModel = context.getCurrentModel();
    if ((_currentModel instanceof CxGridNestedField)) {
      return this.isGridNestedFieldValidProposal(context, proposal, result);
    } else {
      EObject _currentModel_1 = context.getCurrentModel();
      if ((_currentModel_1 instanceof CxGridNestedPath)) {
        return this.isGridNestedPathValidProposal(context, proposal, result);
      }
    }
    EObject _currentModel_2 = context.getCurrentModel();
    if ((_currentModel_2 instanceof TableAxis)) {
      return this.isAxisValidProposal(context, proposal, result);
    }
    if ((((context.getCurrentModel() instanceof TableValue) || (context.getCurrentModel() instanceof TablePreorder)) || 
      (context.getCurrentModel() instanceof TableEvent))) {
      return this.isValueValidProposal(context, proposal, result);
    } else {
      if (((context.getCurrentModel() instanceof TableInterval) || (context.getCurrentModel() instanceof TableLookup))) {
        return this.isIntervalValidProposal(context, proposal, result);
      }
    }
    return result;
  }
  
  @Override
  public void createProposals(final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    if (((context.getCurrentModel() instanceof CxGridProperty) && this.getLastCompleteNodeIsStyle(context))) {
      ImmutableList<AbstractElement> _firstSetGrammarElements = context.getFirstSetGrammarElements();
      for (final AbstractElement element : _firstSetGrammarElements) {
        int _classifierID = element.eClass().getClassifierID();
        boolean _equals = (_classifierID == XtextPackage.KEYWORD);
        if (_equals) {
          Keyword keyword = ((Keyword) element);
          boolean _isKeywordWorthyToPropose = this.isKeywordWorthyToPropose(keyword, context);
          if (_isKeywordWorthyToPropose) {
            EObject _currentModel = context.getCurrentModel();
            final CxGridProperty gridProp = ((CxGridProperty) _currentModel);
            final CxGridNestedField nestedField = gridProp.getPath();
            JvmType nestedFieldType = nestedField.getField().getReturnType().getType();
            if ((nestedFieldType instanceof JvmDeclaredType)) {
              EList<JvmTypeReference> _superTypes = ((JvmDeclaredType) nestedFieldType).getSuperTypes();
              for (final JvmTypeReference type : _superTypes) {
                boolean _equals_1 = IDto.class.getName().equals(type.getQualifiedName());
                if (_equals_1) {
                  CxGridNestedPath _path = null;
                  if (nestedField!=null) {
                    _path=nestedField.getPath();
                  }
                  JvmOperation _field = null;
                  if (_path!=null) {
                    _field=_path.getField();
                  }
                  JvmTypeReference _returnType = null;
                  if (_field!=null) {
                    _returnType=_field.getReturnType();
                  }
                  JvmType _type = null;
                  if (_returnType!=null) {
                    _type=_returnType.getType();
                  }
                  nestedFieldType = _type;
                }
              }
            }
            EObject eObject = ((EObject) element);
            while ((!(eObject instanceof ParserRule))) {
              eObject = eObject.eContainer();
            }
            if ((eObject != null)) {
              ParserRule rule = ((ParserRule) eObject);
              if ((nestedFieldType == null)) {
                TypeRef _type_1 = rule.getType();
                EClassifier _classifier = null;
                if (_type_1!=null) {
                  _classifier=_type_1.getClassifier();
                }
                Class<?> ruleTypeClass = _classifier.getInstanceClass();
                boolean _equals_2 = CxGridPropTextStyle.class.equals(ruleTypeClass);
                if (_equals_2) {
                  this.setProposal(context, acceptor, keyword);
                }
              } else {
                if ((this._typeHelper.isBoolean(nestedFieldType) && this.isBooleanStyle(rule))) {
                  this.setProposal(context, acceptor, keyword);
                } else {
                  if ((this._typeHelper.isString(nestedFieldType) && this.isTextStyle(rule))) {
                    this.setProposal(context, acceptor, keyword);
                  } else {
                    boolean _isNumber = this._typeHelper.isNumber(nestedFieldType);
                    if (_isNumber) {
                      if ((this._typeHelper.isNumberWithDigits(nestedFieldType) && this.isProgressStyle(rule))) {
                        this.setProposal(context, acceptor, keyword);
                      } else {
                        boolean _isNumberStyle = this.isNumberStyle(rule);
                        if (_isNumberStyle) {
                          this.setProposal(context, acceptor, keyword);
                        }
                      }
                    } else {
                      if ((this._typeHelper.isDate(nestedFieldType) && this.isDateStyle(rule))) {
                        this.setProposal(context, acceptor, keyword);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      super.createProposals(context, acceptor);
    }
  }
  
  public boolean getLastCompleteNodeIsStyle(final ContentAssistContext context) {
    INode _lastCompleteNode = context.getLastCompleteNode();
    EObject _grammarElement = null;
    if (_lastCompleteNode!=null) {
      _grammarElement=_lastCompleteNode.getGrammarElement();
    }
    final EObject element = _grammarElement;
    int _classifierID = element.eClass().getClassifierID();
    boolean _equals = (_classifierID == XtextPackage.KEYWORD);
    if (_equals) {
      Keyword keyword = ((Keyword) element);
      boolean _equals_1 = "style".equals(keyword.getValue());
      if (_equals_1) {
        return true;
      }
    }
    return false;
  }
  
  public boolean isBooleanStyle(final ParserRule rule) {
    TypeRef _type = rule.getType();
    EClassifier _classifier = null;
    if (_type!=null) {
      _classifier=_type.getClassifier();
    }
    Class<?> ruleTypeClass = _classifier.getInstanceClass();
    return CxGridPropBooleanStyle.class.equals(ruleTypeClass);
  }
  
  public boolean isTextStyle(final ParserRule rule) {
    TypeRef _type = rule.getType();
    EClassifier _classifier = null;
    if (_type!=null) {
      _classifier=_type.getClassifier();
    }
    Class<?> ruleTypeClass = _classifier.getInstanceClass();
    return ((((CxGridPropTextStyle.class.equals(ruleTypeClass) || CxGridPropHtmlStyle.class.equals(ruleTypeClass)) || CxGridPropButtonStyle.class.equals(ruleTypeClass)) || CxGridPropImageStyle.class.equals(ruleTypeClass)) || CxGridPropBlobImageStyle.class.equals(ruleTypeClass));
  }
  
  public boolean isNumberStyle(final ParserRule rule) {
    TypeRef _type = rule.getType();
    EClassifier _classifier = null;
    if (_type!=null) {
      _classifier=_type.getClassifier();
    }
    Class<?> ruleTypeClass = _classifier.getInstanceClass();
    return (((CxGridPropNumberStyle.class.equals(ruleTypeClass) || CxGridPropPriceStyle.class.equals(ruleTypeClass)) || CxGridPropQuantityStyle.class.equals(ruleTypeClass)) || CxGridPropImageStyle.class.equals(ruleTypeClass));
  }
  
  public boolean isProgressStyle(final ParserRule rule) {
    TypeRef _type = rule.getType();
    EClassifier _classifier = null;
    if (_type!=null) {
      _classifier=_type.getClassifier();
    }
    Class<?> ruleTypeClass = _classifier.getInstanceClass();
    return CxGridPropProgressbarStyle.class.equals(ruleTypeClass);
  }
  
  public boolean isDateStyle(final ParserRule rule) {
    TypeRef _type = rule.getType();
    EClassifier _classifier = null;
    if (_type!=null) {
      _classifier=_type.getClassifier();
    }
    Class<?> ruleTypeClass = _classifier.getInstanceClass();
    return CxGridPropDateStyle.class.equals(ruleTypeClass);
  }
  
  public void setProposal(final ContentAssistContext context, final ICompletionProposalAcceptor acceptor, final Keyword keyword) {
    final ICompletionProposal proposal = this.createCompletionProposal(keyword.getValue(), this.getKeywordDisplayString(keyword), this.getImage(keyword), context);
    acceptor.accept(proposal);
  }
  
  @Override
  public void completeTableIcon_Icon(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.imageFilePickerProposal(model, assignment, context, acceptor, ".jpg,.jpeg,.png");
  }
  
  @Override
  public void completeTableValue_IconName(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.imageFilePickerProposal(model, assignment, context, acceptor, ".jpg,.jpeg,.png");
  }
  
  @Override
  public void completeTableFormatter_Format(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    acceptor.accept(this.createCompletionProposal("\"###,##0.00\" /* money */", "money", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"###,##0.00 &curren;\" /* local national currency */", "local national currency", 
        null, context));
    acceptor.accept(
      this.createCompletionProposal("\"###,##0.00 &curren;&curren;\" /* local international currency */", 
        "local international currency", null, context));
    acceptor.accept(this.createCompletionProposal("\"#0.000\" /* quantity */", "quantity", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"#0.00;(#0.00)\" /* negative in parentheses */", "negative in parentheses", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"##0.0 %\" /* ratio as percentage */", "ratio as percentage", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"##0.0 %%\" /* ratio as per mill */", "ratio as per mill", null, context));
    acceptor.accept(this.createCompletionProposal("\"##0.0\'%\'\" /* percentage sign */", "percentage sign", null, context));
    acceptor.accept(this.createCompletionProposal("\"##0.0\'?\'\" /* per mill sign */", "per mill sign", null, context));
    acceptor.accept(this.createCompletionProposal("\"0.0000000000E00\" /* scientific */", "scientific", null, context));
    acceptor.accept(this.createCompletionProposal("\"SHORTDATE\" /* short date */", "short date", null, context));
    acceptor.accept(this.createCompletionProposal("\"MEDIUMDATE\" /* medium date */", "medium date", null, context));
    acceptor.accept(this.createCompletionProposal("\"LONGDATE\" /* long date */", "long date", null, context));
    acceptor.accept(this.createCompletionProposal("\"FULLDATE\" /* full date */", "full date", null, context));
    acceptor.accept(this.createCompletionProposal("\"SHORTTIME\" /* short time */", "short time", null, context));
    acceptor.accept(this.createCompletionProposal("\"MEDIUMTIME\" /* medium time */", "medium time", null, context));
    acceptor.accept(this.createCompletionProposal("\"LONGTIME\" /* long time */", "long time", null, context));
    acceptor.accept(this.createCompletionProposal("\"FULLTIME\" /* full time */", "full time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"SHORTDATESHORTTIME\" /* short date short time */", "short date short time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"SHORTDATEMEDIUMTIME\" /* short date medium time */", "short date medium time", 
        null, context));
    acceptor.accept(
      this.createCompletionProposal("\"SHORTDATELONGTIME\" /* short date long time */", "short date long time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"SHORTDATEFULLTIME\" /* short date full time */", "short date full time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"MEDIUMDATESHORTTIME\" /* medium date short time */", "medium date short time", 
        null, context));
    acceptor.accept(
      this.createCompletionProposal("\"MEDIUMDATEMEDIUMTIME\" /* medium date medium time */", "medium date medium time", 
        null, context));
    acceptor.accept(
      this.createCompletionProposal("\"MEDIUMDATELONGTIME\" /* medium date long time */", "medium date long time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"MEDIUMDATEFULLTIME\" /* medium date full time */", "medium date full time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"LONGDATESHORTTIME\" /* long date short time */", "long date short time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"LONGDATEMEDIUMTIME\" /* long date medium time */", "long date medium time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"LONGDATELONGTIME\" /* long date long time */", "long date long time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"LONGDATEFULLTIME\" /* long date full time */", "long date full time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"FULLDATESHORTTIME\" /* full date short time */", "full date short time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"FULLDATEMEDIUMTIME\" /* full date medium time */", "full date medium time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"FULLDATELONGTIME\" /* full date long time */", "full date long time", null, context));
    acceptor.accept(
      this.createCompletionProposal("\"FULLDATEFULLTIME\" /* full date full time */", "full date full time", null, context));
  }
  
  @Override
  public void completeTableTextColor_Rgb(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.colorPickerProposal(model, assignment, context, acceptor);
  }
  
  @Override
  public void completeTableCellColor_Rgb(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.colorPickerProposal(model, assignment, context, acceptor);
  }
  
  public void colorPickerProposal(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    ICompletionProposal _createCompletionProposal = this.createCompletionProposal(this.colorSelectionPrompt, context);
    ConfigurableCompletionProposal pickColor = ((ConfigurableCompletionProposal) _createCompletionProposal);
    boolean _notEquals = (!Objects.equal(pickColor, null));
    if (_notEquals) {
      MyReplacementTextApplier applier = new MyReplacementTextApplier();
      applier.setContext(context);
      pickColor.setTextApplier(applier);
    }
    acceptor.accept(pickColor);
    acceptor.accept(this.createCompletionProposal("\"230,128,64\" /* orange */", "orange", null, context));
    acceptor.accept(this.createCompletionProposal("\"255,82,82\" /* light red */", "light red", null, context));
    acceptor.accept(this.createCompletionProposal("\"255,0,0\" /* red */", "red", null, context));
    acceptor.accept(this.createCompletionProposal("\"204,102,102\" /* dark red */", "dark red", null, context));
    acceptor.accept(this.createCompletionProposal("\"255,255,128\" /*light yellow */", "light yellow", null, context));
    acceptor.accept(this.createCompletionProposal("\"255,255,0\" /* yellow */", "yellow", null, context));
    acceptor.accept(this.createCompletionProposal("\"128,128,0\" /* dark yellow */", "dark yellow", null, context));
    acceptor.accept(this.createCompletionProposal("\"102,204,102\" /* light green */", "light green", null, context));
    acceptor.accept(this.createCompletionProposal("\"0,255,0\" /* green */", "green", null, context));
    acceptor.accept(this.createCompletionProposal("\"147,183,95\" /* dark green */", "dark green", null, context));
    acceptor.accept(this.createCompletionProposal("\"102,102,204\" /* light blue */", "light blue", null, context));
    acceptor.accept(this.createCompletionProposal("\"0,0,255\" /* blue */", "blue", null, context));
    acceptor.accept(this.createCompletionProposal("\"147,95,183\" /* dark blue */", "dark blue", null, context));
    acceptor.accept(this.createCompletionProposal("\"0,0,0\" /* black */", "black", null, context));
    acceptor.accept(this.createCompletionProposal("\"255,255,255\" /* white */", "white", null, context));
    acceptor.accept(this.createCompletionProposal("\"0,0,255\" /* blue */", "blue", null, context));
    acceptor.accept(this.createCompletionProposal("\"0,0,128\" /* dark blue */", "dark blue", null, context));
    acceptor.accept(this.createCompletionProposal("\"82,182,255\" /* light blue */", "light blue", null, context));
  }
  
  public boolean isValueValidProposal(final ContentAssistContext context, final String proposal, final boolean result) {
    boolean _isGrammarToken = this.isGrammarToken(proposal);
    if (_isGrammarToken) {
      return true;
    }
    EObject eObj = context.getCurrentModel();
    String requestedAxisName = "";
    while ((!(eObj instanceof TableDatamart))) {
      {
        eObj = eObj.eContainer();
        if ((eObj instanceof TableAxis)) {
          requestedAxisName = ((TableAxis) eObj).getAxis().getLiteral();
        }
      }
    }
    boolean _notEquals = (!Objects.equal(eObj, null));
    if (_notEquals) {
      DatamartDefinition datamart = ((TableDatamart) eObj).getDatamartRef();
      if (((!Objects.equal(datamart, null)) && (!Objects.equal(datamart.getSource(), null)))) {
        DatamartSource _source = datamart.getSource();
        if ((_source instanceof DatamartCube)) {
          DatamartSource _source_1 = datamart.getSource();
          EList<DatamartCubeElement> _axisslicer = ((DatamartCube) _source_1).getAxisslicer();
          for (final DatamartCubeElement axis : _axisslicer) {
            if ((axis instanceof DatamartCubeAxis)) {
              boolean _equals = ((DatamartCubeAxis) axis).getAxis().getName().getLiteral().equals(requestedAxisName);
              if (_equals) {
                boolean hasMeasures = false;
                boolean hasHierarchy = false;
                boolean hasAggregation = false;
                boolean isCrossjoined = false;
                boolean isUndefined = false;
                EList<DatamartElement> _elements = ((DatamartCubeAxis) axis).getElements();
                for (final DatamartElement element : _elements) {
                  if (((element instanceof DatamartMeasure) || 
                    (element instanceof DatamartDerivedMeasure))) {
                    hasMeasures = true;
                  }
                }
                EList<DatamartElement> _elements_1 = ((DatamartCubeAxis) axis).getElements();
                for (final DatamartElement element_1 : _elements_1) {
                  {
                    if ((element_1 instanceof DatamartHierarchy)) {
                      if (hasHierarchy) {
                        isCrossjoined = true;
                      } else {
                        hasHierarchy = true;
                      }
                      boolean _isAllLevels = ((DatamartHierarchy) element_1).isAllLevels();
                      if (_isAllLevels) {
                        isUndefined = true;
                      }
                    }
                    if ((element_1 instanceof DatamartSetAggregation)) {
                      if (hasAggregation) {
                        isCrossjoined = true;
                      } else {
                        hasAggregation = true;
                      }
                      hasAggregation = true;
                    }
                  }
                }
                if ((hasMeasures && (hasHierarchy || hasAggregation))) {
                  isCrossjoined = true;
                }
                if ((isCrossjoined || isUndefined)) {
                  boolean _equals_1 = "ordinal".equals(proposal);
                  if (_equals_1) {
                    return true;
                  }
                  boolean _equals_2 = "allColumns".equals(proposal);
                  if (_equals_2) {
                    return true;
                  }
                  boolean _equals_3 = "all".equals(proposal);
                  if (_equals_3) {
                    return true;
                  }
                }
                if ((!isUndefined)) {
                  EList<DatamartElement> _elements_2 = ((DatamartCubeAxis) axis).getElements();
                  for (final DatamartElement element_2 : _elements_2) {
                    {
                      if (((element_2 instanceof DatamartMeasure) && "measure".equals(proposal))) {
                        return true;
                      }
                      if (((element_2 instanceof DatamartDerivedMeasure) && "derived".equals(proposal))) {
                        return true;
                      }
                      if (((element_2 instanceof DatamartHierarchy) && 
                        "level".equals(proposal))) {
                        return true;
                      }
                      if (((element_2 instanceof DatamartSetAggregation) && 
                        "aggregation".equals(proposal))) {
                        return true;
                      }
                    }
                  }
                }
              }
            }
          }
        } else {
          if (((datamart.getSource() instanceof DatamartEntity) && "property".equals(proposal))) {
            return true;
          } else {
            if (((datamart.getSource() instanceof DatamartTask) && ("column".equals(proposal) || "taskId".equals(proposal)))) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  
  public boolean isGrammarToken(final String proposal) {
    boolean _equals = this.imageSelectionPrompt.equals(proposal);
    if (_equals) {
      return true;
    }
    boolean _equals_1 = "icon".equals(proposal);
    if (_equals_1) {
      return true;
    }
    boolean _equals_2 = "image".equals(proposal);
    if (_equals_2) {
      return true;
    }
    boolean _equals_3 = "using".equals(proposal);
    if (_equals_3) {
      return true;
    }
    boolean _equals_4 = "preorder".equals(proposal);
    if (_equals_4) {
      return true;
    }
    boolean _equals_5 = "intervals".equals(proposal);
    if (_equals_5) {
      return true;
    }
    boolean _equals_6 = "lookups".equals(proposal);
    if (_equals_6) {
      return true;
    }
    boolean _equals_7 = "formatter".equals(proposal);
    if (_equals_7) {
      return true;
    }
    boolean _equals_8 = "hidelabel".equals(proposal);
    if (_equals_8) {
      return true;
    }
    boolean _equals_9 = "collapse".equals(proposal);
    if (_equals_9) {
      return true;
    }
    boolean _equals_10 = "upToNumber".equals(proposal);
    if (_equals_10) {
      return true;
    }
    boolean _equals_11 = "upToInteger".equals(proposal);
    if (_equals_11) {
      return true;
    }
    boolean _equals_12 = "number".equals(proposal);
    if (_equals_12) {
      return true;
    }
    boolean _equals_13 = "integer".equals(proposal);
    if (_equals_13) {
      return true;
    }
    boolean _equals_14 = "string".equals(proposal);
    if (_equals_14) {
      return true;
    }
    boolean _equals_15 = "daysInPast".equals(proposal);
    if (_equals_15) {
      return true;
    }
    boolean _equals_16 = "to".equals(proposal);
    if (_equals_16) {
      return true;
    }
    boolean _equals_17 = "datamart".equals(proposal);
    if (_equals_17) {
      return true;
    }
    boolean _equals_18 = "{".equals(proposal);
    if (_equals_18) {
      return true;
    }
    boolean _equals_19 = "}".equals(proposal);
    if (_equals_19) {
      return true;
    }
    return false;
  }
  
  public boolean isGridNestedFieldValidProposal(final ContentAssistContext context, final String proposal, final boolean b) {
    EObject model = context.getCurrentModel();
    if ((model instanceof CxGridNestedField)) {
      JvmOperation _field = null;
      if (((CxGridNestedField) model)!=null) {
        _field=((CxGridNestedField) model).getField();
      }
      JvmTypeReference _returnType = null;
      if (_field!=null) {
        _returnType=_field.getReturnType();
      }
      JvmType _type = null;
      if (_returnType!=null) {
        _type=_returnType.getType();
      }
      JvmType nestedFieldType = _type;
      if ((nestedFieldType instanceof JvmDeclaredType)) {
        EList<JvmTypeReference> _superTypes = ((JvmDeclaredType) nestedFieldType).getSuperTypes();
        for (final JvmTypeReference type : _superTypes) {
          boolean _equals = IDto.class.getName().equals(type.getQualifiedName());
          if (_equals) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  public boolean isGridNestedPathValidProposal(final ContentAssistContext context, final String proposal, final boolean b) {
    boolean _equals = ".".equals(proposal);
    if (_equals) {
      EObject model = context.getCurrentModel();
      if ((model instanceof CxGridNestedPath)) {
        JvmOperation _field = null;
        if (((CxGridNestedPath) model)!=null) {
          _field=((CxGridNestedPath) model).getField();
        }
        JvmTypeReference _returnType = null;
        if (_field!=null) {
          _returnType=_field.getReturnType();
        }
        JvmType _type = null;
        if (_returnType!=null) {
          _type=_returnType.getType();
        }
        JvmType nestedPathType = _type;
        if ((nestedPathType instanceof JvmDeclaredType)) {
          EList<JvmTypeReference> _superTypes = ((JvmDeclaredType) nestedPathType).getSuperTypes();
          for (final JvmTypeReference type : _superTypes) {
            boolean _equals_1 = IDto.class.getName().equals(type.getQualifiedName());
            if (_equals_1) {
              return true;
            } else {
              return false;
            }
          }
        }
      }
    }
    return b;
  }
  
  public boolean isIntervalValidProposal(final ContentAssistContext context, final String proposal, final boolean b) {
    INode _currentNode = context.getCurrentNode();
    ICompositeNode _parent = null;
    if (_currentNode!=null) {
      _parent=_currentNode.getParent();
    }
    EObject _semanticElement = null;
    if (_parent!=null) {
      _semanticElement=_parent.getSemanticElement();
    }
    EObject node = _semanticElement;
    EObject container = context.getCurrentModel().eContainer();
    if ((container instanceof TableValue)) {
      if ((node instanceof TableRangeElement)) {
        if ((((("textcolor".equals(proposal) || "cellcolor".equals(proposal)) || "icon".equals(proposal)) || 
          "trend".equals(proposal)) || "tooltip".equals(proposal))) {
          return true;
        }
      } else {
        return true;
      }
    }
    return false;
  }
  
  public boolean isAxisValidProposal(final ContentAssistContext context, final String proposal, final boolean b) {
    EObject eObj = context.getCurrentModel();
    while ((!(eObj instanceof Table))) {
      eObj = eObj.eContainer();
    }
    if ((((eObj instanceof Table) && (!(((Table) eObj).getTabletype() instanceof TableTable))) && 
      "details".equals(proposal))) {
      return false;
    }
    return true;
  }
  
  @Override
  public void complete_QualifiedName(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this);
  }
  
  @Override
  public void complete_SignedNumber(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.providerHelper.createNumberProposal(context, acceptor, ruleCall, this);
  }
  
  @Override
  public void complete_Number(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.providerHelper.createNumberProposal(context, acceptor, ruleCall, this);
  }
  
  @Override
  public void completePropertyButtonStyle_EventTopic(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completePropertyButtonStyle_EventTopic(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompletePropertyButtonStyle_EventTopic(acceptor, model, context);
  }
  
  @Override
  public void completePropertyImageStyle_EventTopic(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completePropertyImageStyle_EventTopic(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompletePropertyImageStyle_EventTopic(acceptor, model, context);
  }
  
  @Override
  public void completeStringToResourceStyleConfig_Value(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completeStringToResourceStyleConfig_Value(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompleteStringToResourceStyleConfig_Value(acceptor, model, context);
  }
  
  @Override
  public void completeStringToResourceStyleConfig_ResourceThemePath(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completeStringToResourceStyleConfig_ResourceThemePath(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompleteStringToResourceStyleConfig_ResourceThemePath(acceptor, model, context);
  }
  
  @Override
  public void completeNumericToResourceStyleConfig_ResourceThemePath(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completeNumericToResourceStyleConfig_ResourceThemePath(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompleteNumericToResourceStyleConfig_ResourceThemePath(acceptor, model, context);
  }
  
  @Override
  public void completeNumericToResourceStyleConfig_Value(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completeNumericToResourceStyleConfig_Value(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompleteNumericToResourceStyleConfig_Value(acceptor, model, context);
  }
  
  @Override
  public void completePropertyNumberStyle_NumberFormat(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completePropertyNumberStyle_NumberFormat(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompletePropertyNumberStyle_NumberFormat(acceptor, model, context);
  }
  
  @Override
  public void completePropertyDateStyle_DateFormat(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completePropertyDateStyle_DateFormat(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompletePropertyDateStyle_DateFormat(acceptor, model, context);
  }
  
  @Override
  public void completePropertyProgressbarStyle_MaxValue(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completePropertyProgressbarStyle_MaxValue(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompletePropertyProgressbarStyle_MaxValue(acceptor, model, context);
  }
  
  @Override
  public void completePropertyPriceStyle_HtmlPattern(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completePropertyPriceStyle_HtmlPattern(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompletePropertyPriceStyle_HtmlPattern(acceptor, model, context);
  }
  
  @Override
  public void completePropertyPriceStyle_ValueNumberFormat(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completePropertyPriceStyle_ValueNumberFormat(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompletePropertyPriceStyle_ValueNumberFormat(acceptor, model, context);
  }
  
  @Override
  public void completePropertyQuantityStyle_HtmlPattern(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completePropertyQuantityStyle_HtmlPattern(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompletePropertyQuantityStyle_HtmlPattern(acceptor, model, context);
  }
  
  @Override
  public void completePropertyQuantityStyle_ValueNumberFormat(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.completePropertyQuantityStyle_ValueNumberFormat(model, assignment, context, acceptor);
    this.gridProposalProvider.doCompletePropertyQuantityStyle_ValueNumberFormat(acceptor, model, context);
  }
  
  @Override
  public void complete_TRANSLATABLESTRING(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.provider.complete_STRING(model, ruleCall, context, acceptor);
  }
  
  @Override
  public void complete_ID(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    this.provider.complete_ID(model, ruleCall, context, acceptor);
  }
  
  @Override
  public void completeTableImage_ResizeString(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    AbstractElement _terminal = assignment.getTerminal();
    this.provider.complete_STRING(model, ((RuleCall) _terminal), context, acceptor);
  }
  
  @Override
  public void completeTableStringLookup_LookupValue(final EObject model, final Assignment assignment, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    AbstractElement _terminal = assignment.getTerminal();
    this.provider.complete_STRING(model, ((RuleCall) _terminal), context, acceptor);
  }
}
