/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.table.ui.labeling;

import com.google.inject.Inject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.osbp.infogrid.model.gridsource.CxGridProperty;
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropStyle;
import org.eclipse.osbp.xtext.basic.ui.labeling.BasicDSLLabelProvider;
import org.eclipse.osbp.xtext.table.Table;
import org.eclipse.osbp.xtext.table.TableAggregation;
import org.eclipse.osbp.xtext.table.TableAllColumns;
import org.eclipse.osbp.xtext.table.TableAttribute;
import org.eclipse.osbp.xtext.table.TableAxis;
import org.eclipse.osbp.xtext.table.TableCellColor;
import org.eclipse.osbp.xtext.table.TableColumn;
import org.eclipse.osbp.xtext.table.TableDatamart;
import org.eclipse.osbp.xtext.table.TableDateDayLookup;
import org.eclipse.osbp.xtext.table.TableDerived;
import org.eclipse.osbp.xtext.table.TableDtoDatasource;
import org.eclipse.osbp.xtext.table.TableEvent;
import org.eclipse.osbp.xtext.table.TableFormatter;
import org.eclipse.osbp.xtext.table.TableGrid;
import org.eclipse.osbp.xtext.table.TableHierarchy;
import org.eclipse.osbp.xtext.table.TableIcon;
import org.eclipse.osbp.xtext.table.TableInterval;
import org.eclipse.osbp.xtext.table.TableLookup;
import org.eclipse.osbp.xtext.table.TableMeasure;
import org.eclipse.osbp.xtext.table.TableModel;
import org.eclipse.osbp.xtext.table.TableNumberLookup;
import org.eclipse.osbp.xtext.table.TableOrdinal;
import org.eclipse.osbp.xtext.table.TablePackage;
import org.eclipse.osbp.xtext.table.TablePreorder;
import org.eclipse.osbp.xtext.table.TableStringLookup;
import org.eclipse.osbp.xtext.table.TableTable;
import org.eclipse.osbp.xtext.table.TableTask;
import org.eclipse.osbp.xtext.table.TableTextColor;
import org.eclipse.osbp.xtext.table.TableTooltip;
import org.eclipse.osbp.xtext.table.TableTrend;
import org.eclipse.osbp.xtext.table.TableValue;
import org.eclipse.xtend2.lib.StringConcatenation;

/**
 * Provides labels for a EObjects.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
@SuppressWarnings("all")
public class TableDSLLabelProvider extends BasicDSLLabelProvider {
  @Inject
  public TableDSLLabelProvider(final AdapterFactoryLabelProvider delegate) {
    super(delegate);
  }
  
  @Override
  public Object text(final Object o) {
    Object _switchResult = null;
    boolean _matched = false;
    if (o instanceof TablePackage) {
      _matched=true;
      _switchResult = this.generateText(o, "package", ((TablePackage)o).getName());
    }
    if (!_matched) {
      if (o instanceof Table) {
        _matched=true;
        _switchResult = this.generateText(o, "table model", ((Table)o).getName());
      }
    }
    if (!_matched) {
      if (o instanceof TableTable) {
        _matched=true;
        _switchResult = this.generateText(o, "table", ((TableTable)o).getSource().getDatamartRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof TableGrid) {
        _matched=true;
        _switchResult = this.generateText(o, "grid", ((TableGrid)o).getSource().getDtoSource().getName());
      }
    }
    if (!_matched) {
      if (o instanceof TableAxis) {
        _matched=true;
        _switchResult = this.generateText(o, "axis", ((TableAxis)o).getAxis().getLiteral());
      }
    }
    if (!_matched) {
      if (o instanceof TableDatamart) {
        _matched=true;
        _switchResult = this.generateText(o, "datamart", ((TableDatamart)o).getDatamartRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof TableEvent) {
        _matched=true;
        _switchResult = this.generateText(o, "event");
      }
    }
    if (!_matched) {
      if (o instanceof TableValue) {
        _matched=true;
        _switchResult = this.generateText(o, "value");
      }
    }
    if (!_matched) {
      if (o instanceof TableInterval) {
        _matched=true;
        _switchResult = this.generateText(o, "interval");
      }
    }
    if (!_matched) {
      if (o instanceof TableFormatter) {
        _matched=true;
        _switchResult = this.generateText(o, "formatter", ((TableFormatter)o).getFormat());
      }
    }
    if (!_matched) {
      if (o instanceof TableTextColor) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("textcolor (");
        String _rgb = ((TableTextColor)o).getRgb();
        _builder.append(_rgb);
        _builder.append(")");
        _switchResult = this.generateText(o, _builder.toString());
      }
    }
    if (!_matched) {
      if (o instanceof TableCellColor) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("cellcolor (");
        String _rgb = ((TableCellColor)o).getRgb();
        _builder.append(_rgb);
        _builder.append(")");
        _switchResult = this.generateText(o, _builder.toString());
      }
    }
    if (!_matched) {
      if (o instanceof TableIcon) {
        _matched=true;
        _switchResult = this.generateText(o, "icon", ((TableIcon)o).getIcon());
      }
    }
    if (!_matched) {
      if (o instanceof TableTooltip) {
        _matched=true;
        _switchResult = this.generateText(o, "tooltip", ((TableTooltip)o).getTooltip());
      }
    }
    if (!_matched) {
      if (o instanceof TableTrend) {
        _matched=true;
        _switchResult = this.generateText(o, "trend", ((TableTrend)o).getIcon().toString());
      }
    }
    if (!_matched) {
      if (o instanceof TableAttribute) {
        _matched=true;
        _switchResult = this.generateText(o, "table property", ((TableAttribute)o).getValueRef().getAttributeRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof TableAggregation) {
        _matched=true;
        _switchResult = this.generateText(o, "aggregation", ((TableAggregation)o).getValueRef().getAggregation().toString());
      }
    }
    if (!_matched) {
      if (o instanceof TableHierarchy) {
        _matched=true;
        _switchResult = this.generateText(o, "level", ((TableHierarchy)o).getValueRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof TableMeasure) {
        _matched=true;
        _switchResult = this.generateText(o, "measure", ((TableMeasure)o).getValueRef().getMeasureRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof TableDerived) {
        _matched=true;
        _switchResult = this.generateText(o, "derived", ((TableDerived)o).getValueRef().getDerivedRef().getName());
      }
    }
    if (!_matched) {
      if (o instanceof TableOrdinal) {
        _matched=true;
        _switchResult = this.generateText(o, "ordinal", ((TableOrdinal)o).getValueRef().toString());
      }
    }
    if (!_matched) {
      if (o instanceof TableColumn) {
        _matched=true;
        _switchResult = this.generateText(o, "column", ((TableColumn)o).getValueRef().getColumnRef().toString());
      }
    }
    if (!_matched) {
      if (o instanceof TableTask) {
        _matched=true;
        _switchResult = this.generateText(o, "task");
      }
    }
    if (!_matched) {
      if (o instanceof TableAllColumns) {
        _matched=true;
        _switchResult = this.generateText(o, "all columns");
      }
    }
    if (!_matched) {
      if (o instanceof TablePreorder) {
        _matched=true;
        _switchResult = this.generateText(o, "pre order");
      }
    }
    if (!_matched) {
      if (o instanceof CxGridProperty) {
        _matched=true;
        _switchResult = this.generateText(o, "grid property");
      }
    }
    if (!_matched) {
      if (o instanceof CxGridPropStyle) {
        _matched=true;
        _switchResult = this.generateText(o, "property style");
      }
    }
    if (!_matched) {
      if (o instanceof TableDtoDatasource) {
        _matched=true;
        _switchResult = this.generateText(o, "dto datasource", ((TableDtoDatasource)o).getDtoSource().getName());
      }
    }
    if (!_matched) {
      if (o instanceof TableStringLookup) {
        _matched=true;
        _switchResult = this.generateText(o, "string lookup", ((TableStringLookup)o).getLookupValue());
      }
    }
    if (!_matched) {
      if (o instanceof TableNumberLookup) {
        _matched=true;
        _switchResult = this.generateText(o, "number lookup", Double.valueOf(((TableNumberLookup)o).getLookupValue()).toString());
      }
    }
    if (!_matched) {
      if (o instanceof TableDateDayLookup) {
        _matched=true;
        _switchResult = this.generateText(o, "date lookup", Integer.valueOf(((TableDateDayLookup)o).getLookupValue()).toString());
      }
    }
    if (!_matched) {
      _switchResult = super.text(o);
    }
    return _switchResult;
  }
  
  @Override
  public Object image(final Object o) {
    Object _switchResult = null;
    boolean _matched = false;
    if (o instanceof TableModel) {
      _matched=true;
      _switchResult = this.getInternalImage("model.png", this.getClass());
    }
    if (!_matched) {
      if (o instanceof TablePackage) {
        _matched=true;
        _switchResult = this.getInternalImage("package.gif", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof Table) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_table.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableTable) {
        _matched=true;
        _switchResult = this.getInternalImage("table-table.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableGrid) {
        _matched=true;
        _switchResult = this.getInternalImage("table-grid.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableAxis) {
        _matched=true;
        _switchResult = this.getInternalImage("axis.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableDatamart) {
        _matched=true;
        _switchResult = this.getInternalImage("dsl_datamart.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableEvent) {
        _matched=true;
        _switchResult = this.getInternalImage("event.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableValue) {
        _matched=true;
        _switchResult = this.getInternalImage("value3.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableInterval) {
        _matched=true;
        _switchResult = this.getInternalImage("interval.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableFormatter) {
        _matched=true;
        _switchResult = this.getInternalImage("format.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableTextColor) {
        _matched=true;
        _switchResult = this.getColorOutlineImage(((TableTextColor)o).getRgb(), true);
      }
    }
    if (!_matched) {
      if (o instanceof TableCellColor) {
        _matched=true;
        _switchResult = this.getColorOutlineImage(((TableCellColor)o).getRgb(), true);
      }
    }
    if (!_matched) {
      if (o instanceof TableIcon) {
        _matched=true;
        _switchResult = this.getInternalImage("img.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableTooltip) {
        _matched=true;
        _switchResult = this.getInternalImage("tooltip.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableTrend) {
        _matched=true;
        _switchResult = this.getInternalImage("trend.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableAttribute) {
        _matched=true;
        _switchResult = this.getInternalImage("properties.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableAggregation) {
        _matched=true;
        _switchResult = this.getInternalImage("aggregate.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableAllColumns) {
        _matched=true;
        _switchResult = this.getInternalImage("column.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableHierarchy) {
        _matched=true;
        _switchResult = this.getInternalImage("level.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableMeasure) {
        _matched=true;
        _switchResult = this.getInternalImage("measure.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableDerived) {
        _matched=true;
        _switchResult = this.getInternalImage("derived.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableTask) {
        _matched=true;
        _switchResult = this.getInternalImage("task.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableColumn) {
        _matched=true;
        _switchResult = this.getInternalImage("column.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableLookup) {
        _matched=true;
        _switchResult = this.getInternalImage("lookup.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TablePreorder) {
        _matched=true;
        _switchResult = this.getInternalImage("pre-order.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof CxGridProperty) {
        _matched=true;
        _switchResult = this.getInternalImage("properties.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof CxGridPropStyle) {
        _matched=true;
        _switchResult = this.getInternalImage("style.png", this.getClass());
      }
    }
    if (!_matched) {
      if (o instanceof TableDtoDatasource) {
        _matched=true;
        _switchResult = this.getInternalImage("datasource.png", this.getClass());
      }
    }
    if (!_matched) {
      _switchResult = super.image(o);
    }
    return _switchResult;
  }
}
