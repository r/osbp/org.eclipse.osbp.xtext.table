/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;

import org.eclipse.osbp.xtext.datamartdsl.DatamartDerivedMeasure;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Derived</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableDerived#getValueRef <em>Value Ref</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableDerived()
 * @model
 * @generated
 */
public interface TableDerived extends Expression, TableValueElement {
	/**
	 * Returns the value of the '<em><b>Value Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Ref</em>' reference.
	 * @see #setValueRef(DatamartDerivedMeasure)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableDerived_ValueRef()
	 * @model
	 * @generated
	 */
	DatamartDerivedMeasure getValueRef();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableDerived#getValueRef <em>Value Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Ref</em>' reference.
	 * @see #getValueRef()
	 * @generated
	 */
	void setValueRef(DatamartDerivedMeasure value);

} // TableDerived
