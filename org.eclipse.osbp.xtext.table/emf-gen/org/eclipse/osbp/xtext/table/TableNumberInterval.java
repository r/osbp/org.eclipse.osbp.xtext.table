/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Number Interval</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableNumberInterval#getNumberIntervalValue <em>Number Interval Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableNumberInterval#getNumberRange <em>Number Range</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableNumberInterval()
 * @model
 * @generated
 */
public interface TableNumberInterval extends TableInterval {
	/**
	 * Returns the value of the '<em><b>Number Interval Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Interval Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Interval Value</em>' attribute.
	 * @see #setNumberIntervalValue(double)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableNumberInterval_NumberIntervalValue()
	 * @model unique="false"
	 * @generated
	 */
	double getNumberIntervalValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableNumberInterval#getNumberIntervalValue <em>Number Interval Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Interval Value</em>' attribute.
	 * @see #getNumberIntervalValue()
	 * @generated
	 */
	void setNumberIntervalValue(double value);

	/**
	 * Returns the value of the '<em><b>Number Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Range</em>' containment reference.
	 * @see #setNumberRange(TableRangeElement)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableNumberInterval_NumberRange()
	 * @model containment="true"
	 * @generated
	 */
	TableRangeElement getNumberRange();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableNumberInterval#getNumberRange <em>Number Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Range</em>' containment reference.
	 * @see #getNumberRange()
	 * @generated
	 */
	void setNumberRange(TableRangeElement value);

} // TableNumberInterval
