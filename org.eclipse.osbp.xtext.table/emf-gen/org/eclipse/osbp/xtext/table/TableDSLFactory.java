/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage
 * @generated
 */
public interface TableDSLFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TableDSLFactory eINSTANCE = org.eclipse.osbp.xtext.table.impl.TableDSLFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Table Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Model</em>'.
	 * @generated
	 */
	TableModel createTableModel();

	/**
	 * Returns a new object of class '<em>Table Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Lazy Resolver</em>'.
	 * @generated
	 */
	TableLazyResolver createTableLazyResolver();

	/**
	 * Returns a new object of class '<em>Table Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Package</em>'.
	 * @generated
	 */
	TablePackage createTablePackage();

	/**
	 * Returns a new object of class '<em>Table Base</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Base</em>'.
	 * @generated
	 */
	TableBase createTableBase();

	/**
	 * Returns a new object of class '<em>Table</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table</em>'.
	 * @generated
	 */
	Table createTable();

	/**
	 * Returns a new object of class '<em>Table Selection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Selection</em>'.
	 * @generated
	 */
	TableSelection createTableSelection();

	/**
	 * Returns a new object of class '<em>Table Table</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Table</em>'.
	 * @generated
	 */
	TableTable createTableTable();

	/**
	 * Returns a new object of class '<em>Table Grid</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Grid</em>'.
	 * @generated
	 */
	TableGrid createTableGrid();

	/**
	 * Returns a new object of class '<em>Table Dto Datasource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Dto Datasource</em>'.
	 * @generated
	 */
	TableDtoDatasource createTableDtoDatasource();

	/**
	 * Returns a new object of class '<em>Table Grid Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Grid Property</em>'.
	 * @generated
	 */
	TableGridProperty createTableGridProperty();

	/**
	 * Returns a new object of class '<em>Table Preorder</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Preorder</em>'.
	 * @generated
	 */
	TablePreorder createTablePreorder();

	/**
	 * Returns a new object of class '<em>Table Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Value</em>'.
	 * @generated
	 */
	TableValue createTableValue();

	/**
	 * Returns a new object of class '<em>Table Formatter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Formatter</em>'.
	 * @generated
	 */
	TableFormatter createTableFormatter();

	/**
	 * Returns a new object of class '<em>Table Image</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Image</em>'.
	 * @generated
	 */
	TableImage createTableImage();

	/**
	 * Returns a new object of class '<em>Table Tooltip Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Tooltip Pattern</em>'.
	 * @generated
	 */
	TableTooltipPattern createTableTooltipPattern();

	/**
	 * Returns a new object of class '<em>Table Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Task</em>'.
	 * @generated
	 */
	TableTask createTableTask();

	/**
	 * Returns a new object of class '<em>Table All Columns</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table All Columns</em>'.
	 * @generated
	 */
	TableAllColumns createTableAllColumns();

	/**
	 * Returns a new object of class '<em>Table Ordinal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Ordinal</em>'.
	 * @generated
	 */
	TableOrdinal createTableOrdinal();

	/**
	 * Returns a new object of class '<em>Table Column</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Column</em>'.
	 * @generated
	 */
	TableColumn createTableColumn();

	/**
	 * Returns a new object of class '<em>Table Measure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Measure</em>'.
	 * @generated
	 */
	TableMeasure createTableMeasure();

	/**
	 * Returns a new object of class '<em>Table Derived</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Derived</em>'.
	 * @generated
	 */
	TableDerived createTableDerived();

	/**
	 * Returns a new object of class '<em>Table Hierarchy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Hierarchy</em>'.
	 * @generated
	 */
	TableHierarchy createTableHierarchy();

	/**
	 * Returns a new object of class '<em>Table Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Attribute</em>'.
	 * @generated
	 */
	TableAttribute createTableAttribute();

	/**
	 * Returns a new object of class '<em>Table Aggregation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Aggregation</em>'.
	 * @generated
	 */
	TableAggregation createTableAggregation();

	/**
	 * Returns a new object of class '<em>Table Number Interval</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Number Interval</em>'.
	 * @generated
	 */
	TableNumberInterval createTableNumberInterval();

	/**
	 * Returns a new object of class '<em>Table Int Interval</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Int Interval</em>'.
	 * @generated
	 */
	TableIntInterval createTableIntInterval();

	/**
	 * Returns a new object of class '<em>Table Date Day Interval</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Date Day Interval</em>'.
	 * @generated
	 */
	TableDateDayInterval createTableDateDayInterval();

	/**
	 * Returns a new object of class '<em>Table Number Lookup</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Number Lookup</em>'.
	 * @generated
	 */
	TableNumberLookup createTableNumberLookup();

	/**
	 * Returns a new object of class '<em>Table Int Lookup</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Int Lookup</em>'.
	 * @generated
	 */
	TableIntLookup createTableIntLookup();

	/**
	 * Returns a new object of class '<em>Table String Lookup</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table String Lookup</em>'.
	 * @generated
	 */
	TableStringLookup createTableStringLookup();

	/**
	 * Returns a new object of class '<em>Table Date Day Lookup</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Date Day Lookup</em>'.
	 * @generated
	 */
	TableDateDayLookup createTableDateDayLookup();

	/**
	 * Returns a new object of class '<em>Table Datamart</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Datamart</em>'.
	 * @generated
	 */
	TableDatamart createTableDatamart();

	/**
	 * Returns a new object of class '<em>Table Axis</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Axis</em>'.
	 * @generated
	 */
	TableAxis createTableAxis();

	/**
	 * Returns a new object of class '<em>Table Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Event</em>'.
	 * @generated
	 */
	TableEvent createTableEvent();

	/**
	 * Returns a new object of class '<em>Table Text Color</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Text Color</em>'.
	 * @generated
	 */
	TableTextColor createTableTextColor();

	/**
	 * Returns a new object of class '<em>Table Cell Color</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Cell Color</em>'.
	 * @generated
	 */
	TableCellColor createTableCellColor();

	/**
	 * Returns a new object of class '<em>Table Icon</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Icon</em>'.
	 * @generated
	 */
	TableIcon createTableIcon();

	/**
	 * Returns a new object of class '<em>Table Trend</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Trend</em>'.
	 * @generated
	 */
	TableTrend createTableTrend();

	/**
	 * Returns a new object of class '<em>Table Tooltip</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table Tooltip</em>'.
	 * @generated
	 */
	TableTooltip createTableTooltip();

	/**
	 * Returns a new object of class '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expression</em>'.
	 * @generated
	 */
	Expression createExpression();

	/**
	 * Returns a new object of class '<em>Calculation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Calculation</em>'.
	 * @generated
	 */
	Calculation createCalculation();

	/**
	 * Returns a new object of class '<em>Conjunction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conjunction</em>'.
	 * @generated
	 */
	Conjunction createConjunction();

	/**
	 * Returns a new object of class '<em>Disjunction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Disjunction</em>'.
	 * @generated
	 */
	Disjunction createDisjunction();

	/**
	 * Returns a new object of class '<em>Conditional Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conditional Expression</em>'.
	 * @generated
	 */
	ConditionalExpression createConditionalExpression();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TableDSLPackage getTableDSLPackage();

} //TableDSLFactory
