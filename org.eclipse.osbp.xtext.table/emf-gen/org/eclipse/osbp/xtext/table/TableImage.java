/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Image</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableImage#getImagePathPattern <em>Image Path Pattern</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableImage#isHideImageLabel <em>Hide Image Label</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableImage#isHasParameter <em>Has Parameter</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableImage#isResize <em>Resize</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableImage#getResizeString <em>Resize String</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableImage#getImagePathParameter <em>Image Path Parameter</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableImage()
 * @model
 * @generated
 */
public interface TableImage extends TableLazyResolver {
	/**
	 * Returns the value of the '<em><b>Image Path Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Path Pattern</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Path Pattern</em>' attribute.
	 * @see #setImagePathPattern(String)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableImage_ImagePathPattern()
	 * @model unique="false"
	 * @generated
	 */
	String getImagePathPattern();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableImage#getImagePathPattern <em>Image Path Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image Path Pattern</em>' attribute.
	 * @see #getImagePathPattern()
	 * @generated
	 */
	void setImagePathPattern(String value);

	/**
	 * Returns the value of the '<em><b>Hide Image Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hide Image Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hide Image Label</em>' attribute.
	 * @see #setHideImageLabel(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableImage_HideImageLabel()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHideImageLabel();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableImage#isHideImageLabel <em>Hide Image Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hide Image Label</em>' attribute.
	 * @see #isHideImageLabel()
	 * @generated
	 */
	void setHideImageLabel(boolean value);

	/**
	 * Returns the value of the '<em><b>Has Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Parameter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Parameter</em>' attribute.
	 * @see #setHasParameter(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableImage_HasParameter()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasParameter();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableImage#isHasParameter <em>Has Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Parameter</em>' attribute.
	 * @see #isHasParameter()
	 * @generated
	 */
	void setHasParameter(boolean value);

	/**
	 * Returns the value of the '<em><b>Resize</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resize</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resize</em>' attribute.
	 * @see #setResize(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableImage_Resize()
	 * @model unique="false"
	 * @generated
	 */
	boolean isResize();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableImage#isResize <em>Resize</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resize</em>' attribute.
	 * @see #isResize()
	 * @generated
	 */
	void setResize(boolean value);

	/**
	 * Returns the value of the '<em><b>Resize String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resize String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resize String</em>' attribute.
	 * @see #setResizeString(String)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableImage_ResizeString()
	 * @model unique="false"
	 * @generated
	 */
	String getResizeString();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableImage#getResizeString <em>Resize String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resize String</em>' attribute.
	 * @see #getResizeString()
	 * @generated
	 */
	void setResizeString(String value);

	/**
	 * Returns the value of the '<em><b>Image Path Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Path Parameter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Path Parameter</em>' containment reference.
	 * @see #setImagePathParameter(TableValueElement)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableImage_ImagePathParameter()
	 * @model containment="true"
	 * @generated
	 */
	TableValueElement getImagePathParameter();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableImage#getImagePathParameter <em>Image Path Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image Path Parameter</em>' containment reference.
	 * @see #getImagePathParameter()
	 * @generated
	 */
	void setImagePathParameter(TableValueElement value);

} // TableImage
