/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Date Day Interval</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableDateDayInterval#getDateIntervalValue <em>Date Interval Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableDateDayInterval#getDateRange <em>Date Range</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableDateDayInterval()
 * @model
 * @generated
 */
public interface TableDateDayInterval extends TableInterval {
	/**
	 * Returns the value of the '<em><b>Date Interval Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date Interval Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date Interval Value</em>' attribute.
	 * @see #setDateIntervalValue(int)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableDateDayInterval_DateIntervalValue()
	 * @model unique="false"
	 * @generated
	 */
	int getDateIntervalValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableDateDayInterval#getDateIntervalValue <em>Date Interval Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date Interval Value</em>' attribute.
	 * @see #getDateIntervalValue()
	 * @generated
	 */
	void setDateIntervalValue(int value);

	/**
	 * Returns the value of the '<em><b>Date Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date Range</em>' containment reference.
	 * @see #setDateRange(TableRangeElement)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableDateDayInterval_DateRange()
	 * @model containment="true"
	 * @generated
	 */
	TableRangeElement getDateRange();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableDateDayInterval#getDateRange <em>Date Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date Range</em>' containment reference.
	 * @see #getDateRange()
	 * @generated
	 */
	void setDateRange(TableRangeElement value);

} // TableDateDayInterval
