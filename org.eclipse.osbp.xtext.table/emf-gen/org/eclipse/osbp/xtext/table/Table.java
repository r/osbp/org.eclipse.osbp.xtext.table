/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.Table#isDescription <em>Description</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.Table#getDescriptionValue <em>Description Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.Table#getTabletype <em>Tabletype</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTable()
 * @model
 * @generated
 */
public interface Table extends TableBase {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTable_Description()
	 * @model unique="false"
	 * @generated
	 */
	boolean isDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.Table#isDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #isDescription()
	 * @generated
	 */
	void setDescription(boolean value);

	/**
	 * Returns the value of the '<em><b>Description Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description Value</em>' attribute.
	 * @see #setDescriptionValue(String)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTable_DescriptionValue()
	 * @model unique="false"
	 * @generated
	 */
	String getDescriptionValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.Table#getDescriptionValue <em>Description Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description Value</em>' attribute.
	 * @see #getDescriptionValue()
	 * @generated
	 */
	void setDescriptionValue(String value);

	/**
	 * Returns the value of the '<em><b>Tabletype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tabletype</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tabletype</em>' containment reference.
	 * @see #setTabletype(TableOption)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTable_Tabletype()
	 * @model containment="true"
	 * @generated
	 */
	TableOption getTabletype();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.Table#getTabletype <em>Tabletype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tabletype</em>' containment reference.
	 * @see #getTabletype()
	 * @generated
	 */
	void setTabletype(TableOption value);

} // Table
