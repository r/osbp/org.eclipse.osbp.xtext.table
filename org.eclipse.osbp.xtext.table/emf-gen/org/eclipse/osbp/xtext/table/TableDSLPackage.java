/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.infogrid.model.gridsource.CxGridSourcePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.table.TableDSLFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel fileExtensions='table' modelName='TableDSL' prefix='TableDSL' operationReflection='false' updateClasspath='false' loadInitialization='false' literalsInterface='true' copyrightText='Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)\n All rights reserved. This program and the accompanying materials \n are made available under the terms of the Eclipse Public License 2.0  \n which accompanies this distribution, and is available at \n https://www.eclipse.org/legal/epl-2.0/ \n \n SPDX-License-Identifier: EPL-2.0 \n\n Based on ideas from Xtext, Xtend, Xcore\n\n Contributors:  \n \t\tJoerg Riegel - Initial implementation \n ' basePackage='org.eclipse.osbp.xtext'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore rootPackage='tabledsl'"
 * @generated
 */
public interface TableDSLPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "table";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.eclipse.org/xtext/table/TableDSL";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "tabledsl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TableDSLPackage eINSTANCE = org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableModelImpl <em>Table Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableModelImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableModel()
	 * @generated
	 */
	int TABLE_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Import Section</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_MODEL__IMPORT_SECTION = 0;

	/**
	 * The feature id for the '<em><b>Packages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_MODEL__PACKAGES = 1;

	/**
	 * The number of structural features of the '<em>Table Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_MODEL_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableLazyResolverImpl <em>Table Lazy Resolver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableLazyResolverImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableLazyResolver()
	 * @generated
	 */
	int TABLE_LAZY_RESOLVER = 1;

	/**
	 * The number of structural features of the '<em>Table Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_LAZY_RESOLVER_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TablePackageImpl <em>Table Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TablePackageImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTablePackage()
	 * @generated
	 */
	int TABLE_PACKAGE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_PACKAGE__NAME = OSBPTypesPackage.LPACKAGE__NAME;

	/**
	 * The feature id for the '<em><b>Tables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_PACKAGE__TABLES = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_PACKAGE_FEATURE_COUNT = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableBaseImpl <em>Table Base</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableBaseImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableBase()
	 * @generated
	 */
	int TABLE_BASE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_BASE__NAME = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_BASE_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableImpl <em>Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTable()
	 * @generated
	 */
	int TABLE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE__NAME = TABLE_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE__DESCRIPTION = TABLE_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE__DESCRIPTION_VALUE = TABLE_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tabletype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE__TABLETYPE = TABLE_BASE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_FEATURE_COUNT = TABLE_BASE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.TableOption <em>Table Option</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.TableOption
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableOption()
	 * @generated
	 */
	int TABLE_OPTION = 5;

	/**
	 * The feature id for the '<em><b>Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_OPTION__FILTERING = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Toolbar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_OPTION__TOOLBAR = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Embedded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_OPTION__EMBEDDED = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Table Option</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_OPTION_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableTableImpl <em>Table Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableTableImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableTable()
	 * @generated
	 */
	int TABLE_TABLE = 7;

	/**
	 * The feature id for the '<em><b>Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TABLE__FILTERING = TABLE_OPTION__FILTERING;

	/**
	 * The feature id for the '<em><b>Toolbar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TABLE__TOOLBAR = TABLE_OPTION__TOOLBAR;

	/**
	 * The feature id for the '<em><b>Embedded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TABLE__EMBEDDED = TABLE_OPTION__EMBEDDED;

	/**
	 * The feature id for the '<em><b>Select Id Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TABLE__SELECT_ID_ONLY = TABLE_OPTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Select By Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TABLE__SELECT_BY_ID = TABLE_OPTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Selectalways</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TABLE__SELECTALWAYS = TABLE_OPTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Header Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TABLE__HEADER_MODE = TABLE_OPTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TABLE__SOURCE = TABLE_OPTION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Table Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TABLE_FEATURE_COUNT = TABLE_OPTION_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableSelectionImpl <em>Table Selection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableSelectionImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableSelection()
	 * @generated
	 */
	int TABLE_SELECTION = 6;

	/**
	 * The feature id for the '<em><b>Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_SELECTION__FILTERING = TABLE_TABLE__FILTERING;

	/**
	 * The feature id for the '<em><b>Toolbar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_SELECTION__TOOLBAR = TABLE_TABLE__TOOLBAR;

	/**
	 * The feature id for the '<em><b>Embedded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_SELECTION__EMBEDDED = TABLE_TABLE__EMBEDDED;

	/**
	 * The feature id for the '<em><b>Select Id Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_SELECTION__SELECT_ID_ONLY = TABLE_TABLE__SELECT_ID_ONLY;

	/**
	 * The feature id for the '<em><b>Select By Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_SELECTION__SELECT_BY_ID = TABLE_TABLE__SELECT_BY_ID;

	/**
	 * The feature id for the '<em><b>Selectalways</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_SELECTION__SELECTALWAYS = TABLE_TABLE__SELECTALWAYS;

	/**
	 * The feature id for the '<em><b>Header Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_SELECTION__HEADER_MODE = TABLE_TABLE__HEADER_MODE;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_SELECTION__SOURCE = TABLE_TABLE__SOURCE;

	/**
	 * The feature id for the '<em><b>Multi Selection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_SELECTION__MULTI_SELECTION = TABLE_TABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Selection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_SELECTION_FEATURE_COUNT = TABLE_TABLE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableGridImpl <em>Table Grid</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableGridImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableGrid()
	 * @generated
	 */
	int TABLE_GRID = 8;

	/**
	 * The feature id for the '<em><b>Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID__FILTERING = TABLE_OPTION__FILTERING;

	/**
	 * The feature id for the '<em><b>Toolbar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID__TOOLBAR = TABLE_OPTION__TOOLBAR;

	/**
	 * The feature id for the '<em><b>Embedded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID__EMBEDDED = TABLE_OPTION__EMBEDDED;

	/**
	 * The feature id for the '<em><b>Selectalways</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID__SELECTALWAYS = TABLE_OPTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Header Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID__HEADER_MODE = TABLE_OPTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID__SOURCE = TABLE_OPTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Table Grid</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID_FEATURE_COUNT = TABLE_OPTION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableDtoDatasourceImpl <em>Table Dto Datasource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableDtoDatasourceImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableDtoDatasource()
	 * @generated
	 */
	int TABLE_DTO_DATASOURCE = 9;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DTO_DATASOURCE__ID = CxGridSourcePackage.CX_GRID_SOURCE__ID;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DTO_DATASOURCE__TAGS = CxGridSourcePackage.CX_GRID_SOURCE__TAGS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DTO_DATASOURCE__KIND = CxGridSourcePackage.CX_GRID_SOURCE__KIND;

	/**
	 * The feature id for the '<em><b>Root Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DTO_DATASOURCE__ROOT_TYPE = CxGridSourcePackage.CX_GRID_SOURCE__ROOT_TYPE;

	/**
	 * The feature id for the '<em><b>Root Type FQN</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DTO_DATASOURCE__ROOT_TYPE_FQN = CxGridSourcePackage.CX_GRID_SOURCE__ROOT_TYPE_FQN;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DTO_DATASOURCE__PROPERTIES = CxGridSourcePackage.CX_GRID_SOURCE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Inputs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DTO_DATASOURCE__INPUTS = CxGridSourcePackage.CX_GRID_SOURCE__INPUTS;

	/**
	 * The feature id for the '<em><b>Selection Event Topic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DTO_DATASOURCE__SELECTION_EVENT_TOPIC = CxGridSourcePackage.CX_GRID_SOURCE__SELECTION_EVENT_TOPIC;

	/**
	 * The feature id for the '<em><b>Group Nested Types</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DTO_DATASOURCE__GROUP_NESTED_TYPES = CxGridSourcePackage.CX_GRID_SOURCE__GROUP_NESTED_TYPES;

	/**
	 * The feature id for the '<em><b>Dto Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DTO_DATASOURCE__DTO_SOURCE = CxGridSourcePackage.CX_GRID_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Dto Datasource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DTO_DATASOURCE_FEATURE_COUNT = CxGridSourcePackage.CX_GRID_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableGridPropertyImpl <em>Table Grid Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableGridPropertyImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableGridProperty()
	 * @generated
	 */
	int TABLE_GRID_PROPERTY = 10;

	/**
	 * The feature id for the '<em><b>Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID_PROPERTY__PATH = CxGridSourcePackage.CX_GRID_PROPERTY__PATH;

	/**
	 * The feature id for the '<em><b>Style</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID_PROPERTY__STYLE = CxGridSourcePackage.CX_GRID_PROPERTY__STYLE;

	/**
	 * The feature id for the '<em><b>Dot Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID_PROPERTY__DOT_PATH = CxGridSourcePackage.CX_GRID_PROPERTY__DOT_PATH;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID_PROPERTY__EDITABLE = CxGridSourcePackage.CX_GRID_PROPERTY__EDITABLE;

	/**
	 * The feature id for the '<em><b>Source FQN</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID_PROPERTY__SOURCE_FQN = CxGridSourcePackage.CX_GRID_PROPERTY__SOURCE_FQN;

	/**
	 * The number of structural features of the '<em>Table Grid Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_GRID_PROPERTY_FEATURE_COUNT = CxGridSourcePackage.CX_GRID_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TablePreorderImpl <em>Table Preorder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TablePreorderImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTablePreorder()
	 * @generated
	 */
	int TABLE_PREORDER = 11;

	/**
	 * The feature id for the '<em><b>Column</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_PREORDER__COLUMN = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ascending</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_PREORDER__ASCENDING = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Table Preorder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_PREORDER_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl <em>Table Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableValueImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableValue()
	 * @generated
	 */
	int TABLE_VALUE = 12;

	/**
	 * The feature id for the '<em><b>Column</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE__COLUMN = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Collapsed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE__COLLAPSED = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Formatter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE__FORMATTER = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Tooltip Pattern</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE__TOOLTIP_PATTERN = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Hide Label Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE__HIDE_LABEL_INTERVAL = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Hide Label Lookup</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE__HIDE_LABEL_LOOKUP = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Intervals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE__INTERVALS = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Lookups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE__LOOKUPS = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Has Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE__HAS_IMAGE = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Image</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE__IMAGE = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Icon Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE__ICON_NAME = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 10;

	/**
	 * The number of structural features of the '<em>Table Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 11;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableFormatterImpl <em>Table Formatter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableFormatterImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableFormatter()
	 * @generated
	 */
	int TABLE_FORMATTER = 13;

	/**
	 * The feature id for the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_FORMATTER__FORMAT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Formatter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_FORMATTER_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableImageImpl <em>Table Image</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableImageImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableImage()
	 * @generated
	 */
	int TABLE_IMAGE = 14;

	/**
	 * The feature id for the '<em><b>Image Path Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_IMAGE__IMAGE_PATH_PATTERN = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Hide Image Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_IMAGE__HIDE_IMAGE_LABEL = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Has Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_IMAGE__HAS_PARAMETER = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Resize</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_IMAGE__RESIZE = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Resize String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_IMAGE__RESIZE_STRING = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Image Path Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_IMAGE__IMAGE_PATH_PARAMETER = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Table Image</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_IMAGE_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableTooltipPatternImpl <em>Table Tooltip Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableTooltipPatternImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableTooltipPattern()
	 * @generated
	 */
	int TABLE_TOOLTIP_PATTERN = 15;

	/**
	 * The feature id for the '<em><b>Tooltip Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TOOLTIP_PATTERN__TOOLTIP_PATTERN = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Tooltip Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TOOLTIP_PATTERN_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.TableValueElement <em>Table Value Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.TableValueElement
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableValueElement()
	 * @generated
	 */
	int TABLE_VALUE_ELEMENT = 16;

	/**
	 * The number of structural features of the '<em>Table Value Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_VALUE_ELEMENT_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.ExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.ExpressionImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getExpression()
	 * @generated
	 */
	int EXPRESSION = 45;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__NUMBER_VALUE = TABLE_VALUE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__STRING_VALUE = TABLE_VALUE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = TABLE_VALUE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableTaskImpl <em>Table Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableTaskImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableTask()
	 * @generated
	 */
	int TABLE_TASK = 17;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TASK__NUMBER_VALUE = EXPRESSION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TASK__STRING_VALUE = EXPRESSION__STRING_VALUE;

	/**
	 * The number of structural features of the '<em>Table Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TASK_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableAllColumnsImpl <em>Table All Columns</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableAllColumnsImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableAllColumns()
	 * @generated
	 */
	int TABLE_ALL_COLUMNS = 18;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ALL_COLUMNS__NUMBER_VALUE = EXPRESSION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ALL_COLUMNS__STRING_VALUE = EXPRESSION__STRING_VALUE;

	/**
	 * The number of structural features of the '<em>Table All Columns</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ALL_COLUMNS_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableOrdinalImpl <em>Table Ordinal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableOrdinalImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableOrdinal()
	 * @generated
	 */
	int TABLE_ORDINAL = 19;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ORDINAL__NUMBER_VALUE = EXPRESSION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ORDINAL__STRING_VALUE = EXPRESSION__STRING_VALUE;

	/**
	 * The feature id for the '<em><b>Value Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ORDINAL__VALUE_REF = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Ordinal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ORDINAL_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableColumnImpl <em>Table Column</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableColumnImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableColumn()
	 * @generated
	 */
	int TABLE_COLUMN = 20;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_COLUMN__NUMBER_VALUE = EXPRESSION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_COLUMN__STRING_VALUE = EXPRESSION__STRING_VALUE;

	/**
	 * The feature id for the '<em><b>Value Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_COLUMN__VALUE_REF = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_COLUMN_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableMeasureImpl <em>Table Measure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableMeasureImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableMeasure()
	 * @generated
	 */
	int TABLE_MEASURE = 21;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_MEASURE__NUMBER_VALUE = EXPRESSION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_MEASURE__STRING_VALUE = EXPRESSION__STRING_VALUE;

	/**
	 * The feature id for the '<em><b>Value Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_MEASURE__VALUE_REF = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Measure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_MEASURE_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableDerivedImpl <em>Table Derived</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableDerivedImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableDerived()
	 * @generated
	 */
	int TABLE_DERIVED = 22;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DERIVED__NUMBER_VALUE = EXPRESSION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DERIVED__STRING_VALUE = EXPRESSION__STRING_VALUE;

	/**
	 * The feature id for the '<em><b>Value Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DERIVED__VALUE_REF = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Derived</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DERIVED_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableHierarchyImpl <em>Table Hierarchy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableHierarchyImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableHierarchy()
	 * @generated
	 */
	int TABLE_HIERARCHY = 23;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_HIERARCHY__NUMBER_VALUE = EXPRESSION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_HIERARCHY__STRING_VALUE = EXPRESSION__STRING_VALUE;

	/**
	 * The feature id for the '<em><b>Value Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_HIERARCHY__VALUE_REF = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Hierarchy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_HIERARCHY_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableAttributeImpl <em>Table Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableAttributeImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableAttribute()
	 * @generated
	 */
	int TABLE_ATTRIBUTE = 24;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ATTRIBUTE__NUMBER_VALUE = EXPRESSION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ATTRIBUTE__STRING_VALUE = EXPRESSION__STRING_VALUE;

	/**
	 * The feature id for the '<em><b>Value Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ATTRIBUTE__VALUE_REF = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ATTRIBUTE_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableAggregationImpl <em>Table Aggregation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableAggregationImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableAggregation()
	 * @generated
	 */
	int TABLE_AGGREGATION = 25;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AGGREGATION__NUMBER_VALUE = EXPRESSION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AGGREGATION__STRING_VALUE = EXPRESSION__STRING_VALUE;

	/**
	 * The feature id for the '<em><b>Value Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AGGREGATION__VALUE_REF = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Aggregation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AGGREGATION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.TableInterval <em>Table Interval</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.TableInterval
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableInterval()
	 * @generated
	 */
	int TABLE_INTERVAL = 26;

	/**
	 * The number of structural features of the '<em>Table Interval</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_INTERVAL_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableNumberIntervalImpl <em>Table Number Interval</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableNumberIntervalImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableNumberInterval()
	 * @generated
	 */
	int TABLE_NUMBER_INTERVAL = 27;

	/**
	 * The feature id for the '<em><b>Number Interval Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_NUMBER_INTERVAL__NUMBER_INTERVAL_VALUE = TABLE_INTERVAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Number Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_NUMBER_INTERVAL__NUMBER_RANGE = TABLE_INTERVAL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Table Number Interval</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_NUMBER_INTERVAL_FEATURE_COUNT = TABLE_INTERVAL_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableIntIntervalImpl <em>Table Int Interval</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableIntIntervalImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableIntInterval()
	 * @generated
	 */
	int TABLE_INT_INTERVAL = 28;

	/**
	 * The feature id for the '<em><b>Int Interval Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_INT_INTERVAL__INT_INTERVAL_VALUE = TABLE_INTERVAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Int Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_INT_INTERVAL__INT_RANGE = TABLE_INTERVAL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Table Int Interval</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_INT_INTERVAL_FEATURE_COUNT = TABLE_INTERVAL_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableDateDayIntervalImpl <em>Table Date Day Interval</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableDateDayIntervalImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableDateDayInterval()
	 * @generated
	 */
	int TABLE_DATE_DAY_INTERVAL = 29;

	/**
	 * The feature id for the '<em><b>Date Interval Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DATE_DAY_INTERVAL__DATE_INTERVAL_VALUE = TABLE_INTERVAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Date Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DATE_DAY_INTERVAL__DATE_RANGE = TABLE_INTERVAL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Table Date Day Interval</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DATE_DAY_INTERVAL_FEATURE_COUNT = TABLE_INTERVAL_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.TableLookup <em>Table Lookup</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.TableLookup
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableLookup()
	 * @generated
	 */
	int TABLE_LOOKUP = 30;

	/**
	 * The number of structural features of the '<em>Table Lookup</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_LOOKUP_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableNumberLookupImpl <em>Table Number Lookup</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableNumberLookupImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableNumberLookup()
	 * @generated
	 */
	int TABLE_NUMBER_LOOKUP = 31;

	/**
	 * The feature id for the '<em><b>Lookup Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_NUMBER_LOOKUP__LOOKUP_VALUE = TABLE_LOOKUP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Discrete</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_NUMBER_LOOKUP__DISCRETE = TABLE_LOOKUP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Table Number Lookup</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_NUMBER_LOOKUP_FEATURE_COUNT = TABLE_LOOKUP_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableIntLookupImpl <em>Table Int Lookup</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableIntLookupImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableIntLookup()
	 * @generated
	 */
	int TABLE_INT_LOOKUP = 32;

	/**
	 * The feature id for the '<em><b>Lookup Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_INT_LOOKUP__LOOKUP_VALUE = TABLE_LOOKUP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Discrete</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_INT_LOOKUP__DISCRETE = TABLE_LOOKUP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Table Int Lookup</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_INT_LOOKUP_FEATURE_COUNT = TABLE_LOOKUP_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableStringLookupImpl <em>Table String Lookup</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableStringLookupImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableStringLookup()
	 * @generated
	 */
	int TABLE_STRING_LOOKUP = 33;

	/**
	 * The feature id for the '<em><b>Lookup Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_STRING_LOOKUP__LOOKUP_VALUE = TABLE_LOOKUP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Discrete</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_STRING_LOOKUP__DISCRETE = TABLE_LOOKUP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Table String Lookup</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_STRING_LOOKUP_FEATURE_COUNT = TABLE_LOOKUP_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableDateDayLookupImpl <em>Table Date Day Lookup</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableDateDayLookupImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableDateDayLookup()
	 * @generated
	 */
	int TABLE_DATE_DAY_LOOKUP = 34;

	/**
	 * The feature id for the '<em><b>Lookup Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DATE_DAY_LOOKUP__LOOKUP_VALUE = TABLE_LOOKUP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Discrete</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DATE_DAY_LOOKUP__DISCRETE = TABLE_LOOKUP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Table Date Day Lookup</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DATE_DAY_LOOKUP_FEATURE_COUNT = TABLE_LOOKUP_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableDatamartImpl <em>Table Datamart</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableDatamartImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableDatamart()
	 * @generated
	 */
	int TABLE_DATAMART = 35;

	/**
	 * The feature id for the '<em><b>Datamart Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DATAMART__DATAMART_REF = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DATAMART__ELEMENTS = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Table Datamart</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_DATAMART_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.TableElement <em>Table Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.TableElement
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableElement()
	 * @generated
	 */
	int TABLE_ELEMENT = 36;

	/**
	 * The number of structural features of the '<em>Table Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ELEMENT_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableAxisImpl <em>Table Axis</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableAxisImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableAxis()
	 * @generated
	 */
	int TABLE_AXIS = 37;

	/**
	 * The feature id for the '<em><b>Axis</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AXIS__AXIS = TABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has Row Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AXIS__HAS_ROW_HEIGHT = TABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Row Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AXIS__ROW_HEIGHT = TABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Pre Order</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AXIS__PRE_ORDER = TABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Has Details</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AXIS__HAS_DETAILS = TABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AXIS__VALUES = TABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Has Events</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AXIS__HAS_EVENTS = TABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AXIS__EVENTS = TABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Table Axis</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_AXIS_FEATURE_COUNT = TABLE_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableEventImpl <em>Table Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableEventImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableEvent()
	 * @generated
	 */
	int TABLE_EVENT = 38;

	/**
	 * The feature id for the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_EVENT__SOURCE = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_EVENT_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.TableRangeElement <em>Table Range Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.TableRangeElement
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableRangeElement()
	 * @generated
	 */
	int TABLE_RANGE_ELEMENT = 39;

	/**
	 * The number of structural features of the '<em>Table Range Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_RANGE_ELEMENT_FEATURE_COUNT = TABLE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableTextColorImpl <em>Table Text Color</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableTextColorImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableTextColor()
	 * @generated
	 */
	int TABLE_TEXT_COLOR = 40;

	/**
	 * The feature id for the '<em><b>Rgb</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TEXT_COLOR__RGB = TABLE_RANGE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Text Color</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TEXT_COLOR_FEATURE_COUNT = TABLE_RANGE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableCellColorImpl <em>Table Cell Color</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableCellColorImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableCellColor()
	 * @generated
	 */
	int TABLE_CELL_COLOR = 41;

	/**
	 * The feature id for the '<em><b>Rgb</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_CELL_COLOR__RGB = TABLE_RANGE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Cell Color</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_CELL_COLOR_FEATURE_COUNT = TABLE_RANGE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableIconImpl <em>Table Icon</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableIconImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableIcon()
	 * @generated
	 */
	int TABLE_ICON = 42;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ICON__ICON = TABLE_RANGE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Icon</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_ICON_FEATURE_COUNT = TABLE_RANGE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableTrendImpl <em>Table Trend</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableTrendImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableTrend()
	 * @generated
	 */
	int TABLE_TREND = 43;

	/**
	 * The feature id for the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TREND__ICON = TABLE_RANGE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Trend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TREND_FEATURE_COUNT = TABLE_RANGE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.TableTooltipImpl <em>Table Tooltip</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.TableTooltipImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableTooltip()
	 * @generated
	 */
	int TABLE_TOOLTIP = 44;

	/**
	 * The feature id for the '<em><b>Tooltip</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TOOLTIP__TOOLTIP = TABLE_RANGE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table Tooltip</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_TOOLTIP_FEATURE_COUNT = TABLE_RANGE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.CalculationImpl <em>Calculation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.CalculationImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getCalculation()
	 * @generated
	 */
	int CALCULATION = 46;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION__NUMBER_VALUE = EXPRESSION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION__STRING_VALUE = EXPRESSION__STRING_VALUE;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION__LEFT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION__RIGHT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Calculation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.ConjunctionImpl <em>Conjunction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.ConjunctionImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getConjunction()
	 * @generated
	 */
	int CONJUNCTION = 47;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION__NUMBER_VALUE = CALCULATION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION__STRING_VALUE = CALCULATION__STRING_VALUE;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION__LEFT = CALCULATION__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION__RIGHT = CALCULATION__RIGHT;

	/**
	 * The number of structural features of the '<em>Conjunction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION_FEATURE_COUNT = CALCULATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.DisjunctionImpl <em>Disjunction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.DisjunctionImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getDisjunction()
	 * @generated
	 */
	int DISJUNCTION = 48;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION__NUMBER_VALUE = CALCULATION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION__STRING_VALUE = CALCULATION__STRING_VALUE;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION__LEFT = CALCULATION__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION__RIGHT = CALCULATION__RIGHT;

	/**
	 * The number of structural features of the '<em>Disjunction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION_FEATURE_COUNT = CALCULATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.impl.ConditionalExpressionImpl <em>Conditional Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.impl.ConditionalExpressionImpl
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getConditionalExpression()
	 * @generated
	 */
	int CONDITIONAL_EXPRESSION = 49;

	/**
	 * The feature id for the '<em><b>Number Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__NUMBER_VALUE = CALCULATION__NUMBER_VALUE;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__STRING_VALUE = CALCULATION__STRING_VALUE;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__LEFT = CALCULATION__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__RIGHT = CALCULATION__RIGHT;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__OPERATOR = CALCULATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Conditional Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION_FEATURE_COUNT = CALCULATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.TrendIconEnum <em>Trend Icon Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.TrendIconEnum
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTrendIconEnum()
	 * @generated
	 */
	int TREND_ICON_ENUM = 50;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.OperatorEnum <em>Operator Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.OperatorEnum
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getOperatorEnum()
	 * @generated
	 */
	int OPERATOR_ENUM = 51;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.table.RowHeaderMode <em>Row Header Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.table.RowHeaderMode
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getRowHeaderMode()
	 * @generated
	 */
	int ROW_HEADER_MODE = 52;

	/**
	 * The meta object id for the '<em>Internal EObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getInternalEObject()
	 * @generated
	 */
	int INTERNAL_EOBJECT = 53;


	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableModel <em>Table Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Model</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableModel
	 * @generated
	 */
	EClass getTableModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableModel#getImportSection <em>Import Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Import Section</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableModel#getImportSection()
	 * @see #getTableModel()
	 * @generated
	 */
	EReference getTableModel_ImportSection();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.table.TableModel#getPackages <em>Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Packages</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableModel#getPackages()
	 * @see #getTableModel()
	 * @generated
	 */
	EReference getTableModel_Packages();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableLazyResolver <em>Table Lazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Lazy Resolver</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableLazyResolver
	 * @generated
	 */
	EClass getTableLazyResolver();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TablePackage <em>Table Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Package</em>'.
	 * @see org.eclipse.osbp.xtext.table.TablePackage
	 * @generated
	 */
	EClass getTablePackage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.table.TablePackage#getTables <em>Tables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tables</em>'.
	 * @see org.eclipse.osbp.xtext.table.TablePackage#getTables()
	 * @see #getTablePackage()
	 * @generated
	 */
	EReference getTablePackage_Tables();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableBase <em>Table Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Base</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableBase
	 * @generated
	 */
	EClass getTableBase();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableBase#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableBase#getName()
	 * @see #getTableBase()
	 * @generated
	 */
	EAttribute getTableBase_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.Table <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table</em>'.
	 * @see org.eclipse.osbp.xtext.table.Table
	 * @generated
	 */
	EClass getTable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.Table#isDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.eclipse.osbp.xtext.table.Table#isDescription()
	 * @see #getTable()
	 * @generated
	 */
	EAttribute getTable_Description();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.Table#getDescriptionValue <em>Description Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description Value</em>'.
	 * @see org.eclipse.osbp.xtext.table.Table#getDescriptionValue()
	 * @see #getTable()
	 * @generated
	 */
	EAttribute getTable_DescriptionValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.Table#getTabletype <em>Tabletype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Tabletype</em>'.
	 * @see org.eclipse.osbp.xtext.table.Table#getTabletype()
	 * @see #getTable()
	 * @generated
	 */
	EReference getTable_Tabletype();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableOption <em>Table Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Option</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableOption
	 * @generated
	 */
	EClass getTableOption();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableOption#isFiltering <em>Filtering</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filtering</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableOption#isFiltering()
	 * @see #getTableOption()
	 * @generated
	 */
	EAttribute getTableOption_Filtering();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.table.TableOption#getToolbar <em>Toolbar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Toolbar</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableOption#getToolbar()
	 * @see #getTableOption()
	 * @generated
	 */
	EReference getTableOption_Toolbar();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableOption#isEmbedded <em>Embedded</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Embedded</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableOption#isEmbedded()
	 * @see #getTableOption()
	 * @generated
	 */
	EAttribute getTableOption_Embedded();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableSelection <em>Table Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Selection</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableSelection
	 * @generated
	 */
	EClass getTableSelection();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableSelection#isMultiSelection <em>Multi Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Multi Selection</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableSelection#isMultiSelection()
	 * @see #getTableSelection()
	 * @generated
	 */
	EAttribute getTableSelection_MultiSelection();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableTable <em>Table Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Table</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTable
	 * @generated
	 */
	EClass getTableTable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableTable#isSelectIdOnly <em>Select Id Only</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Select Id Only</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTable#isSelectIdOnly()
	 * @see #getTableTable()
	 * @generated
	 */
	EAttribute getTableTable_SelectIdOnly();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableTable#isSelectById <em>Select By Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Select By Id</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTable#isSelectById()
	 * @see #getTableTable()
	 * @generated
	 */
	EAttribute getTableTable_SelectById();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableTable#isSelectalways <em>Selectalways</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selectalways</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTable#isSelectalways()
	 * @see #getTableTable()
	 * @generated
	 */
	EAttribute getTableTable_Selectalways();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableTable#getHeaderMode <em>Header Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Header Mode</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTable#getHeaderMode()
	 * @see #getTableTable()
	 * @generated
	 */
	EAttribute getTableTable_HeaderMode();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableTable#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTable#getSource()
	 * @see #getTableTable()
	 * @generated
	 */
	EReference getTableTable_Source();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableGrid <em>Table Grid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Grid</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableGrid
	 * @generated
	 */
	EClass getTableGrid();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableGrid#isSelectalways <em>Selectalways</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selectalways</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableGrid#isSelectalways()
	 * @see #getTableGrid()
	 * @generated
	 */
	EAttribute getTableGrid_Selectalways();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableGrid#getHeaderMode <em>Header Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Header Mode</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableGrid#getHeaderMode()
	 * @see #getTableGrid()
	 * @generated
	 */
	EAttribute getTableGrid_HeaderMode();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableGrid#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableGrid#getSource()
	 * @see #getTableGrid()
	 * @generated
	 */
	EReference getTableGrid_Source();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableDtoDatasource <em>Table Dto Datasource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Dto Datasource</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDtoDatasource
	 * @generated
	 */
	EClass getTableDtoDatasource();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.table.TableDtoDatasource#getDtoSource <em>Dto Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dto Source</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDtoDatasource#getDtoSource()
	 * @see #getTableDtoDatasource()
	 * @generated
	 */
	EReference getTableDtoDatasource_DtoSource();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableGridProperty <em>Table Grid Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Grid Property</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableGridProperty
	 * @generated
	 */
	EClass getTableGridProperty();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TablePreorder <em>Table Preorder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Preorder</em>'.
	 * @see org.eclipse.osbp.xtext.table.TablePreorder
	 * @generated
	 */
	EClass getTablePreorder();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TablePreorder#getColumn <em>Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Column</em>'.
	 * @see org.eclipse.osbp.xtext.table.TablePreorder#getColumn()
	 * @see #getTablePreorder()
	 * @generated
	 */
	EReference getTablePreorder_Column();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TablePreorder#isAscending <em>Ascending</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ascending</em>'.
	 * @see org.eclipse.osbp.xtext.table.TablePreorder#isAscending()
	 * @see #getTablePreorder()
	 * @generated
	 */
	EAttribute getTablePreorder_Ascending();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableValue <em>Table Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Value</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValue
	 * @generated
	 */
	EClass getTableValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableValue#getColumn <em>Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Column</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValue#getColumn()
	 * @see #getTableValue()
	 * @generated
	 */
	EReference getTableValue_Column();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableValue#isCollapsed <em>Collapsed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Collapsed</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValue#isCollapsed()
	 * @see #getTableValue()
	 * @generated
	 */
	EAttribute getTableValue_Collapsed();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableValue#getFormatter <em>Formatter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Formatter</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValue#getFormatter()
	 * @see #getTableValue()
	 * @generated
	 */
	EReference getTableValue_Formatter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableValue#getTooltipPattern <em>Tooltip Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Tooltip Pattern</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValue#getTooltipPattern()
	 * @see #getTableValue()
	 * @generated
	 */
	EReference getTableValue_TooltipPattern();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableValue#isHideLabelInterval <em>Hide Label Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hide Label Interval</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValue#isHideLabelInterval()
	 * @see #getTableValue()
	 * @generated
	 */
	EAttribute getTableValue_HideLabelInterval();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableValue#isHideLabelLookup <em>Hide Label Lookup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hide Label Lookup</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValue#isHideLabelLookup()
	 * @see #getTableValue()
	 * @generated
	 */
	EAttribute getTableValue_HideLabelLookup();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.table.TableValue#getIntervals <em>Intervals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Intervals</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValue#getIntervals()
	 * @see #getTableValue()
	 * @generated
	 */
	EReference getTableValue_Intervals();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.table.TableValue#getLookups <em>Lookups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lookups</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValue#getLookups()
	 * @see #getTableValue()
	 * @generated
	 */
	EReference getTableValue_Lookups();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableValue#isHasImage <em>Has Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Image</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValue#isHasImage()
	 * @see #getTableValue()
	 * @generated
	 */
	EAttribute getTableValue_HasImage();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableValue#getImage <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Image</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValue#getImage()
	 * @see #getTableValue()
	 * @generated
	 */
	EReference getTableValue_Image();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableValue#getIconName <em>Icon Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Icon Name</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValue#getIconName()
	 * @see #getTableValue()
	 * @generated
	 */
	EAttribute getTableValue_IconName();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableFormatter <em>Table Formatter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Formatter</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableFormatter
	 * @generated
	 */
	EClass getTableFormatter();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableFormatter#getFormat <em>Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Format</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableFormatter#getFormat()
	 * @see #getTableFormatter()
	 * @generated
	 */
	EAttribute getTableFormatter_Format();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableImage <em>Table Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Image</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableImage
	 * @generated
	 */
	EClass getTableImage();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableImage#getImagePathPattern <em>Image Path Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image Path Pattern</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableImage#getImagePathPattern()
	 * @see #getTableImage()
	 * @generated
	 */
	EAttribute getTableImage_ImagePathPattern();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableImage#isHideImageLabel <em>Hide Image Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hide Image Label</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableImage#isHideImageLabel()
	 * @see #getTableImage()
	 * @generated
	 */
	EAttribute getTableImage_HideImageLabel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableImage#isHasParameter <em>Has Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Parameter</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableImage#isHasParameter()
	 * @see #getTableImage()
	 * @generated
	 */
	EAttribute getTableImage_HasParameter();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableImage#isResize <em>Resize</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Resize</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableImage#isResize()
	 * @see #getTableImage()
	 * @generated
	 */
	EAttribute getTableImage_Resize();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableImage#getResizeString <em>Resize String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Resize String</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableImage#getResizeString()
	 * @see #getTableImage()
	 * @generated
	 */
	EAttribute getTableImage_ResizeString();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableImage#getImagePathParameter <em>Image Path Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Image Path Parameter</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableImage#getImagePathParameter()
	 * @see #getTableImage()
	 * @generated
	 */
	EReference getTableImage_ImagePathParameter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableTooltipPattern <em>Table Tooltip Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Tooltip Pattern</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTooltipPattern
	 * @generated
	 */
	EClass getTableTooltipPattern();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableTooltipPattern#getTooltipPattern <em>Tooltip Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tooltip Pattern</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTooltipPattern#getTooltipPattern()
	 * @see #getTableTooltipPattern()
	 * @generated
	 */
	EAttribute getTableTooltipPattern_TooltipPattern();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableValueElement <em>Table Value Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Value Element</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableValueElement
	 * @generated
	 */
	EClass getTableValueElement();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableTask <em>Table Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Task</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTask
	 * @generated
	 */
	EClass getTableTask();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableAllColumns <em>Table All Columns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table All Columns</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAllColumns
	 * @generated
	 */
	EClass getTableAllColumns();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableOrdinal <em>Table Ordinal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Ordinal</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableOrdinal
	 * @generated
	 */
	EClass getTableOrdinal();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableOrdinal#getValueRef <em>Value Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Ref</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableOrdinal#getValueRef()
	 * @see #getTableOrdinal()
	 * @generated
	 */
	EAttribute getTableOrdinal_ValueRef();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableColumn <em>Table Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Column</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableColumn
	 * @generated
	 */
	EClass getTableColumn();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.table.TableColumn#getValueRef <em>Value Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Ref</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableColumn#getValueRef()
	 * @see #getTableColumn()
	 * @generated
	 */
	EReference getTableColumn_ValueRef();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableMeasure <em>Table Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Measure</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableMeasure
	 * @generated
	 */
	EClass getTableMeasure();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.table.TableMeasure#getValueRef <em>Value Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Ref</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableMeasure#getValueRef()
	 * @see #getTableMeasure()
	 * @generated
	 */
	EReference getTableMeasure_ValueRef();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableDerived <em>Table Derived</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Derived</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDerived
	 * @generated
	 */
	EClass getTableDerived();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.table.TableDerived#getValueRef <em>Value Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Ref</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDerived#getValueRef()
	 * @see #getTableDerived()
	 * @generated
	 */
	EReference getTableDerived_ValueRef();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableHierarchy <em>Table Hierarchy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Hierarchy</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableHierarchy
	 * @generated
	 */
	EClass getTableHierarchy();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.table.TableHierarchy#getValueRef <em>Value Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Ref</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableHierarchy#getValueRef()
	 * @see #getTableHierarchy()
	 * @generated
	 */
	EReference getTableHierarchy_ValueRef();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableAttribute <em>Table Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Attribute</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAttribute
	 * @generated
	 */
	EClass getTableAttribute();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.table.TableAttribute#getValueRef <em>Value Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Ref</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAttribute#getValueRef()
	 * @see #getTableAttribute()
	 * @generated
	 */
	EReference getTableAttribute_ValueRef();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableAggregation <em>Table Aggregation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Aggregation</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAggregation
	 * @generated
	 */
	EClass getTableAggregation();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.table.TableAggregation#getValueRef <em>Value Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Ref</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAggregation#getValueRef()
	 * @see #getTableAggregation()
	 * @generated
	 */
	EReference getTableAggregation_ValueRef();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableInterval <em>Table Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Interval</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableInterval
	 * @generated
	 */
	EClass getTableInterval();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableNumberInterval <em>Table Number Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Number Interval</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableNumberInterval
	 * @generated
	 */
	EClass getTableNumberInterval();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableNumberInterval#getNumberIntervalValue <em>Number Interval Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Interval Value</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableNumberInterval#getNumberIntervalValue()
	 * @see #getTableNumberInterval()
	 * @generated
	 */
	EAttribute getTableNumberInterval_NumberIntervalValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableNumberInterval#getNumberRange <em>Number Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Number Range</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableNumberInterval#getNumberRange()
	 * @see #getTableNumberInterval()
	 * @generated
	 */
	EReference getTableNumberInterval_NumberRange();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableIntInterval <em>Table Int Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Int Interval</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableIntInterval
	 * @generated
	 */
	EClass getTableIntInterval();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableIntInterval#getIntIntervalValue <em>Int Interval Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Int Interval Value</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableIntInterval#getIntIntervalValue()
	 * @see #getTableIntInterval()
	 * @generated
	 */
	EAttribute getTableIntInterval_IntIntervalValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableIntInterval#getIntRange <em>Int Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Int Range</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableIntInterval#getIntRange()
	 * @see #getTableIntInterval()
	 * @generated
	 */
	EReference getTableIntInterval_IntRange();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableDateDayInterval <em>Table Date Day Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Date Day Interval</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDateDayInterval
	 * @generated
	 */
	EClass getTableDateDayInterval();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableDateDayInterval#getDateIntervalValue <em>Date Interval Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date Interval Value</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDateDayInterval#getDateIntervalValue()
	 * @see #getTableDateDayInterval()
	 * @generated
	 */
	EAttribute getTableDateDayInterval_DateIntervalValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableDateDayInterval#getDateRange <em>Date Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Date Range</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDateDayInterval#getDateRange()
	 * @see #getTableDateDayInterval()
	 * @generated
	 */
	EReference getTableDateDayInterval_DateRange();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableLookup <em>Table Lookup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Lookup</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableLookup
	 * @generated
	 */
	EClass getTableLookup();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableNumberLookup <em>Table Number Lookup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Number Lookup</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableNumberLookup
	 * @generated
	 */
	EClass getTableNumberLookup();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableNumberLookup#getLookupValue <em>Lookup Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lookup Value</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableNumberLookup#getLookupValue()
	 * @see #getTableNumberLookup()
	 * @generated
	 */
	EAttribute getTableNumberLookup_LookupValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableNumberLookup#getDiscrete <em>Discrete</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Discrete</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableNumberLookup#getDiscrete()
	 * @see #getTableNumberLookup()
	 * @generated
	 */
	EReference getTableNumberLookup_Discrete();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableIntLookup <em>Table Int Lookup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Int Lookup</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableIntLookup
	 * @generated
	 */
	EClass getTableIntLookup();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableIntLookup#getLookupValue <em>Lookup Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lookup Value</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableIntLookup#getLookupValue()
	 * @see #getTableIntLookup()
	 * @generated
	 */
	EAttribute getTableIntLookup_LookupValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableIntLookup#getDiscrete <em>Discrete</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Discrete</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableIntLookup#getDiscrete()
	 * @see #getTableIntLookup()
	 * @generated
	 */
	EReference getTableIntLookup_Discrete();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableStringLookup <em>Table String Lookup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table String Lookup</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableStringLookup
	 * @generated
	 */
	EClass getTableStringLookup();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableStringLookup#getLookupValue <em>Lookup Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lookup Value</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableStringLookup#getLookupValue()
	 * @see #getTableStringLookup()
	 * @generated
	 */
	EAttribute getTableStringLookup_LookupValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableStringLookup#getDiscrete <em>Discrete</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Discrete</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableStringLookup#getDiscrete()
	 * @see #getTableStringLookup()
	 * @generated
	 */
	EReference getTableStringLookup_Discrete();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableDateDayLookup <em>Table Date Day Lookup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Date Day Lookup</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDateDayLookup
	 * @generated
	 */
	EClass getTableDateDayLookup();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableDateDayLookup#getLookupValue <em>Lookup Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lookup Value</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDateDayLookup#getLookupValue()
	 * @see #getTableDateDayLookup()
	 * @generated
	 */
	EAttribute getTableDateDayLookup_LookupValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableDateDayLookup#getDiscrete <em>Discrete</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Discrete</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDateDayLookup#getDiscrete()
	 * @see #getTableDateDayLookup()
	 * @generated
	 */
	EReference getTableDateDayLookup_Discrete();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableDatamart <em>Table Datamart</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Datamart</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDatamart
	 * @generated
	 */
	EClass getTableDatamart();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.table.TableDatamart#getDatamartRef <em>Datamart Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datamart Ref</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDatamart#getDatamartRef()
	 * @see #getTableDatamart()
	 * @generated
	 */
	EReference getTableDatamart_DatamartRef();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.table.TableDatamart#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableDatamart#getElements()
	 * @see #getTableDatamart()
	 * @generated
	 */
	EReference getTableDatamart_Elements();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableElement <em>Table Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Element</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableElement
	 * @generated
	 */
	EClass getTableElement();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableAxis <em>Table Axis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Axis</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAxis
	 * @generated
	 */
	EClass getTableAxis();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableAxis#getAxis <em>Axis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Axis</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAxis#getAxis()
	 * @see #getTableAxis()
	 * @generated
	 */
	EAttribute getTableAxis_Axis();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableAxis#isHasRowHeight <em>Has Row Height</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Row Height</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAxis#isHasRowHeight()
	 * @see #getTableAxis()
	 * @generated
	 */
	EAttribute getTableAxis_HasRowHeight();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableAxis#getRowHeight <em>Row Height</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Row Height</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAxis#getRowHeight()
	 * @see #getTableAxis()
	 * @generated
	 */
	EAttribute getTableAxis_RowHeight();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableAxis#getPreOrder <em>Pre Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pre Order</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAxis#getPreOrder()
	 * @see #getTableAxis()
	 * @generated
	 */
	EReference getTableAxis_PreOrder();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableAxis#isHasDetails <em>Has Details</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Details</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAxis#isHasDetails()
	 * @see #getTableAxis()
	 * @generated
	 */
	EAttribute getTableAxis_HasDetails();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.table.TableAxis#getValues <em>Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Values</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAxis#getValues()
	 * @see #getTableAxis()
	 * @generated
	 */
	EReference getTableAxis_Values();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableAxis#isHasEvents <em>Has Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Events</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAxis#isHasEvents()
	 * @see #getTableAxis()
	 * @generated
	 */
	EAttribute getTableAxis_HasEvents();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.table.TableAxis#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableAxis#getEvents()
	 * @see #getTableAxis()
	 * @generated
	 */
	EReference getTableAxis_Events();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableEvent <em>Table Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Event</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableEvent
	 * @generated
	 */
	EClass getTableEvent();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.TableEvent#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableEvent#getSource()
	 * @see #getTableEvent()
	 * @generated
	 */
	EReference getTableEvent_Source();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableRangeElement <em>Table Range Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Range Element</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableRangeElement
	 * @generated
	 */
	EClass getTableRangeElement();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableTextColor <em>Table Text Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Text Color</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTextColor
	 * @generated
	 */
	EClass getTableTextColor();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableTextColor#getRgb <em>Rgb</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rgb</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTextColor#getRgb()
	 * @see #getTableTextColor()
	 * @generated
	 */
	EAttribute getTableTextColor_Rgb();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableCellColor <em>Table Cell Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Cell Color</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableCellColor
	 * @generated
	 */
	EClass getTableCellColor();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableCellColor#getRgb <em>Rgb</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rgb</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableCellColor#getRgb()
	 * @see #getTableCellColor()
	 * @generated
	 */
	EAttribute getTableCellColor_Rgb();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableIcon <em>Table Icon</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Icon</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableIcon
	 * @generated
	 */
	EClass getTableIcon();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableIcon#getIcon <em>Icon</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Icon</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableIcon#getIcon()
	 * @see #getTableIcon()
	 * @generated
	 */
	EAttribute getTableIcon_Icon();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableTrend <em>Table Trend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Trend</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTrend
	 * @generated
	 */
	EClass getTableTrend();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableTrend#getIcon <em>Icon</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Icon</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTrend#getIcon()
	 * @see #getTableTrend()
	 * @generated
	 */
	EAttribute getTableTrend_Icon();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.TableTooltip <em>Table Tooltip</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table Tooltip</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTooltip
	 * @generated
	 */
	EClass getTableTooltip();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.TableTooltip#getTooltip <em>Tooltip</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tooltip</em>'.
	 * @see org.eclipse.osbp.xtext.table.TableTooltip#getTooltip()
	 * @see #getTableTooltip()
	 * @generated
	 */
	EAttribute getTableTooltip_Tooltip();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see org.eclipse.osbp.xtext.table.Expression
	 * @generated
	 */
	EClass getExpression();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.Expression#getNumberValue <em>Number Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Value</em>'.
	 * @see org.eclipse.osbp.xtext.table.Expression#getNumberValue()
	 * @see #getExpression()
	 * @generated
	 */
	EAttribute getExpression_NumberValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.Expression#getStringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Value</em>'.
	 * @see org.eclipse.osbp.xtext.table.Expression#getStringValue()
	 * @see #getExpression()
	 * @generated
	 */
	EAttribute getExpression_StringValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.Calculation <em>Calculation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Calculation</em>'.
	 * @see org.eclipse.osbp.xtext.table.Calculation
	 * @generated
	 */
	EClass getCalculation();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.Calculation#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see org.eclipse.osbp.xtext.table.Calculation#getLeft()
	 * @see #getCalculation()
	 * @generated
	 */
	EReference getCalculation_Left();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.table.Calculation#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see org.eclipse.osbp.xtext.table.Calculation#getRight()
	 * @see #getCalculation()
	 * @generated
	 */
	EReference getCalculation_Right();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.Conjunction <em>Conjunction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conjunction</em>'.
	 * @see org.eclipse.osbp.xtext.table.Conjunction
	 * @generated
	 */
	EClass getConjunction();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.Disjunction <em>Disjunction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Disjunction</em>'.
	 * @see org.eclipse.osbp.xtext.table.Disjunction
	 * @generated
	 */
	EClass getDisjunction();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.table.ConditionalExpression <em>Conditional Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conditional Expression</em>'.
	 * @see org.eclipse.osbp.xtext.table.ConditionalExpression
	 * @generated
	 */
	EClass getConditionalExpression();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.table.ConditionalExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see org.eclipse.osbp.xtext.table.ConditionalExpression#getOperator()
	 * @see #getConditionalExpression()
	 * @generated
	 */
	EAttribute getConditionalExpression_Operator();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.table.TrendIconEnum <em>Trend Icon Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Trend Icon Enum</em>'.
	 * @see org.eclipse.osbp.xtext.table.TrendIconEnum
	 * @generated
	 */
	EEnum getTrendIconEnum();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.table.OperatorEnum <em>Operator Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Operator Enum</em>'.
	 * @see org.eclipse.osbp.xtext.table.OperatorEnum
	 * @generated
	 */
	EEnum getOperatorEnum();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.table.RowHeaderMode <em>Row Header Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Row Header Mode</em>'.
	 * @see org.eclipse.osbp.xtext.table.RowHeaderMode
	 * @generated
	 */
	EEnum getRowHeaderMode();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.InternalEObject <em>Internal EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Internal EObject</em>'.
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @model instanceClass="org.eclipse.emf.ecore.InternalEObject"
	 * @generated
	 */
	EDataType getInternalEObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TableDSLFactory getTableDSLFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableModelImpl <em>Table Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableModelImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableModel()
		 * @generated
		 */
		EClass TABLE_MODEL = eINSTANCE.getTableModel();

		/**
		 * The meta object literal for the '<em><b>Import Section</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_MODEL__IMPORT_SECTION = eINSTANCE.getTableModel_ImportSection();

		/**
		 * The meta object literal for the '<em><b>Packages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_MODEL__PACKAGES = eINSTANCE.getTableModel_Packages();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableLazyResolverImpl <em>Table Lazy Resolver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableLazyResolverImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableLazyResolver()
		 * @generated
		 */
		EClass TABLE_LAZY_RESOLVER = eINSTANCE.getTableLazyResolver();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TablePackageImpl <em>Table Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TablePackageImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTablePackage()
		 * @generated
		 */
		EClass TABLE_PACKAGE = eINSTANCE.getTablePackage();

		/**
		 * The meta object literal for the '<em><b>Tables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_PACKAGE__TABLES = eINSTANCE.getTablePackage_Tables();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableBaseImpl <em>Table Base</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableBaseImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableBase()
		 * @generated
		 */
		EClass TABLE_BASE = eINSTANCE.getTableBase();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_BASE__NAME = eINSTANCE.getTableBase_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableImpl <em>Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTable()
		 * @generated
		 */
		EClass TABLE = eINSTANCE.getTable();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE__DESCRIPTION = eINSTANCE.getTable_Description();

		/**
		 * The meta object literal for the '<em><b>Description Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE__DESCRIPTION_VALUE = eINSTANCE.getTable_DescriptionValue();

		/**
		 * The meta object literal for the '<em><b>Tabletype</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE__TABLETYPE = eINSTANCE.getTable_Tabletype();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.TableOption <em>Table Option</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.TableOption
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableOption()
		 * @generated
		 */
		EClass TABLE_OPTION = eINSTANCE.getTableOption();

		/**
		 * The meta object literal for the '<em><b>Filtering</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_OPTION__FILTERING = eINSTANCE.getTableOption_Filtering();

		/**
		 * The meta object literal for the '<em><b>Toolbar</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_OPTION__TOOLBAR = eINSTANCE.getTableOption_Toolbar();

		/**
		 * The meta object literal for the '<em><b>Embedded</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_OPTION__EMBEDDED = eINSTANCE.getTableOption_Embedded();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableSelectionImpl <em>Table Selection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableSelectionImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableSelection()
		 * @generated
		 */
		EClass TABLE_SELECTION = eINSTANCE.getTableSelection();

		/**
		 * The meta object literal for the '<em><b>Multi Selection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_SELECTION__MULTI_SELECTION = eINSTANCE.getTableSelection_MultiSelection();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableTableImpl <em>Table Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableTableImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableTable()
		 * @generated
		 */
		EClass TABLE_TABLE = eINSTANCE.getTableTable();

		/**
		 * The meta object literal for the '<em><b>Select Id Only</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_TABLE__SELECT_ID_ONLY = eINSTANCE.getTableTable_SelectIdOnly();

		/**
		 * The meta object literal for the '<em><b>Select By Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_TABLE__SELECT_BY_ID = eINSTANCE.getTableTable_SelectById();

		/**
		 * The meta object literal for the '<em><b>Selectalways</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_TABLE__SELECTALWAYS = eINSTANCE.getTableTable_Selectalways();

		/**
		 * The meta object literal for the '<em><b>Header Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_TABLE__HEADER_MODE = eINSTANCE.getTableTable_HeaderMode();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_TABLE__SOURCE = eINSTANCE.getTableTable_Source();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableGridImpl <em>Table Grid</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableGridImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableGrid()
		 * @generated
		 */
		EClass TABLE_GRID = eINSTANCE.getTableGrid();

		/**
		 * The meta object literal for the '<em><b>Selectalways</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_GRID__SELECTALWAYS = eINSTANCE.getTableGrid_Selectalways();

		/**
		 * The meta object literal for the '<em><b>Header Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_GRID__HEADER_MODE = eINSTANCE.getTableGrid_HeaderMode();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_GRID__SOURCE = eINSTANCE.getTableGrid_Source();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableDtoDatasourceImpl <em>Table Dto Datasource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableDtoDatasourceImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableDtoDatasource()
		 * @generated
		 */
		EClass TABLE_DTO_DATASOURCE = eINSTANCE.getTableDtoDatasource();

		/**
		 * The meta object literal for the '<em><b>Dto Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_DTO_DATASOURCE__DTO_SOURCE = eINSTANCE.getTableDtoDatasource_DtoSource();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableGridPropertyImpl <em>Table Grid Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableGridPropertyImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableGridProperty()
		 * @generated
		 */
		EClass TABLE_GRID_PROPERTY = eINSTANCE.getTableGridProperty();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TablePreorderImpl <em>Table Preorder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TablePreorderImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTablePreorder()
		 * @generated
		 */
		EClass TABLE_PREORDER = eINSTANCE.getTablePreorder();

		/**
		 * The meta object literal for the '<em><b>Column</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_PREORDER__COLUMN = eINSTANCE.getTablePreorder_Column();

		/**
		 * The meta object literal for the '<em><b>Ascending</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_PREORDER__ASCENDING = eINSTANCE.getTablePreorder_Ascending();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl <em>Table Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableValueImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableValue()
		 * @generated
		 */
		EClass TABLE_VALUE = eINSTANCE.getTableValue();

		/**
		 * The meta object literal for the '<em><b>Column</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_VALUE__COLUMN = eINSTANCE.getTableValue_Column();

		/**
		 * The meta object literal for the '<em><b>Collapsed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_VALUE__COLLAPSED = eINSTANCE.getTableValue_Collapsed();

		/**
		 * The meta object literal for the '<em><b>Formatter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_VALUE__FORMATTER = eINSTANCE.getTableValue_Formatter();

		/**
		 * The meta object literal for the '<em><b>Tooltip Pattern</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_VALUE__TOOLTIP_PATTERN = eINSTANCE.getTableValue_TooltipPattern();

		/**
		 * The meta object literal for the '<em><b>Hide Label Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_VALUE__HIDE_LABEL_INTERVAL = eINSTANCE.getTableValue_HideLabelInterval();

		/**
		 * The meta object literal for the '<em><b>Hide Label Lookup</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_VALUE__HIDE_LABEL_LOOKUP = eINSTANCE.getTableValue_HideLabelLookup();

		/**
		 * The meta object literal for the '<em><b>Intervals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_VALUE__INTERVALS = eINSTANCE.getTableValue_Intervals();

		/**
		 * The meta object literal for the '<em><b>Lookups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_VALUE__LOOKUPS = eINSTANCE.getTableValue_Lookups();

		/**
		 * The meta object literal for the '<em><b>Has Image</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_VALUE__HAS_IMAGE = eINSTANCE.getTableValue_HasImage();

		/**
		 * The meta object literal for the '<em><b>Image</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_VALUE__IMAGE = eINSTANCE.getTableValue_Image();

		/**
		 * The meta object literal for the '<em><b>Icon Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_VALUE__ICON_NAME = eINSTANCE.getTableValue_IconName();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableFormatterImpl <em>Table Formatter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableFormatterImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableFormatter()
		 * @generated
		 */
		EClass TABLE_FORMATTER = eINSTANCE.getTableFormatter();

		/**
		 * The meta object literal for the '<em><b>Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_FORMATTER__FORMAT = eINSTANCE.getTableFormatter_Format();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableImageImpl <em>Table Image</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableImageImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableImage()
		 * @generated
		 */
		EClass TABLE_IMAGE = eINSTANCE.getTableImage();

		/**
		 * The meta object literal for the '<em><b>Image Path Pattern</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_IMAGE__IMAGE_PATH_PATTERN = eINSTANCE.getTableImage_ImagePathPattern();

		/**
		 * The meta object literal for the '<em><b>Hide Image Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_IMAGE__HIDE_IMAGE_LABEL = eINSTANCE.getTableImage_HideImageLabel();

		/**
		 * The meta object literal for the '<em><b>Has Parameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_IMAGE__HAS_PARAMETER = eINSTANCE.getTableImage_HasParameter();

		/**
		 * The meta object literal for the '<em><b>Resize</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_IMAGE__RESIZE = eINSTANCE.getTableImage_Resize();

		/**
		 * The meta object literal for the '<em><b>Resize String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_IMAGE__RESIZE_STRING = eINSTANCE.getTableImage_ResizeString();

		/**
		 * The meta object literal for the '<em><b>Image Path Parameter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_IMAGE__IMAGE_PATH_PARAMETER = eINSTANCE.getTableImage_ImagePathParameter();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableTooltipPatternImpl <em>Table Tooltip Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableTooltipPatternImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableTooltipPattern()
		 * @generated
		 */
		EClass TABLE_TOOLTIP_PATTERN = eINSTANCE.getTableTooltipPattern();

		/**
		 * The meta object literal for the '<em><b>Tooltip Pattern</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_TOOLTIP_PATTERN__TOOLTIP_PATTERN = eINSTANCE.getTableTooltipPattern_TooltipPattern();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.TableValueElement <em>Table Value Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.TableValueElement
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableValueElement()
		 * @generated
		 */
		EClass TABLE_VALUE_ELEMENT = eINSTANCE.getTableValueElement();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableTaskImpl <em>Table Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableTaskImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableTask()
		 * @generated
		 */
		EClass TABLE_TASK = eINSTANCE.getTableTask();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableAllColumnsImpl <em>Table All Columns</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableAllColumnsImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableAllColumns()
		 * @generated
		 */
		EClass TABLE_ALL_COLUMNS = eINSTANCE.getTableAllColumns();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableOrdinalImpl <em>Table Ordinal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableOrdinalImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableOrdinal()
		 * @generated
		 */
		EClass TABLE_ORDINAL = eINSTANCE.getTableOrdinal();

		/**
		 * The meta object literal for the '<em><b>Value Ref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_ORDINAL__VALUE_REF = eINSTANCE.getTableOrdinal_ValueRef();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableColumnImpl <em>Table Column</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableColumnImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableColumn()
		 * @generated
		 */
		EClass TABLE_COLUMN = eINSTANCE.getTableColumn();

		/**
		 * The meta object literal for the '<em><b>Value Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_COLUMN__VALUE_REF = eINSTANCE.getTableColumn_ValueRef();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableMeasureImpl <em>Table Measure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableMeasureImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableMeasure()
		 * @generated
		 */
		EClass TABLE_MEASURE = eINSTANCE.getTableMeasure();

		/**
		 * The meta object literal for the '<em><b>Value Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_MEASURE__VALUE_REF = eINSTANCE.getTableMeasure_ValueRef();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableDerivedImpl <em>Table Derived</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableDerivedImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableDerived()
		 * @generated
		 */
		EClass TABLE_DERIVED = eINSTANCE.getTableDerived();

		/**
		 * The meta object literal for the '<em><b>Value Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_DERIVED__VALUE_REF = eINSTANCE.getTableDerived_ValueRef();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableHierarchyImpl <em>Table Hierarchy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableHierarchyImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableHierarchy()
		 * @generated
		 */
		EClass TABLE_HIERARCHY = eINSTANCE.getTableHierarchy();

		/**
		 * The meta object literal for the '<em><b>Value Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_HIERARCHY__VALUE_REF = eINSTANCE.getTableHierarchy_ValueRef();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableAttributeImpl <em>Table Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableAttributeImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableAttribute()
		 * @generated
		 */
		EClass TABLE_ATTRIBUTE = eINSTANCE.getTableAttribute();

		/**
		 * The meta object literal for the '<em><b>Value Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_ATTRIBUTE__VALUE_REF = eINSTANCE.getTableAttribute_ValueRef();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableAggregationImpl <em>Table Aggregation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableAggregationImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableAggregation()
		 * @generated
		 */
		EClass TABLE_AGGREGATION = eINSTANCE.getTableAggregation();

		/**
		 * The meta object literal for the '<em><b>Value Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_AGGREGATION__VALUE_REF = eINSTANCE.getTableAggregation_ValueRef();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.TableInterval <em>Table Interval</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.TableInterval
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableInterval()
		 * @generated
		 */
		EClass TABLE_INTERVAL = eINSTANCE.getTableInterval();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableNumberIntervalImpl <em>Table Number Interval</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableNumberIntervalImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableNumberInterval()
		 * @generated
		 */
		EClass TABLE_NUMBER_INTERVAL = eINSTANCE.getTableNumberInterval();

		/**
		 * The meta object literal for the '<em><b>Number Interval Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_NUMBER_INTERVAL__NUMBER_INTERVAL_VALUE = eINSTANCE.getTableNumberInterval_NumberIntervalValue();

		/**
		 * The meta object literal for the '<em><b>Number Range</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_NUMBER_INTERVAL__NUMBER_RANGE = eINSTANCE.getTableNumberInterval_NumberRange();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableIntIntervalImpl <em>Table Int Interval</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableIntIntervalImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableIntInterval()
		 * @generated
		 */
		EClass TABLE_INT_INTERVAL = eINSTANCE.getTableIntInterval();

		/**
		 * The meta object literal for the '<em><b>Int Interval Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_INT_INTERVAL__INT_INTERVAL_VALUE = eINSTANCE.getTableIntInterval_IntIntervalValue();

		/**
		 * The meta object literal for the '<em><b>Int Range</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_INT_INTERVAL__INT_RANGE = eINSTANCE.getTableIntInterval_IntRange();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableDateDayIntervalImpl <em>Table Date Day Interval</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableDateDayIntervalImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableDateDayInterval()
		 * @generated
		 */
		EClass TABLE_DATE_DAY_INTERVAL = eINSTANCE.getTableDateDayInterval();

		/**
		 * The meta object literal for the '<em><b>Date Interval Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_DATE_DAY_INTERVAL__DATE_INTERVAL_VALUE = eINSTANCE.getTableDateDayInterval_DateIntervalValue();

		/**
		 * The meta object literal for the '<em><b>Date Range</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_DATE_DAY_INTERVAL__DATE_RANGE = eINSTANCE.getTableDateDayInterval_DateRange();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.TableLookup <em>Table Lookup</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.TableLookup
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableLookup()
		 * @generated
		 */
		EClass TABLE_LOOKUP = eINSTANCE.getTableLookup();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableNumberLookupImpl <em>Table Number Lookup</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableNumberLookupImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableNumberLookup()
		 * @generated
		 */
		EClass TABLE_NUMBER_LOOKUP = eINSTANCE.getTableNumberLookup();

		/**
		 * The meta object literal for the '<em><b>Lookup Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_NUMBER_LOOKUP__LOOKUP_VALUE = eINSTANCE.getTableNumberLookup_LookupValue();

		/**
		 * The meta object literal for the '<em><b>Discrete</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_NUMBER_LOOKUP__DISCRETE = eINSTANCE.getTableNumberLookup_Discrete();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableIntLookupImpl <em>Table Int Lookup</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableIntLookupImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableIntLookup()
		 * @generated
		 */
		EClass TABLE_INT_LOOKUP = eINSTANCE.getTableIntLookup();

		/**
		 * The meta object literal for the '<em><b>Lookup Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_INT_LOOKUP__LOOKUP_VALUE = eINSTANCE.getTableIntLookup_LookupValue();

		/**
		 * The meta object literal for the '<em><b>Discrete</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_INT_LOOKUP__DISCRETE = eINSTANCE.getTableIntLookup_Discrete();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableStringLookupImpl <em>Table String Lookup</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableStringLookupImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableStringLookup()
		 * @generated
		 */
		EClass TABLE_STRING_LOOKUP = eINSTANCE.getTableStringLookup();

		/**
		 * The meta object literal for the '<em><b>Lookup Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_STRING_LOOKUP__LOOKUP_VALUE = eINSTANCE.getTableStringLookup_LookupValue();

		/**
		 * The meta object literal for the '<em><b>Discrete</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_STRING_LOOKUP__DISCRETE = eINSTANCE.getTableStringLookup_Discrete();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableDateDayLookupImpl <em>Table Date Day Lookup</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableDateDayLookupImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableDateDayLookup()
		 * @generated
		 */
		EClass TABLE_DATE_DAY_LOOKUP = eINSTANCE.getTableDateDayLookup();

		/**
		 * The meta object literal for the '<em><b>Lookup Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_DATE_DAY_LOOKUP__LOOKUP_VALUE = eINSTANCE.getTableDateDayLookup_LookupValue();

		/**
		 * The meta object literal for the '<em><b>Discrete</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_DATE_DAY_LOOKUP__DISCRETE = eINSTANCE.getTableDateDayLookup_Discrete();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableDatamartImpl <em>Table Datamart</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableDatamartImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableDatamart()
		 * @generated
		 */
		EClass TABLE_DATAMART = eINSTANCE.getTableDatamart();

		/**
		 * The meta object literal for the '<em><b>Datamart Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_DATAMART__DATAMART_REF = eINSTANCE.getTableDatamart_DatamartRef();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_DATAMART__ELEMENTS = eINSTANCE.getTableDatamart_Elements();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.TableElement <em>Table Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.TableElement
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableElement()
		 * @generated
		 */
		EClass TABLE_ELEMENT = eINSTANCE.getTableElement();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableAxisImpl <em>Table Axis</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableAxisImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableAxis()
		 * @generated
		 */
		EClass TABLE_AXIS = eINSTANCE.getTableAxis();

		/**
		 * The meta object literal for the '<em><b>Axis</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_AXIS__AXIS = eINSTANCE.getTableAxis_Axis();

		/**
		 * The meta object literal for the '<em><b>Has Row Height</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_AXIS__HAS_ROW_HEIGHT = eINSTANCE.getTableAxis_HasRowHeight();

		/**
		 * The meta object literal for the '<em><b>Row Height</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_AXIS__ROW_HEIGHT = eINSTANCE.getTableAxis_RowHeight();

		/**
		 * The meta object literal for the '<em><b>Pre Order</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_AXIS__PRE_ORDER = eINSTANCE.getTableAxis_PreOrder();

		/**
		 * The meta object literal for the '<em><b>Has Details</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_AXIS__HAS_DETAILS = eINSTANCE.getTableAxis_HasDetails();

		/**
		 * The meta object literal for the '<em><b>Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_AXIS__VALUES = eINSTANCE.getTableAxis_Values();

		/**
		 * The meta object literal for the '<em><b>Has Events</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_AXIS__HAS_EVENTS = eINSTANCE.getTableAxis_HasEvents();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_AXIS__EVENTS = eINSTANCE.getTableAxis_Events();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableEventImpl <em>Table Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableEventImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableEvent()
		 * @generated
		 */
		EClass TABLE_EVENT = eINSTANCE.getTableEvent();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE_EVENT__SOURCE = eINSTANCE.getTableEvent_Source();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.TableRangeElement <em>Table Range Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.TableRangeElement
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableRangeElement()
		 * @generated
		 */
		EClass TABLE_RANGE_ELEMENT = eINSTANCE.getTableRangeElement();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableTextColorImpl <em>Table Text Color</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableTextColorImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableTextColor()
		 * @generated
		 */
		EClass TABLE_TEXT_COLOR = eINSTANCE.getTableTextColor();

		/**
		 * The meta object literal for the '<em><b>Rgb</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_TEXT_COLOR__RGB = eINSTANCE.getTableTextColor_Rgb();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableCellColorImpl <em>Table Cell Color</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableCellColorImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableCellColor()
		 * @generated
		 */
		EClass TABLE_CELL_COLOR = eINSTANCE.getTableCellColor();

		/**
		 * The meta object literal for the '<em><b>Rgb</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_CELL_COLOR__RGB = eINSTANCE.getTableCellColor_Rgb();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableIconImpl <em>Table Icon</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableIconImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableIcon()
		 * @generated
		 */
		EClass TABLE_ICON = eINSTANCE.getTableIcon();

		/**
		 * The meta object literal for the '<em><b>Icon</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_ICON__ICON = eINSTANCE.getTableIcon_Icon();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableTrendImpl <em>Table Trend</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableTrendImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableTrend()
		 * @generated
		 */
		EClass TABLE_TREND = eINSTANCE.getTableTrend();

		/**
		 * The meta object literal for the '<em><b>Icon</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_TREND__ICON = eINSTANCE.getTableTrend_Icon();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.TableTooltipImpl <em>Table Tooltip</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.TableTooltipImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTableTooltip()
		 * @generated
		 */
		EClass TABLE_TOOLTIP = eINSTANCE.getTableTooltip();

		/**
		 * The meta object literal for the '<em><b>Tooltip</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TABLE_TOOLTIP__TOOLTIP = eINSTANCE.getTableTooltip_Tooltip();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.ExpressionImpl <em>Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.ExpressionImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getExpression()
		 * @generated
		 */
		EClass EXPRESSION = eINSTANCE.getExpression();

		/**
		 * The meta object literal for the '<em><b>Number Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPRESSION__NUMBER_VALUE = eINSTANCE.getExpression_NumberValue();

		/**
		 * The meta object literal for the '<em><b>String Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPRESSION__STRING_VALUE = eINSTANCE.getExpression_StringValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.CalculationImpl <em>Calculation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.CalculationImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getCalculation()
		 * @generated
		 */
		EClass CALCULATION = eINSTANCE.getCalculation();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALCULATION__LEFT = eINSTANCE.getCalculation_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALCULATION__RIGHT = eINSTANCE.getCalculation_Right();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.ConjunctionImpl <em>Conjunction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.ConjunctionImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getConjunction()
		 * @generated
		 */
		EClass CONJUNCTION = eINSTANCE.getConjunction();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.DisjunctionImpl <em>Disjunction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.DisjunctionImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getDisjunction()
		 * @generated
		 */
		EClass DISJUNCTION = eINSTANCE.getDisjunction();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.impl.ConditionalExpressionImpl <em>Conditional Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.impl.ConditionalExpressionImpl
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getConditionalExpression()
		 * @generated
		 */
		EClass CONDITIONAL_EXPRESSION = eINSTANCE.getConditionalExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITIONAL_EXPRESSION__OPERATOR = eINSTANCE.getConditionalExpression_Operator();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.TrendIconEnum <em>Trend Icon Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.TrendIconEnum
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getTrendIconEnum()
		 * @generated
		 */
		EEnum TREND_ICON_ENUM = eINSTANCE.getTrendIconEnum();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.OperatorEnum <em>Operator Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.OperatorEnum
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getOperatorEnum()
		 * @generated
		 */
		EEnum OPERATOR_ENUM = eINSTANCE.getOperatorEnum();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.table.RowHeaderMode <em>Row Header Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.table.RowHeaderMode
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getRowHeaderMode()
		 * @generated
		 */
		EEnum ROW_HEADER_MODE = eINSTANCE.getRowHeaderMode();

		/**
		 * The meta object literal for the '<em>Internal EObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecore.InternalEObject
		 * @see org.eclipse.osbp.xtext.table.impl.TableDSLPackageImpl#getInternalEObject()
		 * @generated
		 */
		EDataType INTERNAL_EOBJECT = eINSTANCE.getInternalEObject();

	}

} //TableDSLPackage
