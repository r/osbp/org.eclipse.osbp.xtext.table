/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;

import org.eclipse.osbp.infogrid.model.gridsource.CxGridProperty;
import org.eclipse.osbp.infogrid.model.gridsource.CxGridSource;

import org.eclipse.osbp.xtext.table.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage
 * @generated
 */
public class TableDSLSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TableDSLPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableDSLSwitch() {
		if (modelPackage == null) {
			modelPackage = TableDSLPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TableDSLPackage.TABLE_MODEL: {
				TableModel tableModel = (TableModel)theEObject;
				T result = caseTableModel(tableModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_LAZY_RESOLVER: {
				TableLazyResolver tableLazyResolver = (TableLazyResolver)theEObject;
				T result = caseTableLazyResolver(tableLazyResolver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_PACKAGE: {
				TablePackage tablePackage = (TablePackage)theEObject;
				T result = caseTablePackage(tablePackage);
				if (result == null) result = caseLPackage(tablePackage);
				if (result == null) result = caseLLazyResolver(tablePackage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_BASE: {
				TableBase tableBase = (TableBase)theEObject;
				T result = caseTableBase(tableBase);
				if (result == null) result = caseTableLazyResolver(tableBase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE: {
				Table table = (Table)theEObject;
				T result = caseTable(table);
				if (result == null) result = caseTableBase(table);
				if (result == null) result = caseTableLazyResolver(table);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_OPTION: {
				TableOption tableOption = (TableOption)theEObject;
				T result = caseTableOption(tableOption);
				if (result == null) result = caseTableLazyResolver(tableOption);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_SELECTION: {
				TableSelection tableSelection = (TableSelection)theEObject;
				T result = caseTableSelection(tableSelection);
				if (result == null) result = caseTableTable(tableSelection);
				if (result == null) result = caseTableOption(tableSelection);
				if (result == null) result = caseTableLazyResolver(tableSelection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_TABLE: {
				TableTable tableTable = (TableTable)theEObject;
				T result = caseTableTable(tableTable);
				if (result == null) result = caseTableOption(tableTable);
				if (result == null) result = caseTableLazyResolver(tableTable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_GRID: {
				TableGrid tableGrid = (TableGrid)theEObject;
				T result = caseTableGrid(tableGrid);
				if (result == null) result = caseTableOption(tableGrid);
				if (result == null) result = caseTableLazyResolver(tableGrid);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_DTO_DATASOURCE: {
				TableDtoDatasource tableDtoDatasource = (TableDtoDatasource)theEObject;
				T result = caseTableDtoDatasource(tableDtoDatasource);
				if (result == null) result = caseCxGridSource(tableDtoDatasource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_GRID_PROPERTY: {
				TableGridProperty tableGridProperty = (TableGridProperty)theEObject;
				T result = caseTableGridProperty(tableGridProperty);
				if (result == null) result = caseCxGridProperty(tableGridProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_PREORDER: {
				TablePreorder tablePreorder = (TablePreorder)theEObject;
				T result = caseTablePreorder(tablePreorder);
				if (result == null) result = caseTableLazyResolver(tablePreorder);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_VALUE: {
				TableValue tableValue = (TableValue)theEObject;
				T result = caseTableValue(tableValue);
				if (result == null) result = caseTableLazyResolver(tableValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_FORMATTER: {
				TableFormatter tableFormatter = (TableFormatter)theEObject;
				T result = caseTableFormatter(tableFormatter);
				if (result == null) result = caseTableLazyResolver(tableFormatter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_IMAGE: {
				TableImage tableImage = (TableImage)theEObject;
				T result = caseTableImage(tableImage);
				if (result == null) result = caseTableLazyResolver(tableImage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_TOOLTIP_PATTERN: {
				TableTooltipPattern tableTooltipPattern = (TableTooltipPattern)theEObject;
				T result = caseTableTooltipPattern(tableTooltipPattern);
				if (result == null) result = caseTableLazyResolver(tableTooltipPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_VALUE_ELEMENT: {
				TableValueElement tableValueElement = (TableValueElement)theEObject;
				T result = caseTableValueElement(tableValueElement);
				if (result == null) result = caseTableLazyResolver(tableValueElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_TASK: {
				TableTask tableTask = (TableTask)theEObject;
				T result = caseTableTask(tableTask);
				if (result == null) result = caseExpression(tableTask);
				if (result == null) result = caseTableValueElement(tableTask);
				if (result == null) result = caseTableLazyResolver(tableTask);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_ALL_COLUMNS: {
				TableAllColumns tableAllColumns = (TableAllColumns)theEObject;
				T result = caseTableAllColumns(tableAllColumns);
				if (result == null) result = caseExpression(tableAllColumns);
				if (result == null) result = caseTableValueElement(tableAllColumns);
				if (result == null) result = caseTableLazyResolver(tableAllColumns);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_ORDINAL: {
				TableOrdinal tableOrdinal = (TableOrdinal)theEObject;
				T result = caseTableOrdinal(tableOrdinal);
				if (result == null) result = caseExpression(tableOrdinal);
				if (result == null) result = caseTableValueElement(tableOrdinal);
				if (result == null) result = caseTableLazyResolver(tableOrdinal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_COLUMN: {
				TableColumn tableColumn = (TableColumn)theEObject;
				T result = caseTableColumn(tableColumn);
				if (result == null) result = caseExpression(tableColumn);
				if (result == null) result = caseTableValueElement(tableColumn);
				if (result == null) result = caseTableLazyResolver(tableColumn);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_MEASURE: {
				TableMeasure tableMeasure = (TableMeasure)theEObject;
				T result = caseTableMeasure(tableMeasure);
				if (result == null) result = caseExpression(tableMeasure);
				if (result == null) result = caseTableValueElement(tableMeasure);
				if (result == null) result = caseTableLazyResolver(tableMeasure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_DERIVED: {
				TableDerived tableDerived = (TableDerived)theEObject;
				T result = caseTableDerived(tableDerived);
				if (result == null) result = caseExpression(tableDerived);
				if (result == null) result = caseTableValueElement(tableDerived);
				if (result == null) result = caseTableLazyResolver(tableDerived);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_HIERARCHY: {
				TableHierarchy tableHierarchy = (TableHierarchy)theEObject;
				T result = caseTableHierarchy(tableHierarchy);
				if (result == null) result = caseExpression(tableHierarchy);
				if (result == null) result = caseTableValueElement(tableHierarchy);
				if (result == null) result = caseTableLazyResolver(tableHierarchy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_ATTRIBUTE: {
				TableAttribute tableAttribute = (TableAttribute)theEObject;
				T result = caseTableAttribute(tableAttribute);
				if (result == null) result = caseExpression(tableAttribute);
				if (result == null) result = caseTableValueElement(tableAttribute);
				if (result == null) result = caseTableLazyResolver(tableAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_AGGREGATION: {
				TableAggregation tableAggregation = (TableAggregation)theEObject;
				T result = caseTableAggregation(tableAggregation);
				if (result == null) result = caseExpression(tableAggregation);
				if (result == null) result = caseTableValueElement(tableAggregation);
				if (result == null) result = caseTableLazyResolver(tableAggregation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_INTERVAL: {
				TableInterval tableInterval = (TableInterval)theEObject;
				T result = caseTableInterval(tableInterval);
				if (result == null) result = caseTableLazyResolver(tableInterval);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_NUMBER_INTERVAL: {
				TableNumberInterval tableNumberInterval = (TableNumberInterval)theEObject;
				T result = caseTableNumberInterval(tableNumberInterval);
				if (result == null) result = caseTableInterval(tableNumberInterval);
				if (result == null) result = caseTableLazyResolver(tableNumberInterval);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_INT_INTERVAL: {
				TableIntInterval tableIntInterval = (TableIntInterval)theEObject;
				T result = caseTableIntInterval(tableIntInterval);
				if (result == null) result = caseTableInterval(tableIntInterval);
				if (result == null) result = caseTableLazyResolver(tableIntInterval);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_DATE_DAY_INTERVAL: {
				TableDateDayInterval tableDateDayInterval = (TableDateDayInterval)theEObject;
				T result = caseTableDateDayInterval(tableDateDayInterval);
				if (result == null) result = caseTableInterval(tableDateDayInterval);
				if (result == null) result = caseTableLazyResolver(tableDateDayInterval);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_LOOKUP: {
				TableLookup tableLookup = (TableLookup)theEObject;
				T result = caseTableLookup(tableLookup);
				if (result == null) result = caseTableLazyResolver(tableLookup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_NUMBER_LOOKUP: {
				TableNumberLookup tableNumberLookup = (TableNumberLookup)theEObject;
				T result = caseTableNumberLookup(tableNumberLookup);
				if (result == null) result = caseTableLookup(tableNumberLookup);
				if (result == null) result = caseTableLazyResolver(tableNumberLookup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_INT_LOOKUP: {
				TableIntLookup tableIntLookup = (TableIntLookup)theEObject;
				T result = caseTableIntLookup(tableIntLookup);
				if (result == null) result = caseTableLookup(tableIntLookup);
				if (result == null) result = caseTableLazyResolver(tableIntLookup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_STRING_LOOKUP: {
				TableStringLookup tableStringLookup = (TableStringLookup)theEObject;
				T result = caseTableStringLookup(tableStringLookup);
				if (result == null) result = caseTableLookup(tableStringLookup);
				if (result == null) result = caseTableLazyResolver(tableStringLookup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_DATE_DAY_LOOKUP: {
				TableDateDayLookup tableDateDayLookup = (TableDateDayLookup)theEObject;
				T result = caseTableDateDayLookup(tableDateDayLookup);
				if (result == null) result = caseTableLookup(tableDateDayLookup);
				if (result == null) result = caseTableLazyResolver(tableDateDayLookup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_DATAMART: {
				TableDatamart tableDatamart = (TableDatamart)theEObject;
				T result = caseTableDatamart(tableDatamart);
				if (result == null) result = caseTableLazyResolver(tableDatamart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_ELEMENT: {
				TableElement tableElement = (TableElement)theEObject;
				T result = caseTableElement(tableElement);
				if (result == null) result = caseTableLazyResolver(tableElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_AXIS: {
				TableAxis tableAxis = (TableAxis)theEObject;
				T result = caseTableAxis(tableAxis);
				if (result == null) result = caseTableElement(tableAxis);
				if (result == null) result = caseTableLazyResolver(tableAxis);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_EVENT: {
				TableEvent tableEvent = (TableEvent)theEObject;
				T result = caseTableEvent(tableEvent);
				if (result == null) result = caseTableLazyResolver(tableEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_RANGE_ELEMENT: {
				TableRangeElement tableRangeElement = (TableRangeElement)theEObject;
				T result = caseTableRangeElement(tableRangeElement);
				if (result == null) result = caseTableLazyResolver(tableRangeElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_TEXT_COLOR: {
				TableTextColor tableTextColor = (TableTextColor)theEObject;
				T result = caseTableTextColor(tableTextColor);
				if (result == null) result = caseTableRangeElement(tableTextColor);
				if (result == null) result = caseTableLazyResolver(tableTextColor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_CELL_COLOR: {
				TableCellColor tableCellColor = (TableCellColor)theEObject;
				T result = caseTableCellColor(tableCellColor);
				if (result == null) result = caseTableRangeElement(tableCellColor);
				if (result == null) result = caseTableLazyResolver(tableCellColor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_ICON: {
				TableIcon tableIcon = (TableIcon)theEObject;
				T result = caseTableIcon(tableIcon);
				if (result == null) result = caseTableRangeElement(tableIcon);
				if (result == null) result = caseTableLazyResolver(tableIcon);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_TREND: {
				TableTrend tableTrend = (TableTrend)theEObject;
				T result = caseTableTrend(tableTrend);
				if (result == null) result = caseTableRangeElement(tableTrend);
				if (result == null) result = caseTableLazyResolver(tableTrend);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.TABLE_TOOLTIP: {
				TableTooltip tableTooltip = (TableTooltip)theEObject;
				T result = caseTableTooltip(tableTooltip);
				if (result == null) result = caseTableRangeElement(tableTooltip);
				if (result == null) result = caseTableLazyResolver(tableTooltip);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.EXPRESSION: {
				Expression expression = (Expression)theEObject;
				T result = caseExpression(expression);
				if (result == null) result = caseTableValueElement(expression);
				if (result == null) result = caseTableLazyResolver(expression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.CALCULATION: {
				Calculation calculation = (Calculation)theEObject;
				T result = caseCalculation(calculation);
				if (result == null) result = caseExpression(calculation);
				if (result == null) result = caseTableValueElement(calculation);
				if (result == null) result = caseTableLazyResolver(calculation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.CONJUNCTION: {
				Conjunction conjunction = (Conjunction)theEObject;
				T result = caseConjunction(conjunction);
				if (result == null) result = caseCalculation(conjunction);
				if (result == null) result = caseExpression(conjunction);
				if (result == null) result = caseTableValueElement(conjunction);
				if (result == null) result = caseTableLazyResolver(conjunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.DISJUNCTION: {
				Disjunction disjunction = (Disjunction)theEObject;
				T result = caseDisjunction(disjunction);
				if (result == null) result = caseCalculation(disjunction);
				if (result == null) result = caseExpression(disjunction);
				if (result == null) result = caseTableValueElement(disjunction);
				if (result == null) result = caseTableLazyResolver(disjunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TableDSLPackage.CONDITIONAL_EXPRESSION: {
				ConditionalExpression conditionalExpression = (ConditionalExpression)theEObject;
				T result = caseConditionalExpression(conditionalExpression);
				if (result == null) result = caseCalculation(conditionalExpression);
				if (result == null) result = caseExpression(conditionalExpression);
				if (result == null) result = caseTableValueElement(conditionalExpression);
				if (result == null) result = caseTableLazyResolver(conditionalExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableModel(TableModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Lazy Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableLazyResolver(TableLazyResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Package</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Package</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTablePackage(TablePackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Base</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Base</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableBase(TableBase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTable(Table object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Option</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Option</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableOption(TableOption object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Selection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Selection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableSelection(TableSelection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Table</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Table</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableTable(TableTable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Grid</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Grid</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableGrid(TableGrid object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Dto Datasource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Dto Datasource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableDtoDatasource(TableDtoDatasource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Grid Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Grid Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableGridProperty(TableGridProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Preorder</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Preorder</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTablePreorder(TablePreorder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableValue(TableValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Formatter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Formatter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableFormatter(TableFormatter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Image</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Image</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableImage(TableImage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Tooltip Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Tooltip Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableTooltipPattern(TableTooltipPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Value Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Value Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableValueElement(TableValueElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableTask(TableTask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table All Columns</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table All Columns</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableAllColumns(TableAllColumns object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Ordinal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Ordinal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableOrdinal(TableOrdinal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Column</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Column</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableColumn(TableColumn object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Measure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Measure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableMeasure(TableMeasure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Derived</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Derived</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableDerived(TableDerived object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Hierarchy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Hierarchy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableHierarchy(TableHierarchy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableAttribute(TableAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Aggregation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Aggregation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableAggregation(TableAggregation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Interval</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Interval</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableInterval(TableInterval object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Number Interval</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Number Interval</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableNumberInterval(TableNumberInterval object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Int Interval</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Int Interval</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableIntInterval(TableIntInterval object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Date Day Interval</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Date Day Interval</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableDateDayInterval(TableDateDayInterval object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Lookup</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Lookup</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableLookup(TableLookup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Number Lookup</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Number Lookup</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableNumberLookup(TableNumberLookup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Int Lookup</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Int Lookup</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableIntLookup(TableIntLookup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table String Lookup</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table String Lookup</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableStringLookup(TableStringLookup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Date Day Lookup</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Date Day Lookup</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableDateDayLookup(TableDateDayLookup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Datamart</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Datamart</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableDatamart(TableDatamart object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableElement(TableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Axis</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Axis</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableAxis(TableAxis object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableEvent(TableEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Range Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Range Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableRangeElement(TableRangeElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Text Color</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Text Color</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableTextColor(TableTextColor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Cell Color</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Cell Color</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableCellColor(TableCellColor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Icon</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Icon</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableIcon(TableIcon object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Trend</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Trend</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableTrend(TableTrend object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table Tooltip</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table Tooltip</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTableTooltip(TableTooltip object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpression(Expression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Calculation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Calculation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCalculation(Calculation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conjunction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conjunction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConjunction(Conjunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Disjunction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Disjunction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDisjunction(Disjunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conditional Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conditional Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConditionalExpression(ConditionalExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LLazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LLazy Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLLazyResolver(LLazyResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LPackage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LPackage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLPackage(LPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridSource(CxGridSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridProperty(CxGridProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TableDSLSwitch
