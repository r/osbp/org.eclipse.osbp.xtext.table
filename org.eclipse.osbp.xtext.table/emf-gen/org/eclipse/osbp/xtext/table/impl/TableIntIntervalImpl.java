/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.osbp.xtext.table.TableIntInterval;
import org.eclipse.osbp.xtext.table.TableRangeElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table Int Interval</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableIntIntervalImpl#getIntIntervalValue <em>Int Interval Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableIntIntervalImpl#getIntRange <em>Int Range</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TableIntIntervalImpl extends TableLazyResolverImpl implements TableIntInterval {
	/**
	 * The default value of the '{@link #getIntIntervalValue() <em>Int Interval Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntIntervalValue()
	 * @generated
	 * @ordered
	 */
	protected static final int INT_INTERVAL_VALUE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIntIntervalValue() <em>Int Interval Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntIntervalValue()
	 * @generated
	 * @ordered
	 */
	protected int intIntervalValue = INT_INTERVAL_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIntRange() <em>Int Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntRange()
	 * @generated
	 * @ordered
	 */
	protected TableRangeElement intRange;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableIntIntervalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TableDSLPackage.Literals.TABLE_INT_INTERVAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIntIntervalValue() {
		return intIntervalValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntIntervalValue(int newIntIntervalValue) {
		int oldIntIntervalValue = intIntervalValue;
		intIntervalValue = newIntIntervalValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_INT_INTERVAL__INT_INTERVAL_VALUE, oldIntIntervalValue, intIntervalValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableRangeElement getIntRange() {
		return intRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIntRange(TableRangeElement newIntRange, NotificationChain msgs) {
		TableRangeElement oldIntRange = intRange;
		intRange = newIntRange;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_INT_INTERVAL__INT_RANGE, oldIntRange, newIntRange);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntRange(TableRangeElement newIntRange) {
		if (newIntRange != intRange) {
			NotificationChain msgs = null;
			if (intRange != null)
				msgs = ((InternalEObject)intRange).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_INT_INTERVAL__INT_RANGE, null, msgs);
			if (newIntRange != null)
				msgs = ((InternalEObject)newIntRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_INT_INTERVAL__INT_RANGE, null, msgs);
			msgs = basicSetIntRange(newIntRange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_INT_INTERVAL__INT_RANGE, newIntRange, newIntRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TableDSLPackage.TABLE_INT_INTERVAL__INT_RANGE:
				return basicSetIntRange(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TableDSLPackage.TABLE_INT_INTERVAL__INT_INTERVAL_VALUE:
				return getIntIntervalValue();
			case TableDSLPackage.TABLE_INT_INTERVAL__INT_RANGE:
				return getIntRange();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TableDSLPackage.TABLE_INT_INTERVAL__INT_INTERVAL_VALUE:
				setIntIntervalValue((Integer)newValue);
				return;
			case TableDSLPackage.TABLE_INT_INTERVAL__INT_RANGE:
				setIntRange((TableRangeElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_INT_INTERVAL__INT_INTERVAL_VALUE:
				setIntIntervalValue(INT_INTERVAL_VALUE_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_INT_INTERVAL__INT_RANGE:
				setIntRange((TableRangeElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_INT_INTERVAL__INT_INTERVAL_VALUE:
				return intIntervalValue != INT_INTERVAL_VALUE_EDEFAULT;
			case TableDSLPackage.TABLE_INT_INTERVAL__INT_RANGE:
				return intRange != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (intIntervalValue: ");
		result.append(intIntervalValue);
		result.append(')');
		return result.toString();
	}

} //TableIntIntervalImpl
