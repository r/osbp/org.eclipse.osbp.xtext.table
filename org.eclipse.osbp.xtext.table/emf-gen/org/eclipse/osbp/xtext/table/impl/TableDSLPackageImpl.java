/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.dsl.semantic.dto.OSBPDtoPackage;

import org.eclipse.osbp.infogrid.model.gridsource.CxGridSourcePackage;

import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridStylePackage;

import org.eclipse.osbp.xtext.action.ActionDSLPackage;

import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;

import org.eclipse.osbp.xtext.datamartdsl.DatamartDSLPackage;

import org.eclipse.osbp.xtext.table.Calculation;
import org.eclipse.osbp.xtext.table.ConditionalExpression;
import org.eclipse.osbp.xtext.table.Conjunction;
import org.eclipse.osbp.xtext.table.Disjunction;
import org.eclipse.osbp.xtext.table.Expression;
import org.eclipse.osbp.xtext.table.OperatorEnum;
import org.eclipse.osbp.xtext.table.RowHeaderMode;
import org.eclipse.osbp.xtext.table.Table;
import org.eclipse.osbp.xtext.table.TableAggregation;
import org.eclipse.osbp.xtext.table.TableAllColumns;
import org.eclipse.osbp.xtext.table.TableAttribute;
import org.eclipse.osbp.xtext.table.TableAxis;
import org.eclipse.osbp.xtext.table.TableBase;
import org.eclipse.osbp.xtext.table.TableCellColor;
import org.eclipse.osbp.xtext.table.TableColumn;
import org.eclipse.osbp.xtext.table.TableDSLFactory;
import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.osbp.xtext.table.TableDatamart;
import org.eclipse.osbp.xtext.table.TableDateDayInterval;
import org.eclipse.osbp.xtext.table.TableDateDayLookup;
import org.eclipse.osbp.xtext.table.TableDerived;
import org.eclipse.osbp.xtext.table.TableDtoDatasource;
import org.eclipse.osbp.xtext.table.TableElement;
import org.eclipse.osbp.xtext.table.TableEvent;
import org.eclipse.osbp.xtext.table.TableFormatter;
import org.eclipse.osbp.xtext.table.TableGrid;
import org.eclipse.osbp.xtext.table.TableGridProperty;
import org.eclipse.osbp.xtext.table.TableHierarchy;
import org.eclipse.osbp.xtext.table.TableIcon;
import org.eclipse.osbp.xtext.table.TableImage;
import org.eclipse.osbp.xtext.table.TableIntInterval;
import org.eclipse.osbp.xtext.table.TableIntLookup;
import org.eclipse.osbp.xtext.table.TableInterval;
import org.eclipse.osbp.xtext.table.TableLazyResolver;
import org.eclipse.osbp.xtext.table.TableLookup;
import org.eclipse.osbp.xtext.table.TableMeasure;
import org.eclipse.osbp.xtext.table.TableModel;
import org.eclipse.osbp.xtext.table.TableNumberInterval;
import org.eclipse.osbp.xtext.table.TableNumberLookup;
import org.eclipse.osbp.xtext.table.TableOption;
import org.eclipse.osbp.xtext.table.TableOrdinal;
import org.eclipse.osbp.xtext.table.TablePackage;
import org.eclipse.osbp.xtext.table.TablePreorder;
import org.eclipse.osbp.xtext.table.TableRangeElement;
import org.eclipse.osbp.xtext.table.TableSelection;
import org.eclipse.osbp.xtext.table.TableStringLookup;
import org.eclipse.osbp.xtext.table.TableTable;
import org.eclipse.osbp.xtext.table.TableTask;
import org.eclipse.osbp.xtext.table.TableTextColor;
import org.eclipse.osbp.xtext.table.TableTooltip;
import org.eclipse.osbp.xtext.table.TableTooltipPattern;
import org.eclipse.osbp.xtext.table.TableTrend;
import org.eclipse.osbp.xtext.table.TableValue;
import org.eclipse.osbp.xtext.table.TableValueElement;
import org.eclipse.osbp.xtext.table.TrendIconEnum;

import org.eclipse.xtext.xtype.XtypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TableDSLPackageImpl extends EPackageImpl implements TableDSLPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableLazyResolverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tablePackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableBaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableOptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableSelectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableTableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableGridEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableDtoDatasourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableGridPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tablePreorderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableFormatterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableImageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableTooltipPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableValueElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableAllColumnsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableOrdinalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableColumnEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableMeasureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableDerivedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableHierarchyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableAggregationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableIntervalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableNumberIntervalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableIntIntervalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableDateDayIntervalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableLookupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableNumberLookupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableIntLookupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableStringLookupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableDateDayLookupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableDatamartEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableAxisEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableRangeElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableTextColorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableCellColorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableIconEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableTrendEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableTooltipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass calculationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conjunctionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass disjunctionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionalExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum trendIconEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum operatorEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum rowHeaderModeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType internalEObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TableDSLPackageImpl() {
		super(eNS_URI, TableDSLFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TableDSLPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TableDSLPackage init() {
		if (isInited) return (TableDSLPackage)EPackage.Registry.INSTANCE.getEPackage(TableDSLPackage.eNS_URI);

		// Obtain or create and register package
		TableDSLPackageImpl theTableDSLPackage = (TableDSLPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TableDSLPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TableDSLPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ActionDSLPackage.eINSTANCE.eClass();
		CxGridSourcePackage.eINSTANCE.eClass();
		OSBPDtoPackage.eINSTANCE.eClass();
		DatamartDSLPackage.eINSTANCE.eClass();
		CxGridStylePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theTableDSLPackage.createPackageContents();

		// Initialize created meta-data
		theTableDSLPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTableDSLPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TableDSLPackage.eNS_URI, theTableDSLPackage);
		return theTableDSLPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableModel() {
		return tableModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableModel_ImportSection() {
		return (EReference)tableModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableModel_Packages() {
		return (EReference)tableModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableLazyResolver() {
		return tableLazyResolverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTablePackage() {
		return tablePackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTablePackage_Tables() {
		return (EReference)tablePackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableBase() {
		return tableBaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableBase_Name() {
		return (EAttribute)tableBaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTable() {
		return tableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTable_Description() {
		return (EAttribute)tableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTable_DescriptionValue() {
		return (EAttribute)tableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTable_Tabletype() {
		return (EReference)tableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableOption() {
		return tableOptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableOption_Filtering() {
		return (EAttribute)tableOptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableOption_Toolbar() {
		return (EReference)tableOptionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableOption_Embedded() {
		return (EAttribute)tableOptionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableSelection() {
		return tableSelectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableSelection_MultiSelection() {
		return (EAttribute)tableSelectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableTable() {
		return tableTableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableTable_SelectIdOnly() {
		return (EAttribute)tableTableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableTable_SelectById() {
		return (EAttribute)tableTableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableTable_Selectalways() {
		return (EAttribute)tableTableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableTable_HeaderMode() {
		return (EAttribute)tableTableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableTable_Source() {
		return (EReference)tableTableEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableGrid() {
		return tableGridEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableGrid_Selectalways() {
		return (EAttribute)tableGridEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableGrid_HeaderMode() {
		return (EAttribute)tableGridEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableGrid_Source() {
		return (EReference)tableGridEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableDtoDatasource() {
		return tableDtoDatasourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableDtoDatasource_DtoSource() {
		return (EReference)tableDtoDatasourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableGridProperty() {
		return tableGridPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTablePreorder() {
		return tablePreorderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTablePreorder_Column() {
		return (EReference)tablePreorderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTablePreorder_Ascending() {
		return (EAttribute)tablePreorderEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableValue() {
		return tableValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableValue_Column() {
		return (EReference)tableValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableValue_Collapsed() {
		return (EAttribute)tableValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableValue_Formatter() {
		return (EReference)tableValueEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableValue_TooltipPattern() {
		return (EReference)tableValueEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableValue_HideLabelInterval() {
		return (EAttribute)tableValueEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableValue_HideLabelLookup() {
		return (EAttribute)tableValueEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableValue_Intervals() {
		return (EReference)tableValueEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableValue_Lookups() {
		return (EReference)tableValueEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableValue_HasImage() {
		return (EAttribute)tableValueEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableValue_Image() {
		return (EReference)tableValueEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableValue_IconName() {
		return (EAttribute)tableValueEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableFormatter() {
		return tableFormatterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableFormatter_Format() {
		return (EAttribute)tableFormatterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableImage() {
		return tableImageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableImage_ImagePathPattern() {
		return (EAttribute)tableImageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableImage_HideImageLabel() {
		return (EAttribute)tableImageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableImage_HasParameter() {
		return (EAttribute)tableImageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableImage_Resize() {
		return (EAttribute)tableImageEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableImage_ResizeString() {
		return (EAttribute)tableImageEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableImage_ImagePathParameter() {
		return (EReference)tableImageEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableTooltipPattern() {
		return tableTooltipPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableTooltipPattern_TooltipPattern() {
		return (EAttribute)tableTooltipPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableValueElement() {
		return tableValueElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableTask() {
		return tableTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableAllColumns() {
		return tableAllColumnsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableOrdinal() {
		return tableOrdinalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableOrdinal_ValueRef() {
		return (EAttribute)tableOrdinalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableColumn() {
		return tableColumnEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableColumn_ValueRef() {
		return (EReference)tableColumnEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableMeasure() {
		return tableMeasureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableMeasure_ValueRef() {
		return (EReference)tableMeasureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableDerived() {
		return tableDerivedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableDerived_ValueRef() {
		return (EReference)tableDerivedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableHierarchy() {
		return tableHierarchyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableHierarchy_ValueRef() {
		return (EReference)tableHierarchyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableAttribute() {
		return tableAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableAttribute_ValueRef() {
		return (EReference)tableAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableAggregation() {
		return tableAggregationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableAggregation_ValueRef() {
		return (EReference)tableAggregationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableInterval() {
		return tableIntervalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableNumberInterval() {
		return tableNumberIntervalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableNumberInterval_NumberIntervalValue() {
		return (EAttribute)tableNumberIntervalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableNumberInterval_NumberRange() {
		return (EReference)tableNumberIntervalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableIntInterval() {
		return tableIntIntervalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableIntInterval_IntIntervalValue() {
		return (EAttribute)tableIntIntervalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableIntInterval_IntRange() {
		return (EReference)tableIntIntervalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableDateDayInterval() {
		return tableDateDayIntervalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableDateDayInterval_DateIntervalValue() {
		return (EAttribute)tableDateDayIntervalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableDateDayInterval_DateRange() {
		return (EReference)tableDateDayIntervalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableLookup() {
		return tableLookupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableNumberLookup() {
		return tableNumberLookupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableNumberLookup_LookupValue() {
		return (EAttribute)tableNumberLookupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableNumberLookup_Discrete() {
		return (EReference)tableNumberLookupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableIntLookup() {
		return tableIntLookupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableIntLookup_LookupValue() {
		return (EAttribute)tableIntLookupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableIntLookup_Discrete() {
		return (EReference)tableIntLookupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableStringLookup() {
		return tableStringLookupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableStringLookup_LookupValue() {
		return (EAttribute)tableStringLookupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableStringLookup_Discrete() {
		return (EReference)tableStringLookupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableDateDayLookup() {
		return tableDateDayLookupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableDateDayLookup_LookupValue() {
		return (EAttribute)tableDateDayLookupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableDateDayLookup_Discrete() {
		return (EReference)tableDateDayLookupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableDatamart() {
		return tableDatamartEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableDatamart_DatamartRef() {
		return (EReference)tableDatamartEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableDatamart_Elements() {
		return (EReference)tableDatamartEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableElement() {
		return tableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableAxis() {
		return tableAxisEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableAxis_Axis() {
		return (EAttribute)tableAxisEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableAxis_HasRowHeight() {
		return (EAttribute)tableAxisEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableAxis_RowHeight() {
		return (EAttribute)tableAxisEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableAxis_PreOrder() {
		return (EReference)tableAxisEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableAxis_HasDetails() {
		return (EAttribute)tableAxisEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableAxis_Values() {
		return (EReference)tableAxisEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableAxis_HasEvents() {
		return (EAttribute)tableAxisEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableAxis_Events() {
		return (EReference)tableAxisEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableEvent() {
		return tableEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableEvent_Source() {
		return (EReference)tableEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableRangeElement() {
		return tableRangeElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableTextColor() {
		return tableTextColorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableTextColor_Rgb() {
		return (EAttribute)tableTextColorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableCellColor() {
		return tableCellColorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableCellColor_Rgb() {
		return (EAttribute)tableCellColorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableIcon() {
		return tableIconEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableIcon_Icon() {
		return (EAttribute)tableIconEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableTrend() {
		return tableTrendEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableTrend_Icon() {
		return (EAttribute)tableTrendEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableTooltip() {
		return tableTooltipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableTooltip_Tooltip() {
		return (EAttribute)tableTooltipEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpression() {
		return expressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExpression_NumberValue() {
		return (EAttribute)expressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExpression_StringValue() {
		return (EAttribute)expressionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCalculation() {
		return calculationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCalculation_Left() {
		return (EReference)calculationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCalculation_Right() {
		return (EReference)calculationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConjunction() {
		return conjunctionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDisjunction() {
		return disjunctionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConditionalExpression() {
		return conditionalExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConditionalExpression_Operator() {
		return (EAttribute)conditionalExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTrendIconEnum() {
		return trendIconEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOperatorEnum() {
		return operatorEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRowHeaderMode() {
		return rowHeaderModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getInternalEObject() {
		return internalEObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableDSLFactory getTableDSLFactory() {
		return (TableDSLFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tableModelEClass = createEClass(TABLE_MODEL);
		createEReference(tableModelEClass, TABLE_MODEL__IMPORT_SECTION);
		createEReference(tableModelEClass, TABLE_MODEL__PACKAGES);

		tableLazyResolverEClass = createEClass(TABLE_LAZY_RESOLVER);

		tablePackageEClass = createEClass(TABLE_PACKAGE);
		createEReference(tablePackageEClass, TABLE_PACKAGE__TABLES);

		tableBaseEClass = createEClass(TABLE_BASE);
		createEAttribute(tableBaseEClass, TABLE_BASE__NAME);

		tableEClass = createEClass(TABLE);
		createEAttribute(tableEClass, TABLE__DESCRIPTION);
		createEAttribute(tableEClass, TABLE__DESCRIPTION_VALUE);
		createEReference(tableEClass, TABLE__TABLETYPE);

		tableOptionEClass = createEClass(TABLE_OPTION);
		createEAttribute(tableOptionEClass, TABLE_OPTION__FILTERING);
		createEReference(tableOptionEClass, TABLE_OPTION__TOOLBAR);
		createEAttribute(tableOptionEClass, TABLE_OPTION__EMBEDDED);

		tableSelectionEClass = createEClass(TABLE_SELECTION);
		createEAttribute(tableSelectionEClass, TABLE_SELECTION__MULTI_SELECTION);

		tableTableEClass = createEClass(TABLE_TABLE);
		createEAttribute(tableTableEClass, TABLE_TABLE__SELECT_ID_ONLY);
		createEAttribute(tableTableEClass, TABLE_TABLE__SELECT_BY_ID);
		createEAttribute(tableTableEClass, TABLE_TABLE__SELECTALWAYS);
		createEAttribute(tableTableEClass, TABLE_TABLE__HEADER_MODE);
		createEReference(tableTableEClass, TABLE_TABLE__SOURCE);

		tableGridEClass = createEClass(TABLE_GRID);
		createEAttribute(tableGridEClass, TABLE_GRID__SELECTALWAYS);
		createEAttribute(tableGridEClass, TABLE_GRID__HEADER_MODE);
		createEReference(tableGridEClass, TABLE_GRID__SOURCE);

		tableDtoDatasourceEClass = createEClass(TABLE_DTO_DATASOURCE);
		createEReference(tableDtoDatasourceEClass, TABLE_DTO_DATASOURCE__DTO_SOURCE);

		tableGridPropertyEClass = createEClass(TABLE_GRID_PROPERTY);

		tablePreorderEClass = createEClass(TABLE_PREORDER);
		createEReference(tablePreorderEClass, TABLE_PREORDER__COLUMN);
		createEAttribute(tablePreorderEClass, TABLE_PREORDER__ASCENDING);

		tableValueEClass = createEClass(TABLE_VALUE);
		createEReference(tableValueEClass, TABLE_VALUE__COLUMN);
		createEAttribute(tableValueEClass, TABLE_VALUE__COLLAPSED);
		createEReference(tableValueEClass, TABLE_VALUE__FORMATTER);
		createEReference(tableValueEClass, TABLE_VALUE__TOOLTIP_PATTERN);
		createEAttribute(tableValueEClass, TABLE_VALUE__HIDE_LABEL_INTERVAL);
		createEAttribute(tableValueEClass, TABLE_VALUE__HIDE_LABEL_LOOKUP);
		createEReference(tableValueEClass, TABLE_VALUE__INTERVALS);
		createEReference(tableValueEClass, TABLE_VALUE__LOOKUPS);
		createEAttribute(tableValueEClass, TABLE_VALUE__HAS_IMAGE);
		createEReference(tableValueEClass, TABLE_VALUE__IMAGE);
		createEAttribute(tableValueEClass, TABLE_VALUE__ICON_NAME);

		tableFormatterEClass = createEClass(TABLE_FORMATTER);
		createEAttribute(tableFormatterEClass, TABLE_FORMATTER__FORMAT);

		tableImageEClass = createEClass(TABLE_IMAGE);
		createEAttribute(tableImageEClass, TABLE_IMAGE__IMAGE_PATH_PATTERN);
		createEAttribute(tableImageEClass, TABLE_IMAGE__HIDE_IMAGE_LABEL);
		createEAttribute(tableImageEClass, TABLE_IMAGE__HAS_PARAMETER);
		createEAttribute(tableImageEClass, TABLE_IMAGE__RESIZE);
		createEAttribute(tableImageEClass, TABLE_IMAGE__RESIZE_STRING);
		createEReference(tableImageEClass, TABLE_IMAGE__IMAGE_PATH_PARAMETER);

		tableTooltipPatternEClass = createEClass(TABLE_TOOLTIP_PATTERN);
		createEAttribute(tableTooltipPatternEClass, TABLE_TOOLTIP_PATTERN__TOOLTIP_PATTERN);

		tableValueElementEClass = createEClass(TABLE_VALUE_ELEMENT);

		tableTaskEClass = createEClass(TABLE_TASK);

		tableAllColumnsEClass = createEClass(TABLE_ALL_COLUMNS);

		tableOrdinalEClass = createEClass(TABLE_ORDINAL);
		createEAttribute(tableOrdinalEClass, TABLE_ORDINAL__VALUE_REF);

		tableColumnEClass = createEClass(TABLE_COLUMN);
		createEReference(tableColumnEClass, TABLE_COLUMN__VALUE_REF);

		tableMeasureEClass = createEClass(TABLE_MEASURE);
		createEReference(tableMeasureEClass, TABLE_MEASURE__VALUE_REF);

		tableDerivedEClass = createEClass(TABLE_DERIVED);
		createEReference(tableDerivedEClass, TABLE_DERIVED__VALUE_REF);

		tableHierarchyEClass = createEClass(TABLE_HIERARCHY);
		createEReference(tableHierarchyEClass, TABLE_HIERARCHY__VALUE_REF);

		tableAttributeEClass = createEClass(TABLE_ATTRIBUTE);
		createEReference(tableAttributeEClass, TABLE_ATTRIBUTE__VALUE_REF);

		tableAggregationEClass = createEClass(TABLE_AGGREGATION);
		createEReference(tableAggregationEClass, TABLE_AGGREGATION__VALUE_REF);

		tableIntervalEClass = createEClass(TABLE_INTERVAL);

		tableNumberIntervalEClass = createEClass(TABLE_NUMBER_INTERVAL);
		createEAttribute(tableNumberIntervalEClass, TABLE_NUMBER_INTERVAL__NUMBER_INTERVAL_VALUE);
		createEReference(tableNumberIntervalEClass, TABLE_NUMBER_INTERVAL__NUMBER_RANGE);

		tableIntIntervalEClass = createEClass(TABLE_INT_INTERVAL);
		createEAttribute(tableIntIntervalEClass, TABLE_INT_INTERVAL__INT_INTERVAL_VALUE);
		createEReference(tableIntIntervalEClass, TABLE_INT_INTERVAL__INT_RANGE);

		tableDateDayIntervalEClass = createEClass(TABLE_DATE_DAY_INTERVAL);
		createEAttribute(tableDateDayIntervalEClass, TABLE_DATE_DAY_INTERVAL__DATE_INTERVAL_VALUE);
		createEReference(tableDateDayIntervalEClass, TABLE_DATE_DAY_INTERVAL__DATE_RANGE);

		tableLookupEClass = createEClass(TABLE_LOOKUP);

		tableNumberLookupEClass = createEClass(TABLE_NUMBER_LOOKUP);
		createEAttribute(tableNumberLookupEClass, TABLE_NUMBER_LOOKUP__LOOKUP_VALUE);
		createEReference(tableNumberLookupEClass, TABLE_NUMBER_LOOKUP__DISCRETE);

		tableIntLookupEClass = createEClass(TABLE_INT_LOOKUP);
		createEAttribute(tableIntLookupEClass, TABLE_INT_LOOKUP__LOOKUP_VALUE);
		createEReference(tableIntLookupEClass, TABLE_INT_LOOKUP__DISCRETE);

		tableStringLookupEClass = createEClass(TABLE_STRING_LOOKUP);
		createEAttribute(tableStringLookupEClass, TABLE_STRING_LOOKUP__LOOKUP_VALUE);
		createEReference(tableStringLookupEClass, TABLE_STRING_LOOKUP__DISCRETE);

		tableDateDayLookupEClass = createEClass(TABLE_DATE_DAY_LOOKUP);
		createEAttribute(tableDateDayLookupEClass, TABLE_DATE_DAY_LOOKUP__LOOKUP_VALUE);
		createEReference(tableDateDayLookupEClass, TABLE_DATE_DAY_LOOKUP__DISCRETE);

		tableDatamartEClass = createEClass(TABLE_DATAMART);
		createEReference(tableDatamartEClass, TABLE_DATAMART__DATAMART_REF);
		createEReference(tableDatamartEClass, TABLE_DATAMART__ELEMENTS);

		tableElementEClass = createEClass(TABLE_ELEMENT);

		tableAxisEClass = createEClass(TABLE_AXIS);
		createEAttribute(tableAxisEClass, TABLE_AXIS__AXIS);
		createEAttribute(tableAxisEClass, TABLE_AXIS__HAS_ROW_HEIGHT);
		createEAttribute(tableAxisEClass, TABLE_AXIS__ROW_HEIGHT);
		createEReference(tableAxisEClass, TABLE_AXIS__PRE_ORDER);
		createEAttribute(tableAxisEClass, TABLE_AXIS__HAS_DETAILS);
		createEReference(tableAxisEClass, TABLE_AXIS__VALUES);
		createEAttribute(tableAxisEClass, TABLE_AXIS__HAS_EVENTS);
		createEReference(tableAxisEClass, TABLE_AXIS__EVENTS);

		tableEventEClass = createEClass(TABLE_EVENT);
		createEReference(tableEventEClass, TABLE_EVENT__SOURCE);

		tableRangeElementEClass = createEClass(TABLE_RANGE_ELEMENT);

		tableTextColorEClass = createEClass(TABLE_TEXT_COLOR);
		createEAttribute(tableTextColorEClass, TABLE_TEXT_COLOR__RGB);

		tableCellColorEClass = createEClass(TABLE_CELL_COLOR);
		createEAttribute(tableCellColorEClass, TABLE_CELL_COLOR__RGB);

		tableIconEClass = createEClass(TABLE_ICON);
		createEAttribute(tableIconEClass, TABLE_ICON__ICON);

		tableTrendEClass = createEClass(TABLE_TREND);
		createEAttribute(tableTrendEClass, TABLE_TREND__ICON);

		tableTooltipEClass = createEClass(TABLE_TOOLTIP);
		createEAttribute(tableTooltipEClass, TABLE_TOOLTIP__TOOLTIP);

		expressionEClass = createEClass(EXPRESSION);
		createEAttribute(expressionEClass, EXPRESSION__NUMBER_VALUE);
		createEAttribute(expressionEClass, EXPRESSION__STRING_VALUE);

		calculationEClass = createEClass(CALCULATION);
		createEReference(calculationEClass, CALCULATION__LEFT);
		createEReference(calculationEClass, CALCULATION__RIGHT);

		conjunctionEClass = createEClass(CONJUNCTION);

		disjunctionEClass = createEClass(DISJUNCTION);

		conditionalExpressionEClass = createEClass(CONDITIONAL_EXPRESSION);
		createEAttribute(conditionalExpressionEClass, CONDITIONAL_EXPRESSION__OPERATOR);

		// Create enums
		trendIconEnumEEnum = createEEnum(TREND_ICON_ENUM);
		operatorEnumEEnum = createEEnum(OPERATOR_ENUM);
		rowHeaderModeEEnum = createEEnum(ROW_HEADER_MODE);

		// Create data types
		internalEObjectEDataType = createEDataType(INTERNAL_EOBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XtypePackage theXtypePackage = (XtypePackage)EPackage.Registry.INSTANCE.getEPackage(XtypePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		OSBPTypesPackage theOSBPTypesPackage = (OSBPTypesPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPTypesPackage.eNS_URI);
		ActionDSLPackage theActionDSLPackage = (ActionDSLPackage)EPackage.Registry.INSTANCE.getEPackage(ActionDSLPackage.eNS_URI);
		CxGridSourcePackage theCxGridSourcePackage = (CxGridSourcePackage)EPackage.Registry.INSTANCE.getEPackage(CxGridSourcePackage.eNS_URI);
		OSBPDtoPackage theOSBPDtoPackage = (OSBPDtoPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPDtoPackage.eNS_URI);
		DatamartDSLPackage theDatamartDSLPackage = (DatamartDSLPackage)EPackage.Registry.INSTANCE.getEPackage(DatamartDSLPackage.eNS_URI);
		CubeDSLPackage theCubeDSLPackage = (CubeDSLPackage)EPackage.Registry.INSTANCE.getEPackage(CubeDSLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		tablePackageEClass.getESuperTypes().add(theOSBPTypesPackage.getLPackage());
		tableBaseEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableEClass.getESuperTypes().add(this.getTableBase());
		tableOptionEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableSelectionEClass.getESuperTypes().add(this.getTableTable());
		tableTableEClass.getESuperTypes().add(this.getTableOption());
		tableGridEClass.getESuperTypes().add(this.getTableOption());
		tableDtoDatasourceEClass.getESuperTypes().add(theCxGridSourcePackage.getCxGridSource());
		tableGridPropertyEClass.getESuperTypes().add(theCxGridSourcePackage.getCxGridProperty());
		tablePreorderEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableValueEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableFormatterEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableImageEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableTooltipPatternEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableValueElementEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableTaskEClass.getESuperTypes().add(this.getExpression());
		tableTaskEClass.getESuperTypes().add(this.getTableValueElement());
		tableAllColumnsEClass.getESuperTypes().add(this.getExpression());
		tableAllColumnsEClass.getESuperTypes().add(this.getTableValueElement());
		tableOrdinalEClass.getESuperTypes().add(this.getExpression());
		tableOrdinalEClass.getESuperTypes().add(this.getTableValueElement());
		tableColumnEClass.getESuperTypes().add(this.getExpression());
		tableColumnEClass.getESuperTypes().add(this.getTableValueElement());
		tableMeasureEClass.getESuperTypes().add(this.getExpression());
		tableMeasureEClass.getESuperTypes().add(this.getTableValueElement());
		tableDerivedEClass.getESuperTypes().add(this.getExpression());
		tableDerivedEClass.getESuperTypes().add(this.getTableValueElement());
		tableHierarchyEClass.getESuperTypes().add(this.getExpression());
		tableHierarchyEClass.getESuperTypes().add(this.getTableValueElement());
		tableAttributeEClass.getESuperTypes().add(this.getExpression());
		tableAttributeEClass.getESuperTypes().add(this.getTableValueElement());
		tableAggregationEClass.getESuperTypes().add(this.getExpression());
		tableAggregationEClass.getESuperTypes().add(this.getTableValueElement());
		tableIntervalEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableNumberIntervalEClass.getESuperTypes().add(this.getTableInterval());
		tableIntIntervalEClass.getESuperTypes().add(this.getTableInterval());
		tableDateDayIntervalEClass.getESuperTypes().add(this.getTableInterval());
		tableLookupEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableNumberLookupEClass.getESuperTypes().add(this.getTableLookup());
		tableIntLookupEClass.getESuperTypes().add(this.getTableLookup());
		tableStringLookupEClass.getESuperTypes().add(this.getTableLookup());
		tableDateDayLookupEClass.getESuperTypes().add(this.getTableLookup());
		tableDatamartEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableElementEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableAxisEClass.getESuperTypes().add(this.getTableElement());
		tableEventEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableRangeElementEClass.getESuperTypes().add(this.getTableLazyResolver());
		tableTextColorEClass.getESuperTypes().add(this.getTableRangeElement());
		tableCellColorEClass.getESuperTypes().add(this.getTableRangeElement());
		tableIconEClass.getESuperTypes().add(this.getTableRangeElement());
		tableTrendEClass.getESuperTypes().add(this.getTableRangeElement());
		tableTooltipEClass.getESuperTypes().add(this.getTableRangeElement());
		expressionEClass.getESuperTypes().add(this.getTableValueElement());
		calculationEClass.getESuperTypes().add(this.getExpression());
		conjunctionEClass.getESuperTypes().add(this.getCalculation());
		disjunctionEClass.getESuperTypes().add(this.getCalculation());
		conditionalExpressionEClass.getESuperTypes().add(this.getCalculation());

		// Initialize classes and features; add operations and parameters
		initEClass(tableModelEClass, TableModel.class, "TableModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTableModel_ImportSection(), theXtypePackage.getXImportSection(), null, "importSection", null, 0, 1, TableModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableModel_Packages(), this.getTablePackage(), null, "packages", null, 0, -1, TableModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableLazyResolverEClass, TableLazyResolver.class, "TableLazyResolver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = addEOperation(tableLazyResolverEClass, theEcorePackage.getEObject(), "eResolveProxy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInternalEObject(), "proxy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(tablePackageEClass, TablePackage.class, "TablePackage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTablePackage_Tables(), this.getTable(), null, "tables", null, 0, -1, TablePackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableBaseEClass, TableBase.class, "TableBase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableBase_Name(), theEcorePackage.getEString(), "name", null, 0, 1, TableBase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableEClass, Table.class, "Table", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTable_Description(), theEcorePackage.getEBoolean(), "description", null, 0, 1, Table.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTable_DescriptionValue(), theEcorePackage.getEString(), "descriptionValue", null, 0, 1, Table.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTable_Tabletype(), this.getTableOption(), null, "tabletype", null, 0, 1, Table.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableOptionEClass, TableOption.class, "TableOption", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableOption_Filtering(), theEcorePackage.getEBoolean(), "filtering", null, 0, 1, TableOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableOption_Toolbar(), theActionDSLPackage.getActionToolbar(), null, "toolbar", null, 0, 1, TableOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableOption_Embedded(), theEcorePackage.getEBoolean(), "embedded", null, 0, 1, TableOption.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableSelectionEClass, TableSelection.class, "TableSelection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableSelection_MultiSelection(), theEcorePackage.getEBoolean(), "multiSelection", null, 0, 1, TableSelection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableTableEClass, TableTable.class, "TableTable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableTable_SelectIdOnly(), theEcorePackage.getEBoolean(), "selectIdOnly", null, 0, 1, TableTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableTable_SelectById(), theEcorePackage.getEBoolean(), "selectById", null, 0, 1, TableTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableTable_Selectalways(), theEcorePackage.getEBoolean(), "selectalways", null, 0, 1, TableTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableTable_HeaderMode(), this.getRowHeaderMode(), "headerMode", null, 0, 1, TableTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableTable_Source(), this.getTableDatamart(), null, "source", null, 0, 1, TableTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableGridEClass, TableGrid.class, "TableGrid", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableGrid_Selectalways(), theEcorePackage.getEBoolean(), "selectalways", null, 0, 1, TableGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableGrid_HeaderMode(), this.getRowHeaderMode(), "headerMode", null, 0, 1, TableGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableGrid_Source(), this.getTableDtoDatasource(), null, "source", null, 0, 1, TableGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableDtoDatasourceEClass, TableDtoDatasource.class, "TableDtoDatasource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTableDtoDatasource_DtoSource(), theOSBPDtoPackage.getLDto(), null, "dtoSource", null, 0, 1, TableDtoDatasource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableGridPropertyEClass, TableGridProperty.class, "TableGridProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tablePreorderEClass, TablePreorder.class, "TablePreorder", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTablePreorder_Column(), this.getTableValueElement(), null, "column", null, 0, 1, TablePreorder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTablePreorder_Ascending(), theEcorePackage.getEBoolean(), "ascending", null, 0, 1, TablePreorder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableValueEClass, TableValue.class, "TableValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTableValue_Column(), this.getTableValueElement(), null, "column", null, 0, 1, TableValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableValue_Collapsed(), theEcorePackage.getEBoolean(), "collapsed", null, 0, 1, TableValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableValue_Formatter(), this.getTableFormatter(), null, "formatter", null, 0, 1, TableValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableValue_TooltipPattern(), this.getTableTooltipPattern(), null, "tooltipPattern", null, 0, 1, TableValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableValue_HideLabelInterval(), theEcorePackage.getEBoolean(), "hideLabelInterval", null, 0, 1, TableValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableValue_HideLabelLookup(), theEcorePackage.getEBoolean(), "hideLabelLookup", null, 0, 1, TableValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableValue_Intervals(), this.getTableInterval(), null, "intervals", null, 0, -1, TableValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableValue_Lookups(), this.getTableLookup(), null, "lookups", null, 0, -1, TableValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableValue_HasImage(), theEcorePackage.getEBoolean(), "hasImage", null, 0, 1, TableValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableValue_Image(), this.getTableImage(), null, "image", null, 0, 1, TableValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableValue_IconName(), theEcorePackage.getEString(), "iconName", null, 0, 1, TableValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableFormatterEClass, TableFormatter.class, "TableFormatter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableFormatter_Format(), theEcorePackage.getEString(), "format", null, 0, 1, TableFormatter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableImageEClass, TableImage.class, "TableImage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableImage_ImagePathPattern(), theEcorePackage.getEString(), "imagePathPattern", null, 0, 1, TableImage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableImage_HideImageLabel(), theEcorePackage.getEBoolean(), "hideImageLabel", null, 0, 1, TableImage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableImage_HasParameter(), theEcorePackage.getEBoolean(), "hasParameter", null, 0, 1, TableImage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableImage_Resize(), theEcorePackage.getEBoolean(), "resize", null, 0, 1, TableImage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableImage_ResizeString(), theEcorePackage.getEString(), "resizeString", null, 0, 1, TableImage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableImage_ImagePathParameter(), this.getTableValueElement(), null, "imagePathParameter", null, 0, 1, TableImage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableTooltipPatternEClass, TableTooltipPattern.class, "TableTooltipPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableTooltipPattern_TooltipPattern(), theEcorePackage.getEString(), "tooltipPattern", null, 0, 1, TableTooltipPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableValueElementEClass, TableValueElement.class, "TableValueElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tableTaskEClass, TableTask.class, "TableTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tableAllColumnsEClass, TableAllColumns.class, "TableAllColumns", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tableOrdinalEClass, TableOrdinal.class, "TableOrdinal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableOrdinal_ValueRef(), theEcorePackage.getEString(), "valueRef", null, 0, 1, TableOrdinal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableColumnEClass, TableColumn.class, "TableColumn", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTableColumn_ValueRef(), theDatamartDSLPackage.getDatamartColumn(), null, "valueRef", null, 0, 1, TableColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableMeasureEClass, TableMeasure.class, "TableMeasure", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTableMeasure_ValueRef(), theDatamartDSLPackage.getDatamartMeasure(), null, "valueRef", null, 0, 1, TableMeasure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableDerivedEClass, TableDerived.class, "TableDerived", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTableDerived_ValueRef(), theDatamartDSLPackage.getDatamartDerivedMeasure(), null, "valueRef", null, 0, 1, TableDerived.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableHierarchyEClass, TableHierarchy.class, "TableHierarchy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTableHierarchy_ValueRef(), theCubeDSLPackage.getCubeLevel(), null, "valueRef", null, 0, 1, TableHierarchy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableAttributeEClass, TableAttribute.class, "TableAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTableAttribute_ValueRef(), theDatamartDSLPackage.getDatamartAttribute(), null, "valueRef", null, 0, 1, TableAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableAggregationEClass, TableAggregation.class, "TableAggregation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTableAggregation_ValueRef(), theDatamartDSLPackage.getDatamartSetAggregationFunction(), null, "valueRef", null, 0, 1, TableAggregation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableIntervalEClass, TableInterval.class, "TableInterval", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tableNumberIntervalEClass, TableNumberInterval.class, "TableNumberInterval", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableNumberInterval_NumberIntervalValue(), theEcorePackage.getEDouble(), "numberIntervalValue", null, 0, 1, TableNumberInterval.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableNumberInterval_NumberRange(), this.getTableRangeElement(), null, "numberRange", null, 0, 1, TableNumberInterval.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableIntIntervalEClass, TableIntInterval.class, "TableIntInterval", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableIntInterval_IntIntervalValue(), theEcorePackage.getEInt(), "intIntervalValue", null, 0, 1, TableIntInterval.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableIntInterval_IntRange(), this.getTableRangeElement(), null, "intRange", null, 0, 1, TableIntInterval.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableDateDayIntervalEClass, TableDateDayInterval.class, "TableDateDayInterval", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableDateDayInterval_DateIntervalValue(), theEcorePackage.getEInt(), "dateIntervalValue", null, 0, 1, TableDateDayInterval.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableDateDayInterval_DateRange(), this.getTableRangeElement(), null, "dateRange", null, 0, 1, TableDateDayInterval.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableLookupEClass, TableLookup.class, "TableLookup", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tableNumberLookupEClass, TableNumberLookup.class, "TableNumberLookup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableNumberLookup_LookupValue(), theEcorePackage.getEDouble(), "lookupValue", null, 0, 1, TableNumberLookup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableNumberLookup_Discrete(), this.getTableRangeElement(), null, "discrete", null, 0, 1, TableNumberLookup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableIntLookupEClass, TableIntLookup.class, "TableIntLookup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableIntLookup_LookupValue(), theEcorePackage.getEInt(), "lookupValue", null, 0, 1, TableIntLookup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableIntLookup_Discrete(), this.getTableRangeElement(), null, "discrete", null, 0, 1, TableIntLookup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableStringLookupEClass, TableStringLookup.class, "TableStringLookup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableStringLookup_LookupValue(), theEcorePackage.getEString(), "lookupValue", null, 0, 1, TableStringLookup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableStringLookup_Discrete(), this.getTableRangeElement(), null, "discrete", null, 0, 1, TableStringLookup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableDateDayLookupEClass, TableDateDayLookup.class, "TableDateDayLookup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableDateDayLookup_LookupValue(), theEcorePackage.getEInt(), "lookupValue", null, 0, 1, TableDateDayLookup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableDateDayLookup_Discrete(), this.getTableRangeElement(), null, "discrete", null, 0, 1, TableDateDayLookup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableDatamartEClass, TableDatamart.class, "TableDatamart", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTableDatamart_DatamartRef(), theDatamartDSLPackage.getDatamartDefinition(), null, "datamartRef", null, 0, 1, TableDatamart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableDatamart_Elements(), this.getTableElement(), null, "elements", null, 0, -1, TableDatamart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableElementEClass, TableElement.class, "TableElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tableAxisEClass, TableAxis.class, "TableAxis", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableAxis_Axis(), theDatamartDSLPackage.getAxisEnum(), "axis", null, 0, 1, TableAxis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableAxis_HasRowHeight(), theEcorePackage.getEBoolean(), "hasRowHeight", null, 0, 1, TableAxis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableAxis_RowHeight(), theEcorePackage.getEString(), "rowHeight", null, 0, 1, TableAxis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableAxis_PreOrder(), this.getTablePreorder(), null, "preOrder", null, 0, 1, TableAxis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableAxis_HasDetails(), theEcorePackage.getEBoolean(), "hasDetails", null, 0, 1, TableAxis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableAxis_Values(), this.getTableValue(), null, "values", null, 0, -1, TableAxis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableAxis_HasEvents(), theEcorePackage.getEBoolean(), "hasEvents", null, 0, 1, TableAxis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableAxis_Events(), this.getTableEvent(), null, "events", null, 0, -1, TableAxis.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableEventEClass, TableEvent.class, "TableEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTableEvent_Source(), this.getTableValueElement(), null, "source", null, 0, 1, TableEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableRangeElementEClass, TableRangeElement.class, "TableRangeElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(tableTextColorEClass, TableTextColor.class, "TableTextColor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableTextColor_Rgb(), theEcorePackage.getEString(), "rgb", null, 0, 1, TableTextColor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableCellColorEClass, TableCellColor.class, "TableCellColor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableCellColor_Rgb(), theEcorePackage.getEString(), "rgb", null, 0, 1, TableCellColor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableIconEClass, TableIcon.class, "TableIcon", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableIcon_Icon(), theEcorePackage.getEString(), "icon", null, 0, 1, TableIcon.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableTrendEClass, TableTrend.class, "TableTrend", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableTrend_Icon(), this.getTrendIconEnum(), "icon", null, 0, 1, TableTrend.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableTooltipEClass, TableTooltip.class, "TableTooltip", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableTooltip_Tooltip(), theEcorePackage.getEString(), "tooltip", null, 0, 1, TableTooltip.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(expressionEClass, Expression.class, "Expression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExpression_NumberValue(), theEcorePackage.getEString(), "numberValue", null, 0, 1, Expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExpression_StringValue(), theEcorePackage.getEString(), "stringValue", null, 0, 1, Expression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(calculationEClass, Calculation.class, "Calculation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCalculation_Left(), this.getExpression(), null, "left", null, 0, 1, Calculation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCalculation_Right(), this.getExpression(), null, "right", null, 0, 1, Calculation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conjunctionEClass, Conjunction.class, "Conjunction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(disjunctionEClass, Disjunction.class, "Disjunction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(conditionalExpressionEClass, ConditionalExpression.class, "ConditionalExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConditionalExpression_Operator(), this.getOperatorEnum(), "operator", null, 0, 1, ConditionalExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(trendIconEnumEEnum, TrendIconEnum.class, "TrendIconEnum");
		addEEnumLiteral(trendIconEnumEEnum, TrendIconEnum.RISING);
		addEEnumLiteral(trendIconEnumEEnum, TrendIconEnum.BADRISING);
		addEEnumLiteral(trendIconEnumEEnum, TrendIconEnum.SLOPING);
		addEEnumLiteral(trendIconEnumEEnum, TrendIconEnum.GOODSLOPING);
		addEEnumLiteral(trendIconEnumEEnum, TrendIconEnum.STAGNATING);

		initEEnum(operatorEnumEEnum, OperatorEnum.class, "OperatorEnum");
		addEEnumLiteral(operatorEnumEEnum, OperatorEnum.EQUALS);
		addEEnumLiteral(operatorEnumEEnum, OperatorEnum.LESS);
		addEEnumLiteral(operatorEnumEEnum, OperatorEnum.GREATER);
		addEEnumLiteral(operatorEnumEEnum, OperatorEnum.LESSEQUAL);
		addEEnumLiteral(operatorEnumEEnum, OperatorEnum.GREATEREQUAL);
		addEEnumLiteral(operatorEnumEEnum, OperatorEnum.LIKE);

		initEEnum(rowHeaderModeEEnum, RowHeaderMode.class, "RowHeaderMode");
		addEEnumLiteral(rowHeaderModeEEnum, RowHeaderMode.HIDDEN);
		addEEnumLiteral(rowHeaderModeEEnum, RowHeaderMode.EXPLICIT_DEFAULTS_ID);
		addEEnumLiteral(rowHeaderModeEEnum, RowHeaderMode.INDEX);

		// Initialize data types
		initEDataType(internalEObjectEDataType, InternalEObject.class, "InternalEObject", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "rootPackage", "tabledsl"
		   });
	}

} //TableDSLPackageImpl
