/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.osbp.xtext.table.TableNumberInterval;
import org.eclipse.osbp.xtext.table.TableRangeElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table Number Interval</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableNumberIntervalImpl#getNumberIntervalValue <em>Number Interval Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableNumberIntervalImpl#getNumberRange <em>Number Range</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TableNumberIntervalImpl extends TableLazyResolverImpl implements TableNumberInterval {
	/**
	 * The default value of the '{@link #getNumberIntervalValue() <em>Number Interval Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberIntervalValue()
	 * @generated
	 * @ordered
	 */
	protected static final double NUMBER_INTERVAL_VALUE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getNumberIntervalValue() <em>Number Interval Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberIntervalValue()
	 * @generated
	 * @ordered
	 */
	protected double numberIntervalValue = NUMBER_INTERVAL_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNumberRange() <em>Number Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberRange()
	 * @generated
	 * @ordered
	 */
	protected TableRangeElement numberRange;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableNumberIntervalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TableDSLPackage.Literals.TABLE_NUMBER_INTERVAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getNumberIntervalValue() {
		return numberIntervalValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumberIntervalValue(double newNumberIntervalValue) {
		double oldNumberIntervalValue = numberIntervalValue;
		numberIntervalValue = newNumberIntervalValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_INTERVAL_VALUE, oldNumberIntervalValue, numberIntervalValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableRangeElement getNumberRange() {
		return numberRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNumberRange(TableRangeElement newNumberRange, NotificationChain msgs) {
		TableRangeElement oldNumberRange = numberRange;
		numberRange = newNumberRange;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_RANGE, oldNumberRange, newNumberRange);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumberRange(TableRangeElement newNumberRange) {
		if (newNumberRange != numberRange) {
			NotificationChain msgs = null;
			if (numberRange != null)
				msgs = ((InternalEObject)numberRange).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_RANGE, null, msgs);
			if (newNumberRange != null)
				msgs = ((InternalEObject)newNumberRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_RANGE, null, msgs);
			msgs = basicSetNumberRange(newNumberRange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_RANGE, newNumberRange, newNumberRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_RANGE:
				return basicSetNumberRange(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_INTERVAL_VALUE:
				return getNumberIntervalValue();
			case TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_RANGE:
				return getNumberRange();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_INTERVAL_VALUE:
				setNumberIntervalValue((Double)newValue);
				return;
			case TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_RANGE:
				setNumberRange((TableRangeElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_INTERVAL_VALUE:
				setNumberIntervalValue(NUMBER_INTERVAL_VALUE_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_RANGE:
				setNumberRange((TableRangeElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_INTERVAL_VALUE:
				return numberIntervalValue != NUMBER_INTERVAL_VALUE_EDEFAULT;
			case TableDSLPackage.TABLE_NUMBER_INTERVAL__NUMBER_RANGE:
				return numberRange != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (numberIntervalValue: ");
		result.append(numberIntervalValue);
		result.append(')');
		return result.toString();
	}

} //TableNumberIntervalImpl
