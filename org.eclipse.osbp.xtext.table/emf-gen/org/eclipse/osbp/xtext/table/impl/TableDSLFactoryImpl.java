/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.osbp.xtext.table.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TableDSLFactoryImpl extends EFactoryImpl implements TableDSLFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TableDSLFactory init() {
		try {
			TableDSLFactory theTableDSLFactory = (TableDSLFactory)EPackage.Registry.INSTANCE.getEFactory(TableDSLPackage.eNS_URI);
			if (theTableDSLFactory != null) {
				return theTableDSLFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TableDSLFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableDSLFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TableDSLPackage.TABLE_MODEL: return createTableModel();
			case TableDSLPackage.TABLE_LAZY_RESOLVER: return createTableLazyResolver();
			case TableDSLPackage.TABLE_PACKAGE: return createTablePackage();
			case TableDSLPackage.TABLE_BASE: return createTableBase();
			case TableDSLPackage.TABLE: return createTable();
			case TableDSLPackage.TABLE_SELECTION: return createTableSelection();
			case TableDSLPackage.TABLE_TABLE: return createTableTable();
			case TableDSLPackage.TABLE_GRID: return createTableGrid();
			case TableDSLPackage.TABLE_DTO_DATASOURCE: return createTableDtoDatasource();
			case TableDSLPackage.TABLE_GRID_PROPERTY: return createTableGridProperty();
			case TableDSLPackage.TABLE_PREORDER: return createTablePreorder();
			case TableDSLPackage.TABLE_VALUE: return createTableValue();
			case TableDSLPackage.TABLE_FORMATTER: return createTableFormatter();
			case TableDSLPackage.TABLE_IMAGE: return createTableImage();
			case TableDSLPackage.TABLE_TOOLTIP_PATTERN: return createTableTooltipPattern();
			case TableDSLPackage.TABLE_TASK: return createTableTask();
			case TableDSLPackage.TABLE_ALL_COLUMNS: return createTableAllColumns();
			case TableDSLPackage.TABLE_ORDINAL: return createTableOrdinal();
			case TableDSLPackage.TABLE_COLUMN: return createTableColumn();
			case TableDSLPackage.TABLE_MEASURE: return createTableMeasure();
			case TableDSLPackage.TABLE_DERIVED: return createTableDerived();
			case TableDSLPackage.TABLE_HIERARCHY: return createTableHierarchy();
			case TableDSLPackage.TABLE_ATTRIBUTE: return createTableAttribute();
			case TableDSLPackage.TABLE_AGGREGATION: return createTableAggregation();
			case TableDSLPackage.TABLE_NUMBER_INTERVAL: return createTableNumberInterval();
			case TableDSLPackage.TABLE_INT_INTERVAL: return createTableIntInterval();
			case TableDSLPackage.TABLE_DATE_DAY_INTERVAL: return createTableDateDayInterval();
			case TableDSLPackage.TABLE_NUMBER_LOOKUP: return createTableNumberLookup();
			case TableDSLPackage.TABLE_INT_LOOKUP: return createTableIntLookup();
			case TableDSLPackage.TABLE_STRING_LOOKUP: return createTableStringLookup();
			case TableDSLPackage.TABLE_DATE_DAY_LOOKUP: return createTableDateDayLookup();
			case TableDSLPackage.TABLE_DATAMART: return createTableDatamart();
			case TableDSLPackage.TABLE_AXIS: return createTableAxis();
			case TableDSLPackage.TABLE_EVENT: return createTableEvent();
			case TableDSLPackage.TABLE_TEXT_COLOR: return createTableTextColor();
			case TableDSLPackage.TABLE_CELL_COLOR: return createTableCellColor();
			case TableDSLPackage.TABLE_ICON: return createTableIcon();
			case TableDSLPackage.TABLE_TREND: return createTableTrend();
			case TableDSLPackage.TABLE_TOOLTIP: return createTableTooltip();
			case TableDSLPackage.EXPRESSION: return createExpression();
			case TableDSLPackage.CALCULATION: return createCalculation();
			case TableDSLPackage.CONJUNCTION: return createConjunction();
			case TableDSLPackage.DISJUNCTION: return createDisjunction();
			case TableDSLPackage.CONDITIONAL_EXPRESSION: return createConditionalExpression();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case TableDSLPackage.TREND_ICON_ENUM:
				return createTrendIconEnumFromString(eDataType, initialValue);
			case TableDSLPackage.OPERATOR_ENUM:
				return createOperatorEnumFromString(eDataType, initialValue);
			case TableDSLPackage.ROW_HEADER_MODE:
				return createRowHeaderModeFromString(eDataType, initialValue);
			case TableDSLPackage.INTERNAL_EOBJECT:
				return createInternalEObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case TableDSLPackage.TREND_ICON_ENUM:
				return convertTrendIconEnumToString(eDataType, instanceValue);
			case TableDSLPackage.OPERATOR_ENUM:
				return convertOperatorEnumToString(eDataType, instanceValue);
			case TableDSLPackage.ROW_HEADER_MODE:
				return convertRowHeaderModeToString(eDataType, instanceValue);
			case TableDSLPackage.INTERNAL_EOBJECT:
				return convertInternalEObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableModel createTableModel() {
		TableModelImpl tableModel = new TableModelImpl();
		return tableModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableLazyResolver createTableLazyResolver() {
		TableLazyResolverImpl tableLazyResolver = new TableLazyResolverImpl();
		return tableLazyResolver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TablePackage createTablePackage() {
		TablePackageImpl tablePackage = new TablePackageImpl();
		return tablePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableBase createTableBase() {
		TableBaseImpl tableBase = new TableBaseImpl();
		return tableBase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Table createTable() {
		TableImpl table = new TableImpl();
		return table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableSelection createTableSelection() {
		TableSelectionImpl tableSelection = new TableSelectionImpl();
		return tableSelection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableTable createTableTable() {
		TableTableImpl tableTable = new TableTableImpl();
		return tableTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableGrid createTableGrid() {
		TableGridImpl tableGrid = new TableGridImpl();
		return tableGrid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableDtoDatasource createTableDtoDatasource() {
		TableDtoDatasourceImpl tableDtoDatasource = new TableDtoDatasourceImpl();
		return tableDtoDatasource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableGridProperty createTableGridProperty() {
		TableGridPropertyImpl tableGridProperty = new TableGridPropertyImpl();
		return tableGridProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TablePreorder createTablePreorder() {
		TablePreorderImpl tablePreorder = new TablePreorderImpl();
		return tablePreorder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableValue createTableValue() {
		TableValueImpl tableValue = new TableValueImpl();
		return tableValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableFormatter createTableFormatter() {
		TableFormatterImpl tableFormatter = new TableFormatterImpl();
		return tableFormatter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableImage createTableImage() {
		TableImageImpl tableImage = new TableImageImpl();
		return tableImage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableTooltipPattern createTableTooltipPattern() {
		TableTooltipPatternImpl tableTooltipPattern = new TableTooltipPatternImpl();
		return tableTooltipPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableTask createTableTask() {
		TableTaskImpl tableTask = new TableTaskImpl();
		return tableTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableAllColumns createTableAllColumns() {
		TableAllColumnsImpl tableAllColumns = new TableAllColumnsImpl();
		return tableAllColumns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableOrdinal createTableOrdinal() {
		TableOrdinalImpl tableOrdinal = new TableOrdinalImpl();
		return tableOrdinal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableColumn createTableColumn() {
		TableColumnImpl tableColumn = new TableColumnImpl();
		return tableColumn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableMeasure createTableMeasure() {
		TableMeasureImpl tableMeasure = new TableMeasureImpl();
		return tableMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableDerived createTableDerived() {
		TableDerivedImpl tableDerived = new TableDerivedImpl();
		return tableDerived;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableHierarchy createTableHierarchy() {
		TableHierarchyImpl tableHierarchy = new TableHierarchyImpl();
		return tableHierarchy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableAttribute createTableAttribute() {
		TableAttributeImpl tableAttribute = new TableAttributeImpl();
		return tableAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableAggregation createTableAggregation() {
		TableAggregationImpl tableAggregation = new TableAggregationImpl();
		return tableAggregation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableNumberInterval createTableNumberInterval() {
		TableNumberIntervalImpl tableNumberInterval = new TableNumberIntervalImpl();
		return tableNumberInterval;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableIntInterval createTableIntInterval() {
		TableIntIntervalImpl tableIntInterval = new TableIntIntervalImpl();
		return tableIntInterval;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableDateDayInterval createTableDateDayInterval() {
		TableDateDayIntervalImpl tableDateDayInterval = new TableDateDayIntervalImpl();
		return tableDateDayInterval;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableNumberLookup createTableNumberLookup() {
		TableNumberLookupImpl tableNumberLookup = new TableNumberLookupImpl();
		return tableNumberLookup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableIntLookup createTableIntLookup() {
		TableIntLookupImpl tableIntLookup = new TableIntLookupImpl();
		return tableIntLookup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableStringLookup createTableStringLookup() {
		TableStringLookupImpl tableStringLookup = new TableStringLookupImpl();
		return tableStringLookup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableDateDayLookup createTableDateDayLookup() {
		TableDateDayLookupImpl tableDateDayLookup = new TableDateDayLookupImpl();
		return tableDateDayLookup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableDatamart createTableDatamart() {
		TableDatamartImpl tableDatamart = new TableDatamartImpl();
		return tableDatamart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableAxis createTableAxis() {
		TableAxisImpl tableAxis = new TableAxisImpl();
		return tableAxis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableEvent createTableEvent() {
		TableEventImpl tableEvent = new TableEventImpl();
		return tableEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableTextColor createTableTextColor() {
		TableTextColorImpl tableTextColor = new TableTextColorImpl();
		return tableTextColor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableCellColor createTableCellColor() {
		TableCellColorImpl tableCellColor = new TableCellColorImpl();
		return tableCellColor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableIcon createTableIcon() {
		TableIconImpl tableIcon = new TableIconImpl();
		return tableIcon;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableTrend createTableTrend() {
		TableTrendImpl tableTrend = new TableTrendImpl();
		return tableTrend;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableTooltip createTableTooltip() {
		TableTooltipImpl tableTooltip = new TableTooltipImpl();
		return tableTooltip;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expression createExpression() {
		ExpressionImpl expression = new ExpressionImpl();
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Calculation createCalculation() {
		CalculationImpl calculation = new CalculationImpl();
		return calculation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Conjunction createConjunction() {
		ConjunctionImpl conjunction = new ConjunctionImpl();
		return conjunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Disjunction createDisjunction() {
		DisjunctionImpl disjunction = new DisjunctionImpl();
		return disjunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionalExpression createConditionalExpression() {
		ConditionalExpressionImpl conditionalExpression = new ConditionalExpressionImpl();
		return conditionalExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrendIconEnum createTrendIconEnumFromString(EDataType eDataType, String initialValue) {
		TrendIconEnum result = TrendIconEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTrendIconEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorEnum createOperatorEnumFromString(EDataType eDataType, String initialValue) {
		OperatorEnum result = OperatorEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOperatorEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RowHeaderMode createRowHeaderModeFromString(EDataType eDataType, String initialValue) {
		RowHeaderMode result = RowHeaderMode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRowHeaderModeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalEObject createInternalEObjectFromString(EDataType eDataType, String initialValue) {
		return (InternalEObject)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInternalEObjectToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableDSLPackage getTableDSLPackage() {
		return (TableDSLPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TableDSLPackage getPackage() {
		return TableDSLPackage.eINSTANCE;
	}

} //TableDSLFactoryImpl
