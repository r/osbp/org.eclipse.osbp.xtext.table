/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.osbp.xtext.table.TableDateDayInterval;
import org.eclipse.osbp.xtext.table.TableRangeElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table Date Day Interval</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableDateDayIntervalImpl#getDateIntervalValue <em>Date Interval Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableDateDayIntervalImpl#getDateRange <em>Date Range</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TableDateDayIntervalImpl extends TableLazyResolverImpl implements TableDateDayInterval {
	/**
	 * The default value of the '{@link #getDateIntervalValue() <em>Date Interval Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateIntervalValue()
	 * @generated
	 * @ordered
	 */
	protected static final int DATE_INTERVAL_VALUE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDateIntervalValue() <em>Date Interval Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateIntervalValue()
	 * @generated
	 * @ordered
	 */
	protected int dateIntervalValue = DATE_INTERVAL_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDateRange() <em>Date Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateRange()
	 * @generated
	 * @ordered
	 */
	protected TableRangeElement dateRange;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableDateDayIntervalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TableDSLPackage.Literals.TABLE_DATE_DAY_INTERVAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDateIntervalValue() {
		return dateIntervalValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDateIntervalValue(int newDateIntervalValue) {
		int oldDateIntervalValue = dateIntervalValue;
		dateIntervalValue = newDateIntervalValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_INTERVAL_VALUE, oldDateIntervalValue, dateIntervalValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableRangeElement getDateRange() {
		return dateRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDateRange(TableRangeElement newDateRange, NotificationChain msgs) {
		TableRangeElement oldDateRange = dateRange;
		dateRange = newDateRange;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_RANGE, oldDateRange, newDateRange);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDateRange(TableRangeElement newDateRange) {
		if (newDateRange != dateRange) {
			NotificationChain msgs = null;
			if (dateRange != null)
				msgs = ((InternalEObject)dateRange).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_RANGE, null, msgs);
			if (newDateRange != null)
				msgs = ((InternalEObject)newDateRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_RANGE, null, msgs);
			msgs = basicSetDateRange(newDateRange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_RANGE, newDateRange, newDateRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_RANGE:
				return basicSetDateRange(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_INTERVAL_VALUE:
				return getDateIntervalValue();
			case TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_RANGE:
				return getDateRange();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_INTERVAL_VALUE:
				setDateIntervalValue((Integer)newValue);
				return;
			case TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_RANGE:
				setDateRange((TableRangeElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_INTERVAL_VALUE:
				setDateIntervalValue(DATE_INTERVAL_VALUE_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_RANGE:
				setDateRange((TableRangeElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_INTERVAL_VALUE:
				return dateIntervalValue != DATE_INTERVAL_VALUE_EDEFAULT;
			case TableDSLPackage.TABLE_DATE_DAY_INTERVAL__DATE_RANGE:
				return dateRange != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dateIntervalValue: ");
		result.append(dateIntervalValue);
		result.append(')');
		return result.toString();
	}

} //TableDateDayIntervalImpl
