/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.dsl.semantic.dto.LDto;

import org.eclipse.osbp.infogrid.model.gridsource.impl.CxGridSourceImpl;

import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.osbp.xtext.table.TableDtoDatasource;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table Dto Datasource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableDtoDatasourceImpl#getDtoSource <em>Dto Source</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TableDtoDatasourceImpl extends CxGridSourceImpl implements TableDtoDatasource {
	/**
	 * The cached value of the '{@link #getDtoSource() <em>Dto Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDtoSource()
	 * @generated
	 * @ordered
	 */
	protected LDto dtoSource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableDtoDatasourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TableDSLPackage.Literals.TABLE_DTO_DATASOURCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDto getDtoSource() {
		if (dtoSource != null && dtoSource.eIsProxy()) {
			InternalEObject oldDtoSource = (InternalEObject)dtoSource;
			dtoSource = (LDto)eResolveProxy(oldDtoSource);
			if (dtoSource != oldDtoSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TableDSLPackage.TABLE_DTO_DATASOURCE__DTO_SOURCE, oldDtoSource, dtoSource));
			}
		}
		return dtoSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LDto basicGetDtoSource() {
		return dtoSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDtoSource(LDto newDtoSource) {
		LDto oldDtoSource = dtoSource;
		dtoSource = newDtoSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_DTO_DATASOURCE__DTO_SOURCE, oldDtoSource, dtoSource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DTO_DATASOURCE__DTO_SOURCE:
				if (resolve) return getDtoSource();
				return basicGetDtoSource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DTO_DATASOURCE__DTO_SOURCE:
				setDtoSource((LDto)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DTO_DATASOURCE__DTO_SOURCE:
				setDtoSource((LDto)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DTO_DATASOURCE__DTO_SOURCE:
				return dtoSource != null;
		}
		return super.eIsSet(featureID);
	}

} //TableDtoDatasourceImpl
