/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.datamartdsl.DatamartDefinition;

import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.osbp.xtext.table.TableDatamart;
import org.eclipse.osbp.xtext.table.TableElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table Datamart</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableDatamartImpl#getDatamartRef <em>Datamart Ref</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableDatamartImpl#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TableDatamartImpl extends TableLazyResolverImpl implements TableDatamart {
	/**
	 * The cached value of the '{@link #getDatamartRef() <em>Datamart Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatamartRef()
	 * @generated
	 * @ordered
	 */
	protected DatamartDefinition datamartRef;

	/**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
	protected EList<TableElement> elements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableDatamartImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TableDSLPackage.Literals.TABLE_DATAMART;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatamartDefinition getDatamartRef() {
		if (datamartRef != null && datamartRef.eIsProxy()) {
			InternalEObject oldDatamartRef = (InternalEObject)datamartRef;
			datamartRef = (DatamartDefinition)eResolveProxy(oldDatamartRef);
			if (datamartRef != oldDatamartRef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TableDSLPackage.TABLE_DATAMART__DATAMART_REF, oldDatamartRef, datamartRef));
			}
		}
		return datamartRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatamartDefinition basicGetDatamartRef() {
		return datamartRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDatamartRef(DatamartDefinition newDatamartRef) {
		DatamartDefinition oldDatamartRef = datamartRef;
		datamartRef = newDatamartRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_DATAMART__DATAMART_REF, oldDatamartRef, datamartRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TableElement> getElements() {
		if (elements == null) {
			elements = new EObjectContainmentEList<TableElement>(TableElement.class, this, TableDSLPackage.TABLE_DATAMART__ELEMENTS);
		}
		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DATAMART__ELEMENTS:
				return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DATAMART__DATAMART_REF:
				if (resolve) return getDatamartRef();
				return basicGetDatamartRef();
			case TableDSLPackage.TABLE_DATAMART__ELEMENTS:
				return getElements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DATAMART__DATAMART_REF:
				setDatamartRef((DatamartDefinition)newValue);
				return;
			case TableDSLPackage.TABLE_DATAMART__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends TableElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DATAMART__DATAMART_REF:
				setDatamartRef((DatamartDefinition)null);
				return;
			case TableDSLPackage.TABLE_DATAMART__ELEMENTS:
				getElements().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_DATAMART__DATAMART_REF:
				return datamartRef != null;
			case TableDSLPackage.TABLE_DATAMART__ELEMENTS:
				return elements != null && !elements.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TableDatamartImpl
