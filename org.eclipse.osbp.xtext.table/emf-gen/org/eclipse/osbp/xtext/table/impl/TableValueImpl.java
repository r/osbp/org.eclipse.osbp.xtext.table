/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.osbp.xtext.table.TableFormatter;
import org.eclipse.osbp.xtext.table.TableImage;
import org.eclipse.osbp.xtext.table.TableInterval;
import org.eclipse.osbp.xtext.table.TableLookup;
import org.eclipse.osbp.xtext.table.TableTooltipPattern;
import org.eclipse.osbp.xtext.table.TableValue;
import org.eclipse.osbp.xtext.table.TableValueElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl#getColumn <em>Column</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl#isCollapsed <em>Collapsed</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl#getFormatter <em>Formatter</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl#getTooltipPattern <em>Tooltip Pattern</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl#isHideLabelInterval <em>Hide Label Interval</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl#isHideLabelLookup <em>Hide Label Lookup</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl#getIntervals <em>Intervals</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl#getLookups <em>Lookups</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl#isHasImage <em>Has Image</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl#getImage <em>Image</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableValueImpl#getIconName <em>Icon Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TableValueImpl extends TableLazyResolverImpl implements TableValue {
	/**
	 * The cached value of the '{@link #getColumn() <em>Column</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn()
	 * @generated
	 * @ordered
	 */
	protected TableValueElement column;

	/**
	 * The default value of the '{@link #isCollapsed() <em>Collapsed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCollapsed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COLLAPSED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCollapsed() <em>Collapsed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCollapsed()
	 * @generated
	 * @ordered
	 */
	protected boolean collapsed = COLLAPSED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFormatter() <em>Formatter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormatter()
	 * @generated
	 * @ordered
	 */
	protected TableFormatter formatter;

	/**
	 * The cached value of the '{@link #getTooltipPattern() <em>Tooltip Pattern</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTooltipPattern()
	 * @generated
	 * @ordered
	 */
	protected TableTooltipPattern tooltipPattern;

	/**
	 * The default value of the '{@link #isHideLabelInterval() <em>Hide Label Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHideLabelInterval()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HIDE_LABEL_INTERVAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHideLabelInterval() <em>Hide Label Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHideLabelInterval()
	 * @generated
	 * @ordered
	 */
	protected boolean hideLabelInterval = HIDE_LABEL_INTERVAL_EDEFAULT;

	/**
	 * The default value of the '{@link #isHideLabelLookup() <em>Hide Label Lookup</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHideLabelLookup()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HIDE_LABEL_LOOKUP_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHideLabelLookup() <em>Hide Label Lookup</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHideLabelLookup()
	 * @generated
	 * @ordered
	 */
	protected boolean hideLabelLookup = HIDE_LABEL_LOOKUP_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIntervals() <em>Intervals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntervals()
	 * @generated
	 * @ordered
	 */
	protected EList<TableInterval> intervals;

	/**
	 * The cached value of the '{@link #getLookups() <em>Lookups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLookups()
	 * @generated
	 * @ordered
	 */
	protected EList<TableLookup> lookups;

	/**
	 * The default value of the '{@link #isHasImage() <em>Has Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasImage()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_IMAGE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasImage() <em>Has Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasImage()
	 * @generated
	 * @ordered
	 */
	protected boolean hasImage = HAS_IMAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getImage() <em>Image</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImage()
	 * @generated
	 * @ordered
	 */
	protected TableImage image;

	/**
	 * The default value of the '{@link #getIconName() <em>Icon Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIconName()
	 * @generated
	 * @ordered
	 */
	protected static final String ICON_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIconName() <em>Icon Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIconName()
	 * @generated
	 * @ordered
	 */
	protected String iconName = ICON_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TableDSLPackage.Literals.TABLE_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableValueElement getColumn() {
		return column;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetColumn(TableValueElement newColumn, NotificationChain msgs) {
		TableValueElement oldColumn = column;
		column = newColumn;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__COLUMN, oldColumn, newColumn);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColumn(TableValueElement newColumn) {
		if (newColumn != column) {
			NotificationChain msgs = null;
			if (column != null)
				msgs = ((InternalEObject)column).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_VALUE__COLUMN, null, msgs);
			if (newColumn != null)
				msgs = ((InternalEObject)newColumn).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_VALUE__COLUMN, null, msgs);
			msgs = basicSetColumn(newColumn, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__COLUMN, newColumn, newColumn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCollapsed() {
		return collapsed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollapsed(boolean newCollapsed) {
		boolean oldCollapsed = collapsed;
		collapsed = newCollapsed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__COLLAPSED, oldCollapsed, collapsed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableFormatter getFormatter() {
		return formatter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormatter(TableFormatter newFormatter, NotificationChain msgs) {
		TableFormatter oldFormatter = formatter;
		formatter = newFormatter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__FORMATTER, oldFormatter, newFormatter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormatter(TableFormatter newFormatter) {
		if (newFormatter != formatter) {
			NotificationChain msgs = null;
			if (formatter != null)
				msgs = ((InternalEObject)formatter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_VALUE__FORMATTER, null, msgs);
			if (newFormatter != null)
				msgs = ((InternalEObject)newFormatter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_VALUE__FORMATTER, null, msgs);
			msgs = basicSetFormatter(newFormatter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__FORMATTER, newFormatter, newFormatter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableTooltipPattern getTooltipPattern() {
		return tooltipPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTooltipPattern(TableTooltipPattern newTooltipPattern, NotificationChain msgs) {
		TableTooltipPattern oldTooltipPattern = tooltipPattern;
		tooltipPattern = newTooltipPattern;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__TOOLTIP_PATTERN, oldTooltipPattern, newTooltipPattern);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTooltipPattern(TableTooltipPattern newTooltipPattern) {
		if (newTooltipPattern != tooltipPattern) {
			NotificationChain msgs = null;
			if (tooltipPattern != null)
				msgs = ((InternalEObject)tooltipPattern).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_VALUE__TOOLTIP_PATTERN, null, msgs);
			if (newTooltipPattern != null)
				msgs = ((InternalEObject)newTooltipPattern).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_VALUE__TOOLTIP_PATTERN, null, msgs);
			msgs = basicSetTooltipPattern(newTooltipPattern, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__TOOLTIP_PATTERN, newTooltipPattern, newTooltipPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHideLabelInterval() {
		return hideLabelInterval;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHideLabelInterval(boolean newHideLabelInterval) {
		boolean oldHideLabelInterval = hideLabelInterval;
		hideLabelInterval = newHideLabelInterval;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__HIDE_LABEL_INTERVAL, oldHideLabelInterval, hideLabelInterval));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHideLabelLookup() {
		return hideLabelLookup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHideLabelLookup(boolean newHideLabelLookup) {
		boolean oldHideLabelLookup = hideLabelLookup;
		hideLabelLookup = newHideLabelLookup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__HIDE_LABEL_LOOKUP, oldHideLabelLookup, hideLabelLookup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TableInterval> getIntervals() {
		if (intervals == null) {
			intervals = new EObjectContainmentEList<TableInterval>(TableInterval.class, this, TableDSLPackage.TABLE_VALUE__INTERVALS);
		}
		return intervals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TableLookup> getLookups() {
		if (lookups == null) {
			lookups = new EObjectContainmentEList<TableLookup>(TableLookup.class, this, TableDSLPackage.TABLE_VALUE__LOOKUPS);
		}
		return lookups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasImage() {
		return hasImage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasImage(boolean newHasImage) {
		boolean oldHasImage = hasImage;
		hasImage = newHasImage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__HAS_IMAGE, oldHasImage, hasImage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableImage getImage() {
		return image;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImage(TableImage newImage, NotificationChain msgs) {
		TableImage oldImage = image;
		image = newImage;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__IMAGE, oldImage, newImage);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImage(TableImage newImage) {
		if (newImage != image) {
			NotificationChain msgs = null;
			if (image != null)
				msgs = ((InternalEObject)image).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_VALUE__IMAGE, null, msgs);
			if (newImage != null)
				msgs = ((InternalEObject)newImage).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_VALUE__IMAGE, null, msgs);
			msgs = basicSetImage(newImage, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__IMAGE, newImage, newImage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIconName() {
		return iconName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIconName(String newIconName) {
		String oldIconName = iconName;
		iconName = newIconName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_VALUE__ICON_NAME, oldIconName, iconName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TableDSLPackage.TABLE_VALUE__COLUMN:
				return basicSetColumn(null, msgs);
			case TableDSLPackage.TABLE_VALUE__FORMATTER:
				return basicSetFormatter(null, msgs);
			case TableDSLPackage.TABLE_VALUE__TOOLTIP_PATTERN:
				return basicSetTooltipPattern(null, msgs);
			case TableDSLPackage.TABLE_VALUE__INTERVALS:
				return ((InternalEList<?>)getIntervals()).basicRemove(otherEnd, msgs);
			case TableDSLPackage.TABLE_VALUE__LOOKUPS:
				return ((InternalEList<?>)getLookups()).basicRemove(otherEnd, msgs);
			case TableDSLPackage.TABLE_VALUE__IMAGE:
				return basicSetImage(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TableDSLPackage.TABLE_VALUE__COLUMN:
				return getColumn();
			case TableDSLPackage.TABLE_VALUE__COLLAPSED:
				return isCollapsed();
			case TableDSLPackage.TABLE_VALUE__FORMATTER:
				return getFormatter();
			case TableDSLPackage.TABLE_VALUE__TOOLTIP_PATTERN:
				return getTooltipPattern();
			case TableDSLPackage.TABLE_VALUE__HIDE_LABEL_INTERVAL:
				return isHideLabelInterval();
			case TableDSLPackage.TABLE_VALUE__HIDE_LABEL_LOOKUP:
				return isHideLabelLookup();
			case TableDSLPackage.TABLE_VALUE__INTERVALS:
				return getIntervals();
			case TableDSLPackage.TABLE_VALUE__LOOKUPS:
				return getLookups();
			case TableDSLPackage.TABLE_VALUE__HAS_IMAGE:
				return isHasImage();
			case TableDSLPackage.TABLE_VALUE__IMAGE:
				return getImage();
			case TableDSLPackage.TABLE_VALUE__ICON_NAME:
				return getIconName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TableDSLPackage.TABLE_VALUE__COLUMN:
				setColumn((TableValueElement)newValue);
				return;
			case TableDSLPackage.TABLE_VALUE__COLLAPSED:
				setCollapsed((Boolean)newValue);
				return;
			case TableDSLPackage.TABLE_VALUE__FORMATTER:
				setFormatter((TableFormatter)newValue);
				return;
			case TableDSLPackage.TABLE_VALUE__TOOLTIP_PATTERN:
				setTooltipPattern((TableTooltipPattern)newValue);
				return;
			case TableDSLPackage.TABLE_VALUE__HIDE_LABEL_INTERVAL:
				setHideLabelInterval((Boolean)newValue);
				return;
			case TableDSLPackage.TABLE_VALUE__HIDE_LABEL_LOOKUP:
				setHideLabelLookup((Boolean)newValue);
				return;
			case TableDSLPackage.TABLE_VALUE__INTERVALS:
				getIntervals().clear();
				getIntervals().addAll((Collection<? extends TableInterval>)newValue);
				return;
			case TableDSLPackage.TABLE_VALUE__LOOKUPS:
				getLookups().clear();
				getLookups().addAll((Collection<? extends TableLookup>)newValue);
				return;
			case TableDSLPackage.TABLE_VALUE__HAS_IMAGE:
				setHasImage((Boolean)newValue);
				return;
			case TableDSLPackage.TABLE_VALUE__IMAGE:
				setImage((TableImage)newValue);
				return;
			case TableDSLPackage.TABLE_VALUE__ICON_NAME:
				setIconName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_VALUE__COLUMN:
				setColumn((TableValueElement)null);
				return;
			case TableDSLPackage.TABLE_VALUE__COLLAPSED:
				setCollapsed(COLLAPSED_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_VALUE__FORMATTER:
				setFormatter((TableFormatter)null);
				return;
			case TableDSLPackage.TABLE_VALUE__TOOLTIP_PATTERN:
				setTooltipPattern((TableTooltipPattern)null);
				return;
			case TableDSLPackage.TABLE_VALUE__HIDE_LABEL_INTERVAL:
				setHideLabelInterval(HIDE_LABEL_INTERVAL_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_VALUE__HIDE_LABEL_LOOKUP:
				setHideLabelLookup(HIDE_LABEL_LOOKUP_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_VALUE__INTERVALS:
				getIntervals().clear();
				return;
			case TableDSLPackage.TABLE_VALUE__LOOKUPS:
				getLookups().clear();
				return;
			case TableDSLPackage.TABLE_VALUE__HAS_IMAGE:
				setHasImage(HAS_IMAGE_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_VALUE__IMAGE:
				setImage((TableImage)null);
				return;
			case TableDSLPackage.TABLE_VALUE__ICON_NAME:
				setIconName(ICON_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_VALUE__COLUMN:
				return column != null;
			case TableDSLPackage.TABLE_VALUE__COLLAPSED:
				return collapsed != COLLAPSED_EDEFAULT;
			case TableDSLPackage.TABLE_VALUE__FORMATTER:
				return formatter != null;
			case TableDSLPackage.TABLE_VALUE__TOOLTIP_PATTERN:
				return tooltipPattern != null;
			case TableDSLPackage.TABLE_VALUE__HIDE_LABEL_INTERVAL:
				return hideLabelInterval != HIDE_LABEL_INTERVAL_EDEFAULT;
			case TableDSLPackage.TABLE_VALUE__HIDE_LABEL_LOOKUP:
				return hideLabelLookup != HIDE_LABEL_LOOKUP_EDEFAULT;
			case TableDSLPackage.TABLE_VALUE__INTERVALS:
				return intervals != null && !intervals.isEmpty();
			case TableDSLPackage.TABLE_VALUE__LOOKUPS:
				return lookups != null && !lookups.isEmpty();
			case TableDSLPackage.TABLE_VALUE__HAS_IMAGE:
				return hasImage != HAS_IMAGE_EDEFAULT;
			case TableDSLPackage.TABLE_VALUE__IMAGE:
				return image != null;
			case TableDSLPackage.TABLE_VALUE__ICON_NAME:
				return ICON_NAME_EDEFAULT == null ? iconName != null : !ICON_NAME_EDEFAULT.equals(iconName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (collapsed: ");
		result.append(collapsed);
		result.append(", hideLabelInterval: ");
		result.append(hideLabelInterval);
		result.append(", hideLabelLookup: ");
		result.append(hideLabelLookup);
		result.append(", hasImage: ");
		result.append(hasImage);
		result.append(", iconName: ");
		result.append(iconName);
		result.append(')');
		return result.toString();
	}

} //TableValueImpl
