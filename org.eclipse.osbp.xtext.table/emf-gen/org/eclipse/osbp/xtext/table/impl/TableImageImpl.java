/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.osbp.xtext.table.TableImage;
import org.eclipse.osbp.xtext.table.TableValueElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table Image</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableImageImpl#getImagePathPattern <em>Image Path Pattern</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableImageImpl#isHideImageLabel <em>Hide Image Label</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableImageImpl#isHasParameter <em>Has Parameter</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableImageImpl#isResize <em>Resize</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableImageImpl#getResizeString <em>Resize String</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableImageImpl#getImagePathParameter <em>Image Path Parameter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TableImageImpl extends TableLazyResolverImpl implements TableImage {
	/**
	 * The default value of the '{@link #getImagePathPattern() <em>Image Path Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImagePathPattern()
	 * @generated
	 * @ordered
	 */
	protected static final String IMAGE_PATH_PATTERN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getImagePathPattern() <em>Image Path Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImagePathPattern()
	 * @generated
	 * @ordered
	 */
	protected String imagePathPattern = IMAGE_PATH_PATTERN_EDEFAULT;

	/**
	 * The default value of the '{@link #isHideImageLabel() <em>Hide Image Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHideImageLabel()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HIDE_IMAGE_LABEL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHideImageLabel() <em>Hide Image Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHideImageLabel()
	 * @generated
	 * @ordered
	 */
	protected boolean hideImageLabel = HIDE_IMAGE_LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #isHasParameter() <em>Has Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasParameter()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_PARAMETER_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasParameter() <em>Has Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasParameter()
	 * @generated
	 * @ordered
	 */
	protected boolean hasParameter = HAS_PARAMETER_EDEFAULT;

	/**
	 * The default value of the '{@link #isResize() <em>Resize</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResize()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RESIZE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isResize() <em>Resize</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isResize()
	 * @generated
	 * @ordered
	 */
	protected boolean resize = RESIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getResizeString() <em>Resize String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResizeString()
	 * @generated
	 * @ordered
	 */
	protected static final String RESIZE_STRING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResizeString() <em>Resize String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResizeString()
	 * @generated
	 * @ordered
	 */
	protected String resizeString = RESIZE_STRING_EDEFAULT;

	/**
	 * The cached value of the '{@link #getImagePathParameter() <em>Image Path Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImagePathParameter()
	 * @generated
	 * @ordered
	 */
	protected TableValueElement imagePathParameter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableImageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TableDSLPackage.Literals.TABLE_IMAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImagePathPattern() {
		return imagePathPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImagePathPattern(String newImagePathPattern) {
		String oldImagePathPattern = imagePathPattern;
		imagePathPattern = newImagePathPattern;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PATTERN, oldImagePathPattern, imagePathPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHideImageLabel() {
		return hideImageLabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHideImageLabel(boolean newHideImageLabel) {
		boolean oldHideImageLabel = hideImageLabel;
		hideImageLabel = newHideImageLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_IMAGE__HIDE_IMAGE_LABEL, oldHideImageLabel, hideImageLabel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasParameter() {
		return hasParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasParameter(boolean newHasParameter) {
		boolean oldHasParameter = hasParameter;
		hasParameter = newHasParameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_IMAGE__HAS_PARAMETER, oldHasParameter, hasParameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isResize() {
		return resize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResize(boolean newResize) {
		boolean oldResize = resize;
		resize = newResize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_IMAGE__RESIZE, oldResize, resize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getResizeString() {
		return resizeString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResizeString(String newResizeString) {
		String oldResizeString = resizeString;
		resizeString = newResizeString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_IMAGE__RESIZE_STRING, oldResizeString, resizeString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableValueElement getImagePathParameter() {
		return imagePathParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImagePathParameter(TableValueElement newImagePathParameter, NotificationChain msgs) {
		TableValueElement oldImagePathParameter = imagePathParameter;
		imagePathParameter = newImagePathParameter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PARAMETER, oldImagePathParameter, newImagePathParameter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImagePathParameter(TableValueElement newImagePathParameter) {
		if (newImagePathParameter != imagePathParameter) {
			NotificationChain msgs = null;
			if (imagePathParameter != null)
				msgs = ((InternalEObject)imagePathParameter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PARAMETER, null, msgs);
			if (newImagePathParameter != null)
				msgs = ((InternalEObject)newImagePathParameter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PARAMETER, null, msgs);
			msgs = basicSetImagePathParameter(newImagePathParameter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PARAMETER, newImagePathParameter, newImagePathParameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PARAMETER:
				return basicSetImagePathParameter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PATTERN:
				return getImagePathPattern();
			case TableDSLPackage.TABLE_IMAGE__HIDE_IMAGE_LABEL:
				return isHideImageLabel();
			case TableDSLPackage.TABLE_IMAGE__HAS_PARAMETER:
				return isHasParameter();
			case TableDSLPackage.TABLE_IMAGE__RESIZE:
				return isResize();
			case TableDSLPackage.TABLE_IMAGE__RESIZE_STRING:
				return getResizeString();
			case TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PARAMETER:
				return getImagePathParameter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PATTERN:
				setImagePathPattern((String)newValue);
				return;
			case TableDSLPackage.TABLE_IMAGE__HIDE_IMAGE_LABEL:
				setHideImageLabel((Boolean)newValue);
				return;
			case TableDSLPackage.TABLE_IMAGE__HAS_PARAMETER:
				setHasParameter((Boolean)newValue);
				return;
			case TableDSLPackage.TABLE_IMAGE__RESIZE:
				setResize((Boolean)newValue);
				return;
			case TableDSLPackage.TABLE_IMAGE__RESIZE_STRING:
				setResizeString((String)newValue);
				return;
			case TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PARAMETER:
				setImagePathParameter((TableValueElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PATTERN:
				setImagePathPattern(IMAGE_PATH_PATTERN_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_IMAGE__HIDE_IMAGE_LABEL:
				setHideImageLabel(HIDE_IMAGE_LABEL_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_IMAGE__HAS_PARAMETER:
				setHasParameter(HAS_PARAMETER_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_IMAGE__RESIZE:
				setResize(RESIZE_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_IMAGE__RESIZE_STRING:
				setResizeString(RESIZE_STRING_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PARAMETER:
				setImagePathParameter((TableValueElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PATTERN:
				return IMAGE_PATH_PATTERN_EDEFAULT == null ? imagePathPattern != null : !IMAGE_PATH_PATTERN_EDEFAULT.equals(imagePathPattern);
			case TableDSLPackage.TABLE_IMAGE__HIDE_IMAGE_LABEL:
				return hideImageLabel != HIDE_IMAGE_LABEL_EDEFAULT;
			case TableDSLPackage.TABLE_IMAGE__HAS_PARAMETER:
				return hasParameter != HAS_PARAMETER_EDEFAULT;
			case TableDSLPackage.TABLE_IMAGE__RESIZE:
				return resize != RESIZE_EDEFAULT;
			case TableDSLPackage.TABLE_IMAGE__RESIZE_STRING:
				return RESIZE_STRING_EDEFAULT == null ? resizeString != null : !RESIZE_STRING_EDEFAULT.equals(resizeString);
			case TableDSLPackage.TABLE_IMAGE__IMAGE_PATH_PARAMETER:
				return imagePathParameter != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (imagePathPattern: ");
		result.append(imagePathPattern);
		result.append(", hideImageLabel: ");
		result.append(hideImageLabel);
		result.append(", hasParameter: ");
		result.append(hasParameter);
		result.append(", resize: ");
		result.append(resize);
		result.append(", resizeString: ");
		result.append(resizeString);
		result.append(')');
		return result.toString();
	}

} //TableImageImpl
