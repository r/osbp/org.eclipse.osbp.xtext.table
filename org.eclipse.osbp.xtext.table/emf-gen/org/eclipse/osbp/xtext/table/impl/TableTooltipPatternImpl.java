/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.osbp.xtext.table.TableTooltipPattern;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table Tooltip Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableTooltipPatternImpl#getTooltipPattern <em>Tooltip Pattern</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TableTooltipPatternImpl extends TableLazyResolverImpl implements TableTooltipPattern {
	/**
	 * The default value of the '{@link #getTooltipPattern() <em>Tooltip Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTooltipPattern()
	 * @generated
	 * @ordered
	 */
	protected static final String TOOLTIP_PATTERN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTooltipPattern() <em>Tooltip Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTooltipPattern()
	 * @generated
	 * @ordered
	 */
	protected String tooltipPattern = TOOLTIP_PATTERN_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableTooltipPatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TableDSLPackage.Literals.TABLE_TOOLTIP_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTooltipPattern() {
		return tooltipPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTooltipPattern(String newTooltipPattern) {
		String oldTooltipPattern = tooltipPattern;
		tooltipPattern = newTooltipPattern;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_TOOLTIP_PATTERN__TOOLTIP_PATTERN, oldTooltipPattern, tooltipPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TableDSLPackage.TABLE_TOOLTIP_PATTERN__TOOLTIP_PATTERN:
				return getTooltipPattern();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TableDSLPackage.TABLE_TOOLTIP_PATTERN__TOOLTIP_PATTERN:
				setTooltipPattern((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_TOOLTIP_PATTERN__TOOLTIP_PATTERN:
				setTooltipPattern(TOOLTIP_PATTERN_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_TOOLTIP_PATTERN__TOOLTIP_PATTERN:
				return TOOLTIP_PATTERN_EDEFAULT == null ? tooltipPattern != null : !TOOLTIP_PATTERN_EDEFAULT.equals(tooltipPattern);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tooltipPattern: ");
		result.append(tooltipPattern);
		result.append(')');
		return result.toString();
	}

} //TableTooltipPatternImpl
