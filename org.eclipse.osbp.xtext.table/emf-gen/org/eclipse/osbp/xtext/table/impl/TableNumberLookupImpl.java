/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.osbp.xtext.table.TableNumberLookup;
import org.eclipse.osbp.xtext.table.TableRangeElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table Number Lookup</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableNumberLookupImpl#getLookupValue <em>Lookup Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableNumberLookupImpl#getDiscrete <em>Discrete</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TableNumberLookupImpl extends TableLazyResolverImpl implements TableNumberLookup {
	/**
	 * The default value of the '{@link #getLookupValue() <em>Lookup Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLookupValue()
	 * @generated
	 * @ordered
	 */
	protected static final double LOOKUP_VALUE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getLookupValue() <em>Lookup Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLookupValue()
	 * @generated
	 * @ordered
	 */
	protected double lookupValue = LOOKUP_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDiscrete() <em>Discrete</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscrete()
	 * @generated
	 * @ordered
	 */
	protected TableRangeElement discrete;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableNumberLookupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TableDSLPackage.Literals.TABLE_NUMBER_LOOKUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLookupValue() {
		return lookupValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLookupValue(double newLookupValue) {
		double oldLookupValue = lookupValue;
		lookupValue = newLookupValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_NUMBER_LOOKUP__LOOKUP_VALUE, oldLookupValue, lookupValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableRangeElement getDiscrete() {
		return discrete;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscrete(TableRangeElement newDiscrete, NotificationChain msgs) {
		TableRangeElement oldDiscrete = discrete;
		discrete = newDiscrete;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_NUMBER_LOOKUP__DISCRETE, oldDiscrete, newDiscrete);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscrete(TableRangeElement newDiscrete) {
		if (newDiscrete != discrete) {
			NotificationChain msgs = null;
			if (discrete != null)
				msgs = ((InternalEObject)discrete).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_NUMBER_LOOKUP__DISCRETE, null, msgs);
			if (newDiscrete != null)
				msgs = ((InternalEObject)newDiscrete).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_NUMBER_LOOKUP__DISCRETE, null, msgs);
			msgs = basicSetDiscrete(newDiscrete, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_NUMBER_LOOKUP__DISCRETE, newDiscrete, newDiscrete));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TableDSLPackage.TABLE_NUMBER_LOOKUP__DISCRETE:
				return basicSetDiscrete(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TableDSLPackage.TABLE_NUMBER_LOOKUP__LOOKUP_VALUE:
				return getLookupValue();
			case TableDSLPackage.TABLE_NUMBER_LOOKUP__DISCRETE:
				return getDiscrete();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TableDSLPackage.TABLE_NUMBER_LOOKUP__LOOKUP_VALUE:
				setLookupValue((Double)newValue);
				return;
			case TableDSLPackage.TABLE_NUMBER_LOOKUP__DISCRETE:
				setDiscrete((TableRangeElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_NUMBER_LOOKUP__LOOKUP_VALUE:
				setLookupValue(LOOKUP_VALUE_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_NUMBER_LOOKUP__DISCRETE:
				setDiscrete((TableRangeElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_NUMBER_LOOKUP__LOOKUP_VALUE:
				return lookupValue != LOOKUP_VALUE_EDEFAULT;
			case TableDSLPackage.TABLE_NUMBER_LOOKUP__DISCRETE:
				return discrete != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (lookupValue: ");
		result.append(lookupValue);
		result.append(')');
		return result.toString();
	}

} //TableNumberLookupImpl
