/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.datamartdsl.AxisEnum;

import org.eclipse.osbp.xtext.table.TableAxis;
import org.eclipse.osbp.xtext.table.TableDSLPackage;
import org.eclipse.osbp.xtext.table.TableEvent;
import org.eclipse.osbp.xtext.table.TablePreorder;
import org.eclipse.osbp.xtext.table.TableValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table Axis</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableAxisImpl#getAxis <em>Axis</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableAxisImpl#isHasRowHeight <em>Has Row Height</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableAxisImpl#getRowHeight <em>Row Height</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableAxisImpl#getPreOrder <em>Pre Order</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableAxisImpl#isHasDetails <em>Has Details</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableAxisImpl#getValues <em>Values</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableAxisImpl#isHasEvents <em>Has Events</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.impl.TableAxisImpl#getEvents <em>Events</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TableAxisImpl extends TableLazyResolverImpl implements TableAxis {
	/**
	 * The default value of the '{@link #getAxis() <em>Axis</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxis()
	 * @generated
	 * @ordered
	 */
	protected static final AxisEnum AXIS_EDEFAULT = AxisEnum.DEFAULT;

	/**
	 * The cached value of the '{@link #getAxis() <em>Axis</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxis()
	 * @generated
	 * @ordered
	 */
	protected AxisEnum axis = AXIS_EDEFAULT;

	/**
	 * The default value of the '{@link #isHasRowHeight() <em>Has Row Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasRowHeight()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_ROW_HEIGHT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasRowHeight() <em>Has Row Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasRowHeight()
	 * @generated
	 * @ordered
	 */
	protected boolean hasRowHeight = HAS_ROW_HEIGHT_EDEFAULT;

	/**
	 * The default value of the '{@link #getRowHeight() <em>Row Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRowHeight()
	 * @generated
	 * @ordered
	 */
	protected static final String ROW_HEIGHT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRowHeight() <em>Row Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRowHeight()
	 * @generated
	 * @ordered
	 */
	protected String rowHeight = ROW_HEIGHT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPreOrder() <em>Pre Order</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreOrder()
	 * @generated
	 * @ordered
	 */
	protected TablePreorder preOrder;

	/**
	 * The default value of the '{@link #isHasDetails() <em>Has Details</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasDetails()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_DETAILS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasDetails() <em>Has Details</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasDetails()
	 * @generated
	 * @ordered
	 */
	protected boolean hasDetails = HAS_DETAILS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValues() <em>Values</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValues()
	 * @generated
	 * @ordered
	 */
	protected EList<TableValue> values;

	/**
	 * The default value of the '{@link #isHasEvents() <em>Has Events</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasEvents()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_EVENTS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasEvents() <em>Has Events</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasEvents()
	 * @generated
	 * @ordered
	 */
	protected boolean hasEvents = HAS_EVENTS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<TableEvent> events;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableAxisImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TableDSLPackage.Literals.TABLE_AXIS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AxisEnum getAxis() {
		return axis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAxis(AxisEnum newAxis) {
		AxisEnum oldAxis = axis;
		axis = newAxis == null ? AXIS_EDEFAULT : newAxis;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_AXIS__AXIS, oldAxis, axis));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasRowHeight() {
		return hasRowHeight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasRowHeight(boolean newHasRowHeight) {
		boolean oldHasRowHeight = hasRowHeight;
		hasRowHeight = newHasRowHeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_AXIS__HAS_ROW_HEIGHT, oldHasRowHeight, hasRowHeight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRowHeight() {
		return rowHeight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRowHeight(String newRowHeight) {
		String oldRowHeight = rowHeight;
		rowHeight = newRowHeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_AXIS__ROW_HEIGHT, oldRowHeight, rowHeight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TablePreorder getPreOrder() {
		return preOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreOrder(TablePreorder newPreOrder, NotificationChain msgs) {
		TablePreorder oldPreOrder = preOrder;
		preOrder = newPreOrder;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_AXIS__PRE_ORDER, oldPreOrder, newPreOrder);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreOrder(TablePreorder newPreOrder) {
		if (newPreOrder != preOrder) {
			NotificationChain msgs = null;
			if (preOrder != null)
				msgs = ((InternalEObject)preOrder).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_AXIS__PRE_ORDER, null, msgs);
			if (newPreOrder != null)
				msgs = ((InternalEObject)newPreOrder).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TableDSLPackage.TABLE_AXIS__PRE_ORDER, null, msgs);
			msgs = basicSetPreOrder(newPreOrder, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_AXIS__PRE_ORDER, newPreOrder, newPreOrder));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasDetails() {
		return hasDetails;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasDetails(boolean newHasDetails) {
		boolean oldHasDetails = hasDetails;
		hasDetails = newHasDetails;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_AXIS__HAS_DETAILS, oldHasDetails, hasDetails));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TableValue> getValues() {
		if (values == null) {
			values = new EObjectContainmentEList<TableValue>(TableValue.class, this, TableDSLPackage.TABLE_AXIS__VALUES);
		}
		return values;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasEvents() {
		return hasEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasEvents(boolean newHasEvents) {
		boolean oldHasEvents = hasEvents;
		hasEvents = newHasEvents;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TableDSLPackage.TABLE_AXIS__HAS_EVENTS, oldHasEvents, hasEvents));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TableEvent> getEvents() {
		if (events == null) {
			events = new EObjectContainmentEList<TableEvent>(TableEvent.class, this, TableDSLPackage.TABLE_AXIS__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TableDSLPackage.TABLE_AXIS__PRE_ORDER:
				return basicSetPreOrder(null, msgs);
			case TableDSLPackage.TABLE_AXIS__VALUES:
				return ((InternalEList<?>)getValues()).basicRemove(otherEnd, msgs);
			case TableDSLPackage.TABLE_AXIS__EVENTS:
				return ((InternalEList<?>)getEvents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TableDSLPackage.TABLE_AXIS__AXIS:
				return getAxis();
			case TableDSLPackage.TABLE_AXIS__HAS_ROW_HEIGHT:
				return isHasRowHeight();
			case TableDSLPackage.TABLE_AXIS__ROW_HEIGHT:
				return getRowHeight();
			case TableDSLPackage.TABLE_AXIS__PRE_ORDER:
				return getPreOrder();
			case TableDSLPackage.TABLE_AXIS__HAS_DETAILS:
				return isHasDetails();
			case TableDSLPackage.TABLE_AXIS__VALUES:
				return getValues();
			case TableDSLPackage.TABLE_AXIS__HAS_EVENTS:
				return isHasEvents();
			case TableDSLPackage.TABLE_AXIS__EVENTS:
				return getEvents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TableDSLPackage.TABLE_AXIS__AXIS:
				setAxis((AxisEnum)newValue);
				return;
			case TableDSLPackage.TABLE_AXIS__HAS_ROW_HEIGHT:
				setHasRowHeight((Boolean)newValue);
				return;
			case TableDSLPackage.TABLE_AXIS__ROW_HEIGHT:
				setRowHeight((String)newValue);
				return;
			case TableDSLPackage.TABLE_AXIS__PRE_ORDER:
				setPreOrder((TablePreorder)newValue);
				return;
			case TableDSLPackage.TABLE_AXIS__HAS_DETAILS:
				setHasDetails((Boolean)newValue);
				return;
			case TableDSLPackage.TABLE_AXIS__VALUES:
				getValues().clear();
				getValues().addAll((Collection<? extends TableValue>)newValue);
				return;
			case TableDSLPackage.TABLE_AXIS__HAS_EVENTS:
				setHasEvents((Boolean)newValue);
				return;
			case TableDSLPackage.TABLE_AXIS__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends TableEvent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_AXIS__AXIS:
				setAxis(AXIS_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_AXIS__HAS_ROW_HEIGHT:
				setHasRowHeight(HAS_ROW_HEIGHT_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_AXIS__ROW_HEIGHT:
				setRowHeight(ROW_HEIGHT_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_AXIS__PRE_ORDER:
				setPreOrder((TablePreorder)null);
				return;
			case TableDSLPackage.TABLE_AXIS__HAS_DETAILS:
				setHasDetails(HAS_DETAILS_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_AXIS__VALUES:
				getValues().clear();
				return;
			case TableDSLPackage.TABLE_AXIS__HAS_EVENTS:
				setHasEvents(HAS_EVENTS_EDEFAULT);
				return;
			case TableDSLPackage.TABLE_AXIS__EVENTS:
				getEvents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TableDSLPackage.TABLE_AXIS__AXIS:
				return axis != AXIS_EDEFAULT;
			case TableDSLPackage.TABLE_AXIS__HAS_ROW_HEIGHT:
				return hasRowHeight != HAS_ROW_HEIGHT_EDEFAULT;
			case TableDSLPackage.TABLE_AXIS__ROW_HEIGHT:
				return ROW_HEIGHT_EDEFAULT == null ? rowHeight != null : !ROW_HEIGHT_EDEFAULT.equals(rowHeight);
			case TableDSLPackage.TABLE_AXIS__PRE_ORDER:
				return preOrder != null;
			case TableDSLPackage.TABLE_AXIS__HAS_DETAILS:
				return hasDetails != HAS_DETAILS_EDEFAULT;
			case TableDSLPackage.TABLE_AXIS__VALUES:
				return values != null && !values.isEmpty();
			case TableDSLPackage.TABLE_AXIS__HAS_EVENTS:
				return hasEvents != HAS_EVENTS_EDEFAULT;
			case TableDSLPackage.TABLE_AXIS__EVENTS:
				return events != null && !events.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (axis: ");
		result.append(axis);
		result.append(", hasRowHeight: ");
		result.append(hasRowHeight);
		result.append(", rowHeight: ");
		result.append(rowHeight);
		result.append(", hasDetails: ");
		result.append(hasDetails);
		result.append(", hasEvents: ");
		result.append(hasEvents);
		result.append(')');
		return result.toString();
	}

} //TableAxisImpl
