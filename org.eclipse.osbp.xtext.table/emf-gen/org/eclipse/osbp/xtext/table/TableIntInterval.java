/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Int Interval</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableIntInterval#getIntIntervalValue <em>Int Interval Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableIntInterval#getIntRange <em>Int Range</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableIntInterval()
 * @model
 * @generated
 */
public interface TableIntInterval extends TableInterval {
	/**
	 * Returns the value of the '<em><b>Int Interval Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Int Interval Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Int Interval Value</em>' attribute.
	 * @see #setIntIntervalValue(int)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableIntInterval_IntIntervalValue()
	 * @model unique="false"
	 * @generated
	 */
	int getIntIntervalValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableIntInterval#getIntIntervalValue <em>Int Interval Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Int Interval Value</em>' attribute.
	 * @see #getIntIntervalValue()
	 * @generated
	 */
	void setIntIntervalValue(int value);

	/**
	 * Returns the value of the '<em><b>Int Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Int Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Int Range</em>' containment reference.
	 * @see #setIntRange(TableRangeElement)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableIntInterval_IntRange()
	 * @model containment="true"
	 * @generated
	 */
	TableRangeElement getIntRange();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableIntInterval#getIntRange <em>Int Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Int Range</em>' containment reference.
	 * @see #getIntRange()
	 * @generated
	 */
	void setIntRange(TableRangeElement value);

} // TableIntInterval
