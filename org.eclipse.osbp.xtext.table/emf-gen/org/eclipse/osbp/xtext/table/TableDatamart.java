/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;

import org.eclipse.emf.common.util.EList;

import org.eclipse.osbp.xtext.datamartdsl.DatamartDefinition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Datamart</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableDatamart#getDatamartRef <em>Datamart Ref</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableDatamart#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableDatamart()
 * @model
 * @generated
 */
public interface TableDatamart extends TableLazyResolver {
	/**
	 * Returns the value of the '<em><b>Datamart Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datamart Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datamart Ref</em>' reference.
	 * @see #setDatamartRef(DatamartDefinition)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableDatamart_DatamartRef()
	 * @model
	 * @generated
	 */
	DatamartDefinition getDatamartRef();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableDatamart#getDatamartRef <em>Datamart Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datamart Ref</em>' reference.
	 * @see #getDatamartRef()
	 * @generated
	 */
	void setDatamartRef(DatamartDefinition value);

	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.table.TableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableDatamart_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<TableElement> getElements();

} // TableDatamart
