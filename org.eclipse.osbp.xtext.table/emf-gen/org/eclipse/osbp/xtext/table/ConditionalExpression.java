/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conditional Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.ConditionalExpression#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getConditionalExpression()
 * @model
 * @generated
 */
public interface ConditionalExpression extends Calculation {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.table.OperatorEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see org.eclipse.osbp.xtext.table.OperatorEnum
	 * @see #setOperator(OperatorEnum)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getConditionalExpression_Operator()
	 * @model unique="false"
	 * @generated
	 */
	OperatorEnum getOperator();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.ConditionalExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see org.eclipse.osbp.xtext.table.OperatorEnum
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(OperatorEnum value);

} // ConditionalExpression
