/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableValue#getColumn <em>Column</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableValue#isCollapsed <em>Collapsed</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableValue#getFormatter <em>Formatter</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableValue#getTooltipPattern <em>Tooltip Pattern</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableValue#isHideLabelInterval <em>Hide Label Interval</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableValue#isHideLabelLookup <em>Hide Label Lookup</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableValue#getIntervals <em>Intervals</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableValue#getLookups <em>Lookups</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableValue#isHasImage <em>Has Image</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableValue#getImage <em>Image</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableValue#getIconName <em>Icon Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableValue()
 * @model
 * @generated
 */
public interface TableValue extends TableLazyResolver {
	/**
	 * Returns the value of the '<em><b>Column</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column</em>' containment reference.
	 * @see #setColumn(TableValueElement)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableValue_Column()
	 * @model containment="true"
	 * @generated
	 */
	TableValueElement getColumn();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableValue#getColumn <em>Column</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Column</em>' containment reference.
	 * @see #getColumn()
	 * @generated
	 */
	void setColumn(TableValueElement value);

	/**
	 * Returns the value of the '<em><b>Collapsed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collapsed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collapsed</em>' attribute.
	 * @see #setCollapsed(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableValue_Collapsed()
	 * @model unique="false"
	 * @generated
	 */
	boolean isCollapsed();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableValue#isCollapsed <em>Collapsed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collapsed</em>' attribute.
	 * @see #isCollapsed()
	 * @generated
	 */
	void setCollapsed(boolean value);

	/**
	 * Returns the value of the '<em><b>Formatter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formatter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formatter</em>' containment reference.
	 * @see #setFormatter(TableFormatter)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableValue_Formatter()
	 * @model containment="true"
	 * @generated
	 */
	TableFormatter getFormatter();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableValue#getFormatter <em>Formatter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formatter</em>' containment reference.
	 * @see #getFormatter()
	 * @generated
	 */
	void setFormatter(TableFormatter value);

	/**
	 * Returns the value of the '<em><b>Tooltip Pattern</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tooltip Pattern</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tooltip Pattern</em>' containment reference.
	 * @see #setTooltipPattern(TableTooltipPattern)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableValue_TooltipPattern()
	 * @model containment="true"
	 * @generated
	 */
	TableTooltipPattern getTooltipPattern();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableValue#getTooltipPattern <em>Tooltip Pattern</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tooltip Pattern</em>' containment reference.
	 * @see #getTooltipPattern()
	 * @generated
	 */
	void setTooltipPattern(TableTooltipPattern value);

	/**
	 * Returns the value of the '<em><b>Hide Label Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hide Label Interval</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hide Label Interval</em>' attribute.
	 * @see #setHideLabelInterval(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableValue_HideLabelInterval()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHideLabelInterval();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableValue#isHideLabelInterval <em>Hide Label Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hide Label Interval</em>' attribute.
	 * @see #isHideLabelInterval()
	 * @generated
	 */
	void setHideLabelInterval(boolean value);

	/**
	 * Returns the value of the '<em><b>Hide Label Lookup</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hide Label Lookup</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hide Label Lookup</em>' attribute.
	 * @see #setHideLabelLookup(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableValue_HideLabelLookup()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHideLabelLookup();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableValue#isHideLabelLookup <em>Hide Label Lookup</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hide Label Lookup</em>' attribute.
	 * @see #isHideLabelLookup()
	 * @generated
	 */
	void setHideLabelLookup(boolean value);

	/**
	 * Returns the value of the '<em><b>Intervals</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.table.TableInterval}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intervals</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intervals</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableValue_Intervals()
	 * @model containment="true"
	 * @generated
	 */
	EList<TableInterval> getIntervals();

	/**
	 * Returns the value of the '<em><b>Lookups</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.table.TableLookup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lookups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lookups</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableValue_Lookups()
	 * @model containment="true"
	 * @generated
	 */
	EList<TableLookup> getLookups();

	/**
	 * Returns the value of the '<em><b>Has Image</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Image</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Image</em>' attribute.
	 * @see #setHasImage(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableValue_HasImage()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasImage();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableValue#isHasImage <em>Has Image</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Image</em>' attribute.
	 * @see #isHasImage()
	 * @generated
	 */
	void setHasImage(boolean value);

	/**
	 * Returns the value of the '<em><b>Image</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image</em>' containment reference.
	 * @see #setImage(TableImage)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableValue_Image()
	 * @model containment="true"
	 * @generated
	 */
	TableImage getImage();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableValue#getImage <em>Image</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image</em>' containment reference.
	 * @see #getImage()
	 * @generated
	 */
	void setImage(TableImage value);

	/**
	 * Returns the value of the '<em><b>Icon Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icon Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Icon Name</em>' attribute.
	 * @see #setIconName(String)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableValue_IconName()
	 * @model unique="false"
	 * @generated
	 */
	String getIconName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableValue#getIconName <em>Icon Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icon Name</em>' attribute.
	 * @see #getIconName()
	 * @generated
	 */
	void setIconName(String value);

} // TableValue
