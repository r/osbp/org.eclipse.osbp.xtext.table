/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Tooltip Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableTooltipPattern#getTooltipPattern <em>Tooltip Pattern</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableTooltipPattern()
 * @model
 * @generated
 */
public interface TableTooltipPattern extends TableLazyResolver {
	/**
	 * Returns the value of the '<em><b>Tooltip Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tooltip Pattern</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tooltip Pattern</em>' attribute.
	 * @see #setTooltipPattern(String)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableTooltipPattern_TooltipPattern()
	 * @model unique="false"
	 * @generated
	 */
	String getTooltipPattern();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableTooltipPattern#getTooltipPattern <em>Tooltip Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tooltip Pattern</em>' attribute.
	 * @see #getTooltipPattern()
	 * @generated
	 */
	void setTooltipPattern(String value);

} // TableTooltipPattern
