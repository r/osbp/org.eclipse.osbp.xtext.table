/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;

import org.eclipse.osbp.xtext.action.ActionToolbar;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Option</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableOption#isFiltering <em>Filtering</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableOption#getToolbar <em>Toolbar</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableOption#isEmbedded <em>Embedded</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableOption()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface TableOption extends TableLazyResolver {
	/**
	 * Returns the value of the '<em><b>Filtering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filtering</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filtering</em>' attribute.
	 * @see #setFiltering(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableOption_Filtering()
	 * @model unique="false"
	 * @generated
	 */
	boolean isFiltering();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableOption#isFiltering <em>Filtering</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filtering</em>' attribute.
	 * @see #isFiltering()
	 * @generated
	 */
	void setFiltering(boolean value);

	/**
	 * Returns the value of the '<em><b>Toolbar</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Toolbar</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Toolbar</em>' reference.
	 * @see #setToolbar(ActionToolbar)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableOption_Toolbar()
	 * @model
	 * @generated
	 */
	ActionToolbar getToolbar();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableOption#getToolbar <em>Toolbar</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Toolbar</em>' reference.
	 * @see #getToolbar()
	 * @generated
	 */
	void setToolbar(ActionToolbar value);

	/**
	 * Returns the value of the '<em><b>Embedded</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Embedded</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Embedded</em>' attribute.
	 * @see #setEmbedded(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableOption_Embedded()
	 * @model unique="false"
	 * @generated
	 */
	boolean isEmbedded();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableOption#isEmbedded <em>Embedded</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Embedded</em>' attribute.
	 * @see #isEmbedded()
	 * @generated
	 */
	void setEmbedded(boolean value);

} // TableOption
