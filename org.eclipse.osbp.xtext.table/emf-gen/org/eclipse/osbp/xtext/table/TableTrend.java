/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Trend</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableTrend#getIcon <em>Icon</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableTrend()
 * @model
 * @generated
 */
public interface TableTrend extends TableRangeElement {
	/**
	 * Returns the value of the '<em><b>Icon</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.table.TrendIconEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icon</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Icon</em>' attribute.
	 * @see org.eclipse.osbp.xtext.table.TrendIconEnum
	 * @see #setIcon(TrendIconEnum)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableTrend_Icon()
	 * @model unique="false"
	 * @generated
	 */
	TrendIconEnum getIcon();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableTrend#getIcon <em>Icon</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icon</em>' attribute.
	 * @see org.eclipse.osbp.xtext.table.TrendIconEnum
	 * @see #getIcon()
	 * @generated
	 */
	void setIcon(TrendIconEnum value);

} // TableTrend
