/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Grid</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableGrid#isSelectalways <em>Selectalways</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableGrid#getHeaderMode <em>Header Mode</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableGrid#getSource <em>Source</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableGrid()
 * @model
 * @generated
 */
public interface TableGrid extends TableOption {
	/**
	 * Returns the value of the '<em><b>Selectalways</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selectalways</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selectalways</em>' attribute.
	 * @see #setSelectalways(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableGrid_Selectalways()
	 * @model unique="false"
	 * @generated
	 */
	boolean isSelectalways();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableGrid#isSelectalways <em>Selectalways</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selectalways</em>' attribute.
	 * @see #isSelectalways()
	 * @generated
	 */
	void setSelectalways(boolean value);

	/**
	 * Returns the value of the '<em><b>Header Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.table.RowHeaderMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header Mode</em>' attribute.
	 * @see org.eclipse.osbp.xtext.table.RowHeaderMode
	 * @see #setHeaderMode(RowHeaderMode)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableGrid_HeaderMode()
	 * @model unique="false"
	 * @generated
	 */
	RowHeaderMode getHeaderMode();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableGrid#getHeaderMode <em>Header Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Header Mode</em>' attribute.
	 * @see org.eclipse.osbp.xtext.table.RowHeaderMode
	 * @see #getHeaderMode()
	 * @generated
	 */
	void setHeaderMode(RowHeaderMode value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' containment reference.
	 * @see #setSource(TableDtoDatasource)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableGrid_Source()
	 * @model containment="true"
	 * @generated
	 */
	TableDtoDatasource getSource();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableGrid#getSource <em>Source</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' containment reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(TableDtoDatasource value);

} // TableGrid
