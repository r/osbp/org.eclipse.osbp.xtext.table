/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableTable#isSelectIdOnly <em>Select Id Only</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableTable#isSelectById <em>Select By Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableTable#isSelectalways <em>Selectalways</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableTable#getHeaderMode <em>Header Mode</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableTable#getSource <em>Source</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableTable()
 * @model
 * @generated
 */
public interface TableTable extends TableOption {
	/**
	 * Returns the value of the '<em><b>Select Id Only</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select Id Only</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Select Id Only</em>' attribute.
	 * @see #setSelectIdOnly(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableTable_SelectIdOnly()
	 * @model unique="false"
	 * @generated
	 */
	boolean isSelectIdOnly();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableTable#isSelectIdOnly <em>Select Id Only</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Select Id Only</em>' attribute.
	 * @see #isSelectIdOnly()
	 * @generated
	 */
	void setSelectIdOnly(boolean value);

	/**
	 * Returns the value of the '<em><b>Select By Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select By Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Select By Id</em>' attribute.
	 * @see #setSelectById(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableTable_SelectById()
	 * @model unique="false"
	 * @generated
	 */
	boolean isSelectById();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableTable#isSelectById <em>Select By Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Select By Id</em>' attribute.
	 * @see #isSelectById()
	 * @generated
	 */
	void setSelectById(boolean value);

	/**
	 * Returns the value of the '<em><b>Selectalways</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selectalways</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selectalways</em>' attribute.
	 * @see #setSelectalways(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableTable_Selectalways()
	 * @model unique="false"
	 * @generated
	 */
	boolean isSelectalways();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableTable#isSelectalways <em>Selectalways</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selectalways</em>' attribute.
	 * @see #isSelectalways()
	 * @generated
	 */
	void setSelectalways(boolean value);

	/**
	 * Returns the value of the '<em><b>Header Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.table.RowHeaderMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header Mode</em>' attribute.
	 * @see org.eclipse.osbp.xtext.table.RowHeaderMode
	 * @see #setHeaderMode(RowHeaderMode)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableTable_HeaderMode()
	 * @model unique="false"
	 * @generated
	 */
	RowHeaderMode getHeaderMode();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableTable#getHeaderMode <em>Header Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Header Mode</em>' attribute.
	 * @see org.eclipse.osbp.xtext.table.RowHeaderMode
	 * @see #getHeaderMode()
	 * @generated
	 */
	void setHeaderMode(RowHeaderMode value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' containment reference.
	 * @see #setSource(TableDatamart)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableTable_Source()
	 * @model containment="true"
	 * @generated
	 */
	TableDatamart getSource();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableTable#getSource <em>Source</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' containment reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(TableDatamart value);

} // TableTable
