/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;

import org.eclipse.emf.common.util.EList;

import org.eclipse.osbp.xtext.datamartdsl.AxisEnum;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Axis</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableAxis#getAxis <em>Axis</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableAxis#isHasRowHeight <em>Has Row Height</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableAxis#getRowHeight <em>Row Height</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableAxis#getPreOrder <em>Pre Order</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableAxis#isHasDetails <em>Has Details</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableAxis#getValues <em>Values</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableAxis#isHasEvents <em>Has Events</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableAxis#getEvents <em>Events</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableAxis()
 * @model
 * @generated
 */
public interface TableAxis extends TableElement {
	/**
	 * Returns the value of the '<em><b>Axis</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.datamartdsl.AxisEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Axis</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Axis</em>' attribute.
	 * @see org.eclipse.osbp.xtext.datamartdsl.AxisEnum
	 * @see #setAxis(AxisEnum)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableAxis_Axis()
	 * @model unique="false"
	 * @generated
	 */
	AxisEnum getAxis();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableAxis#getAxis <em>Axis</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Axis</em>' attribute.
	 * @see org.eclipse.osbp.xtext.datamartdsl.AxisEnum
	 * @see #getAxis()
	 * @generated
	 */
	void setAxis(AxisEnum value);

	/**
	 * Returns the value of the '<em><b>Has Row Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Row Height</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Row Height</em>' attribute.
	 * @see #setHasRowHeight(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableAxis_HasRowHeight()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasRowHeight();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableAxis#isHasRowHeight <em>Has Row Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Row Height</em>' attribute.
	 * @see #isHasRowHeight()
	 * @generated
	 */
	void setHasRowHeight(boolean value);

	/**
	 * Returns the value of the '<em><b>Row Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Row Height</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Row Height</em>' attribute.
	 * @see #setRowHeight(String)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableAxis_RowHeight()
	 * @model unique="false"
	 * @generated
	 */
	String getRowHeight();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableAxis#getRowHeight <em>Row Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Row Height</em>' attribute.
	 * @see #getRowHeight()
	 * @generated
	 */
	void setRowHeight(String value);

	/**
	 * Returns the value of the '<em><b>Pre Order</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pre Order</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Order</em>' containment reference.
	 * @see #setPreOrder(TablePreorder)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableAxis_PreOrder()
	 * @model containment="true"
	 * @generated
	 */
	TablePreorder getPreOrder();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableAxis#getPreOrder <em>Pre Order</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Order</em>' containment reference.
	 * @see #getPreOrder()
	 * @generated
	 */
	void setPreOrder(TablePreorder value);

	/**
	 * Returns the value of the '<em><b>Has Details</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Details</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Details</em>' attribute.
	 * @see #setHasDetails(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableAxis_HasDetails()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasDetails();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableAxis#isHasDetails <em>Has Details</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Details</em>' attribute.
	 * @see #isHasDetails()
	 * @generated
	 */
	void setHasDetails(boolean value);

	/**
	 * Returns the value of the '<em><b>Values</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.table.TableValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableAxis_Values()
	 * @model containment="true"
	 * @generated
	 */
	EList<TableValue> getValues();

	/**
	 * Returns the value of the '<em><b>Has Events</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Events</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Events</em>' attribute.
	 * @see #setHasEvents(boolean)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableAxis_HasEvents()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasEvents();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableAxis#isHasEvents <em>Has Events</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Events</em>' attribute.
	 * @see #isHasEvents()
	 * @generated
	 */
	void setHasEvents(boolean value);

	/**
	 * Returns the value of the '<em><b>Events</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.table.TableEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableAxis_Events()
	 * @model containment="true"
	 * @generated
	 */
	EList<TableEvent> getEvents();

} // TableAxis
