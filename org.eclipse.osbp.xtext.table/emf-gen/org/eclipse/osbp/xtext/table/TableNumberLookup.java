/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 * 
 *  Contributors:  
 *  		Joerg Riegel - Initial implementation 
 *  
 */
package org.eclipse.osbp.xtext.table;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Number Lookup</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableNumberLookup#getLookupValue <em>Lookup Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.table.TableNumberLookup#getDiscrete <em>Discrete</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableNumberLookup()
 * @model
 * @generated
 */
public interface TableNumberLookup extends TableLookup {
	/**
	 * Returns the value of the '<em><b>Lookup Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lookup Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lookup Value</em>' attribute.
	 * @see #setLookupValue(double)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableNumberLookup_LookupValue()
	 * @model unique="false"
	 * @generated
	 */
	double getLookupValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableNumberLookup#getLookupValue <em>Lookup Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lookup Value</em>' attribute.
	 * @see #getLookupValue()
	 * @generated
	 */
	void setLookupValue(double value);

	/**
	 * Returns the value of the '<em><b>Discrete</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete</em>' containment reference.
	 * @see #setDiscrete(TableRangeElement)
	 * @see org.eclipse.osbp.xtext.table.TableDSLPackage#getTableNumberLookup_Discrete()
	 * @model containment="true"
	 * @generated
	 */
	TableRangeElement getDiscrete();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.table.TableNumberLookup#getDiscrete <em>Discrete</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete</em>' containment reference.
	 * @see #getDiscrete()
	 * @generated
	 */
	void setDiscrete(TableRangeElement value);

} // TableNumberLookup
