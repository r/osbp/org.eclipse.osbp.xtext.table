/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 * 
 */

package org.eclipse.osbp.xtext.table.scoping

import java.util.ArrayList
import javax.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.osbp.dsl.common.xtext.extensions.ModelExtensions
import org.eclipse.osbp.dsl.entity.xtext.extensions.EntityTypesBuilder
import org.eclipse.osbp.infogrid.model.gridsource.CxGridNestedField
import org.eclipse.osbp.infogrid.model.gridsource.CxGridNestedPath
import org.eclipse.osbp.infogrid.model.gridsource.CxGridProperty
import org.eclipse.osbp.infogrid.model.gridsource.CxGridSourcePackage
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropPriceStyle
import org.eclipse.osbp.infogrid.model.gridsource.style.CxGridPropQuantityStyle
import org.eclipse.osbp.xtext.action.ActionButton
import org.eclipse.osbp.xtext.action.ActionFunction
import org.eclipse.osbp.xtext.action.ActionTable
import org.eclipse.osbp.xtext.action.ActionTask
import org.eclipse.osbp.xtext.action.ActionToolbar
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity
import org.eclipse.osbp.xtext.cubedsl.CubeHierarchy
import org.eclipse.osbp.xtext.cubedsl.CubeLevel
import org.eclipse.osbp.xtext.datamartdsl.AxisEnum
import org.eclipse.osbp.xtext.datamartdsl.DatamartAttribute
import org.eclipse.osbp.xtext.datamartdsl.DatamartCube
import org.eclipse.osbp.xtext.datamartdsl.DatamartCubeAxis
import org.eclipse.osbp.xtext.datamartdsl.DatamartDerivedMeasure
import org.eclipse.osbp.xtext.datamartdsl.DatamartEntity
import org.eclipse.osbp.xtext.datamartdsl.DatamartHierarchy
import org.eclipse.osbp.xtext.datamartdsl.DatamartHierarchyLevelSingle
import org.eclipse.osbp.xtext.datamartdsl.DatamartMeasure
import org.eclipse.osbp.xtext.datamartdsl.DatamartMember
import org.eclipse.osbp.xtext.datamartdsl.DatamartOwner
import org.eclipse.osbp.xtext.datamartdsl.DatamartSetAggregation
import org.eclipse.osbp.xtext.datamartdsl.DatamartSetAggregationFunction
import org.eclipse.osbp.xtext.datamartdsl.DatamartTask
import org.eclipse.osbp.xtext.datamartdsl.jvmmodel.DatamartDSLJvmModelInferrer
import org.eclipse.osbp.xtext.datamartdsl.scoping.DatamartDSLScopeProvider
import org.eclipse.osbp.xtext.datamartdsl.util.DatamartAttributeUtil
import org.eclipse.osbp.xtext.gridsource.scoping.GridSourceScopeProvider
import org.eclipse.osbp.xtext.oxtype.resource.EcoreUtil3
import org.eclipse.osbp.xtext.table.TableAxis
import org.eclipse.osbp.xtext.table.TableDSLPackage
import org.eclipse.osbp.xtext.table.TableDatamart
import org.eclipse.osbp.xtext.table.TableDtoDatasource
import org.eclipse.osbp.xtext.table.TableOption
import org.eclipse.xtext.common.types.JvmType
import org.eclipse.xtext.common.types.util.TypeReferences
import org.eclipse.xtext.resource.EObjectDescription
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.impl.FilteringScope
import org.eclipse.xtext.scoping.impl.MapBasedScope
import org.eclipse.osbp.xtext.action.ActionCommand
import org.eclipse.osbp.xtext.action.ActionDatainterchange

class TableDSLScopeProvider extends AbstractTableDSLScopeProvider {
	@Inject extension ModelExtensions
	@Inject extension DatamartDSLScopeProvider datamartScopeProvider
	@Inject extension DatamartDSLJvmModelInferrer datamartInferrer
	@Inject GridSourceScopeProvider gridScopeProvider
	@Inject TypeReferences typeReferences

	@Override
	override IScope getScope(EObject context, EReference reference) {

		if (reference == CxGridSourcePackage.Literals.CX_GRID_NESTED_FIELD__FIELD) {
			return getScope_GridAttribute_path(context, reference)
		} else if (reference == CxGridSourcePackage.Literals.CX_GRID_NESTED_PATH__FIELD) {
			return getScope_GridAttribute_path(context, reference)
		} else if (reference == TableDSLPackage.Literals.TABLE_AXIS__AXIS) {
			return getScope_Table_Axis_axis(context)
		} else if (reference == TableDSLPackage.Literals.TABLE_MEASURE__VALUE_REF ||
			reference == TableDSLPackage.Literals.TABLE_DERIVED__VALUE_REF ||
			reference == TableDSLPackage.Literals.TABLE_ATTRIBUTE__VALUE_REF ||
			reference == TableDSLPackage.Literals.TABLE_COLUMN__VALUE_REF ||
			reference == TableDSLPackage.Literals.TABLE_HIERARCHY__VALUE_REF) {
				return getScope_Table_valueRef(context, reference)
			} else if (reference == TableDSLPackage.Literals.TABLE_AGGREGATION__VALUE_REF) {
				return getScope_Table_Aggregation_valueRef(context)
			} else if (reference == TableDSLPackage.Literals.TABLE_OPTION__TOOLBAR) {
				return getScope_Table_Toolbar(context as TableOption, reference)
			} else {
				return super.getScope(context, reference)
			}
		}

		def IScope getScope_Table_Toolbar(TableOption table, EReference reference) {
			var superScope = super.getScope(table, reference)
			return new FilteringScope(superScope, [
				if (it.EObjectOrProxy instanceof ActionToolbar) {
					var toolbar = it.EObjectOrProxy as ActionToolbar
					toolbar = EcoreUtil3.resolve(toolbar, table.eResource.resourceSet) as ActionToolbar
					return toolbar.filterToolbarScopeCondition
				}
				return false
			])
		}

		private def filterToolbarScopeCondition(ActionToolbar toolbar) {
			var allItemsOfTable = true
			if (toolbar !== null) {
				for (item : toolbar.actions) {
					if (item instanceof ActionButton) {
						if (!(item.command.actionType instanceof ActionTable ||
							item.command.actionType instanceof ActionFunction ||
							item.command.actionType instanceof ActionTask ||
							item.command.actionType instanceof ActionCommand ||
							item.command.actionType instanceof ActionDatainterchange)) {
								allItemsOfTable = false
							}
						}
					}
				}
				return allItemsOfTable
			}

			def IScope getScope_GridAttribute_path(EObject object, EReference reference) {
				switch (object) {
					CxGridProperty: {
						val CxGridProperty prop = object as CxGridProperty
						val ds = prop.eContainer as TableDtoDatasource
						if (ds !== null && ds.dtoSource !== null) {
							val JvmType type = typeReferences.findDeclaredType(ds.dtoSource.toQualifiedName, prop)
							return gridScopeProvider.toGetterScope(type)
						}
						return null
					}
					CxGridNestedField,
					CxGridNestedPath,
					CxGridPropPriceStyle,
					CxGridPropQuantityStyle: {
						if (object.eContainer instanceof CxGridProperty) {
							return object.eContainer.getScope_GridAttribute_path(reference)
						} else {
							if (object instanceof CxGridNestedField) {
								var CxGridNestedField containerField = object as CxGridNestedField
								// if (containerField!==null && containerField.field!==null && containerField.field.returnType!==null){
								return gridScopeProvider.toGetterScope(containerField?.field?.returnType?.type)
							// }
							} else if (object.eContainer instanceof CxGridNestedPath) {
								var CxGridNestedPath containerField = object as CxGridNestedPath
								// if (containerField!==null && containerField.field!==null && containerField.field.returnType!==null){
								return gridScopeProvider.toGetterScope(containerField?.field?.returnType?.type)
							// }
							} else if (object.eContainer instanceof CxGridNestedField) {
								var CxGridNestedField containerField = object.eContainer as CxGridNestedField
								// if (containerField!==null && containerField.field!==null && containerField.field.returnType!==null){
								return gridScopeProvider.toGetterScope(containerField?.field?.returnType?.type)
							// }
							}
							return null
						}
					}
				}
				return null
			}

			def IScope getScope_Table_Axis_axis(EObject object) {
				var result = <IEObjectDescription>newArrayList
				var eObj = object
				while (!(eObj instanceof TableDatamart)) {
					eObj = eObj.eContainer
				}
				if (eObj !== null) {
					var datamart = (eObj as TableDatamart).datamartRef
					if (datamart !== null && datamart.source !== null) {
						if (datamart.source instanceof DatamartCube) {
							for (axis : (datamart.source as DatamartCube).axisslicer) {
								if (axis instanceof DatamartCubeAxis) {
									var literal = (axis as DatamartCubeAxis).axis.name.literal
									result.add(EObjectDescription.create(literal, (axis as DatamartCubeAxis)))
								}
							}
						} else if (datamart.source instanceof DatamartEntity) {
							getEntityAxes(datamart.source as DatamartEntity, result)

							// user must not supply any axes, but we need it with tables
							if (!result.containsName(AxisEnum.COLUMNS.literal)) {
								result.add(
									EObjectDescription.create(AxisEnum.COLUMNS.literal,
										(datamart.source as DatamartEntity)))
							}
							if (!result.containsName(AxisEnum.ROWS.literal)) {
								result.add(
									EObjectDescription.create(AxisEnum.ROWS.literal,
										(datamart.source as DatamartEntity)))
								}
							} else if (datamart.source instanceof DatamartTask) {
								result.add(
									EObjectDescription.create(AxisEnum.COLUMNS.literal,
										(datamart.source as DatamartTask)))
								result.add(
									EObjectDescription.create(AxisEnum.ROWS.literal, (datamart.source as DatamartTask)))
							}
						}
					}
					return MapBasedScope.createScope(IScope.NULLSCOPE, result)
				}

	def void getEntityAxes(DatamartEntity entity, ArrayList<IEObjectDescription> result) {
		for (navigation : entity.navigations) {
			if (navigation instanceof DatamartMember) {
				(navigation as DatamartMember).datamartEntity.getEntityAxes(result)
				for (property : (navigation as DatamartMember).datamartEntity.attributes) {
					if (!result.containsName(property.axis.name.literal)) {
						result.add(
							EObjectDescription.create(property.axis.name.literal,
								property as DatamartCubeAxis))
					}
				}
			}
			if (navigation instanceof DatamartOwner) {
				(navigation as DatamartOwner).datamartEntity.getEntityAxes(result)
				for (property : (navigation as DatamartOwner).datamartEntity.attributes) {
					if (!result.containsName(property.axis.name.literal)) {
						result.add(
							EObjectDescription.create(property.axis.name.literal,
								property as DatamartCubeAxis))
					}
				}
			}
		}
		for (property : entity.attributes) {
			if (property.axis !== null) {
				if (!result.containsName(property.axis.name.literal)) {
					result.add(
						EObjectDescription.create(property.axis.name.literal, property as DatamartCubeAxis))
				}
			} else {
				if (!result.containsName(property.axis.name.literal)) {
					result.add(
						EObjectDescription.create(property.axis.name.literal, property as DatamartCubeAxis))
				}
			}
		}
	}

	def boolean containsName(ArrayList<IEObjectDescription> list, String name) {
		for (obj : list) {
			var oname = obj.name.toString
			if (oname.equals(name)) {
				return true
			}
		}
		return false
	}
	
	def void allMembers(DatamartHierarchy hierarchy, ArrayList<IEObjectDescription> result) {
		if ((hierarchy.level instanceof DatamartHierarchyLevelSingle) &&
			(hierarchy.level as DatamartHierarchyLevelSingle).levelRef !== null) {
			if (hierarchy.all) { // detailed was modeled - we must add the complete hierarchy
				var dimEntity = ((hierarchy.level as DatamartHierarchyLevelSingle).levelRef as CubeLevel).
					eContainer as CubeDimensionEntity
				for (cubeLevel : dimEntity.hierarchLevels) {
					result.add(EObjectDescription.create(cubeLevel.name, cubeLevel))
				}
			} else {
				result.add(EObjectDescription.create((hierarchy.level as DatamartHierarchyLevelSingle).levelRef.name,
					((hierarchy.level as DatamartHierarchyLevelSingle).levelRef as CubeLevel)))
			}

		} else if ((hierarchy.hierarchyRef as CubeHierarchy).allMemberName) {
			result.add(EObjectDescription.create((hierarchy.hierarchyRef as CubeHierarchy).allMemberNameValue.toString,
				(hierarchy.hierarchyRef as CubeHierarchy)))
		} else if ((hierarchy.defaultMember)) {
			result.add(EObjectDescription.create((hierarchy.hierarchyRef as CubeHierarchy).defaultMemberValue.toString,
				(hierarchy.hierarchyRef as CubeHierarchy)))
		}

	}

				def void getEntityProperties(DatamartEntity entity, ArrayList<IEObjectDescription> result,
		String requestedAxisName) {
		for (navigation : entity.navigations) {
			if (navigation instanceof DatamartMember) {
				(navigation as DatamartMember).datamartEntity.getEntityProperties(result, requestedAxisName)
				for (property : (navigation as DatamartMember).datamartEntity.attributes) {
					if (property.axis.name.literal.equals(requestedAxisName)) {
						result.add(
							EObjectDescription.create(DatamartAttributeUtil.getAliasedAttributeName(property),
								property as DatamartAttribute))
					}
				}
			}
			if (navigation instanceof DatamartOwner) {
				(navigation as DatamartOwner).datamartEntity.getEntityProperties(result, requestedAxisName)
				for (property : (navigation as DatamartOwner).datamartEntity.attributes) {
					if (property.axis.name.literal.equals(requestedAxisName)) {
						result.add(
							EObjectDescription.create(DatamartAttributeUtil.getAliasedAttributeName(property),
								property as DatamartAttribute))
					}
				}
			}
		}
		for (property : entity.attributes) {
			if (property.axis !== null) {
				if (property.axis.name.literal.equals(requestedAxisName)) {
					result.add(
						EObjectDescription.create(DatamartAttributeUtil.getAliasedAttributeName(property),
							property as DatamartAttribute))
				}
			} else {
				result.add(
					EObjectDescription.create(DatamartAttributeUtil.getAliasedAttributeName(property),
						property as DatamartAttribute))

			}
		}
	}

	def void getTaskColumns(DatamartTask task, ArrayList<IEObjectDescription> result, String requestedAxisName) {
		for (column : task.columns) {
			if (AxisEnum.COLUMNS.literal.equals(requestedAxisName)) {
				result.add(EObjectDescription.create(column.columnRef.literal, column))
			}
		}
	}

	def IScope getScope_Table_valueRef(EObject object, EReference reference) {
		var result = <IEObjectDescription>newArrayList
		var eObj = object
		var requestedAxisName = ""
		while (!(eObj instanceof TableDatamart)) {
			eObj = eObj.eContainer
			if (eObj instanceof TableAxis) {
				requestedAxisName = (eObj as TableAxis).axis.literal
			}
		}
		if (eObj !== null) {
			var datamart = (eObj as TableDatamart).datamartRef
			if (datamart !== null && datamart.source !== null) {
				if (datamart.source instanceof DatamartCube) {
					for (axis : (datamart.source as DatamartCube).axisslicer) {
						if (axis instanceof DatamartCubeAxis) {
							if ((axis as DatamartCubeAxis).axis.name.literal.equals(requestedAxisName)) {
								var hasMeasures = false
								var hasHierarchy = false
								var hasAggregation = false
								var isCrossjoined = false
								for (element : (axis as DatamartCubeAxis).elements) {
									if (element instanceof DatamartMeasure ||
										element instanceof DatamartDerivedMeasure) {
										hasMeasures = true
									}
								}
								for (element : (axis as DatamartCubeAxis).elements) {
									switch(element) {
										DatamartHierarchy: {
											if (hasHierarchy) {
												isCrossjoined = true
											} else {
												hasHierarchy = true
											}
										}
										DatamartSetAggregation: {
											if (hasAggregation) {
												isCrossjoined = true
											} else {
												hasAggregation = true
											}
											hasAggregation = true
										}
									}
								}
								if (hasMeasures && (hasHierarchy || hasAggregation)) {
									isCrossjoined = true
								}
								for (element : (axis as DatamartCubeAxis).elements) {
									switch(element) {
										DatamartMeasure: {
											if(reference ==	TableDSLPackage.Literals.TABLE_MEASURE__VALUE_REF && element.measureRef !== null) { 
												result.add(EObjectDescription.create(element.measureRef.name, element))
											}
										}
										DatamartDerivedMeasure: {
											if(reference == TableDSLPackage.Literals.TABLE_DERIVED__VALUE_REF && element.derivedRef !== null) {
												result.add(EObjectDescription.create((element as DatamartDerivedMeasure).derivedRef.name, element))
											}	
										}
										DatamartHierarchy: {
											if(!hasMeasures && reference ==	TableDSLPackage.Literals.TABLE_HIERARCHY__VALUE_REF && element.hierarchyRef !== null) {
												element.allMembers(result)
											}
										}
									}
								}
							}
						}
					}
				} else if (datamart.source instanceof DatamartEntity) {
					getEntityProperties(datamart.source as DatamartEntity, result, requestedAxisName)
				} else if (datamart.source instanceof DatamartTask) {
					getTaskColumns(datamart.source as DatamartTask, result, requestedAxisName)
				}
			}
		}
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}

	def IScope getScope_Table_Aggregation_valueRef(EObject object) {
		var result = <IEObjectDescription>newArrayList
		var requestedAxisName = ""
		var eObj = object
		while (!(eObj instanceof TableDatamart)) {
			eObj = eObj.eContainer
			if (eObj instanceof TableAxis) {
				requestedAxisName = (eObj as TableAxis).axis.literal
			}
		}
		if (eObj !== null) {
			var datamart = (eObj as TableDatamart).datamartRef
			if (datamart !== null && datamart.source !== null) {
				if (datamart.source instanceof DatamartCube) {
					for (axis : (datamart.source as DatamartCube).axisslicer) {
						if (axis instanceof DatamartCubeAxis) {
							if ((axis as DatamartCubeAxis).axis.name.literal.equals(requestedAxisName)) {
								var hasMeasures = false
								var hasHierarchy = false
								var hasAggregation = false
								var isCrossjoined = false
								for (element : (axis as DatamartCubeAxis).elements) {
									if (element instanceof DatamartMeasure ||
										element instanceof DatamartDerivedMeasure) {
										hasMeasures = true
									}
								}
								for (element : (axis as DatamartCubeAxis).elements) {
									if (element instanceof DatamartHierarchy) {
										if (hasHierarchy) {
											isCrossjoined = true
										} else {
											hasHierarchy = true
										}
									}
									if (element instanceof DatamartSetAggregation) {
										if (hasAggregation) {
											isCrossjoined = true
										} else {
											hasAggregation = true
										}
										hasAggregation = true
									}
								}
								if (hasMeasures && (hasHierarchy || hasAggregation)) {
									isCrossjoined = true
								}
								if (!hasMeasures && !isCrossjoined) {
									for (element : (axis as DatamartCubeAxis).elements) {
										if (element instanceof DatamartSetAggregation) {
											if(element.left !== null && element.left.aggregation !== null && element.left.aggregation instanceof DatamartSetAggregationFunction) {
												var aggregation = element.left.aggregation as DatamartSetAggregationFunction
												result.add(EObjectDescription.create(aggregation.aggregation.literal, aggregation))
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return MapBasedScope.createScope(IScope.NULLSCOPE, result)
	}
}
																								