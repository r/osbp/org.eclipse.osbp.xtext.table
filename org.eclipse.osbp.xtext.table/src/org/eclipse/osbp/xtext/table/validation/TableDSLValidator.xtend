/**
 *                                                                            
 *  Copyright (c) 2015 - 2016 - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Loetz GmbH&Co.KG - Initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
package org.eclipse.osbp.xtext.table.validation

import java.util.List
import org.eclipse.osbp.xtext.table.TableDSLPackage
import org.eclipse.osbp.xtext.table.TableModel
import org.eclipse.xtext.common.types.JvmGenericType
import org.eclipse.xtext.validation.Check
import org.tepi.filtertable.FilterTable
import org.vaadin.hene.popupbutton.PopupButton

//import org.eclipse.xtext.validation.Check
/**
 * Custom validation rules. 
 *
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
class TableDSLValidator extends AbstractTableDSLValidator {

}
