/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.table.jvmmodel

import com.vaadin.server.ClientConnector.AttachEvent
import com.vaadin.server.ClientConnector.AttachListener
import com.vaadin.ui.Button
import com.vaadin.ui.Component
import com.vaadin.ui.VerticalLayout
import java.util.ArrayList
import java.util.Comparator
import java.util.Date
import java.util.HashMap
import java.util.HashSet
import java.util.Locale
import java.util.Map
import java.util.Set
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy
import javax.inject.Inject
import javax.inject.Named
import org.eclipse.e4.core.contexts.IContextFunction
import org.eclipse.e4.core.contexts.IEclipseContext
import org.eclipse.e4.core.di.annotations.Optional
import org.eclipse.e4.ui.di.Focus
import org.eclipse.e4.ui.model.application.MApplication
import org.eclipse.e4.ui.workbench.IPresentationEngine
import org.eclipse.osbp.bpm.api.BPMTaskEventType
import org.eclipse.osbp.bpm.api.BPMTaskSummary
import org.eclipse.osbp.bpm.api.BPMTaskUserEvent
import org.eclipse.osbp.bpm.api.IBPMTaskClient
import org.eclipse.osbp.bpm.api.IBPMTaskEventNotification
import org.eclipse.osbp.bpm.api.IBlipBPMWorkloadModifiableItem
import org.eclipse.osbp.dsl.common.xtext.extensions.AnnotationExtension
import org.eclipse.osbp.dsl.entity.xtext.extensions.ModelExtensions
import org.eclipse.osbp.dsl.semantic.common.types.LDataType
import org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue
import org.eclipse.osbp.dsl.semantic.entity.LEntity
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute
import org.eclipse.osbp.infogrid.api.IGridSourceFacade
import org.eclipse.osbp.osgi.hybrid.api.AbstractHybridVaaclipseView
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent
import org.eclipse.osbp.runtime.common.event.IEventDispatcher
import org.eclipse.osbp.runtime.common.layouting.IPropertyConstants
import org.eclipse.osbp.runtime.common.types.ITypeProviderService
import org.eclipse.osbp.ui.api.contextfunction.IViewEmbeddedProvider
import org.eclipse.osbp.ui.api.customfields.IBlobService
import org.eclipse.osbp.ui.api.datamart.DatamartFilter
import org.eclipse.osbp.ui.api.datamart.IDatamartFilterGenerator
import org.eclipse.osbp.ui.api.e4.IE4Table
import org.eclipse.osbp.ui.api.layout.IViewLayoutManager
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService
import org.eclipse.osbp.ui.api.table.ITableRowCheckListener
import org.eclipse.osbp.ui.api.themes.IThemeResourceService
import org.eclipse.osbp.ui.api.user.IUser
import org.eclipse.osbp.ui.api.useraccess.IUserAccessService
import org.eclipse.osbp.utils.common.EntityUtils
import org.eclipse.osbp.vaaclipse.common.ecview.api.IECViewSessionHelper
import org.eclipse.osbp.xtext.action.ActionPackage
import org.eclipse.osbp.xtext.cubedsl.CubeLevel
import org.eclipse.osbp.xtext.datamart.common.DatamartDefinitionUtil
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedCellSet
import org.eclipse.osbp.xtext.datamart.common.sql.OperativeDtoContainer
import org.eclipse.osbp.xtext.datamartdsl.AxisEnum
import org.eclipse.osbp.xtext.datamartdsl.DatamartAttribute
import org.eclipse.osbp.xtext.datamartdsl.DatamartCube
import org.eclipse.osbp.xtext.datamartdsl.DatamartDefinition
import org.eclipse.osbp.xtext.datamartdsl.DatamartEntity
import org.eclipse.osbp.xtext.datamartdsl.DatamartPackage
import org.eclipse.osbp.xtext.datamartdsl.DatamartTask
import org.eclipse.osbp.xtext.datamartdsl.jvmmodel.DatamartDSLJvmModelInferrer
import org.eclipse.osbp.xtext.datamartdsl.util.DatamartAttributeUtil
import org.eclipse.osbp.xtext.table.Table
import org.eclipse.osbp.xtext.table.TableAggregation
import org.eclipse.osbp.xtext.table.TableAllColumns
import org.eclipse.osbp.xtext.table.TableAttribute
import org.eclipse.osbp.xtext.table.TableAxis
import org.eclipse.osbp.xtext.table.TableCellColor
import org.eclipse.osbp.xtext.table.TableColumn
import org.eclipse.osbp.xtext.table.TableDSLFactory
import org.eclipse.osbp.xtext.table.TableDatamart
import org.eclipse.osbp.xtext.table.TableDateDayInterval
import org.eclipse.osbp.xtext.table.TableDateDayLookup
import org.eclipse.osbp.xtext.table.TableDerived
import org.eclipse.osbp.xtext.table.TableDtoDatasource
import org.eclipse.osbp.xtext.table.TableGrid
import org.eclipse.osbp.xtext.table.TableHierarchy
import org.eclipse.osbp.xtext.table.TableIcon
import org.eclipse.osbp.xtext.table.TableIntInterval
import org.eclipse.osbp.xtext.table.TableIntLookup
import org.eclipse.osbp.xtext.table.TableMeasure
import org.eclipse.osbp.xtext.table.TableNumberInterval
import org.eclipse.osbp.xtext.table.TableNumberLookup
import org.eclipse.osbp.xtext.table.TableOption
import org.eclipse.osbp.xtext.table.TableOrdinal
import org.eclipse.osbp.xtext.table.TablePackage
import org.eclipse.osbp.xtext.table.TablePreorder
import org.eclipse.osbp.xtext.table.TableSelection
import org.eclipse.osbp.xtext.table.TableStringLookup
import org.eclipse.osbp.xtext.table.TableTable
import org.eclipse.osbp.xtext.table.TableTextColor
import org.eclipse.osbp.xtext.table.TableTooltip
import org.eclipse.osbp.xtext.table.TableTrend
import org.eclipse.osbp.xtext.table.common.CellSetFilterTable
import org.eclipse.osbp.xtext.table.common.CellSetIndexedContainer
import org.eclipse.osbp.xtext.table.common.CheckboxSelectionCellSetFilterTable
import org.eclipse.osbp.xtext.table.common.PropertyLookup
import org.eclipse.osbp.xtext.table.common.TableFilterDecorator
import org.eclipse.xtext.Constants
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.common.types.JvmDeclaredType
import org.eclipse.xtext.common.types.JvmField
import org.eclipse.xtext.common.types.JvmGenericType
import org.eclipse.xtext.common.types.JvmOperation
import org.eclipse.xtext.common.types.JvmTypeReference
import org.eclipse.xtext.common.types.JvmVisibility
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder
import org.osgi.service.component.annotations.Reference
import org.osgi.service.component.annotations.ReferenceCardinality
import org.osgi.service.component.annotations.ReferencePolicy
import org.slf4j.Logger

/**
 * <p>
 * This inferrer infers models of extension .table and generates code to be used by a e4 application as view. Underlying components
 * are from the vaadin component repository
 * </p>
 * 
 * @author Joerg Riegel
 */
/**
 * <p>Helper class for a Treemap in order to make double values comparable.</p> 
 *
 */
class DoubleComparator implements Comparator<Double> { 
	override compare(Double arg0, Double arg1) {
		if (arg0 < arg1) {
			return -1
		} else if (arg0 > arg1) {
			return 1
		} else {
			return 0
		}
	}
}

class IntegerComparator implements Comparator<Integer> {
	override compare(Integer arg0, Integer arg1) {
		if (arg0 < arg1) {
			return -1
		} else if (arg0 > arg1) {
			return 1
		} else {
			return 0
		}
	}
}

class DateComparator implements Comparator<Date> {
	override compare(Date o1, Date o2) {
		return o1.compareTo(o2);
	}
}

/**
 * <p>Container class for a Hashmap value. It carries look and feel information for table cells.</p> 
 *
 */
class FormatAttribute {
	public String formatter = null
	public String tooltipPattern = null
	public boolean hideLabelLookup = false
	public boolean hideLabelInterval = false
	public boolean hideImageLabel = false
	public boolean collapsed = false;
	public boolean hasImage = false
	public boolean hasImageParameter = false
	public String imagePathParameter = null
	public String imagePath = null
	public boolean hasImageResize = false;
	public String imageResizeString = null
	public String iconName = null;
}

class IdAttribute {
	public boolean collapsed = false;
}

/**
 * <p>Infers a JVM model from the source model.</p> 
 *
 * <p>The JVM model should contain all elements that would appear in the Java code 
 * which is generated from the source model. Other models link against the JVM model rather than the source model.</p>     
 */
class TableDSLJvmModelInferrer extends AbstractModelInferrer {

	@Inject extension JvmTypesBuilder
	@Inject extension IQualifiedNameProvider
	@Inject extension DatamartDSLJvmModelInferrer datamartInferrer
	@Inject extension ModelExtensions
	@Inject extension AnnotationExtension

	@Inject
	@Named(Constants.FILE_EXTENSIONS) String fileExtension

	var customformatCounter = 0

	val CONVERT_METHOD_PREFIX = "getStringTo"
	
	var String binderClassName = ""
	
	def String getFileExtension() {
	}

	/**
	 * <p>infer method dispatches the necessary routines to build fields, setter, getter, constructors and methods of the generated code.</p> 
	 *
	 */
	def dispatch void infer(TablePackage pkg, IJvmDeclaredTypeAcceptor acceptor, boolean isPreIndexingPhase) {

		// the service binder class
		binderClassName = pkg.fullyQualifiedName.toString + "." +
			pkg.fullyQualifiedName.lastSegment.toFirstUpper + "ServiceBinder"
		val clsBinder = pkg.toClass(pkg.name.toString.concat("ServiceBinder"))
		clsBinder.simpleName = clsBinder.simpleName.toFirstUpper
		acceptor.accept(clsBinder, [
			annotations += _annotationTypesBuilder.annotationRef(SuppressWarnings, "serial")
			annotations += _annotationTypesBuilder.annotationRef(typeof(org.osgi.service.component.annotations.Component))

			packageName = pkg.fullyQualifiedName.toString
			it.fileHeader = pkg.documentation
			it.toBinderFields(pkg)
			it.toBinderOperations(pkg)
		])

		// create table classes
		for (table : pkg.tables) {
			var clsName = ""
			if (table.isEmbedded) {
				clsName = pkg.name.toString.concat(".").concat(table.name).concat(fileExtension.toFirstUpper).
					concat("Embedded")
			}
			else if (table.isGrid) {
				clsName = pkg.name.toString.concat(".").concat(table.name).concat("Grid")
			}
			else if (table.isSelection) {
				clsName = pkg.name.toString.concat(".").concat(table.name).concat("Select")
			}
			else {
				clsName = pkg.name.toString.concat(".").concat(table.name).concat(fileExtension.toFirstUpper)
			}
			val className = clsName
			val cls = table.toClass(clsName)
			if (table.isEmbedded) {
				cls.getSuperTypes().add(_typeReferenceBuilder.typeRef(IViewEmbeddedProvider))
				cls.getSuperTypes().add(_typeReferenceBuilder.typeRef(AttachListener))
			} else {
				cls.getSuperTypes().add(_typeReferenceBuilder.typeRef(AbstractHybridVaaclipseView))
			}
			cls.superTypes.add(_typeReferenceBuilder.typeRef(IUser.UserLocaleListener))
			cls.superTypes.add(_typeReferenceBuilder.typeRef(IEventDispatcher.Receiver))
			cls.superTypes.add(_typeReferenceBuilder.typeRef(IE4Table))
			if(!table.grid && !table.isEmbedded) {
				cls.superTypes.add(_typeReferenceBuilder.typeRef(IDatamartFilterGenerator.FilterChangeListener))
			}
			if (table.task) {
				cls.getSuperTypes().add(_typeReferenceBuilder.typeRef(IBPMTaskEventNotification))
			}
			if(table.isSelection) {
				cls.getSuperTypes().add(_typeReferenceBuilder.typeRef(ITableRowCheckListener.CheckRecipient))
			}
			acceptor.accept(cls,
				[
				it.toFields(table)
				it.toConstructor(table)
				it.toGetterSetter(table)
				it.toOperations(pkg, table, className)
			])
			if (table.isEmbedded) {

				// the context function class
				var contextClassName = table.name.concat(fileExtension.toFirstUpper).concat("Embedded") +
					"ContextFunction"
				val contextCls = pkg.toClass(contextClassName)
				val tableClsName = clsName
				contextCls.getSuperTypes().add(_typeReferenceBuilder.typeRef(IContextFunction))
				contextCls.simpleName = contextCls.simpleName.toFirstUpper
				acceptor.accept(contextCls,
					[
					var annotationRef = _annotationTypesBuilder.annotationRef(
						typeof(org.osgi.service.component.annotations.Component))
					annotationRef.addAnnAttr(table, "service",
						_typeReferenceBuilder.typeRef(IContextFunction) as JvmTypeReference)
					var propContent = '''«IContextFunction.SERVICE_CONTEXT_KEY»=«table.name.toFirstUpper»'''
					annotationRef.addAnnAttr(table, "property", propContent)
					annotations += annotationRef
					packageName = pkg.fullyQualifiedName.toString
					it.toContextOperations(table, tableClsName)
				])
			}
		}
	}

	/**
	 * <p>create an injectable context function to be used by an e4 application.</p> 
	 *
	 */
	def void toContextOperations(JvmGenericType type, Table table, String clsName) {
		// compute override
		type.members += table.toMethod("compute", _typeReferenceBuilder.typeRef(Object),
			[
				annotations += _annotationTypesBuilder.annotationRef(Override)
				visibility = JvmVisibility.PUBLIC
				parameters += table.toParameter("eclipseContext", _typeReferenceBuilder.typeRef(IEclipseContext))
				parameters += table.toParameter("contextKey", _typeReferenceBuilder.typeRef(String))
				body = [
					append(
						'''
						MApplication application = eclipseContext.get(MApplication.class);
						IEclipseContext appCtx = application.getContext();
						IViewEmbeddedProvider provider = ContextInjectionFactory.make(«clsName».class, appCtx);
						appCtx.set(IViewEmbeddedProvider.class, provider);
						return provider;''')]
			])
	}

	/**
	 * <p>build the constructors to be used by an e4 application.</p> 
	 *
	 */
	def void toConstructor(JvmDeclaredType type, Table table) {
		if (!table.isEmbedded) {
			type.members += table.toConstructor(
				[
					annotations += _annotationTypesBuilder.annotationRef(Inject)
					parameters += table.toParameter("parent", _typeReferenceBuilder.typeRef(VerticalLayout))
					parameters += table.toParameter("context", _typeReferenceBuilder.typeRef(IEclipseContext))
					parameters += table.toParameter("app", _typeReferenceBuilder.typeRef(MApplication))
					body = [
						append(
							'''
							super(parent,context,app);
							''')]
				])
		}
	}

	def TableDatamart getDataMartTable(Table table) {
		return table.tabletype.dataMartTable
	}

	def TableDatamart getDataMartTable(TableOption option) {
		switch option {
			TableTable: return option.source
		}
		return null
	}

	def TableDtoDatasource getDtoGridTable(Table table) {
		return table.tabletype.dtoGridTable
	}

	def TableDtoDatasource getDtoGridTable(TableOption option) {
		switch option {
			TableGrid: return option.source as TableDtoDatasource
		}
		return null
	}

	def String getSourceDataMartRefName(Table table) {
		if (table.dataMartTable !== null && table.dataMartTable.datamartRef !== null &&
			table.dataMartTable.datamartRef.name !== null) {
			return table.dataMartTable.datamartRef.name.toString
		} else {
			return null;
		}
	}

	/**
	 * <p>build the class variables.</p> 
	 *
	 */
	def void toFields(JvmDeclaredType type, Table table) {
		var JvmField field = null

		// create logger
		field = table.toField("log", _typeReferenceBuilder.typeRef(Logger))[
			setInitializer([append('''org.slf4j.LoggerFactory.getLogger("tables")''')])]
		field.static = true
		field.visibility = JvmVisibility::PRIVATE
		type.members += field
		field = table.toField("initDone", _typeReferenceBuilder.typeRef(boolean)) [initializer = [append('''false''')]]
		type.members += field
		field = table.toField("parent", _typeReferenceBuilder.typeRef(VerticalLayout))
		type.members += field
		field = table.toField("eclipseContext", _typeReferenceBuilder.typeRef(IEclipseContext))[annotations += _annotationTypesBuilder.annotationRef(Inject)]
		type.members += field
		field = table.toField("user", _typeReferenceBuilder.typeRef(IUser))[annotations += _annotationTypesBuilder.annotationRef(Inject)]
		type.members += field
		field = table.toField("eventDispatcher", _typeReferenceBuilder.typeRef(IEventDispatcher))[annotations += _annotationTypesBuilder.annotationRef(Inject)]
		type.members += field
		field = table.toField("coordinateSystem", _typeReferenceBuilder.typeRef(ArrayList, _typeReferenceBuilder.typeRef(Integer)))
		type.members += field
		field = table.toField("layoutManager", _typeReferenceBuilder.typeRef(IViewLayoutManager))
		type.members += field
		field = table.toField("dslMetadataService", _typeReferenceBuilder.typeRef(IDSLMetadataService)) [annotations += _annotationTypesBuilder.annotationRef(Inject)]
		type.members += field
		field = table.toField("blobService", _typeReferenceBuilder.typeRef(IBlobService)) [annotations += _annotationTypesBuilder.annotationRef(Inject)]
		type.members += field
		// the package name of the referenced datamart
		if (getSourceDataMartRefName(table) !== null) {
			var packageName = (table.dataMartTable.datamartRef.eContainer as DatamartPackage).fullyQualifiedName.
				toString
			field = table.toField("datamartInstance",
				_typeReferenceBuilder.typeRef('''«packageName».«getSourceDataMartRefName(table)»Datamart'''))
			type.members += field
			field = table.toField("filterGenerator", _typeReferenceBuilder.typeRef(IDatamartFilterGenerator))
			type.members += field
			if(!table.task) {
				field = table.toField("moreToLoad", _typeReferenceBuilder.typeRef(boolean)) [initializer = [append('''false''')]]
				type.members += field
			}
		}
		if(table.grid) {
			field = table.toField("gridSourceService", _typeReferenceBuilder.typeRef(IGridSourceFacade))[annotations += _annotationTypesBuilder.annotationRef(Inject)]
			type.members += field
			field = table.toField("ecviewPropsProvider", _typeReferenceBuilder.typeRef(IECViewSessionHelper))[annotations += _annotationTypesBuilder.annotationRef(Inject)]
			type.members += field
			field = table.toField("bundleSpaceTypeProvider", _typeReferenceBuilder.typeRef(ITypeProviderService))[annotations += _annotationTypesBuilder.annotationRef(Inject)]
			type.members += field
			field = table.toField("grid", _typeReferenceBuilder.typeRef("org.eclipse.osbp.infogrid.vaaclipse.SingleInfoGridComponent"))
			type.members += field
		}
		field = table.toField("dataComponent", _typeReferenceBuilder.typeRef(Component))
		type.members += field
		field = table.toField("mustRefresh", _typeReferenceBuilder.typeRef(boolean)) [initializer = [append('''false''')]]
		type.members += field
		field = table.toField("attributeLookupMap",
			_typeReferenceBuilder.typeRef(HashMap, _typeReferenceBuilder.typeRef(String), _typeReferenceBuilder.typeRef(PropertyLookup))) 
		type.members += field
		field = table.toField("tabSheets",
			_typeReferenceBuilder.typeRef(HashMap, _typeReferenceBuilder.typeRef(Component), _typeReferenceBuilder.typeRef(ArrayList, _typeReferenceBuilder.typeRef(ArrayList, _typeReferenceBuilder.typeRef(Integer)))))
		type.members += field
		
		field = table.toField("dataSourceContainer", _typeReferenceBuilder.typeRef(CellSetIndexedContainer))
		type.members += field
		
		field = table.toField("operativeDtoContainer", _typeReferenceBuilder.typeRef(OperativeDtoContainer))
		type.members += field
		field = table.toField("tables",	_typeReferenceBuilder.typeRef(ArrayList, _typeReferenceBuilder.typeRef(CellSetFilterTable)))
		type.members += field
		field = table.toField("renderingEngine", _typeReferenceBuilder.typeRef(IPresentationEngine))[
			annotations += _annotationTypesBuilder.annotationRef(Inject)]
		type.members += field
		field = table.toField("themeResourceService", _typeReferenceBuilder.typeRef(IThemeResourceService))[
			annotations += _annotationTypesBuilder.annotationRef(Inject)]
		type.members += field
		field = table.toField("tableFilterDecorator", _typeReferenceBuilder.typeRef(TableFilterDecorator))
		type.members += field
		if (table.task) {
			field = table.toField("taskClient", _typeReferenceBuilder.typeRef(IBPMTaskClient))[
				annotations += _annotationTypesBuilder.annotationRef(Inject)
				annotations += _annotationTypesBuilder.annotationRef(Optional)]
			type.members += field
			field = table.toField("currentTaskId", _typeReferenceBuilder.typeRef(Long))
			type.members += field
			field = table.toField("taskButtons",
				_typeReferenceBuilder.typeRef(ArrayList, _typeReferenceBuilder.typeRef(Button)))
			type.members += field
		}
		if	(table.isSelection) {
			field = table.toField("taskClient", _typeReferenceBuilder.typeRef(IBPMTaskClient))[
				annotations += _annotationTypesBuilder.annotationRef(Inject)
				annotations += _annotationTypesBuilder.annotationRef(Optional)]
			type.members += field
			field = table.toField("selectTable", _typeReferenceBuilder.typeRef(CheckboxSelectionCellSetFilterTable))
			type.members += field
			field = table.toField("workLoadItem", _typeReferenceBuilder.typeRef(IBlipBPMWorkloadModifiableItem))
			type.members += field
			field = table.toField("taskSummary", _typeReferenceBuilder.typeRef(BPMTaskSummary))
			type.members += field
		}
		if (!table.isEmbedded && table.tabletype.toolbar !== null && table.tabletype.toolbar.eContainer !== null) {
			var packageName = (table.tabletype.toolbar.eContainer as ActionPackage).fullyQualifiedName.toString
			field = table.toField("toolbarUUID", _typeReferenceBuilder.typeRef(String))
			type.members += field
			field = table.toField("toolbarClass", _typeReferenceBuilder.typeRef('''«packageName».«table.tabletype.toolbar.name.toString.toFirstUpper»Toolbar'''))
			type.members += field
		}
	}

	/**
	 * <p>build the getters and setters from class variables.</p> 
	 *
	 */
	def void toGetterSetter(JvmDeclaredType type, Table table) {
		var JvmOperation operation = null
		operation = table.toGetter("coordinateSystem",
			_typeReferenceBuilder.typeRef(ArrayList, _typeReferenceBuilder.typeRef(Integer)))
		operation.visibility = JvmVisibility::PUBLIC
		type.members += operation
	}

	/**
	 * <p>build the methods.</p> 
	 *
	 */
	def void toOperations(JvmDeclaredType type, TablePackage pkg, Table table, String clsName) {

		// create view
		type.members += table.toMethod("getCellSet", _typeReferenceBuilder.typeRef(DerivedCellSet),
			[
				parameters += table.toParameter("limited", _typeReferenceBuilder.typeRef(boolean))
				body = [append(
				'''
				operativeDtoContainer = null;
				«IF (table.dataMartTable !== null)»
				DerivedCellSet cellSet = datamartInstance.getResults(«IF table.task||table.cube»«ELSE»«IF ((table.isSelection) || table.isEmbedded)»limited, null, null«ELSE»limited, getTaskOperativeDtoClass(), getTaskInitialOperativeDtos()«ENDIF»«ENDIF»);
				«IF !table.task && !table.isCube»moreToLoad = datamartInstance.isMoreToLoad();«ENDIF»
				«ELSE»
				DerivedCellSet cellSet = null;
				«ENDIF»
				if	(cellSet != null) {
					operativeDtoContainer = cellSet.getOperativeDtoContainer();
				}
				return cellSet;
				''')]
			])
		
		// focus
		type.members += table.toMethod("setFocus", _typeReferenceBuilder.typeRef(Void::TYPE),
			[
				annotations += _annotationTypesBuilder.annotationRef(Focus)
				if (table.isGrid) {
					body = [append(
					'''
					if(grid != null) {
						grid.focus();
					}''')]
				} else {
					body = [append(
					'''
					if(tables != null && !tables.isEmpty()) {
						tables.get(0).focus();
					}''')]
				}
			])
		// activate
		type.members += table.toMethod("activate", _typeReferenceBuilder.typeRef(Void::TYPE),
			[
				annotations += _annotationTypesBuilder.annotationRef(PostConstruct)
				body = [append(
				'''
				«IF !table.embedded»super.initView();«ENDIF»
				user.addUserLocaleListener(this);
				eventDispatcher.addEventReceiver(this);
				«IF table.task»
				taskClient.subscribeTaskEventNotification(this);
				taskClient.subscribeProcessCompletedEventNotification(this);
				«ENDIF»
				«IF !table.grid && !table.isEmbedded»
				filterGenerator.addFilterChangeListener(this);
				«ENDIF»
				''')]
				
			])

		// deactivate
		type.members += table.toMethod("deactivate", _typeReferenceBuilder.typeRef(Void::TYPE),
			[
				annotations += _annotationTypesBuilder.annotationRef(PreDestroy)
				body = [append(
				'''
				user.removeUserLocaleListener(this);
				eventDispatcher.removeEventReceiver(this);
				«IF table.task»
				taskClient.unsubscribeTaskEventNotification(this);
				taskClient.unsubscribeProcessCompletedEventNotification(this);
				«ENDIF»
				«IF !table.grid && !table.isEmbedded»
				filterGenerator.removeFilterChangeListener(this);
				«ENDIF»
				«IF !table.embedded»super.destroyView();«ENDIF»''')]
			])
		// create view
		type.members += table.toMethod("createView", _typeReferenceBuilder.typeRef(Void::TYPE),
			[
				parameters += table.toParameter("parent", _typeReferenceBuilder.typeRef(VerticalLayout))
				if (table.isEmbedded) {
					annotations += _annotationTypesBuilder.annotationRef(Override)
				}
				body = [append('''«table.createView(pkg)»''')]
			])

		// create components
		type.members += table.toMethod("createComponents", _typeReferenceBuilder.typeRef(Void::TYPE),
			[
				body = [append('''«table.createComponents(clsName)»''')]
			])
		// setEnableManualInput dummy
		type.members += table.toMethod("setEnableManualInput", _typeReferenceBuilder.typeRef(Void::TYPE), [
			documentation = table.tabletype.documentation
			visibility = JvmVisibility.PUBLIC
			parameters += table.toParameter("manualInput", _typeReferenceBuilder.typeRef(boolean))
			body = [append('''''')]
		])
		// setInput dummy
		type.members += table.toMethod("setInput", _typeReferenceBuilder.typeRef(Void::TYPE), [
			documentation = table.tabletype.documentation
			visibility = JvmVisibility.PUBLIC
			parameters += table.toParameter("dto", _typeReferenceBuilder.typeRef(Object))
			body = [append('''''')]
		])

		// getSelectById
		type.members += table.toMethod("getSelectById", _typeReferenceBuilder.typeRef(boolean), [
			documentation = table.tabletype.documentation
			visibility = JvmVisibility.PRIVATE
			body = [
				append('''
				return «IF (table.tabletype instanceof TableTable)»«(table.tabletype as TableTable).selectById.booleanValue»«ELSE»false«ENDIF»;''')
			]
		])

		switch (table.tabletype) {
			TableTable: {
				// create tab
				type.members += table.toMethod("createTabSheet", _typeReferenceBuilder.typeRef(Component),
					[
						parameters += table.toParameter("cellSet", _typeReferenceBuilder.typeRef(DerivedCellSet))
						parameters += table.toParameter("axisNo", _typeReferenceBuilder.typeRef(Integer))
						body = [append('''«table.createTabSheet»''')]
					])

				// create table
				type.members += table.toMethod("createTable", _typeReferenceBuilder.typeRef(Component),
					[
						parameters += table.toParameter("cellSet", _typeReferenceBuilder.typeRef(DerivedCellSet))
						body = [append('''«table.createTable»''')]
					])
					
				// init table
				type.members += table.toMethod("initTable", _typeReferenceBuilder.typeRef(Void::TYPE),
					[
						parameters += table.toParameter("table", _typeReferenceBuilder.typeRef(CellSetFilterTable))
						parameters += table.toParameter("cellSet", _typeReferenceBuilder.typeRef(DerivedCellSet))
						body = [append('''«table.initTable»''')]
					])
					
				type.members += table.toMethod("tableSettingsByUserProperties", _typeReferenceBuilder.typeRef(Void::TYPE),
					[
						parameters += table.toParameter("table", _typeReferenceBuilder.typeRef(CellSetFilterTable))
						body = [append('''«table.tableSettingsByUserProperties»''')]
					])
					
				type.members += table.toMethod("dataRefresh", _typeReferenceBuilder.typeRef(Void::TYPE),
					[
						body = [append('''«table.dataRefresh»''')]
					])
				type.members += table.toMethod("renderAllowed", _typeReferenceBuilder.typeRef(boolean),
					[
						body = [append('''«renderAllowed»''')]
					])
			}
			TableGrid: {
				// create tab
				type.members += table.toMethod("createDtoGrid", _typeReferenceBuilder.typeRef(Component),
					[
						val gridDtoFqn = table.dtoGridTable.dtoSource.fullyQualifiedName?.toString
						val entity = EcoreUtil2.resolve(table.dtoGridTable.dtoSource.wrappedType, table.eResource) as LEntity
						body = [append('''
						«createGridComponent(pkg, table, entity, gridDtoFqn)»
						return grid;''')]
					])
			}
		}

		// locale notification
		type.members += table.toMethod("localeChanged", _typeReferenceBuilder.typeRef(Void::TYPE),
			[
				visibility = JvmVisibility.PUBLIC
				annotations += _annotationTypesBuilder.annotationRef(Override)
				parameters += table.toParameter("locale", _typeReferenceBuilder.typeRef(Locale))
				body = [append('''«table.localeChanged»''')]
			])
		// event notification
		type.members += table.toMethod("receiveEvent", _typeReferenceBuilder.typeRef(Void::TYPE),
			[
				visibility = JvmVisibility.PUBLIC
				annotations += _annotationTypesBuilder.annotationRef(Override)
				parameters += table.toParameter("event", _typeReferenceBuilder.typeRef(EventDispatcherEvent))
				body = [append('''«table.receiveEvent»''')]
			])
		if(!table.isGrid && !table.isEmbedded) {
			// filter notification
			type.members += table.toMethod("filterChanged", _typeReferenceBuilder.typeRef(Void::TYPE),
				[
					parameters += table.toParameter("changedFilter", _typeReferenceBuilder.typeRef(DatamartFilter))
					visibility = JvmVisibility.PUBLIC
					annotations += _annotationTypesBuilder.annotationRef(Override)
					body = [append('''«table.filterChanged»''')]
				])
		}		
		if (table.task) {

			type.members += table.toMethod("notifyTaskEvent", _typeReferenceBuilder.typeRef(Void::TYPE),
				[
					parameters += table.toParameter("type", _typeReferenceBuilder.typeRef(BPMTaskEventType))
					parameters += table.toParameter("event", _typeReferenceBuilder.typeRef(BPMTaskUserEvent))
					visibility = JvmVisibility.PUBLIC
					annotations += _annotationTypesBuilder.annotationRef(Override)
					body = [append('''«table.notifyTaskEvent»''')]
				])

			type.members += table.toMethod("notifyProcessCompletedEvent", _typeReferenceBuilder.typeRef(Void::TYPE),
				[
					visibility = JvmVisibility.PUBLIC
					annotations += _annotationTypesBuilder.annotationRef(Override)
					body = [append('''«table.notifyTaskEvent»''')]
				])
		}
		// any select table modifying workload dto methods
		if (table.isSelection) {
			type.members += table.toMethod("selectItemsFromWorkloadDto", _typeReferenceBuilder.typeRef(Void::TYPE),
				[
					visibility = JvmVisibility.PUBLIC
					body = [append('''«table.selectItemsFromWorkloadDto»''')]
				])
			type.members += table.toMethod("rowChecked", _typeReferenceBuilder.typeRef(Void::TYPE),
				[
					visibility = JvmVisibility.PUBLIC
					annotations += _annotationTypesBuilder.annotationRef(Override)
					parameters += table.toParameter("selection", _typeReferenceBuilder.typeRef(Object))
					parameters += table.toParameter("selected", _typeReferenceBuilder.typeRef(boolean))
					body = [append('''«table.rowChecked»''')]
				])				
		}
		if(table.isEmbedded) {
			type.members += table.toMethod("attach", _typeReferenceBuilder.typeRef(Void::TYPE),
				[
					visibility = JvmVisibility.PUBLIC
					annotations += _annotationTypesBuilder.annotationRef(Override)
					parameters += table.toParameter("event", _typeReferenceBuilder.typeRef(AttachEvent))
					body = [append(
					'''
					if(mustRefresh){
						dataRefresh();
						mustRefresh = false;
					}''')]
				])				
		}
	}
	
	def baseType(LEntity entityRef) {
		if(entityRef.superType !== null &&
			!entityRef.superType.mappedSuperclass &&  
			!entityRef.superType.equals(entityRef)) {
				return entityRef.superType 
		}
		return entityRef
	}
	
	def private Map<String, String> getUniqueReferenceFQIdName(LEntity entity) {
		var Map<String, String> uniqueReferenceNames = new HashMap
		for (entityReference : entity.allReferences) {
			var uniqueReferenceName = uniqueReferenceNames.get(entityReference.type.fullyQualifiedName)
			if (uniqueReferenceName === null) {
				uniqueReferenceName = '''«entityReference.type.fullyQualifiedName».«entityReference.type.idAttributeName»'''
				uniqueReferenceNames.put(entityReference.type.fullyQualifiedName.toString, uniqueReferenceName)
			}
		}
		return uniqueReferenceNames
	}
	
	protected def CharSequence createGridComponent(TablePackage pkg, Table table, LEntity entity, String gridDtoFqn)
		'''
			IEclipseContext childContext = eclipseContext.createChild();
			childContext.set(Locale.class, user.getLocale());
			childContext.set("gridSourceId", "«pkg.name.toString.concat(".").concat(table.name).concat("Grid")»");
			LinkedHashMap<String, EventDispatcherEvent> selectionEvntList = new LinkedHashMap<>();
			MPerspective perspective = eclipseContext.get(MPerspective.class);
			EventDispatcherEvent evnt = new EventDispatcherEvent(perspective, EventDispatcherCommand.SELECT, "«entity.baseType.fullyQualifiedName».«entity.idAttributeName»", "«table.fullyQualifiedName»");
			selectionEvntList.put("«entity.baseType.fullyQualifiedName».«entity.idAttributeName»", evnt);
			«FOR entityReferenceFQIdName : entity.uniqueReferenceFQIdName.values»
			evnt = new EventDispatcherEvent(perspective, EventDispatcherCommand.SELECT, "«entityReferenceFQIdName»", "«table.fullyQualifiedName»");
			selectionEvntList.put("«entityReferenceFQIdName»", evnt);
			«ENDFOR»
			childContext.set("selectionEvnt", selectionEvntList);
			if ("«gridDtoFqn»".equals(getTaskOperativeDtoFqn())) {
				childContext.set("inputdata", getTaskInitialOperativeDtos());
			}
			grid = ContextInjectionFactory.make(SingleInfoGridComponent.class, childContext);'''
	
	def String filterChanged(Table table) {
		var body = ""
		body = '''
		«body»
		if(changedFilter != null) {
			MPerspective perspective = eclipseContext.get(MPerspective.class);
			EventDispatcherEvent evnt = new EventDispatcherEvent(perspective, EventDispatcherCommand.SELECT, changedFilter.getName(), "«table.fullyQualifiedName»");
			evnt.addData(changedFilter.getSelectedData());
			eventDispatcher.sendEvent(evnt);
		}
		dataRefresh();
		'''
		return body
	}
	
	def String receiveEvent(Table table) {
		var body = ""
		if(table.isTable) {
			body = '''
			«body»
			switch(event.getCommand()) {
				case SELECT: 
					MPerspective perspective = eclipseContext.get(MPerspective.class);
					if(perspective != null) {
						ArrayList<String> allowedSenderParts = (ArrayList<String>) eclipseContext.getActive(MPart.class).getTransientData().get(IPerspectiveProvider.EventDispatcherConstants.ACCEPTED_SENDERS);
						if((event.getPerspective() == null || (perspective != null && event.getPerspective().equals(perspective))) && ((allowedSenderParts != null && allowedSenderParts.contains(event.getSender())) || allowedSenderParts == null)){
							if(!event.getSender().equals("«table.fullyQualifiedName»")) {
								if(filterGenerator.selectItem(event, getSelectById())) {
									«IF table.isEmbedded || table.task»
									createComponents();
									«ELSE»
									renderData();
									«ENDIF»
								}
							}
						}
					}
					break;
				case SAVE:
				case DELETE:
					if(!event.getSender().equals("«table.fullyQualifiedName»")) {
						«FOR e:findRequestedEntities(table)»
						if(event.getTopic().equals("«e.fullyQualifiedName»")){
							datamartInstance.clearCache();
							if(datamartInstance.contains(event.getData().get(EventDispatcherDataTag.ID))) {
								dataRefresh();
							}
						}
						«ENDFOR»
					}
					break;
				case REFRESH:
					if(!event.getSender().equals("«table.fullyQualifiedName»")) {
						«FOR e:findRequestedEntities(table)»
						if(event.getTopic().equals("«e.fullyQualifiedName»")){
							datamartInstance.clearCache();
							dataRefresh();
						}
						«ENDFOR»
					}
					/* must be activated if sure that the filter is to be refreshed
					if (filterGenerator != null) {
						filterGenerator.updateFilter();
					}*/
					break;
			'''
			if (!table.isEmbedded && table.tabletype.toolbar !== null && table.tabletype.toolbar.eContainer !== null) {
				body = '''
				«body»
					case ACTION:
						if (toolbarUUID.equals(event.getTopic()) && event.getData().containsKey(EventDispatcherDataTag.BUTTON_ID)) {
							Object id = event.getData().get(EventDispatcherDataTag.BUTTON_ID);
							if (id instanceof TableActionEnum) {
								TableActionEnum enumId = (TableActionEnum) id;
								switch (enumId) {
								case EXPORT_EXCEL:
									for(CellSetFilterTable table:tables) {
										ExcelExport excelExport = new ExcelExport(table, themeResourceService);
						                excelExport.excludeCollapsedColumns();
						                excelExport.setReportTitle(layoutManager.getCaption());
						                excelExport.export();
						            }
					                break;
								case EXPORT_CSV:
									for(CellSetFilterTable table:tables) {
										CsvExport csvExport = new CsvExport(table, themeResourceService);
						                csvExport.excludeCollapsedColumns();
						                csvExport.setReportTitle(layoutManager.getCaption());
						                csvExport.export();
						            }
					                break;
								case EXPORT_PDF:
									for(CellSetFilterTable table:tables) {
										PdfExport pdfExport = new PdfExport(table, themeResourceService);
						                pdfExport.excludeCollapsedColumns();
						                pdfExport.setReportTitle(layoutManager.getCaption());
						                pdfExport.export();
						            }
									break;
								}
							}
						}
						break;
				'''
			}
			body = '''
			«body»
				}
			'''
		} else if(table.isGrid) {
			body = '''
			«body»
			switch(event.getCommand()) {
				case SAVE:
				case DELETE:
				case REFRESH:
					if(!event.getSender().equals("«table.fullyQualifiedName»")) {
						«FOR e:findRequestedEntities(table)»
						if(event.getTopic().equals("«e.fullyQualifiedName»")){
							// TODO: GridRefresh
							grid.refreshData();
						}
						«ENDFOR»
					}
					break;
			}
			'''
		}
		return body
	}
	
	def refreshCondition(LEntity entity) {
		return entity.name as CharSequence
	}
	
	def getCallDataRefreshMethod(Table table) {
		switch (table.tabletype) {
			TableTable: {
				"dataRefresh();"
			}
			TableGrid: {
				""
			}
		}
	}

	def notifyTaskEvent(Table table) {
		'''
		mustRefresh = true;
		'''
	}

	/**
	 * <p>build the main method to be called from e4.</p> 
	 *
		tablePageLabel = new Label();
		tablePageLabel.setContentMode(ContentMode.HTML);
		tablePageLabel.addStyleName("pagelabel");
		tablePageLabel.setId("tablePageLabel");
		
		«"Page".bindComponent("tablePageLabel", "Value")»
		tableItemsLabel = new Label();
		tableItemsLabel.setContentMode(ContentMode.HTML);
		tableItemsLabel.addStyleName("pagelabel");
		tableItemsLabel.setId("tableItemsLabel");
		«"Items per page".bindComponent("tableItemsLabel", "Value")»
	 */
	def String createView(Table table, TablePackage pkg) {

		var body = '''
			«IF table.isEmbedded»eclipseContext«ELSE»getContext()«ENDIF».set(IE4Table.class, this);
			«IF table.task»
				currentTaskId = -1L;
			«ENDIF»
			tables = new ArrayList<CellSetFilterTable>();
			tableFilterDecorator = new TableFilterDecorator(dslMetadataService, user.getLocale());
			// the timeout to begin the filter process after the last key pressed
			tableFilterDecorator.setTextChangeTimeout(500);
			parent.setPrimaryStyleName("osbp");
			parent.setId("parent");
			parent.setSizeFull();
			«IF table.isEmbedded»parent.addAttachListener(this);«ENDIF»
			layoutManager = new ViewLayoutManager();
			layoutManager.init(parent);
			this.parent = parent;
		'''
		if(!table.isGrid) {
			body = '''
			«body»
			datamartInstance = new «getSourceDataMartRefName(table)»Datamart();
			datamartInstance.setUser(user);
			filterGenerator = new DatamartFilterGenerator(datamartInstance, «IF table.isEmbedded»eclipseContext«ELSE»getContext()«ENDIF», «table.dataMartTable.datamartRef.showCaption», «Integer.max(10, table.dataMartTable.datamartRef.numMultiRows)»);
			filterGenerator.createUIFilters(layoutManager);
			'''
		}
		if(table.isSelection) {
		body = '''
		«body»
		workLoadItem = (IBlipBPMWorkloadModifiableItem)getTransientDataVariable(IBlipBPMConstants.VARIABLE_TASK_START_INFO);
		taskSummary = (BPMTaskSummary)getTransientDataVariable(IBlipBPMConstants.VARIABLE_TASK_JBPM_SUMMARY);
		'''
		}
		body = '''
			«body»
			coordinateSystem = new ArrayList<Integer>();
			attributeLookupMap = new HashMap<>();
			tabSheets = new HashMap<>();'''

		if (!table.isEmbedded && table.tabletype.toolbar !== null) {
			body = '''
			«body»
			toolbarUUID = UUID.randomUUID().toString();
			toolbarClass = new «table.tabletype.toolbar.name.toString.toFirstUpper»Toolbar();
			toolbarClass.createToolbar(eclipseContext, renderingEngine, themeResourceService, toolbarUUID);'''
		}
		body = '''
			«body»
			initDone = true;
		'''
		return body
	}

	def boolean isTask(Table table) {
		return table.dataMartTable instanceof TableDatamart &&
			(table.dataMartTable as TableDatamart).datamartRef.source instanceof DatamartTask;
	}
	
	def boolean isCube(Table table) {
		return table.dataMartTable instanceof TableDatamart &&
			(table.dataMartTable as TableDatamart).datamartRef.source instanceof DatamartCube;
	}
	
	def boolean isGrid(Table table) {
		return table.tabletype instanceof TableGrid
	}

	def boolean isTable(Table table) {
		return table.tabletype instanceof TableTable
	}

	def boolean isSelectIdOnly(Table table) {
		return table.tabletype instanceof TableTable && (table.tabletype as TableTable).selectIdOnly 
	}

	def boolean isEmbedded(Table table) {
		return table.tabletype.embedded
	}

	def boolean isSelection(Table table) {
		return table.tabletype instanceof TableSelection
	}

	def renderAllowed() 
	'''
	if (getSelectById()) {
		for (DatamartFilter filter : datamartInstance.getFilters()) {
			if(filter.isOptional()) {
				return true;
			}
			if(!filter.getSql().isEmpty()) {
				return true;
			}
		} 
		return false;
	}
	return true;'''
	
	def dataRefresh(Table table) {
		var body = ""
		body = 
		'''
		«body»
		try {
			// if no current ui found
			if (UI.getCurrent() == null) {
				for(CellSetFilterTable table:tables) {
					UI.setCurrent(table.getUI());
					break;
				}
			}
			if(dataSourceContainer== null || !dataSourceContainer.updateContainer(getCellSet(true))) {
				log.debug("cannot update container - full refresh");
				«IF table.isEmbedded»
				createComponents();
				«ELSE»
				renderData();
				«ENDIF»
			}
			for(CellSetFilterTable table:tables) {
				table.sanitizeSelection();
			}
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionDetails = sw.toString();
			log.error("table data refresh:{}", exceptionDetails);
		}'''
		return body
	}
	
	/**
	 * <p>build the data components.</p> 
	 *
	 */
	def String createComponents(Table table, String clsName) {
		var body = ""
		if (table.dataMartTable !== null) {
			body = '''
				if (renderAllowed()){
					// get the results
					final DerivedCellSet cellSet = getCellSet(true);
					if (cellSet == null) {
						«IF !table.isEmbedded»promptSecurityMessage(dslMetadataService.translate(user.getLocale().toLanguageTag(), "securityMessage"), layoutManager.getDataArea());«ENDIF»
						return;
					} else {
						layoutManager.getDataArea().removeAllComponents();
						«IF table.isSelection»
						for(CellSetFilterTable table:tables) {
							if(table instanceof CheckboxSelectionCellSetFilterTable) {
								((CheckboxSelectionCellSetFilterTable)table).removeTableRowCheckListener(this);
							}
						}
						«ENDIF»
					}
					getCoordinateSystem().clear();
					tables.clear();
					// generate a new result component
					if (cellSet != null) {
						«IF !table.task && !table.cube»
						if(moreToLoad) {
							Runnable backgroundFetch = new Runnable() {
								public void run() {
								    operativeDtoContainer = null;
								    final DerivedCellSet cellSetAll = datamartInstance.getResults(false, getTaskOperativeDtoClass(), getTaskInitialOperativeDtos());
								    if	(cellSet != null) {
								    	operativeDtoContainer = cellSet.getOperativeDtoContainer();
								    }
									for(CellSetFilterTable table:tables) {
									    dataSourceContainer = new CellSetIndexedContainer(eclipseContext, attributeLookupMap, cellSetAll, coordinateSystem, false);
										UI.getCurrent().access(new Runnable() {
											@Override
											public void run() {
											    table.setContainerDataSource(dataSourceContainer);
											    initTable(table, cellSetAll);
											    filterGenerator.addFilterChangeListener(«clsName».this);
											}
										});
									}
								}
							};
							new Thread(backgroundFetch).start();
							moreToLoad = false;
						} else {
						    filterGenerator.addFilterChangeListener(«clsName».this);
						}
						«ENDIF»
						// create a multidimensional coordinate system against the cellSet
						for	(int axis = 0; axis < cellSet.getAxes().size(); axis++) {
							getCoordinateSystem().add(0);
						}
						// remove any previous component
						if	(dataComponent != null) {
							layoutManager.getDataArea().removeComponent(dataComponent);
							dataComponent = null;
						}
						if (cellSet.getAxes().size() < 2) {
							log.error("at least 2 axes from referenced datamart «table.dataMartTable.datamartRef.name» are needed to render «table.name»");
						} else {
							dataComponent = createTabSheet(cellSet, cellSet.getAxes().size());
							if	(dataComponent != null) {
								dataComponent.setSizeFull();
								dataComponent.setId("dataComponent");
								layoutManager.getDataArea().addComponent(dataComponent);
								«IF (table.isSelection)»
								selectItemsFromWorkloadDto();
								«ENDIF»
								layoutManager.getDataArea().setExpandRatio(dataComponent, 1);
								«table.selectAlways»
							}
						}
					} 
					else {
						log.error("referenced datamart «table.dataMartTable.datamartRef.name» generates no results");
					}
				}
			'''
		} else if(table.isGrid) {
			body = '''
				«body»
				// generate a new result component
				// remove any previous component
				if	(dataComponent != null) {
					layoutManager.getDataArea().removeComponent(dataComponent);
					dataComponent = null;
				}
				dataComponent = createDtoGrid();
				dataComponent.setSizeFull();
				dataComponent.setId("dataComponent");
				layoutManager.getDataArea().addComponent(dataComponent);
				layoutManager.getDataArea().setExpandRatio(dataComponent, 1);
			'''
		}
		return body
	}

	def String getSelectAlways(Table table) {
		var body = ""
		if (table.isTable && (table.tabletype as TableTable).selectalways) {
			body = '''
			«body»
			UI.getCurrent().access(new Runnable() {
				@Override
				public void run() {
					for(CellSetFilterTable table:tables) {
						if (table.getItemIds().size()>0) {
							table.select(table.getItemIds().iterator().next());
						}
					}
				}
			});'''
		}
		return body
	}

	/**
	 * <p>if more than 2 axes (dimensions) are found in the cellSet, build recursive tabsheets as long as the remaining axes are more than 2.</p> 
	 *
	 */
	def String createTabSheet(Table table) {
		var body = ""
		body = '''
		«body»
		// either create a recursive tabsheet or a table
		Component component = null;
		if	(axisNo == 2) {
			component = createTable(cellSet);
		}
		else {		
			Integer axis = axisNo-1;
			TabSheet tabsheet = new TabSheet();
			tabsheet.setSizeFull();
			tabsheet.addSelectedTabChangeListener(new SelectedTabChangeListener() {
				@Override
				public void selectedTabChange(SelectedTabChangeEvent event) {
					if(tabSheets.containsKey(event.getTabSheet())) {
						getCoordinateSystem().clear();
						getCoordinateSystem().addAll(getCoordinates(event.getTabSheet()));
					}
				}
				private Collection<? extends Integer> getCoordinates(TabSheet tabsheet) {
					if(!(tabsheet.getTab(tabsheet.getSelectedTab()) instanceof TabSheet)) {
						int index = tabsheet.getTabPosition(tabsheet.getTab(tabsheet.getSelectedTab()));
						return tabSheets.get(tabsheet).get(index);
					}
					return getCoordinates((TabSheet)tabsheet.getTab(tabsheet.getSelectedTab()));
				}
			});
			tabSheets.put(tabsheet, new ArrayList<>());
			DerivedAxis tabAxis = cellSet.getAxes().get(axis);
			// create a tab page for all tab axis members
			int tabNo = 0;
			for	(DerivedPosition column : tabAxis.getPositions()) {
				// create the title for the axis
				String title = null;
				for (DerivedMember member : column.getMembers()) {
					if	(title == null) {
						title = dslMetadataService.translate(user.getLocale().toLanguageTag(), member.getCaption());
					}
					else {
						title += " / "+dslMetadataService.translate(user.getLocale().toLanguageTag(), member.getCaption());
					}
				}
				// position the data to this coordinate
				getCoordinateSystem().set(axis, tabNo);
				tabSheets.get(tabsheet).add(new ArrayList<Integer>(getCoordinateSystem()));
				component = createTabSheet(cellSet, axis);
				// set the caption
				if (component != null) {
					component.setCaption(title);
					tabsheet.addComponent(component);
		    		if (component instanceof CellSetFilterTable) {
			    		tabsheet.addComponent(((CellSetFilterTable)component));
		    		}
				}
				tabNo++;
			}
			component = tabsheet;
		}
		return component;'''
		return body
	}

	/**
	 * <p>build the table component.</p> 
	 *
	 */
	def String createTable(Table table) {
		var body = ""
		var constructTable = "final CellSetFilterTable table = new CellSetFilterTable();"
		var multiSelect = false;
		if	(table.isSelection) {
			constructTable = '''
				final CheckboxSelectionCellSetFilterTable table = new CheckboxSelectionCellSetFilterTable();
				table.addTableRowCheckListener(this);
				selectTable = table;
			'''
			multiSelect = (table.tabletype as TableSelection).multiSelection
		}
		body = '''
			«body»«constructTable»
			VerticalLayout tableLayout = new VerticalLayout();
			«IF table.isEmbedded && table.isTask»
			taskButtons = new ArrayList<Button>();
			HorizontalLayout tools = new HorizontalLayout();
			tools.addStyleName("os-embedded-toolbar");
			tools.setSpacing(false);
			
			Button b = new Button(null, new Button.ClickListener() {
			    public void buttonClick(ClickEvent event) {
			    	taskClient.operations(dataSourceContainer.getTaskId((int)table.getValue()), user, null, BPMOperation.Start);
			    	MPerspective perspective = eclipseContext.get(MPerspective.class);
					EventDispatcherEvent evnt = new EventDispatcherEvent(perspective, EventDispatcherCommand.CLOSE, "UserMenu", "«table.fullyQualifiedName»");
					eventDispatcher.sendEvent(evnt);
			    }
			});
			b.setIcon(themeResourceService.getThemeResource("task_action_resume", ThemeResourceType.ICON));
			b.setDescription(null);
			b.setEnabled(false);
			b.addStyleName("v-button-small");
			tools.addComponent(b);
			taskButtons.add(b);
			tableLayout.addComponent(tools);
			«ENDIF»
			table.setImmediate(true);
			table.setMultiSelect(«multiSelect»);
			table.setColumnCollapsingAllowed(true);
			table.setColumnReorderingAllowed(true);
			table.setId("table");
			«IF table.isTable»
			table.setRowHeaderMode(RowHeaderMode.«IF (table.tabletype as TableTable).headerMode !== null»«(table.tabletype as TableTable).headerMode.name()»«ELSE»HIDDEN«ENDIF»);
			«ENDIF»
			table.setFilterDecorator(tableFilterDecorator);
			table.setFilterBarVisible(«table.tabletype.filtering.booleanValue»);
			table.setSelectable(true);
			table.setSizeFull();
			tableLayout.addComponent(table);
			tables.add(table);
			initTable(table, cellSet);
			return tableLayout;'''
	}

	/**
	 * <p>build the table component.</p> 
	 *
	 */
	def String initTable(Table table) {
		var body = ""
		var TableAxis category_axis = null
		var TableAxis data_axis = null
		var idMap = <String, IdAttribute>newHashMap
		var formatMap = <String, FormatAttribute>newHashMap
		var intervalStyleMap = <String, Map<Double, Map<String, String>>>newHashMap
		var intervalIconMap = <String, Map<Double, Map<String, String>>>newHashMap
		var intervalTooltipMap = <String, Map<Double, Map<String, String>>>newHashMap
		var numberLookupStyleMap = <String, Map<Double, Map<String, String>>>newHashMap
		var numberLookupIconMap = <String, Map<Double, Map<String, String>>>newHashMap
		var numberLookupTooltipMap = <String, Map<Double, Map<String, String>>>newHashMap
		var intIntervalStyleMap = <String, Map<Integer, Map<String, String>>>newHashMap
		var intIntervalIconMap = <String, Map<Integer, Map<String, String>>>newHashMap
		var intIntervalTooltipMap = <String, Map<Integer, Map<String, String>>>newHashMap
		var intLookupStyleMap = <String, Map<Integer, Map<String, String>>>newHashMap
		var intLookupIconMap = <String, Map<Integer, Map<String, String>>>newHashMap
		var intLookupTooltipMap = <String, Map<Integer, Map<String, String>>>newHashMap
		var stringLookupStyleMap = <String, Map<String, Map<String, String>>>newHashMap
		var stringLookupIconMap = <String, Map<String, Map<String, String>>>newHashMap
		var stringLookupTooltipMap = <String, Map<String, Map<String, String>>>newHashMap
		var dateLookupStyleMap = <String, Map<Double, Map<String, String>>>newHashMap
		var dateLookupIconMap = <String, Map<Double, Map<String, String>>>newHashMap
		var dateLookupTooltipMap = <String, Map<Double, Map<String, String>>>newHashMap
		var blobLookupMap = <String, String>newHashMap()
		var enumLookupMap = <String, String>newHashMap()

		var rowStyle = ""

		for (element : table.dataMartTable.elements) {
			if (element instanceof TableAxis) {
				var axis = element as TableAxis
				if (axis.axis.getName.equals('COLUMNS')) {
					data_axis = axis
				}
				if (axis.axis.getName.equals('ROWS')) {
					category_axis = axis
				}
			}
		}

		if (category_axis === null) {
			category_axis = TableDSLFactory.eINSTANCE.createTableAxis();
			category_axis.axis = AxisEnum.ROWS;
			table.dataMartTable.elements.add(category_axis);
		}
		if (data_axis === null) {
			data_axis = TableDSLFactory.eINSTANCE.createTableAxis();
			data_axis.axis = AxisEnum.COLUMNS;
			table.dataMartTable.elements.add(data_axis);
		}
		// Initialization of the BlobLookupMap
		table.initBlobLookup(blobLookupMap)
		// Initialization of the EnumSet
		table.initEnumLookup(enumLookupMap)
		table.initIdMap(idMap)
		if (category_axis.hasDetails) {
			category_axis.initPropertyLookups(formatMap, 
				intervalStyleMap, intervalIconMap, intervalTooltipMap, numberLookupStyleMap, numberLookupIconMap, numberLookupTooltipMap, 
				intIntervalStyleMap, intIntervalIconMap, intIntervalTooltipMap, intLookupStyleMap, intLookupIconMap, intLookupTooltipMap, 
				stringLookupStyleMap, stringLookupIconMap, stringLookupTooltipMap, 
				dateLookupStyleMap, dateLookupIconMap, dateLookupTooltipMap)
		}
		if (data_axis.hasDetails) {
			data_axis.initPropertyLookups(formatMap, 
				intervalStyleMap, intervalIconMap, intervalTooltipMap, numberLookupStyleMap, numberLookupIconMap, numberLookupTooltipMap, 
				intIntervalStyleMap, intIntervalIconMap, intIntervalTooltipMap, intLookupStyleMap, intLookupIconMap, intLookupTooltipMap, 
				stringLookupStyleMap, stringLookupIconMap, stringLookupTooltipMap, 
				dateLookupStyleMap, dateLookupIconMap, dateLookupTooltipMap)
		}
		if (intervalStyleMap.keySet.size + numberLookupStyleMap.keySet.size + stringLookupStyleMap.keySet.size + intLookupStyleMap.keySet.size + intIntervalStyleMap.keySet.size +
			dateLookupStyleMap.keySet.size > 0 || category_axis.hasRowHeight) {
			body = '''
				«body»// add styles to page for the axis's custom styles
				Styles styles = Page.getCurrent().getStyles();
			'''
		}
		if (category_axis.hasRowHeight) {
			customformatCounter = customformatCounter + 1
			rowStyle = '''rowcustomformat«customformatCounter»'''
			body = '''«body»styles.add(".v-table-row-«rowStyle» {height: «category_axis.rowHeight»px;}");
			'''
		}
		body = '''«body»«createStyles(intervalStyleMap, numberLookupStyleMap, intIntervalStyleMap, intLookupStyleMap, stringLookupStyleMap, dateLookupStyleMap)» '''

		// all detail value information if present is mapped to methods of TableCellGenerator
		body = '''«body»«generatePropertyLookups(idMap, formatMap, 
			intervalStyleMap, intervalIconMap, intervalTooltipMap, numberLookupStyleMap, numberLookupIconMap, numberLookupTooltipMap, 
			intIntervalStyleMap, intIntervalIconMap, intIntervalTooltipMap, intLookupStyleMap, intLookupIconMap, intLookupTooltipMap, 
			stringLookupStyleMap, stringLookupIconMap, stringLookupTooltipMap, 
			dateLookupStyleMap, dateLookupIconMap, dateLookupTooltipMap, blobLookupMap, enumLookupMap)»'''
		body = '''
			«body»// DO NOT MOVE THESE LINES!!!!! The data source container creation has to be before the user properties readings
			dataSourceContainer = new CellSetIndexedContainer(eclipseContext, attributeLookupMap, cellSet, coordinateSystem, «table.isDatamartCrossJoined»);
			table.setContainerDataSource(dataSourceContainer);
			dataSourceContainer.addExtras(table);
			tableSettingsByUserProperties(table);
		'''
		body = '''
		«body»
		«table.sendEvent»
		«columnCollapsedListener»
		«columnReorderListener»
		«columnResizeListener»
		'''
		if (category_axis.hasRowHeight) {
			body = '''
				«body»dataSourceContainer.setRowStyle("«rowStyle»");
			'''
		}
		if (table.isTable) {
			if (table.dataMartTable instanceof TableDatamart) {
				for (element : (table.dataMartTable as TableDatamart).elements) {
					if (element instanceof TableAxis) {
						if ((element as TableAxis).preOrder !== null) {
							// create a ordering refresh event if applicable
							body = '''«body»«(element as TableAxis).preOrder.preOrder(element as TableAxis)»'''
						}
					}
				}
			}
		}

		//		if (!(table.tabletype as TableTable).pageable) {
		//			body = '''
		//			«body»if (cellSet.getAxes().get(«category_axis.axis.name.ordinal»).getPositions().size() > 15) {
		//			    table.setPageLength(15);
		//			}
		//			else
		//			{
		//			    table.setPageLength(0);
		//			}
		//			'''
		//		}
		// send a selection event if modeled
		if (table.isSelection) {
			body = '''
			«body»
			selectItemsFromWorkloadDto();
			'''
		}
		return body
	}
	
	def boolean isDatamartCrossJoined(Table table) {
		return (table.dataMartTable as TableDatamart).datamartRef.isCrossJoined(); 
	}

	def String modifyOddStyle(String style) {
		var start = style.indexOf("background-color:");
		var end = 0;
		if (start >= 0) {
			var pos = style.indexOf("(", start);
			if (pos >= 0) {
				var newrgb = "";
				end = style.indexOf(")", pos);
				var rgb = style.substring(pos + 1, end);
				var colors = rgb.split(",")
				for (color : colors) {
					var colorValue = Integer.parseInt(color)
					var diff = 15
					if (diff > colorValue) {
						diff = colorValue
					}
					colorValue = colorValue - diff
					newrgb = '''«newrgb»«IF newrgb.length > 0»,«ENDIF»«colorValue.toString»'''
				}
				return '''«style.substring(0, start)»background-color: rgb(«newrgb»)«style.substring(end + 1)»'''
			}
		}
		return style
	}

	/**
	 * <p>collected styles from the model are applied to code. Styles aside from the CSS are created at runtime.</p> 
	 *
	 */
	def String createStyles(
		HashMap<String, Map<Double, Map<String, String>>> intervalStyleMap,
		HashMap<String, Map<Double, Map<String, String>>> numberLookupStyleMap,
		HashMap<String, Map<Integer, Map<String, String>>> intIntervalStyleMap,
		HashMap<String, Map<Integer, Map<String, String>>> intLookupStyleMap,
		HashMap<String, Map<String, Map<String, String>>> stringLookupStyleMap,
		HashMap<String, Map<Double, Map<String, String>>> dateLookupStyleMap
	) {
		var stylePrefix = ".v-table-cell-content-"
		var body = ""
		for (format : intervalStyleMap.keySet) {
			for (interval : intervalStyleMap.get(format).keySet) {
				for (style : intervalStyleMap.get(format).get(interval).keySet) {
					body = '''«body»styles.add("«stylePrefix»«style» {«intervalStyleMap.get(format).get(interval).get(
						style)»}");
						'''
					body = '''«body»styles.add("«stylePrefix»«style»-odd {«modifyOddStyle(
						intervalStyleMap.get(format).get(interval).get(style))»}");
						'''
				}
			}
		}
		for (format : numberLookupStyleMap.keySet) {
			for (interval : numberLookupStyleMap.get(format).keySet) {
				for (style : numberLookupStyleMap.get(format).get(interval).keySet) {
					body = '''«body»styles.add("«stylePrefix»«style» {«numberLookupStyleMap.get(format).get(interval).
						get(style)»}");
						'''
					body = '''«body»styles.add("«stylePrefix»«style»-odd {«modifyOddStyle(
						numberLookupStyleMap.get(format).get(interval).get(style))»}");
						'''
				}
			}
		}
		for (format : intIntervalStyleMap.keySet) {
			for (interval : intIntervalStyleMap.get(format).keySet) {
				for (style : intIntervalStyleMap.get(format).get(interval).keySet) {
					body = '''«body»styles.add("«stylePrefix»«style» {«intIntervalStyleMap.get(format).get(interval).get(
						style)»}");
						'''
					body = '''«body»styles.add("«stylePrefix»«style»-odd {«modifyOddStyle(
						intIntervalStyleMap.get(format).get(interval).get(style))»}");
						'''
				}
			}
		}
		for (format : intLookupStyleMap.keySet) {
			for (interval : intLookupStyleMap.get(format).keySet) {
				for (style : intLookupStyleMap.get(format).get(interval).keySet) {
					body = '''«body»styles.add("«stylePrefix»«style» {«intLookupStyleMap.get(format).get(interval).
						get(style)»}");
						'''
					body = '''«body»styles.add("«stylePrefix»«style»-odd {«modifyOddStyle(
						intLookupStyleMap.get(format).get(interval).get(style))»}");
						'''
				}
			}
		}
		for (format : stringLookupStyleMap.keySet) {
			for (interval : stringLookupStyleMap.get(format).keySet) {
				for (style : stringLookupStyleMap.get(format).get(interval).keySet) {
					body = '''«body»styles.add("«stylePrefix»«style» {«stringLookupStyleMap.get(format).get(interval).
						get(style)»}");
						'''
					body = '''«body»styles.add("«stylePrefix»«style»-odd {«modifyOddStyle(
						stringLookupStyleMap.get(format).get(interval).get(style))»}");
						'''
				}
			}
		}
		for (format : dateLookupStyleMap.keySet) {
			for (interval : dateLookupStyleMap.get(format).keySet) {
				for (style : dateLookupStyleMap.get(format).get(interval).keySet) {
					body = '''«body»styles.add("«stylePrefix»«style» {«dateLookupStyleMap.get(format).get(interval).
						get(style)»}");
						'''
					body = '''«body»styles.add("«stylePrefix»«style»-odd {«modifyOddStyle(
						dateLookupStyleMap.get(format).get(interval).get(style))»}");
						'''
				}
			}
		}
		return body
	}

	/**
	 * <p>In order to customize the cellGenerator, methods must be called inferred from the model.</p> 
	 *
	 */
	def String generatePropertyLookups(
		HashMap<String, IdAttribute> idMap,
		HashMap<String, FormatAttribute> formatMap,
		HashMap<String, Map<Double, Map<String, String>>> intervalStyleMap,
		HashMap<String, Map<Double, Map<String, String>>> intervalIconMap,
		HashMap<String, Map<Double, Map<String, String>>> intervalTooltipMap,
		HashMap<String, Map<Double, Map<String, String>>> numberLookupStyleMap,
		HashMap<String, Map<Double, Map<String, String>>> numberLookupIconMap,
		HashMap<String, Map<Double, Map<String, String>>> numberLookupTooltipMap,
		HashMap<String, Map<Integer, Map<String, String>>> intIntervalStyleMap,
		HashMap<String, Map<Integer, Map<String, String>>> intIntervalIconMap,
		HashMap<String, Map<Integer, Map<String, String>>> intIntervalTooltipMap,
		HashMap<String, Map<Integer, Map<String, String>>> intLookupStyleMap,
		HashMap<String, Map<Integer, Map<String, String>>> intLookupIconMap,
		HashMap<String, Map<Integer, Map<String, String>>> intLookupTooltipMap,
		HashMap<String, Map<String, Map<String, String>>> stringLookupStyleMap,
		HashMap<String, Map<String, Map<String, String>>> stringLookupIconMap,
		HashMap<String, Map<String, Map<String, String>>> stringLookupTooltipMap,
		HashMap<String, Map<Double, Map<String, String>>> dateLookupStyleMap,
		HashMap<String, Map<Double, Map<String, String>>> dateLookupIconMap,
		HashMap<String, Map<Double, Map<String, String>>> dateLookupTooltipMap,
		HashMap<String, String> blobLookupMap,
		HashMap<String, String> enumLookupMap
	) {
		var attributes = <String>newHashSet
		attributes.addAll(formatMap.keySet)
		attributes.addAll(intervalStyleMap.keySet)
		attributes.addAll(intervalIconMap.keySet)
		attributes.addAll(intervalTooltipMap.keySet)
		attributes.addAll(numberLookupStyleMap.keySet)
		attributes.addAll(numberLookupIconMap.keySet)
		attributes.addAll(numberLookupTooltipMap.keySet)
		attributes.addAll(intIntervalStyleMap.keySet)
		attributes.addAll(intIntervalIconMap.keySet)
		attributes.addAll(intIntervalTooltipMap.keySet)
		attributes.addAll(intLookupStyleMap.keySet)
		attributes.addAll(intLookupIconMap.keySet)
		attributes.addAll(intLookupTooltipMap.keySet)
		attributes.addAll(stringLookupStyleMap.keySet)
		attributes.addAll(stringLookupIconMap.keySet)
		attributes.addAll(stringLookupTooltipMap.keySet)
		attributes.addAll(dateLookupStyleMap.keySet)
		attributes.addAll(dateLookupIconMap.keySet)
		attributes.addAll(dateLookupTooltipMap.keySet)
		attributes.addAll(blobLookupMap.keySet)
		attributes.addAll(enumLookupMap.keySet)
		attributes.addAll(idMap.keySet)
		var body = ""

		if (attributes.size > 0) {
			body = '''«body»// add attributeLookups for column and row attribute supplements
				'''
		}

		// all detail value information if present is mapped to methods of PropertyLookup
		// styles and icons can be mapped to value ranges 
		for (attribute : attributes) {
			var hadAttribute = false
			if(attribute !== null) {
				hadAttribute = true
				body = '''«body»attributeLookupMap.put("«attribute.toString.toUpperCase»", new PropertyLookup(themeResourceService, dslMetadataService, blobService, user.getLocale())'''
	
				// apply all collapsed id columns
				if (idMap.containsKey(attribute)) {
					body = '''
						«body»
							.setCollapseColumn(«idMap.get(attribute).collapsed.booleanValue.toString»)
					'''
				}
	
				// apply all found formatter information
				if (formatMap.containsKey(attribute)) {
					if (formatMap.get(attribute).formatter !== null) {
						body = '''
							«body»
								.setFormat("«formatMap.get(attribute).formatter»")
						'''
					}
					if (formatMap.get(attribute).tooltipPattern !== null) {
						body = '''
							«body»
								.setTooltipPattern("«formatMap.get(attribute).tooltipPattern»")
						'''
					}
					if (intervalIconMap.size > 0) {
						body = '''
							«body»
								.setHideLabel(«formatMap.get(attribute).hideLabelInterval.booleanValue.toString»)
						'''
					} else if (numberLookupIconMap.size + intLookupIconMap.size + stringLookupIconMap.size + dateLookupIconMap.size > 0) {
						body = '''
							«body»
								.setHideLabel(«formatMap.get(attribute).hideLabelLookup.booleanValue.toString»)
						'''
					} else {
						body = '''
							«body»
								.setHideLabel(«formatMap.get(attribute).hideImageLabel.booleanValue.toString»)
						'''
					}
					body = '''
						«body»
							.setCollapseColumn(«formatMap.get(attribute).collapsed.booleanValue.toString»)
					'''
					if (formatMap.get(attribute).hasImage) {
						body = '''
							«body»
								.setImageService("«formatMap.get(attribute).imagePath»", «formatMap.get(attribute).hasImageParameter.booleanValue.
								toString»)
						'''
						if (formatMap.get(attribute).hasImageResize) {
							body = '''
							«body»
								.setResizeString("«formatMap.get(attribute).imageResizeString»")'''
						}
					}
					if (formatMap.get(attribute).iconName !== null) {
						body = '''
							«body»
								.setColumnIcon("«formatMap.get(attribute).iconName»")
						'''
					}
				}
				// number
				// apply all interval styles information
				if (intervalStyleMap.containsKey(attribute)) {
					for (interval : intervalStyleMap.get(attribute).keySet) {
						for (style : intervalStyleMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addStyleInterval(«IF style.startsWith("date")»ExtendedDate.add(new Date(), (long)«interval * -1», TimeUnit.DAYS)«ELSE»«interval»«ENDIF», "«style»")
							'''
						}
					}
				}
	
				// apply all interval icons information
				if (intervalIconMap.containsKey(attribute)) {
					for (interval : intervalIconMap.get(attribute).keySet) {
						for (style : intervalIconMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addResourceInterval(«IF style.startsWith("date")»ExtendedDate.add(new Date(), (long)«interval * -1», TimeUnit.DAYS)«ELSE»«interval»«ENDIF», "«intervalIconMap.
									get(attribute).get(interval).get(style)»")
							'''
						}
					}
				}
	
				// apply all interval tooltip information
				if (intervalTooltipMap.containsKey(attribute)) {
					for (interval : intervalTooltipMap.get(attribute).keySet) {
						for (style : intervalTooltipMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addTooltipInterval(«IF style.startsWith("date")»ExtendedDate.add(new Date(), (long)«interval * -1», TimeUnit.DAYS)«ELSE»«interval»«ENDIF», "«intervalTooltipMap.get(attribute).get(interval).get(style)»")
							'''
						}
					}
				}
	
				// styles and icons can be mapped to discrete double values (lookups) 
				// apply all lookup styles information
				if (numberLookupStyleMap.containsKey(attribute)) {
					for (interval : numberLookupStyleMap.get(attribute).keySet) {
						for (style : numberLookupStyleMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addStyleLookup(«interval», "«style»")
							'''
						}
					}
				}
	
				// apply all lookup icons information
				if (numberLookupIconMap.containsKey(attribute)) {
					for (interval : numberLookupIconMap.get(attribute).keySet) {
						for (style : numberLookupIconMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addResourceLookup(«interval», "«numberLookupIconMap.get(attribute).get(interval).get(style)»")
							'''
						}
					}
				}
	
				// apply all lookup tooltip information
				if (numberLookupTooltipMap.containsKey(attribute)) {
					for (interval : numberLookupTooltipMap.get(attribute).keySet) {
						for (style : numberLookupTooltipMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addTooltipLookup(«interval», "«numberLookupTooltipMap.get(attribute).get(interval).get(style)»")
							'''
						}
					}
				}
				
				// integer
				// apply all interval styles information
				if (intIntervalStyleMap.containsKey(attribute)) {
					for (intInterval : intIntervalStyleMap.get(attribute).keySet) {
						for (style : intIntervalStyleMap.get(attribute).get(intInterval).keySet) {
							body = '''
								«body»
									.addStyleInterval(«IF style.startsWith("date")»ExtendedDate.add(new Date(), (long)«intInterval * -1», TimeUnit.DAYS)«ELSE»«intInterval»«ENDIF», "«style»")
							'''
						}
					}
				}
	
				// apply all interval icons information
				if (intIntervalIconMap.containsKey(attribute)) {
					for (intInterval : intIntervalIconMap.get(attribute).keySet) {
						for (style : intIntervalIconMap.get(attribute).get(intInterval).keySet) {
							body = '''
								«body»
									.addResourceInterval(«IF style.startsWith("date")»ExtendedDate.add(new Date(), (long)«intInterval * -1», TimeUnit.DAYS)«ELSE»«intInterval»«ENDIF», "«intIntervalIconMap.
									get(attribute).get(intInterval).get(style)»")
							'''
						}
					}
				}
	
				// apply all interval tooltip information
				if (intIntervalTooltipMap.containsKey(attribute)) {
					for (intInterval : intIntervalTooltipMap.get(attribute).keySet) {
						for (style : intIntervalTooltipMap.get(attribute).get(intInterval).keySet) {
							body = '''
								«body»
									.addTooltipInterval(«IF style.startsWith("date")»ExtendedDate.add(new Date(), (long)«intInterval * -1», TimeUnit.DAYS)«ELSE»«intInterval»«ENDIF», "«intIntervalTooltipMap.get(attribute).get(intInterval).get(style)»")
							'''
						}
					}
				}
	
				// styles and icons can be mapped to discrete double values (lookups) 
				// apply all lookup styles information
				if (intLookupStyleMap.containsKey(attribute)) {
					for (interval : intLookupStyleMap.get(attribute).keySet) {
						for (style : intLookupStyleMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addStyleLookup(«interval», "«style»")
							'''
						}
					}
				}
	
				// apply all lookup icons information
				if (intLookupIconMap.containsKey(attribute)) {
					for (interval : intLookupIconMap.get(attribute).keySet) {
						for (style : intLookupIconMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addResourceLookup(«interval», "«intLookupIconMap.get(attribute).get(interval).get(style)»")
							'''
						}
					}
				}
	
				// apply all lookup tooltip information
				if (intLookupTooltipMap.containsKey(attribute)) {
					for (interval : intLookupTooltipMap.get(attribute).keySet) {
						for (style : intLookupTooltipMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addTooltipLookup(«interval», "«intLookupTooltipMap.get(attribute).get(interval).get(style)»")
							'''
						}
					}
				}
	
				// styles and icons can be mapped to discrete string values (lookups) 
				// apply all lookup styles information
				if (stringLookupStyleMap.containsKey(attribute)) {
					for (interval : stringLookupStyleMap.get(attribute).keySet) {
						for (style : stringLookupStyleMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addStyleLookup("«interval»", "«style»")
							'''
						}
					}
				}
	
				// apply all lookup icons information
				if (stringLookupIconMap.containsKey(attribute)) {
					for (interval : stringLookupIconMap.get(attribute).keySet) {
						for (style : stringLookupIconMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addResourceLookup("«interval»", "«stringLookupIconMap.get(attribute).get(interval).get(style)»")
							'''
						}
					}
				}
	
				// apply all lookup tooltip information
				if (stringLookupTooltipMap.containsKey(attribute)) {
					for (interval : stringLookupTooltipMap.get(attribute).keySet) {
						for (style : stringLookupTooltipMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addTooltipLookup("«interval»", "«stringLookupTooltipMap.get(attribute).get(interval).get(style)»")
							'''
						}
					}
				}
	
				// styles and icons can be mapped to discrete date values (lookups) 
				// apply all lookup styles information
				if (dateLookupStyleMap.containsKey(attribute)) {
					for (interval : dateLookupStyleMap.get(attribute).keySet) {
						for (style : dateLookupStyleMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addStyleLookup(ExtendedDate.add(new Date(),«interval * -1»,TimeUnit.DAYS), "«style»")
							'''
						}
					}
				}
	
				// apply all lookup icons information
				if (dateLookupIconMap.containsKey(attribute)) {
					for (interval : dateLookupIconMap.get(attribute).keySet) {
						for (style : dateLookupIconMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addResourceLookup(ExtendedDate.add(new Date(),«interval * -1»,TimeUnit.DAYS), "«dateLookupIconMap.get(attribute).
									get(interval).get(style)»")
							'''
						}
					}
				}
	
				// apply all lookup tooltip information
				if (dateLookupTooltipMap.containsKey(attribute)) {
					for (interval : dateLookupTooltipMap.get(attribute).keySet) {
						for (style : dateLookupTooltipMap.get(attribute).get(interval).keySet) {
							body = '''
								«body»
									.addTooltipLookup(ExtendedDate.add(new Date(),«interval * -1»,TimeUnit.DAYS), "«dateLookupTooltipMap.get(attribute).get(interval).get(style)»")
							'''
						}
					}
				}
	
				// apply all lookup blob information
				if (blobLookupMap.containsKey(attribute)) {
					body = '''
						«body»
							.setBlob(true, «blobLookupMap.get(attribute)»)
					'''
				}
				// apply all lookup enum information
				if (enumLookupMap.containsKey(attribute)) {
					body = '''
						«body»
							.setEnum(true, «enumLookupMap.get(attribute)».class)
					'''
				}
				
			}
			if(hadAttribute) {
				body = '''«body»);
'''
			}
		}
		return body
	}

	/**
	 * <p>infer the model's setting for cell configuration including blobs.</p> 
	 *
	 */
	def initBlobLookup(Table table, Map<String, String> blobMap) {
		var datamartSource = table.dataMartTable.datamartRef.source
		if (datamartSource instanceof DatamartEntity) {
			// Initial all attributes of the entity
			for (attribute : table.dataMartTable.datamartRef.allEntityAttributes()) {
				var resolution = attribute.attributeRef.resolution
				if (resolution!== null) {
					blobMap.put('''«attribute?.attributeRef?.name?.replace("^", "")»''', resolution?.value)
				}
			}
			// In case of having any explicit attribute definition
			for (attribute : table.dataMartTable.datamartRef.allEntityAttributes()) {
				var resolution = attribute.attributeRef.resolution
				if (resolution !== null) {
					blobMap.put(DatamartAttributeUtil.getAliasedAttributeName(attribute), resolution?.value)
				}
			}
		}
	}
	
	/**
	 * <p>infer the model's setting for cell configuration including enums.</p> 
	 *
	 */
	def initEnumLookup(Table table, Map<String, String> enumMap) {
		var datamartSource = table.dataMartTable.datamartRef.source
		if (datamartSource instanceof DatamartEntity) {
			// Initial all attributes of the entity
			for (attribute : table.dataMartTable.datamartRef.allEntityAttributes()) {
				if (attribute.attributeRef.typeIsEnum) {
					enumMap.put('''«attribute?.attributeRef?.name?.replace("^", "")»''', '''«attribute?.attributeRef?.type?.fullyQualifiedName»''')
				}
			}
		}
	}
	
	def LKeyAndValue getResolution(LEntityAttribute attribute) {
		if(attribute.properties.exists[key.toLowerCase.equals(IPropertyConstants.PROPERTY_BLOB)]) {		// try entity attributes
			return attribute.properties.findFirst[key.toLowerCase.equals(IPropertyConstants.PROPERTY_BLOB)]
		} else if (attribute.type instanceof LDataType){	// try data type attributes
			if((attribute.type as LDataType).properties.exists[key.toLowerCase.equals(IPropertyConstants.PROPERTY_BLOB)]) {
				return (attribute.type as LDataType).properties.findFirst[key.toLowerCase.equals(IPropertyConstants.PROPERTY_BLOB)]
			}
		}
	}

	/**
	 * <p>infer the model's setting for cell configuration including styles and images.</p> 
	 *
	 */
	def initPropertyLookups(
		TableAxis axis,
		HashMap<String, FormatAttribute> formatMap,
		HashMap<String, Map<Double, Map<String, String>>> intervalStyleMap,
		HashMap<String, Map<Double, Map<String, String>>> intervalIconMap,
		HashMap<String, Map<Double, Map<String, String>>> intervalTooltipMap,
		HashMap<String, Map<Double, Map<String, String>>> numberLookupStyleMap,
		HashMap<String, Map<Double, Map<String, String>>> numberLookupIconMap,
		HashMap<String, Map<Double, Map<String, String>>> numberLookupTooltipMap,
		HashMap<String, Map<Integer, Map<String, String>>> intIntervalStyleMap,
		HashMap<String, Map<Integer, Map<String, String>>> intIntervalIconMap,
		HashMap<String, Map<Integer, Map<String, String>>> intIntervalTooltipMap,
		HashMap<String, Map<Integer, Map<String, String>>> intLookupStyleMap,
		HashMap<String, Map<Integer, Map<String, String>>> intLookupIconMap,
		HashMap<String, Map<Integer, Map<String, String>>> intLookupTooltipMap,
		HashMap<String, Map<String, Map<String, String>>> stringLookupStyleMap,
		HashMap<String, Map<String, Map<String, String>>> stringLookupIconMap,
		HashMap<String, Map<String, Map<String, String>>> stringLookupTooltipMap,
		HashMap<String, Map<Double, Map<String, String>>> dateLookupStyleMap,
		HashMap<String, Map<Double, Map<String, String>>> dateLookupIconMap,
		HashMap<String, Map<Double, Map<String, String>>> dateLookupTooltipMap
	) {
		for (value : axis.values) {

			// create formatter map
			var prop = new FormatAttribute
			if (value.formatter !== null) {
				prop.formatter = value.formatter.format
			}
			if (value.tooltipPattern !== null) {
				prop.tooltipPattern = value.tooltipPattern.tooltipPattern
			}
			prop.hideLabelLookup = value.hideLabelLookup
			prop.collapsed = value.collapsed
			prop.hasImage = value.hasImage
			prop.iconName = value.iconName
			if (value.hasImage && value.image !== null) {
				prop.hasImageParameter = value.image.hasParameter
				prop.imagePath = value.image.imagePathPattern
				prop.hideImageLabel = value.image.hideImageLabel
				prop.hasImageResize = value.image.resize
				prop.imageResizeString = value.image.resizeString
				if (value.image.hasParameter) {
					switch (value.image.imagePathParameter) {
						TableColumn:
							prop.imagePathParameter = (value.image.imagePathParameter as TableColumn).valueRef.
								columnRef.literal
						TableMeasure:
							prop.imagePathParameter = (value.image.imagePathParameter as TableMeasure).valueRef.
								measureRef.name
						TableDerived:
							prop.imagePathParameter = (value.image.imagePathParameter as TableDerived).valueRef.
								derivedRef.name
						TableHierarchy:
							prop.imagePathParameter = (value.image.imagePathParameter as TableHierarchy).valueRef.
								levelName
						TableAttribute:
							prop.imagePathParameter = DatamartAttributeUtil.getAliasedAttributeName((value.image.imagePathParameter as TableAttribute).valueRef)
						TableAggregation:
							prop.imagePathParameter = (value.image.imagePathParameter as TableAggregation).valueRef.
								aggregation.name()
					}
				}
			}
			// create a columnValueMap
			var columnValueMap = <String,String>newHashMap()
			switch value.column {
				TableOrdinal: {columnValueMap.put(TableOrdinal.canonicalName, '''#«axis.axis.literal»«(value.column as TableOrdinal).valueRef.toString»''')
							  formatMap.put(columnValueMap.get(TableOrdinal.canonicalName), prop)}
				TableAllColumns: {columnValueMap.put(TableAllColumns.canonicalName, '''?«axis.axis.literal»''')  
								  formatMap.put(columnValueMap.get(TableAllColumns.canonicalName), prop)}
				TableMeasure: {columnValueMap.put(TableMeasure.canonicalName, '''«(value.column as TableMeasure).valueRef.measureRef.name»''')
							   formatMap.put(columnValueMap.get(TableMeasure.canonicalName), prop)}
				TableDerived: {columnValueMap.put(TableDerived.canonicalName, '''«(value.column as TableDerived).valueRef.derivedRef.name»''')
							   formatMap.put(columnValueMap.get(TableDerived.canonicalName), prop)}
				TableHierarchy: {columnValueMap.put(TableHierarchy.canonicalName, '''«(value.column as TableHierarchy).valueRef.levelName»''')
								 formatMap.put(columnValueMap.get(TableHierarchy.canonicalName), prop)}
				TableAttribute: {columnValueMap.put(TableAttribute.canonicalName, '''«DatamartAttributeUtil.getAliasedAttributeName((value.column as TableAttribute).valueRef)»''')
								 formatMap.put(columnValueMap.get(TableAttribute.canonicalName), prop)}
				TableAggregation: {columnValueMap.put(TableAggregation.canonicalName, '''«(value.column as TableAggregation).valueRef.aggregation.name()»''')
								   formatMap.put(columnValueMap.get(TableAggregation.canonicalName), prop)}
				TableColumn: {columnValueMap.put(TableColumn.canonicalName, '''«(value.column as TableColumn).valueRef.columnRef.literal»''')
							  formatMap.put(columnValueMap.get(TableColumn.canonicalName), prop)}
			}

			// create styles interval map
			var valueMap = <Double, Map<String, String>>newTreeMap(new DoubleComparator())
			var intMap = <Integer, Map<String, String>>newTreeMap(new IntegerComparator())
			for (interval : value.intervals) {
				switch interval {
					TableNumberInterval:
						if ((interval as TableNumberInterval).numberRange instanceof TableTextColor ||
							(interval as TableNumberInterval).numberRange instanceof TableCellColor) {
							var key = new Double((interval as TableNumberInterval).numberIntervalValue)
							var styleAspect = ""
							var Map<String, String> styleMap = null
							if (valueMap.containsKey(key)) {
								styleMap = valueMap.get(key)
								styleAspect = styleMap.get(styleMap.keySet.get(0))
							} else {
								styleMap = <String, String>newHashMap
								styleAspect = ""
								customformatCounter = customformatCounter + 1
							}
							switch (interval as TableNumberInterval).numberRange {
								TableTextColor: styleAspect = '''«styleAspect»color: «((interval as TableNumberInterval).
									numberRange as TableTextColor).rgb.toRGB»;'''
								TableCellColor: styleAspect = '''«styleAspect»background-color: «((interval as TableNumberInterval).
									numberRange as TableCellColor).rgb.toRGB»;'''
							}
							if (styleAspect.length > 0) {
								styleMap.put('''numbercustomformat«customformatCounter»''', styleAspect)
								valueMap.put(key, styleMap)
							}
						}
					TableIntInterval:
						if ((interval as TableIntInterval).intRange instanceof TableTextColor ||
							(interval as TableIntInterval).intRange instanceof TableCellColor) {
							var key = new Integer((interval as TableIntInterval).intIntervalValue)
							var styleAspect = ""
							var Map<String, String> styleMap = null
							if (valueMap.containsKey(key)) {
								styleMap = valueMap.get(key)
								styleAspect = styleMap.get(styleMap.keySet.get(0))
							} else {
								styleMap = <String, String>newHashMap
								styleAspect = ""
								customformatCounter = customformatCounter + 1
							}
							switch (interval as TableIntInterval).intRange {
								TableTextColor: styleAspect = '''«styleAspect»color: «((interval as TableIntInterval).
									intRange as TableTextColor).rgb.toRGB»;'''
								TableCellColor: styleAspect = '''«styleAspect»background-color: «((interval as TableIntInterval).
									intRange as TableCellColor).rgb.toRGB»;'''
							}
							if (styleAspect.length > 0) {
								styleMap.put('''numbercustomformat«customformatCounter»''', styleAspect)
								intMap.put(key, styleMap)
							}
						}
					TableDateDayInterval:
						if ((interval as TableDateDayInterval).dateRange instanceof TableTextColor ||
							(interval as TableDateDayInterval).dateRange instanceof TableCellColor) {
							var key = new Double((interval as TableDateDayInterval).dateIntervalValue)
							var styleAspect = ""
							var Map<String, String> styleMap = null
							if (valueMap.containsKey(key)) {
								styleMap = valueMap.get(key)
								styleAspect = styleMap.get(styleMap.keySet.get(0))
							} else {
								styleMap = <String, String>newHashMap
								styleAspect = ""
								customformatCounter = customformatCounter + 1
							}
							switch (interval as TableDateDayInterval).dateRange {
								TableTextColor: styleAspect = '''«styleAspect»color: «((interval as TableDateDayInterval).
									dateRange as TableTextColor).rgb.toRGB»;'''
								TableCellColor: styleAspect = '''«styleAspect»background-color: «((interval as TableDateDayInterval).
									dateRange as TableCellColor).rgb.toRGB»;'''
							}
							if (styleAspect.length > 0) {
								styleMap.put('''datecustomformat«customformatCounter»''', styleAspect)
								valueMap.put(key, styleMap)
							}
						}
				}
			}
			if (valueMap.size > 0) {
				switch value.column {
					TableOrdinal: intervalStyleMap.put(columnValueMap.get(TableOrdinal.canonicalName), valueMap)
					TableAllColumns: intervalStyleMap.put(columnValueMap.get(TableAllColumns.canonicalName), valueMap)
					TableMeasure: intervalStyleMap.put(columnValueMap.get(TableMeasure.canonicalName), valueMap)
					TableDerived: intervalStyleMap.put(columnValueMap.get(TableDerived.canonicalName), valueMap)
					TableHierarchy: intervalStyleMap.put(columnValueMap.get(TableHierarchy.canonicalName), valueMap)
					TableAttribute: intervalStyleMap.put(columnValueMap.get(TableAttribute.canonicalName), valueMap)
					TableAggregation: intervalStyleMap.put(columnValueMap.get(TableAggregation.canonicalName), valueMap)
					TableColumn: intervalStyleMap.put(columnValueMap.get(TableColumn.canonicalName), valueMap)
				}
			}
			if (intMap.size > 0) {
				switch value.column {
					TableOrdinal: intIntervalStyleMap.put(columnValueMap.get(TableOrdinal.canonicalName), intMap)
					TableAllColumns: intIntervalStyleMap.put(columnValueMap.get(TableAllColumns.canonicalName), intMap)
					TableMeasure: intIntervalStyleMap.put(columnValueMap.get(TableMeasure.canonicalName), intMap)
					TableDerived: intIntervalStyleMap.put(columnValueMap.get(TableDerived.canonicalName), intMap)
					TableHierarchy: intIntervalStyleMap.put(columnValueMap.get(TableHierarchy.canonicalName), intMap)
					TableAttribute: intIntervalStyleMap.put(columnValueMap.get(TableAttribute.canonicalName), intMap)
					TableAggregation: intIntervalStyleMap.put(columnValueMap.get(TableAggregation.canonicalName), intMap)
					TableColumn: intIntervalStyleMap.put(columnValueMap.get(TableColumn.canonicalName), intMap)
				}
			}

			// create icon interval map
			var valueIconMap = <Double, Map<String, String>>newTreeMap(new DoubleComparator())
			var intIconMap = <Integer, Map<String, String>>newTreeMap(new IntegerComparator())
			for (interval : value.intervals) {
				switch interval {
					TableNumberInterval: {
						var key = new Double((interval as TableNumberInterval).numberIntervalValue)
						var iconAspect = ""
						var Map<String, String> iMap = <String, String>newHashMap
						switch (interval as TableNumberInterval).numberRange {
							TableIcon: iconAspect = '''«((interval as TableNumberInterval).numberRange as TableIcon).
								icon»'''
							TableTrend: iconAspect = '''trend_«((interval as TableNumberInterval).numberRange as TableTrend).
								icon.literal»'''
						}
						if (iconAspect.length > 0) {
							iMap.put("numbericon", iconAspect)
							valueIconMap.put(key, iMap)
						}

					}
					TableIntInterval: {
						var key = new Integer((interval as TableIntInterval).intIntervalValue)
						var iconAspect = ""
						var Map<String, String> iMap = <String, String>newHashMap
						switch (interval as TableIntInterval).intRange {
							TableIcon: iconAspect = '''«((interval as TableIntInterval).intRange as TableIcon).
								icon»'''
							TableTrend: iconAspect = '''trend_«((interval as TableIntInterval).intRange as TableTrend).
								icon.literal»'''
						}
						if (iconAspect.length > 0) {
							iMap.put("numbericon", iconAspect)
							intIconMap.put(key, iMap)
						}

					}
					TableDateDayInterval: {
						var key = new Double((interval as TableDateDayInterval).dateIntervalValue)
						var iconAspect = ""
						var Map<String, String> iMap = <String, String>newHashMap
						switch (interval as TableDateDayInterval).dateRange {
							TableIcon: iconAspect = '''«((interval as TableDateDayInterval).dateRange as TableIcon).
								icon»'''
							TableTrend: iconAspect = '''trend_«((interval as TableDateDayInterval).dateRange as TableTrend).
								icon.literal»'''
						}
						if (iconAspect.length > 0) {
							iMap.put("dateicon", iconAspect)
							valueIconMap.put(key, iMap)
						}

					}
				}
			}
			if (valueIconMap.size > 0) {
				switch value.column {
					TableOrdinal: intervalIconMap.put(columnValueMap.get(TableOrdinal.canonicalName), valueIconMap)
					TableAllColumns: intervalIconMap.put(columnValueMap.get(TableAllColumns.canonicalName), valueIconMap)
					TableMeasure: intervalIconMap.put(columnValueMap.get(TableMeasure.canonicalName), valueIconMap)
					TableDerived: intervalIconMap.put(columnValueMap.get(TableDerived.canonicalName), valueIconMap)
					TableHierarchy: intervalIconMap.put(columnValueMap.get(TableHierarchy.canonicalName), valueIconMap)
					TableAttribute: intervalIconMap.put(columnValueMap.get(TableAttribute.canonicalName), valueIconMap)
					TableAggregation: intervalIconMap.put(columnValueMap.get(TableAggregation.canonicalName), valueIconMap)
					TableColumn: intervalIconMap.put(columnValueMap.get(TableColumn.canonicalName), valueIconMap)
				}
			}
			if (intIconMap.size > 0) {
				switch value.column {
					TableOrdinal: intIntervalIconMap.put(columnValueMap.get(TableOrdinal.canonicalName), intIconMap)
					TableAllColumns: intIntervalIconMap.put(columnValueMap.get(TableAllColumns.canonicalName), intIconMap)
					TableMeasure: intIntervalIconMap.put(columnValueMap.get(TableMeasure.canonicalName), intIconMap)
					TableDerived: intIntervalIconMap.put(columnValueMap.get(TableDerived.canonicalName), intIconMap)
					TableHierarchy: intIntervalIconMap.put(columnValueMap.get(TableHierarchy.canonicalName), intIconMap)
					TableAttribute: intIntervalIconMap.put(columnValueMap.get(TableAttribute.canonicalName), intIconMap)
					TableAggregation: intIntervalIconMap.put(columnValueMap.get(TableAggregation.canonicalName), intIconMap)
					TableColumn: intIntervalIconMap.put(columnValueMap.get(TableColumn.canonicalName), intIconMap)
				}
			}

			// create tooltip interval map
			var valueTooltipMap = <Double, Map<String, String>>newTreeMap(new DoubleComparator())
			var intTooltipMap = <Integer, Map<String, String>>newTreeMap(new IntegerComparator())
			for (interval : value.intervals) {
				switch interval {
					TableNumberInterval: {
						var key = new Double((interval as TableNumberInterval).numberIntervalValue)
						var tooltipAspect = ""
						var Map<String, String> tMap = <String, String>newHashMap
						switch (interval as TableNumberInterval).numberRange {
							TableTooltip: tooltipAspect = '''«((interval as TableNumberInterval).numberRange as TableTooltip).
								tooltip»'''
						}
						if (tooltipAspect.length > 0) {
							tMap.put("numbertooltip", tooltipAspect)
							valueTooltipMap.put(key, tMap)
						}
					}
					TableIntInterval: {
						var key = new Integer((interval as TableIntInterval).intIntervalValue)
						var tooltipAspect = ""
						var Map<String, String> tMap = <String, String>newHashMap
						switch (interval as TableIntInterval).intRange {
							TableTooltip: tooltipAspect = '''«((interval as TableIntInterval).intRange as TableTooltip).
								tooltip»'''
						}
						if (tooltipAspect.length > 0) {
							tMap.put("numbertooltip", tooltipAspect)
							intTooltipMap.put(key, tMap)
						}
					}
					TableDateDayInterval: {
						var key = new Double((interval as TableDateDayInterval).dateIntervalValue)
						var tooltipAspect = ""
						var Map<String, String> tMap = <String, String>newHashMap
						switch (interval as TableDateDayInterval).dateRange {
							TableTooltip: tooltipAspect = '''«((interval as TableDateDayInterval).dateRange as TableTooltip).
								tooltip»'''
						}
						if (tooltipAspect.length > 0) {
							tMap.put("datetooltip", tooltipAspect)
							valueTooltipMap.put(key, tMap)
						}
					}
				}
			}
			if (valueTooltipMap.size > 0) {
				switch value.column {
					TableOrdinal: intervalTooltipMap.put(columnValueMap.get(TableOrdinal.canonicalName), valueTooltipMap)
					TableAllColumns: intervalTooltipMap.put(columnValueMap.get(TableAllColumns.canonicalName), valueTooltipMap)
					TableMeasure: intervalTooltipMap.put(columnValueMap.get(TableMeasure.canonicalName), valueTooltipMap)
					TableDerived: intervalTooltipMap.put(columnValueMap.get(TableDerived.canonicalName), valueTooltipMap)
					TableHierarchy: intervalTooltipMap.put(columnValueMap.get(TableHierarchy.canonicalName), valueTooltipMap)
					TableAttribute: intervalTooltipMap.put(columnValueMap.get(TableAttribute.canonicalName), valueTooltipMap)
					TableAggregation: intervalTooltipMap.put(columnValueMap.get(TableAggregation.canonicalName), valueTooltipMap)
					TableColumn: intervalTooltipMap.put(columnValueMap.get(TableColumn.canonicalName), valueTooltipMap)
				}
			}
			if (intTooltipMap.size > 0) {
				switch value.column {
					TableOrdinal: intIntervalTooltipMap.put(columnValueMap.get(TableOrdinal.canonicalName), intTooltipMap)
					TableAllColumns: intIntervalTooltipMap.put(columnValueMap.get(TableAllColumns.canonicalName), intTooltipMap)
					TableMeasure: intIntervalTooltipMap.put(columnValueMap.get(TableMeasure.canonicalName), intTooltipMap)
					TableDerived: intIntervalTooltipMap.put(columnValueMap.get(TableDerived.canonicalName), intTooltipMap)
					TableHierarchy: intIntervalTooltipMap.put(columnValueMap.get(TableHierarchy.canonicalName), intTooltipMap)
					TableAttribute: intIntervalTooltipMap.put(columnValueMap.get(TableAttribute.canonicalName), intTooltipMap)
					TableAggregation: intIntervalTooltipMap.put(columnValueMap.get(TableAggregation.canonicalName), intTooltipMap)
					TableColumn: intIntervalTooltipMap.put(columnValueMap.get(TableColumn.canonicalName), intTooltipMap)
				}
			}

			// create styles lookup map
			var numberValueLookupStyleMap = <Double, Map<String, String>>newTreeMap(new DoubleComparator())
			var intValueLookupStyleMap = <Integer, Map<String, String>>newTreeMap(new IntegerComparator())
			var stringValueLookupStyleMap = <String, Map<String, String>>newHashMap()
			var dateValueLookupStyleMap = <Double, Map<String, String>>newTreeMap(new DoubleComparator())
			for (lookup : value.lookups) {
				if (lookup instanceof TableNumberLookup &&
					((lookup as TableNumberLookup).discrete instanceof TableTextColor ||
						(lookup as TableNumberLookup).discrete instanceof TableCellColor)) {
					var key = new Double((lookup as TableNumberLookup).lookupValue)
					var styleAspect = ""
					var Map<String, String> styleMap = null
					if (numberValueLookupStyleMap.containsKey(key)) {
						styleMap = numberValueLookupStyleMap.get(key)
						styleAspect = styleMap.get(styleMap.keySet.get(0))
					} else {
						styleMap = <String, String>newHashMap
						styleAspect = ""
						customformatCounter = customformatCounter + 1
					}
					switch (lookup as TableNumberLookup).discrete {
						TableTextColor: styleAspect = '''«styleAspect»color: «((lookup as TableNumberLookup).
							discrete as TableTextColor).rgb.toRGB»;'''
						TableCellColor: styleAspect = '''«styleAspect»background-color: «((lookup as TableNumberLookup).
							discrete as TableCellColor).rgb.toRGB»;'''
					}
					if (styleAspect.length > 0) {
						styleMap.put('''customformat«customformatCounter»''', styleAspect)
						numberValueLookupStyleMap.put(key, styleMap)
					}
				}
				if (lookup instanceof TableIntLookup &&
					((lookup as TableIntLookup).discrete instanceof TableTextColor ||
						(lookup as TableIntLookup).discrete instanceof TableCellColor)) {
					var key = new Integer((lookup as TableIntLookup).lookupValue)
					var styleAspect = ""
					var Map<String, String> styleMap = null
					if (intValueLookupStyleMap.containsKey(key)) {
						styleMap = intValueLookupStyleMap.get(key)
						styleAspect = styleMap.get(styleMap.keySet.get(0))
					} else {
						styleMap = <String, String>newHashMap
						styleAspect = ""
						customformatCounter = customformatCounter + 1
					}
					switch (lookup as TableIntLookup).discrete {
						TableTextColor: styleAspect = '''«styleAspect»color: «((lookup as TableIntLookup).
							discrete as TableTextColor).rgb.toRGB»;'''
						TableCellColor: styleAspect = '''«styleAspect»background-color: «((lookup as TableIntLookup).
							discrete as TableCellColor).rgb.toRGB»;'''
					}
					if (styleAspect.length > 0) {
						styleMap.put('''customformat«customformatCounter»''', styleAspect)
						intValueLookupStyleMap.put(key, styleMap)
					}
				}
				if (lookup instanceof TableStringLookup) {
					var key = new String((lookup as TableStringLookup).lookupValue)
					var styleAspect = ""
					var Map<String, String> styleMap = null
					if (stringValueLookupStyleMap.containsKey(key)) {
						styleMap = stringValueLookupStyleMap.get(key)
						styleAspect = styleMap.get(styleMap.keySet.get(0))
					} else {
						styleMap = <String, String>newHashMap
						styleAspect = ""
						customformatCounter = customformatCounter + 1
					}
					switch lookup.discrete {
						TableTextColor: styleAspect = '''«styleAspect»color: «(lookup.discrete as TableTextColor).
							rgb.toRGB»;'''
						TableCellColor: styleAspect = '''«styleAspect»background-color: «(lookup.discrete as TableCellColor).
							rgb.toRGB»;'''
					}
					if (styleAspect.length > 0) {
						styleMap.put('''customformat«customformatCounter»''', styleAspect)
						stringValueLookupStyleMap.put(key, styleMap)
					}
				}
				if (lookup instanceof TableDateDayLookup &&
					((lookup as TableDateDayLookup).discrete instanceof TableTextColor ||
						(lookup as TableDateDayLookup).discrete instanceof TableCellColor)) {
					var key = new Double((lookup as TableDateDayLookup).lookupValue)
					var styleAspect = ""
					var Map<String, String> styleMap = null
					if (dateValueLookupStyleMap.containsKey(key)) {
						styleMap = dateValueLookupStyleMap.get(key)
						styleAspect = styleMap.get(styleMap.keySet.get(0))
					} else {
						styleMap = <String, String>newHashMap
						styleAspect = ""
						customformatCounter = customformatCounter + 1
					}
					switch (lookup as TableDateDayLookup).discrete {
						TableTextColor: styleAspect = '''«styleAspect»color: «((lookup as TableDateDayLookup).
							discrete as TableTextColor).rgb.toRGB»;'''
						TableCellColor: styleAspect = '''«styleAspect»background-color: «((lookup as TableDateDayLookup).
							discrete as TableCellColor).rgb.toRGB»;'''
					}
					if (styleAspect.length > 0) {
						styleMap.put('''customformat«customformatCounter»''', styleAspect)
						dateValueLookupStyleMap.put(key, styleMap)
					}
				}
			}
			if (numberValueLookupStyleMap.size > 0) {
				switch value.column {
					TableOrdinal: numberLookupStyleMap.put(columnValueMap.get(TableOrdinal.canonicalName), numberValueLookupStyleMap)
					TableAllColumns: numberLookupStyleMap.put(columnValueMap.get(TableAllColumns.canonicalName), numberValueLookupStyleMap)
					TableMeasure: numberLookupStyleMap.put(columnValueMap.get(TableMeasure.canonicalName), numberValueLookupStyleMap)
					TableDerived: numberLookupStyleMap.put(columnValueMap.get(TableDerived.canonicalName), numberValueLookupStyleMap)
					TableHierarchy: numberLookupStyleMap.put(columnValueMap.get(TableHierarchy.canonicalName), numberValueLookupStyleMap)
					TableAttribute: numberLookupStyleMap.put(columnValueMap.get(TableAttribute.canonicalName), numberValueLookupStyleMap)
					TableAggregation: numberLookupStyleMap.put(columnValueMap.get(TableAggregation.canonicalName), numberValueLookupStyleMap)
					TableColumn: numberLookupStyleMap.put(columnValueMap.get(TableColumn.canonicalName), numberValueLookupStyleMap)
				}
			}
			if (intValueLookupStyleMap.size > 0) {
				switch value.column {
					TableOrdinal: intLookupStyleMap.put(columnValueMap.get(TableOrdinal.canonicalName), intValueLookupStyleMap)
					TableAllColumns: intLookupStyleMap.put(columnValueMap.get(TableAllColumns.canonicalName), intValueLookupStyleMap)
					TableMeasure: intLookupStyleMap.put(columnValueMap.get(TableMeasure.canonicalName), intValueLookupStyleMap)
					TableDerived: intLookupStyleMap.put(columnValueMap.get(TableDerived.canonicalName), intValueLookupStyleMap)
					TableHierarchy: intLookupStyleMap.put(columnValueMap.get(TableHierarchy.canonicalName), intValueLookupStyleMap)
					TableAttribute: intLookupStyleMap.put(columnValueMap.get(TableAttribute.canonicalName), intValueLookupStyleMap)
					TableAggregation: intLookupStyleMap.put(columnValueMap.get(TableAggregation.canonicalName), intValueLookupStyleMap)
					TableColumn: intLookupStyleMap.put(columnValueMap.get(TableColumn.canonicalName), intValueLookupStyleMap)
				}
			}
			if (stringValueLookupStyleMap.size > 0) {
				switch value.column {
					TableOrdinal: stringLookupStyleMap.put(columnValueMap.get(TableOrdinal.canonicalName), stringValueLookupStyleMap)
					TableAllColumns: stringLookupStyleMap.put(columnValueMap.get(TableAllColumns.canonicalName), stringValueLookupStyleMap)
					TableMeasure: stringLookupStyleMap.put(columnValueMap.get(TableMeasure.canonicalName), stringValueLookupStyleMap)
					TableDerived: stringLookupStyleMap.put(columnValueMap.get(TableDerived.canonicalName), stringValueLookupStyleMap)
					TableHierarchy: stringLookupStyleMap.put(columnValueMap.get(TableHierarchy.canonicalName), stringValueLookupStyleMap)
					TableAttribute: stringLookupStyleMap.put(columnValueMap.get(TableAttribute.canonicalName), stringValueLookupStyleMap)
					TableAggregation: stringLookupStyleMap.put(columnValueMap.get(TableAggregation.canonicalName), stringValueLookupStyleMap)
					TableColumn: stringLookupStyleMap.put(columnValueMap.get(TableColumn.canonicalName), stringValueLookupStyleMap)
				}
			}
			if (dateValueLookupStyleMap.size > 0) {
				switch value.column {
					TableOrdinal: dateLookupStyleMap.put(columnValueMap.get(TableOrdinal.canonicalName), dateValueLookupStyleMap)
					TableAllColumns: dateLookupStyleMap.put(columnValueMap.get(TableAllColumns.canonicalName), dateValueLookupStyleMap)
					TableMeasure: dateLookupStyleMap.put(columnValueMap.get(TableMeasure.canonicalName), dateValueLookupStyleMap)
					TableDerived: dateLookupStyleMap.put(columnValueMap.get(TableDerived.canonicalName), dateValueLookupStyleMap)
					TableHierarchy: dateLookupStyleMap.put(columnValueMap.get(TableHierarchy.canonicalName), dateValueLookupStyleMap)
					TableAttribute: dateLookupStyleMap.put(columnValueMap.get(TableAttribute.canonicalName), dateValueLookupStyleMap)
					TableAggregation: dateLookupStyleMap.put(columnValueMap.get(TableAggregation.canonicalName), dateValueLookupStyleMap)
					TableColumn: dateLookupStyleMap.put(columnValueMap.get(TableColumn.canonicalName), dateValueLookupStyleMap)
				}
			}

			// create icon lookup map
			var numberValueLookupIconMap = <Double, Map<String, String>>newTreeMap(new DoubleComparator())
			var intValueLookupIconMap = <Integer, Map<String, String>>newTreeMap(new IntegerComparator())
			var stringValueLookupIconMap = <String, Map<String, String>>newHashMap()
			var dateValueLookupIconMap = <Double, Map<String, String>>newTreeMap(new DoubleComparator())
			for (lookup : value.lookups) {
				if (lookup instanceof TableNumberLookup) {
					var key = new Double((lookup as TableNumberLookup).lookupValue)
					var iconAspect = ""
					var Map<String, String> iMap = <String, String>newHashMap
					switch lookup.discrete {
						TableIcon: iconAspect = '''«(lookup.discrete as TableIcon).icon»'''
						TableTrend: iconAspect = '''trend_«(lookup.discrete as TableTrend).icon.literal»'''
					}
					if (iconAspect.length > 0) {
						iMap.put("icon", iconAspect)
						numberValueLookupIconMap.put(key, iMap)
					}
				}
				if (lookup instanceof TableIntLookup) {
					var key = new Integer((lookup as TableIntLookup).lookupValue)
					var iconAspect = ""
					var Map<String, String> iMap = <String, String>newHashMap
					switch lookup.discrete {
						TableIcon: iconAspect = '''«(lookup.discrete as TableIcon).icon»'''
						TableTrend: iconAspect = '''trend_«(lookup.discrete as TableTrend).icon.literal»'''
					}
					if (iconAspect.length > 0) {
						iMap.put("icon", iconAspect)
						intValueLookupIconMap.put(key, iMap)
					}
				}
				if (lookup instanceof TableStringLookup) {
					var key = (lookup as TableStringLookup).lookupValue
					var iconAspect = ""
					var Map<String, String> iMap = <String, String>newHashMap
					switch lookup.discrete {
						TableIcon: iconAspect = '''«(lookup.discrete as TableIcon).icon»'''
					}
					if (iconAspect.length > 0) {
						iMap.put("icon", iconAspect)
						stringValueLookupIconMap.put(key, iMap)
					}
				}
				if (lookup instanceof TableDateDayLookup) {
					var key = new Double((lookup as TableDateDayLookup).lookupValue)
					var iconAspect = ""
					var Map<String, String> iMap = <String, String>newHashMap
					switch lookup.discrete {
						TableIcon: iconAspect = '''«(lookup.discrete as TableIcon).icon»'''
						TableTrend: iconAspect = '''trend_«(lookup.discrete as TableTrend).icon.literal»'''
					}
					if (iconAspect.length > 0) {
						iMap.put("icon", iconAspect)
						dateValueLookupIconMap.put(key, iMap)
					}
				}
			}
			if (numberValueLookupIconMap.size > 0) {
				switch value.column {
					TableOrdinal: numberLookupIconMap.put(columnValueMap.get(TableOrdinal.canonicalName), numberValueLookupIconMap)
					TableAllColumns: numberLookupIconMap.put(columnValueMap.get(TableAllColumns.canonicalName), numberValueLookupIconMap)
					TableMeasure: numberLookupIconMap.put(columnValueMap.get(TableMeasure.canonicalName), numberValueLookupIconMap)
					TableDerived: numberLookupIconMap.put(columnValueMap.get(TableDerived.canonicalName), numberValueLookupIconMap)
					TableHierarchy: numberLookupIconMap.put(columnValueMap.get(TableHierarchy.canonicalName), numberValueLookupIconMap)
					TableAttribute: numberLookupIconMap.put(columnValueMap.get(TableAttribute.canonicalName), numberValueLookupIconMap)
					TableAggregation: numberLookupIconMap.put(columnValueMap.get(TableAggregation.canonicalName), numberValueLookupIconMap)
					TableColumn: numberLookupIconMap.put(columnValueMap.get(TableColumn.canonicalName), numberValueLookupIconMap)
				}
			}
			if (intValueLookupIconMap.size > 0) {
				switch value.column {
					TableOrdinal: intLookupIconMap.put(columnValueMap.get(TableOrdinal.canonicalName), intValueLookupIconMap)
					TableAllColumns: intLookupIconMap.put(columnValueMap.get(TableAllColumns.canonicalName), intValueLookupIconMap)
					TableMeasure: intLookupIconMap.put(columnValueMap.get(TableMeasure.canonicalName), intValueLookupIconMap)
					TableDerived: intLookupIconMap.put(columnValueMap.get(TableDerived.canonicalName), intValueLookupIconMap)
					TableHierarchy: intLookupIconMap.put(columnValueMap.get(TableHierarchy.canonicalName), intValueLookupIconMap)
					TableAttribute: intLookupIconMap.put(columnValueMap.get(TableAttribute.canonicalName), intValueLookupIconMap)
					TableAggregation: intLookupIconMap.put(columnValueMap.get(TableAggregation.canonicalName), intValueLookupIconMap)
					TableColumn: intLookupIconMap.put(columnValueMap.get(TableColumn.canonicalName), intValueLookupIconMap)
				}
			}
			if (stringValueLookupIconMap.size > 0) {
				switch value.column {
					TableOrdinal: stringLookupIconMap.put(columnValueMap.get(TableOrdinal.canonicalName), stringValueLookupIconMap)
					TableAllColumns: stringLookupIconMap.put(columnValueMap.get(TableAllColumns.canonicalName), stringValueLookupIconMap)
					TableMeasure: stringLookupIconMap.put(columnValueMap.get(TableMeasure.canonicalName), stringValueLookupIconMap)
					TableDerived: stringLookupIconMap.put(columnValueMap.get(TableDerived.canonicalName), stringValueLookupIconMap)
					TableHierarchy: stringLookupIconMap.put(columnValueMap.get(TableHierarchy.canonicalName), stringValueLookupIconMap)
					TableAttribute: stringLookupIconMap.put(columnValueMap.get(TableAttribute.canonicalName), stringValueLookupIconMap)
					TableAggregation: stringLookupIconMap.put(columnValueMap.get(TableAggregation.canonicalName), stringValueLookupIconMap)
					TableColumn: stringLookupIconMap.put(columnValueMap.get(TableColumn.canonicalName), stringValueLookupIconMap)
				}
			}
			if (dateValueLookupIconMap.size > 0) {
				switch value.column {
					TableOrdinal: dateLookupIconMap.put(columnValueMap.get(TableOrdinal.canonicalName), dateValueLookupIconMap)
					TableAllColumns: dateLookupIconMap.put(columnValueMap.get(TableAllColumns.canonicalName), dateValueLookupIconMap)
					TableMeasure: dateLookupIconMap.put(columnValueMap.get(TableMeasure.canonicalName), dateValueLookupIconMap)
					TableDerived: dateLookupIconMap.put(columnValueMap.get(TableDerived.canonicalName), dateValueLookupIconMap)
					TableHierarchy: dateLookupIconMap.put(columnValueMap.get(TableHierarchy.canonicalName), dateValueLookupIconMap)
					TableAttribute: dateLookupIconMap.put(columnValueMap.get(TableAttribute.canonicalName), dateValueLookupIconMap)
					TableAggregation: dateLookupIconMap.put(columnValueMap.get(TableAggregation.canonicalName), dateValueLookupIconMap)
					TableColumn: dateLookupIconMap.put(columnValueMap.get(TableColumn.canonicalName), dateValueLookupIconMap)
				}
			}

			// create tooltip lookup map
			var numberValueLookupTooltipMap = <Double, Map<String, String>>newTreeMap(new DoubleComparator())
			var intValueLookupTooltipMap = <Integer, Map<String, String>>newTreeMap(new IntegerComparator())
			var stringValueLookupTooltipMap = <String, Map<String, String>>newHashMap()
			var dateValueLookupTooltipMap = <Double, Map<String, String>>newTreeMap(new DoubleComparator())
			for (lookup : value.lookups) {
				if (lookup instanceof TableNumberLookup) {
					var key = new Double((lookup as TableNumberLookup).lookupValue)
					var tooltipAspect = ""
					var Map<String, String> tMap = <String, String>newHashMap
					switch lookup.discrete {
						TableTooltip: tooltipAspect = '''«(lookup.discrete as TableTooltip).tooltip»'''
					}
					if (tooltipAspect.length > 0) {
						tMap.put("tooltip", tooltipAspect)
						numberValueLookupTooltipMap.put(key, tMap)
					}
				}
				if (lookup instanceof TableIntLookup) {
					var key = new Integer((lookup as TableIntLookup).lookupValue)
					var tooltipAspect = ""
					var Map<String, String> tMap = <String, String>newHashMap
					switch lookup.discrete {
						TableTooltip: tooltipAspect = '''«(lookup.discrete as TableTooltip).tooltip»'''
					}
					if (tooltipAspect.length > 0) {
						tMap.put("tooltip", tooltipAspect)
						intValueLookupTooltipMap.put(key, tMap)
					}
				}
				if (lookup instanceof TableStringLookup) {
					var key = (lookup as TableStringLookup).lookupValue
					var tooltipAspect = ""
					var Map<String, String> tMap = <String, String>newHashMap
					switch lookup.discrete {
						TableTooltip: tooltipAspect = '''«(lookup.discrete as TableTooltip).tooltip»'''
					}
					if (tooltipAspect.length > 0) {
						tMap.put("tooltip", tooltipAspect)
						stringValueLookupTooltipMap.put(key, tMap)
					}
				}
				if (lookup instanceof TableDateDayLookup) {
					var key = new Double((lookup as TableDateDayLookup).lookupValue)
					var tooltipAspect = ""
					var Map<String, String> tMap = <String, String>newHashMap
					switch lookup.discrete {
						TableTooltip: tooltipAspect = '''«(lookup.discrete as TableTooltip).tooltip»'''
					}
					if (tooltipAspect.length > 0) {
						tMap.put("tooltip", tooltipAspect)
						dateValueLookupTooltipMap.put(key, tMap)
					}
				}
			}
			if (numberValueLookupTooltipMap.size > 0) {
				switch value.column {
					TableOrdinal: numberLookupTooltipMap.put(columnValueMap.get(TableOrdinal.canonicalName), numberValueLookupTooltipMap)
					TableAllColumns: numberLookupTooltipMap.put(columnValueMap.get(TableAllColumns.canonicalName), numberValueLookupTooltipMap)
					TableMeasure: numberLookupTooltipMap.put(columnValueMap.get(TableMeasure.canonicalName), numberValueLookupTooltipMap)
					TableDerived: numberLookupTooltipMap.put(columnValueMap.get(TableDerived.canonicalName), numberValueLookupTooltipMap)
					TableHierarchy: numberLookupTooltipMap.put(columnValueMap.get(TableHierarchy.canonicalName), numberValueLookupTooltipMap)
					TableAttribute: numberLookupTooltipMap.put(columnValueMap.get(TableAttribute.canonicalName), numberValueLookupTooltipMap)
					TableAggregation: numberLookupTooltipMap.put(columnValueMap.get(TableAggregation.canonicalName), numberValueLookupTooltipMap)
					TableColumn: numberLookupTooltipMap.put(columnValueMap.get(TableColumn.canonicalName), numberValueLookupTooltipMap)
				}
			}
			if (intValueLookupTooltipMap.size > 0) {
				switch value.column {
					TableOrdinal: intLookupStyleMap.put(columnValueMap.get(TableOrdinal.canonicalName), intValueLookupTooltipMap)
					TableAllColumns: intLookupStyleMap.put(columnValueMap.get(TableAllColumns.canonicalName), intValueLookupTooltipMap)
					TableMeasure: intLookupStyleMap.put(columnValueMap.get(TableMeasure.canonicalName), intValueLookupTooltipMap)
					TableDerived: intLookupStyleMap.put(columnValueMap.get(TableDerived.canonicalName), intValueLookupTooltipMap)
					TableHierarchy: intLookupStyleMap.put(columnValueMap.get(TableHierarchy.canonicalName), intValueLookupTooltipMap)
					TableAttribute: intLookupStyleMap.put(columnValueMap.get(TableAttribute.canonicalName), intValueLookupTooltipMap)
					TableAggregation: intLookupStyleMap.put(columnValueMap.get(TableAggregation.canonicalName), intValueLookupTooltipMap)
					TableColumn: intLookupStyleMap.put(columnValueMap.get(TableColumn.canonicalName), intValueLookupTooltipMap)
				}
			}
			if (stringValueLookupTooltipMap.size > 0) {
				switch value.column {
					TableOrdinal: stringLookupTooltipMap.put(columnValueMap.get(TableOrdinal.canonicalName), stringValueLookupTooltipMap)
					TableAllColumns: stringLookupTooltipMap.put(columnValueMap.get(TableAllColumns.canonicalName), stringValueLookupTooltipMap)
					TableMeasure: stringLookupTooltipMap.put(columnValueMap.get(TableMeasure.canonicalName), stringValueLookupTooltipMap)
					TableDerived: stringLookupTooltipMap.put(columnValueMap.get(TableDerived.canonicalName), stringValueLookupTooltipMap)
					TableHierarchy: stringLookupTooltipMap.put(columnValueMap.get(TableHierarchy.canonicalName), stringValueLookupTooltipMap)
					TableAttribute: stringLookupTooltipMap.put(columnValueMap.get(TableAttribute.canonicalName), stringValueLookupTooltipMap)
					TableAggregation: stringLookupTooltipMap.put(columnValueMap.get(TableAggregation.canonicalName), stringValueLookupTooltipMap)
					TableColumn: stringLookupTooltipMap.put(columnValueMap.get(TableColumn.canonicalName), stringValueLookupTooltipMap)
				}
			}
			if (dateValueLookupTooltipMap.size > 0) {
				switch value.column {
					TableOrdinal: dateLookupTooltipMap.put(columnValueMap.get(TableOrdinal.canonicalName), dateValueLookupTooltipMap)
					TableAllColumns: dateLookupTooltipMap.put(columnValueMap.get(TableAllColumns.canonicalName), dateValueLookupTooltipMap)
					TableMeasure: dateLookupTooltipMap.put(columnValueMap.get(TableMeasure.canonicalName), dateValueLookupTooltipMap)
					TableDerived: dateLookupTooltipMap.put(columnValueMap.get(TableDerived.canonicalName), dateValueLookupTooltipMap)
					TableHierarchy: dateLookupTooltipMap.put(columnValueMap.get(TableHierarchy.canonicalName), dateValueLookupTooltipMap)
					TableAttribute: dateLookupTooltipMap.put(columnValueMap.get(TableAttribute.canonicalName), dateValueLookupTooltipMap)
					TableAggregation: dateLookupTooltipMap.put(columnValueMap.get(TableAggregation.canonicalName), dateValueLookupTooltipMap)
					TableColumn: dateLookupTooltipMap.put(columnValueMap.get(TableColumn.canonicalName), dateValueLookupTooltipMap)
				}
			}
		}
	}

	/**
	 * <p>helper method to get a attribute name or its alias if present of a entity's attribute object.</p> 
	 *
	 */
	def String attributeName(DatamartAttribute attribute) {
		return attribute.getAttributeName(null);
	}

	/**
	 * <p>helper method to get a level name of a hierarchy's level object.</p> 
	 *
	 */
	def String getLevelName(CubeLevel level) {
		return level.name
	}

	/**
	 * <p>generate code for the pre-ordering refresh event.</p> 
	 *
	 */
	def String preOrder(TablePreorder preorder, TableAxis axis) {
		var name = ""
		switch (preorder.column) {
			TableOrdinal: name = '''#«axis.axis.literal»«(preorder.column as TableOrdinal).valueRef.toString»'''
			TableAllColumns: name = '''?«axis.axis.literal»'''
			TableMeasure: name = '''«(preorder.column as TableMeasure).valueRef.measureRef.name»'''
			TableDerived: name = '''«(preorder.column as TableDerived).valueRef.derivedRef.name»'''
			TableHierarchy: name = '''«(preorder.column as TableHierarchy).valueRef.levelName»'''
			TableAttribute: name = '''«DatamartAttributeUtil.getAliasedAttributeName((preorder.column as TableAttribute).valueRef)»'''
			TableAggregation: name = '''«(preorder.column as TableAggregation).valueRef.aggregation.name()»'''
			TableColumn: name = '''«(preorder.column as TableColumn).valueRef.columnRef.literal»'''
		}
		var body = '''dataSourceContainer.setSort(table, "«name»", «preorder.ascending.booleanValue.toString»);
			'''
		return body
	}

	def Set<LEntity> findRequestedEntities(Table table) {
		var entities = new HashSet<LEntity>()
		switch (table.tabletype) {
			TableTable: {
				entities.addAll((table.tabletype as TableTable).source.datamartRef.findAllEntities())
			}
			TableGrid: {
				val entity = EcoreUtil2.resolve(table.dtoGridTable.dtoSource.wrappedType, table.eResource) as LEntity
				entities.add(entity);
			}
		}
		return entities
	}

	def String sendEvent(Table table) {
		var body = "";
		body = '''
			«body»
			table.addValueChangeListener(new ValueChangeListener() {
				@Override
				public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
					Object msg = null;
					Object id = null;
					String target = null;
					Object value = table.getValue();
					int selection = -1;
					if	(value instanceof Integer) {
						selection = (Integer)value;
					}
					else {
						if ((value instanceof Collection<?>) && (((Collection<?>)value).size() == 1)) {
							value = ((Collection<?>)value).iterator().next();
			    			if	(value instanceof Integer) {
			    				selection = (Integer)value;
			    			}
						}
					}
					«IF table.isEmbedded && table.isTask»
					for(Button b:taskButtons) {
						b.setEnabled(table.getValue() != null && 
							(dataSourceContainer.getTaskStatus(selection).equals(BPMStatus.Created) ||
							dataSourceContainer.getTaskStatus(selection).equals(BPMStatus.Ready) ||
							dataSourceContainer.getTaskStatus(selection).equals(BPMStatus.Suspended)));
					}
					«ENDIF»
		'''
		if (table.isTable) {
			if (table.dataMartTable instanceof TableDatamart) {
				switch ((table.dataMartTable as TableDatamart).datamartRef.source) {
					DatamartTask:
						body = 
						'''
						«body»
								if (table.getValue() != null && taskClient != null) {
									eclipseContext.set(BPMTaskSummary.class, taskClient.getUserTask(dataSourceContainer.getTaskId(selection), user));
								} else {
									eclipseContext.set(BPMTaskSummary.class, null);
								}
						'''
					DatamartEntity: {
						var datamartEntity = ((table.dataMartTable as TableDatamart).datamartRef.source) as DatamartEntity
						var entity = EcoreUtil2.resolve(datamartEntity.entityRef, table.eResource) as LEntity
						
						var alias = DatamartDefinitionUtil.getEntityIdAliasName(entity)
						if(alias !== null) {
							body = '''
							«body»
									«IF entity.primaryKeyAttribute !== null»
									if(selection != -1) {
										Object idObj = datamartInstance.getPrimaryListKeys("«alias»").get(selection);
										SelectionStore.putSelectionToPerspectiveContext(getPart(), "«entity.baseType.fullyQualifiedName».«entity.idAttributeName»", idObj);
										
										EventDispatcherEvent evnt = new EventDispatcherEvent(getContext().get(MPerspective.class), EventDispatcherCommand.SELECT, "«entity.baseType.fullyQualifiedName».«entity.idAttributeName»", "«table.fullyQualifiedName»");
										evnt.addItem(EventDispatcherDataTag.ID, idObj);
										eventDispatcher.sendEvent(evnt);
										«IF !table.isSelectIdOnly »
										// send all refs too
										for (String alias : datamartInstance.getPrimaryList().keySet()){
											if (!"«alias»".equals(alias)){
												evnt = new EventDispatcherEvent(getContext().get(MPerspective.class), EventDispatcherCommand.SELECT, datamartInstance.getPrimaryList().get(alias).getEntityName() + "." + datamartInstance.getPrimaryList().get(alias).getAttributeName(), "«table.fullyQualifiedName»");
												evnt.addItem(EventDispatcherDataTag.ID, datamartInstance.getPrimaryListKeys(alias).get(selection));
												eventDispatcher.sendEvent(evnt);
											}
										}
										«ENDIF»
									}
									«ENDIF»
							'''
						}
					}
				}
			}
		}
		for (element : table.dataMartTable.elements) {
			if (element instanceof TableAxis) {
				var axis = element as TableAxis
				var evntCnt = 0
				for (event : axis.events) {
					body = '''
					«body»
							if(selection != -1) {
								«IF event.source instanceof TableOrdinal»
								EventDispatcherEvent evnt«evntCnt» = new EventDispatcherEvent(getContext().get(MPerspective.class), EventDispatcherCommand.SELECT, "«(event.source as TableOrdinal).valueRef»", "«table.fullyQualifiedName»");
								«ELSEIF event.source instanceof TableAttribute»
								EventDispatcherEvent evnt«evntCnt» = new EventDispatcherEvent(getContext().get(MPerspective.class), EventDispatcherCommand.SELECT, "«(event.source as TableAttribute).valueRef.attributeRef.name»", "«table.fullyQualifiedName»");
								«ENDIF» 
								«IF event.source instanceof TableOrdinal»
								evnt«evntCnt».addItem(EventDispatcherDataTag.LIST, dataSourceContainer.getValueByProperty(selection, "«(event.source as TableOrdinal).valueRef»"));
								eventDispatcher.sendEvent(evnt«evntCnt»);
								«ELSEIF event.source instanceof TableAttribute»
								evnt«evntCnt».addItem(EventDispatcherDataTag.LIST, dataSourceContainer.getValueByProperty(selection, "«(event.source as TableAttribute).valueRef.attributeRef.name»"));
								eventDispatcher.sendEvent(evnt«evntCnt»);
								«ENDIF»
							} 
					'''
					evntCnt = evntCnt + 1
				}
			}
		}
		body = '''
			«body»
				}
			});
		'''
		return body
	}
	
	def columnResizeListener() 
'''table.addColumnResizeListener(new ColumnResizeListener(){
	public void columnResize(ColumnResizeEvent event) {
		// Get the new width of the resized column
		int width = event.getCurrentWidth();
		
		// Get the property ID of the resized column
		Object columnId = event.getPropertyId();
		user.addToProperties(this.getClass().getEnclosingClass().getCanonicalName() + "." + columnId.toString() + ".width", String.valueOf(width));
	}
});
// Must be immediate to send the resize events immediately
table.setImmediate(true);
'''

	def columnReorderListener() 
'''table.addColumnReorderListener(new ColumnReorderListener(){
	public void columnReorder(ColumnReorderEvent event) {
		Object source = event.getSource();
		String propertyKey = this.getClass().getEnclosingClass().getCanonicalName() + ".columns";
		if (source instanceof CellSetFilterTable){
			Map<Object, Integer> visibleColumnsPositions = new HashMap<>();
			Object[] visibleColumns = ((CellSetFilterTable)source).getVisibleColumns();
			for (int i = 0; i < visibleColumns.length; i++) {
				CellSetIndexedContainer container = ((CellSetFilterTable)source).getCellSetIndexedContainer();
				Object visibleColumnKey = visibleColumns[i];
				«positionMembersHashCode»
				visibleColumnsPositions.put(visibleColumnKey, membersHash);
			}
			if (visibleColumns!=null){
				user.addToProperties(propertyKey, visibleColumnsPositions);
			}
		}
	}
});
'''
	
	def private getPositionMembersHashCode() '''
		// creates a hash code by the column position members		
		int membersHash = 0;
		if (container != null) {
			DerivedPosition pos = container.getColumnPosition(visibleColumnKey);
			if ( pos != null ) {
				List<DerivedMember> members = pos.getMembers();
				for (DerivedMember member : members) {
					membersHash += (member.getUniqueName() != null) ? member.getUniqueName().hashCode() : member.hashCode();
				}
			}
		}
	'''

	def columnCollapsedListener() 
'''table.addColumnCollapseListener(new ColumnCollapseListener(){
	public void columnCollapseStateChange(ColumnCollapseEvent event) {
		// Get the ID of the column collapse event
		Object columnId = event.getPropertyId();
		Object source = event.getSource();
		String propertyKey = this.getClass().getEnclosingClass().getCanonicalName() + "." + columnId.toString() + ".collapsed";
		if (source instanceof CustomTable){
			if (((CustomTable)source).isColumnCollapsed(columnId)) {
				user.addToProperties(propertyKey, "true");
			} else {
				user.removeFromProperties(propertyKey);
			}
		}
	}
});
'''
	
	def tableSettingsByUserProperties(Table table) 
		'''
		// Setting the order of the columns via the visibleColumns list
		Map<Object, Integer> visibleColumnsMap = user.getColumnUtil().getVisibleColumns(this.getClass().getCanonicalName() + ".columns");
		if (visibleColumnsMap != null){
			boolean unmodifiedVisibleColumns = true;
			for (Object visibleColumnKey : visibleColumnsMap.keySet()) {
				CellSetIndexedContainer container = dataSourceContainer;
				«positionMembersHashCode»
				if (membersHash != visibleColumnsMap.get(visibleColumnKey)) {
					unmodifiedVisibleColumns = false;
					break;
				}
			}
			if (unmodifiedVisibleColumns) {
				try {
					Object[] visibleColumns = visibleColumnsMap.values().toArray();
					table.setVisibleColumns(visibleColumns );
				} catch (IllegalArgumentException e) {
					// this occurs if a saved column is no longer existing due to model changes
				}
			}
		}
		// Setting table columns width from the persisted user properties
		for (Object visibleId : table.getVisibleColumns()) {
			String widthStr = user.getColumnUtil().getColumnWidth(this.getClass().getCanonicalName() + "." + visibleId.toString() + ".width");
			if (widthStr!= null) {
				try {
					table.setColumnWidth(visibleId, Integer.parseInt(widthStr));
				} catch (IllegalArgumentException e) {
					// this occurs if a saved column is no longer existing due to model changes
				}
			}
		}
		// Setting table collapsed columns from the persisted user properties
		for (Object propertyId : table.getContainerDataSource().getContainerPropertyIds()) {
			String collapsedColumn = user.getColumnUtil().getColumnCollapsed(this.getClass().getCanonicalName() + "." + propertyId + ".collapsed");
			try {
				if (collapsedColumn != null){
					table.setColumnCollapsed(propertyId, true);
				}
			} catch (IllegalArgumentException e) {
				// this occurs if a saved column is no longer existing due to model changes
			}
		}
		'''

	def getMsgValue(LEntityAttribute attribute, String string) '''«CONVERT_METHOD_PREFIX»«attribute.typeName.
		toFirstUpper»(«string»)'''

	def String buildTopic(DatamartDefinition datamart, String referenceName) {
		var topic = ""
		if (datamart.source instanceof DatamartEntity) {
			var entity = (datamart.source as DatamartEntity).entityRef
			var eObj = datamart.eContainer
			while (!(eObj instanceof DatamartPackage)) {
				eObj = eObj.eContainer
			}
			if (eObj !== null) {
				var pckName = (eObj as DatamartPackage).fullyQualifiedName.toString.replace(".", "/")
				topic = pckName.concat("/").concat(datamart.name).concat("/").concat(entity.name).concat("/").
					concat(referenceName)
			}
		}
		return topic
	}

	/**
	 * <p>helper method to convert a rgb string to a hex string.</p> 
	 *
	 */
	def StringtoHex(String rgb) {
		var colorHex = "#"
		for (color : rgb.split(",")) {
			var i = new Integer(color);
			colorHex = '''«colorHex»«String.format("%02x", i)»'''
		}
		return colorHex
	}

	/**
	 * <p>helper method to convert a rgb string to a rgb syntax.</p> 
	 *
	 */
	def String toRGB(String rgb) {
		return '''rgb(«rgb»)'''
	}

	def void initIdMap(Table table, HashMap<String, IdAttribute> map) {
		map.clear
		if (table.dataMartTable instanceof TableDatamart) {
			var datamart = (table.dataMartTable as TableDatamart).datamartRef
			if (datamart.source instanceof DatamartEntity) {
				var entity = datamart.source as DatamartEntity
				entity.iterateIdMap(map)
			}
		}
	}

	def void iterateIdMap(DatamartEntity entity, HashMap<String, IdAttribute> map) {
		var prop = new IdAttribute()
		prop.collapsed = true
		map.put(DatamartDefinitionUtil.getEntityIdAliasName(entity.entityRef), prop)
		if(entity.entityRef.historizedOrTimedependentWithParent) {
			map.put(DatamartDefinitionUtil.getEntityValidAliasName(entity.entityRef), prop)
			map.put(DatamartDefinitionUtil.getEntityCurrentAliasName(entity.entityRef), prop)
		}
		for (navigation : entity.navigations) {
			navigation.datamartEntity.iterateIdMap(map)
		}
	}

	def localeChanged(Table table)
		'''
		if(initDone) {
			tableFilterDecorator.setLocale(locale);
			for(CellSetFilterTable table:tables) {
				table.setLocale(locale);
				«IF table.isTable &&
					(table.dataMartTable as TableDatamart).datamartRef.description && 
					(table.dataMartTable as TableDatamart).datamartRef.descriptionValue !== null»
					table.setDescription(dslMetadataService.translate(locale.toLanguageTag(), "«(table.dataMartTable as TableDatamart).datamartRef.descriptionValue»"));
				«ENDIF»
			}
			for(String key:attributeLookupMap.keySet()) {
				attributeLookupMap.get(key).setLocale(locale);
			}
			if(dataSourceContainer != null) {
				dataSourceContainer.setLocale(locale);
			}
			layoutManager.setLabelValue(dslMetadataService.translate(locale.toLanguageTag(), "«table.name»"));
		}
		'''
		
	def selectItemsFromWorkloadDto(Table table) {
		var datamartEntity = null as DatamartEntity
		var entity = null as LEntity
		var alias = null as String
		var dtoFqn = null as String
		var keyType = null as LDataType
		var keyGetter = null as String
		if (table.dataMartTable instanceof TableDatamart) {
			switch ((table.dataMartTable as TableDatamart).datamartRef.source) {
				DatamartEntity: {
					datamartEntity = ((table.dataMartTable as TableDatamart).datamartRef.source) as DatamartEntity
					entity = EcoreUtil2.resolve(datamartEntity.entityRef, table.eResource) as LEntity
					keyType = entity.primaryKeyAttribute.datatype
					keyGetter = '''get«entity.primaryKeyAttribute.name.toFirstUpper»()''' 
					alias = DatamartDefinitionUtil.getEntityIdAliasName(entity).toLowerCase
					dtoFqn = EntityUtils.getDtoFQNForLEntity(entity)
				}
			}
		}
		'''
		List<Integer> selections = new ArrayList<>();
		if(taskSummary != null) {
			IDto workloadDto = taskClient.getWorkloadDto(taskSummary);
			List<IDto> operativeDtos = workLoadItem.getOperativeDtos(workloadDto);
			if	(operativeDtos != null) {
				Set<String> ids = new TreeSet<String>();
			    for	(IDto operativeDto : operativeDtos) {
			    	ids.add(String.valueOf(((«dtoFqn»)operativeDto).«keyGetter»));
			    }
			    for	(Object selection : dataSourceContainer.getAllItemIds()) {
			        String idValue = dataSourceContainer.getStringValueByProperty((Integer)selection,"«alias»");
			    	if	(ids.contains(idValue)) {
			    		selections.add((Integer)selection);
			    	}
			    }
			}
			selectTable.setValue(selections);
		}
		'''
	}
		
	def rowChecked(Table table) {
		if (table.dataMartTable instanceof TableDatamart) {
			switch ((table.dataMartTable as TableDatamart).datamartRef.source) {
				DatamartEntity: {
					var datamartEntity = ((table.dataMartTable as TableDatamart).datamartRef.source) as DatamartEntity
					var entity = EcoreUtil2.resolve(datamartEntity.entityRef, table.eResource) as LEntity
					var alias = DatamartDefinitionUtil.getEntityIdAliasName(entity).toLowerCase
					var keyType = entity.primaryKeyAttribute.datatype
					var dtoFqn = EntityUtils.getDtoFQNForLEntity(entity)
					'''
					IDTOService<«dtoFqn»> dtoService = (IDTOService<«dtoFqn»>) DtoServiceAccess.getService(«dtoFqn».class);
					String idValue = dataSourceContainer.getStringValueByProperty((Integer)selection,"«alias»");
					if(taskSummary != null) {
						if	(selected) {
							workLoadItem.addOperativeDto(taskClient.getWorkloadDto(taskSummary), «IF "int".equals(keyType.name)»dtoService.get((int) Integer.parseInt(idValue))«ELSE»dtoService.get(idValue)«ENDIF»);
						}
						else {
						   	for(IDto dtoToRemove : workLoadItem.getOperativeDtos(taskClient.getWorkloadDto(taskSummary))) {
								if(((«dtoFqn»)dtoToRemove).getId()«IF "int".equals(keyType.name)»==(int) Integer.parseInt(idValue)«ELSE».equals(idValue)«ENDIF») {
									workLoadItem.removeOperativeDto(taskClient.getWorkloadDto(taskSummary), dtoToRemove);
									break;
								}
							}
						}
					} else {
						if(selected) {
							SelectionStore.addSelectionToPerspectiveContext(getPart(), "«dtoFqn»", «IF "int".equals(keyType.name)»dtoService.get((int) Integer.parseInt(idValue))«ELSE»dtoService.get(idValue)«ENDIF»);
						} else {
							SelectionStore.removeSelectionFromPerspectiveContext(getPart(), "«dtoFqn»", «IF "int".equals(keyType.name)»dtoService.get((int) Integer.parseInt(idValue))«ELSE»dtoService.get(idValue)«ENDIF»);
						}
					}
					'''
				}
			}
		}
	}
	
	def void toBinderFields(JvmDeclaredType type,TablePackage tablePckg) {
		var JvmField field = null
		// create logger
		field = tablePckg.toField("log", _typeReferenceBuilder.typeRef(Logger)) [
			setInitializer([append('''org.slf4j.LoggerFactory.getLogger("servicebinder")''')])
		]
		field.static = true
		field.visibility = JvmVisibility::PRIVATE
		type.members += field
		// create user service field
		field = tablePckg.toField("userAccessService", _typeReferenceBuilder.typeRef(IUserAccessService))
		field.static = true
		field.visibility = JvmVisibility::PRIVATE
		type.members += field
	}
	
		def void toBinderOperations(JvmDeclaredType type, TablePackage tablePckg) {
		type.members += tablePckg.toMethod("getUserAccessService", _typeReferenceBuilder.typeRef(IUserAccessService), [
			visibility = JvmVisibility.PUBLIC
			static = true
			body = [
				append(
			'''return userAccessService;''')
			]
		])
		// bind userAccessService
		type.members += tablePckg.toMethod("bindUserAccessMethod", _typeReferenceBuilder.typeRef(Void::TYPE), [
			var annotationRef = _annotationTypesBuilder.annotationRef(typeof(Reference))
			annotationRef.addAnnAttr(tablePckg, "cardinality", ReferenceCardinality.MANDATORY)
			annotationRef.addAnnAttr(tablePckg, "policy", ReferencePolicy.STATIC)
			annotations += annotationRef
			visibility = JvmVisibility.PUBLIC
			synchronized = true
			parameters += tablePckg.toParameter("userAccessService", _typeReferenceBuilder.typeRef(IUserAccessService))
			body = [
				append(
			'''
				«tablePckg.fullyQualifiedName.lastSegment.toFirstUpper+"ServiceBinder"».userAccessService = userAccessService;
				log.debug("Datamart UserAccessService bound");''')
			]
		])
		// unbind userAccessService
		type.members += tablePckg.toMethod("unbindUserAccessMethod", _typeReferenceBuilder.typeRef(Void::TYPE), [
			visibility = JvmVisibility.PUBLIC
			synchronized = true
			parameters += tablePckg.toParameter("userAccessService", _typeReferenceBuilder.typeRef(IUserAccessService))
			body = [
				append(
			'''
				«tablePckg.fullyQualifiedName.lastSegment.toFirstUpper+"ServiceBinder"».userAccessService = null;
				log.debug("Datamart UserAccessService unbound");''')
			]
		])
	}
}
