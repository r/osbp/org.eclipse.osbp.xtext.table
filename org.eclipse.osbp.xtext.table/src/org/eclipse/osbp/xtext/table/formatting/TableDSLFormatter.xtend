/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
package org.eclipse.osbp.xtext.table.formatting

import com.google.inject.Inject
import org.eclipse.osbp.xtext.oxtype.formatting.GenericFormatter
import org.eclipse.osbp.xtext.oxtype.services.OXtypeGrammarAccess
import org.eclipse.xtext.GrammarUtil
import org.eclipse.xtext.ParserRule
import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter
import org.eclipse.xtext.formatting.impl.FormattingConfig

/**
 * This class contains custom formatting description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#formatting
 * on how and when to use it 
 * 
 * Also see {@link org.eclipse.xtext.xtext.XtextFormattingTokenSerializer} as an example
 */
class TableDSLFormatter extends AbstractDeclarativeFormatter {

	//	@Inject extension TableDSLGrammarAccess
	@Inject OXtypeGrammarAccess grammarAccess

	override protected void configureFormatting(FormattingConfig c) {
		val genericFormatter = new GenericFormatter()
		var ParserRule nestedFieldDef = GrammarUtil.findRuleForName(grammar.grammar, "NestedField") as ParserRule
		var ParserRule propertyStyleDef = GrammarUtil.findRuleForName(grammar.grammar, "PropertyStyle") as ParserRule
		var ParserRule rangeElemetDef = GrammarUtil.findRuleForName(grammar.grammar, "TableRangeElement") as ParserRule

		//	TableRangeElement includes "TableTextColor", "TableCellColor", "TableIcon", "TableTrend" and "TableTooltip"!
		//	dsl-specific formatting:
		c.setNoLinewrap().before(nestedFieldDef)
		c.setNoLinewrap().before(propertyStyleDef)
		c.setNoLinewrap().before(rangeElemetDef)

		//	common formatting:
		genericFormatter.formatFirstLevelBlocks(c, grammar.grammar, "Table")
		genericFormatter.genericFormatting(c, grammar, grammarAccess)
	}
}
