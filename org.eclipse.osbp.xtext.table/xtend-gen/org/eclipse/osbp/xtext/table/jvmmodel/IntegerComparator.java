/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.table.jvmmodel;

import java.util.Comparator;

@SuppressWarnings("all")
public class IntegerComparator implements Comparator<Integer> {
  @Override
  public int compare(final Integer arg0, final Integer arg1) {
    boolean _lessThan = (arg0.compareTo(arg1) < 0);
    if (_lessThan) {
      return (-1);
    } else {
      boolean _greaterThan = (arg0.compareTo(arg1) > 0);
      if (_greaterThan) {
        return 1;
      } else {
        return 0;
      }
    }
  }
}
