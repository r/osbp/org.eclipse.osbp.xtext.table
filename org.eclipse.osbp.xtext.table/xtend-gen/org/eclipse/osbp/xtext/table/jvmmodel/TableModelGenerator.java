/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.table.jvmmodel;

import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomTable;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.eclipse.bpmn2.Task;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.di.extensions.EventUtils;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.osbp.bpm.api.BPMOperation;
import org.eclipse.osbp.bpm.api.BPMStatus;
import org.eclipse.osbp.bpm.api.BPMTaskEventType;
import org.eclipse.osbp.bpm.api.BPMTaskSummary;
import org.eclipse.osbp.bpm.api.BPMTaskUserEvent;
import org.eclipse.osbp.bpm.api.IBlipBPMConstants;
import org.eclipse.osbp.bpm.api.IBlipBPMFunctionProvider;
import org.eclipse.osbp.bpm.api.IBlipBPMStartInfo;
import org.eclipse.osbp.bpm.api.IBlipBPMWorkloadModifiableItem;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.dsl.dto.lib.impl.DtoServiceAccess;
import org.eclipse.osbp.eventbroker.EventBrokerMsg;
import org.eclipse.osbp.preferences.ProductConfiguration;
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent;
import org.eclipse.osbp.runtime.common.event.IDualData;
import org.eclipse.osbp.runtime.common.event.SelectionStore;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.eclipse.osbp.runtime.web.vaadin.common.data.IBeanSearchServiceFactory;
import org.eclipse.osbp.ui.api.contextfunction.IViewEmbeddedProvider;
import org.eclipse.osbp.ui.api.customfields.IBlobService;
import org.eclipse.osbp.ui.api.datamart.DatamartFilter;
import org.eclipse.osbp.ui.api.datamart.DatamartPrimary;
import org.eclipse.osbp.ui.api.datamart.IDatamartFilterGenerator;
import org.eclipse.osbp.ui.api.e4.IE4Table;
import org.eclipse.osbp.ui.api.perspective.IPerspectiveProvider;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.utils.constants.ExtendedDate;
import org.eclipse.osbp.utils.vaadin.SelectUserWindow;
import org.eclipse.osbp.utils.vaadin.ViewLayoutManager;
import org.eclipse.osbp.vaaclipse.common.ecview.api.IECViewSessionHelper;
import org.eclipse.osbp.xtext.action.SelectWorkloadActionEnum;
import org.eclipse.osbp.xtext.action.TableActionEnum;
import org.eclipse.osbp.xtext.basic.generator.BasicDslGeneratorUtils;
import org.eclipse.osbp.xtext.blip.BlipItem;
import org.eclipse.osbp.xtext.datamart.common.AEntityDatamart;
import org.eclipse.osbp.xtext.datamart.common.DatamartFilterGenerator;
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedAxis;
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedHierarchy;
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedLevel;
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedMember;
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedPosition;
import org.eclipse.osbp.xtext.datamart.common.sql.OperativeDtoContainer;
import org.eclipse.osbp.xtext.i18n.I18NModelGenerator;
import org.eclipse.osbp.xtext.table.common.BeanFilterTable;
import org.eclipse.osbp.xtext.table.common.CellSetFilterTable;
import org.eclipse.osbp.xtext.table.common.CellSetIndexedContainer;
import org.eclipse.osbp.xtext.table.common.CellSetPagedFilterTable;
import org.eclipse.osbp.xtext.table.common.CheckboxSelectionCellSetFilterTable;
import org.eclipse.osbp.xtext.table.common.TableFilterDecorator;
import org.eclipse.osbp.xtext.table.common.TableFilterGenerator;
import org.eclipse.osbp.xtext.table.common.export.CsvExport;
import org.eclipse.osbp.xtext.table.common.export.ExcelExport;
import org.eclipse.osbp.xtext.table.common.export.PdfExport;
import org.eclipse.osbp.xtext.table.generator.TableGridSourceGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.xbase.compiler.GeneratorConfig;
import org.eclipse.xtext.xbase.compiler.ImportManager;
import org.eclipse.xtext.xbase.compiler.output.TreeAppendable;
import org.eclipse.xtext.xbase.lib.Extension;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.vaadin.hene.popupbutton.PopupButton;

@SuppressWarnings("all")
public class TableModelGenerator extends I18NModelGenerator {
  @Inject
  private TableGridSourceGenerator delegate;
  
  @Inject
  @Extension
  private BasicDslGeneratorUtils _basicDslGeneratorUtils;
  
  @Override
  public TreeAppendable createAppendable(final EObject context, final ImportManager importManager, final GeneratorConfig config) {
    TreeAppendable _xblockexpression = null;
    {
      this.setBuilder(context.eResource());
      this._basicDslGeneratorUtils.addImportFor(this, importManager, 
        this._typeReferenceBuilder, 
        HashMap.class, 
        Map.class, 
        Collection.class, 
        Property.ValueChangeListener.class, 
        Label.class, 
        List.class, 
        Set.class, 
        TreeSet.class, 
        Iterator.class, 
        ArrayList.class, 
        Page.class, 
        Page.Styles.class, 
        DerivedAxis.class, 
        DerivedPosition.class, 
        DerivedMember.class, 
        HorizontalLayout.class, 
        TabSheet.class, 
        Notification.class, 
        Button.ClickEvent.class, 
        Button.class, 
        Notification.Type.class, 
        CustomTable.Align.class, 
        OperativeDtoContainer.class, 
        DerivedHierarchy.class, 
        DerivedLevel.class, 
        PopupButton.class, 
        CellSetFilterTable.class, 
        BeanFilterTable.class, 
        CellSetPagedFilterTable.class, 
        CheckboxSelectionCellSetFilterTable.class, 
        CustomTable.class, 
        CustomTable.ColumnResizeEvent.class, 
        CustomTable.ColumnResizeListener.class, 
        CustomTable.ColumnReorderEvent.class, 
        CustomTable.ColumnReorderListener.class, 
        CustomTable.ColumnCollapseEvent.class, 
        CustomTable.ColumnCollapseListener.class, 
        TabSheet.SelectedTabChangeListener.class, 
        TabSheet.SelectedTabChangeEvent.class, 
        CustomTable.RowHeaderMode.class, 
        Container.class, 
        CellSetIndexedContainer.class, 
        TableFilterDecorator.class, 
        TableFilterGenerator.class, 
        Date.class, 
        ExtendedDate.class, 
        TimeUnit.class, 
        EventHandler.class, 
        Event.class, 
        EventUtils.class, 
        ContentMode.class, 
        EventBrokerMsg.class, 
        Executors.class, 
        SelectUserWindow.class, 
        UUID.class, 
        ProductConfiguration.class, 
        AEntityDatamart.class, 
        IViewEmbeddedProvider.class, 
        MApplication.class, 
        ContextInjectionFactory.class, 
        UI.class, 
        Locale.class, 
        IThemeResourceService.class, 
        IThemeResourceService.ThemeResourceType.class, 
        IBlipBPMFunctionProvider.class, 
        IBlipBPMConstants.class, 
        IBlipBPMStartInfo.class, 
        IBlipBPMWorkloadModifiableItem.class, 
        IDto.class, 
        Task.class, 
        BlipItem.class, 
        IDTOService.class, 
        DtoServiceAccess.class, 
        DatamartFilterGenerator.class, 
        ViewLayoutManager.class, 
        IDatamartFilterGenerator.FilterChangeListener.class, 
        EventDispatcherEvent.class, 
        EventDispatcherEvent.EventDispatcherDataTag.class, 
        EventDispatcherEvent.EventDispatcherCommand.class, 
        DatamartFilter.class, 
        DatamartPrimary.class, 
        IE4Table.class, 
        Focus.class, 
        Collectors.class, 
        IBeanSearchServiceFactory.class, 
        IECViewSessionHelper.class, 
        IBlobService.class, 
        BPMTaskSummary.class, 
        BPMOperation.class, 
        BPMTaskEventType.class, 
        BPMTaskUserEvent.class, 
        SelectWorkloadActionEnum.class, 
        StringWriter.class, 
        PrintWriter.class, 
        LinkedHashMap.class, 
        IDualData.class, 
        MPerspective.class, 
        MPart.class, 
        IPerspectiveProvider.class, 
        SelectionStore.class, 
        BPMStatus.class, 
        ExcelExport.class, 
        CsvExport.class, 
        PdfExport.class, 
        TableActionEnum.class);
      _xblockexpression = super.createAppendable(context, importManager, config);
    }
    return _xblockexpression;
  }
  
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess fsa) {
    this.addTranslatables(
      "save,cancel,securityMessage,Page,Items per page,showAll,endDate,startDate,setFilter,clearFilter,lesser_than,greater_than,equal_to,ok,reset,[x]");
    super.doGenerate(resource, fsa);
    this.delegate.doGenerate(resource, fsa);
  }
}
