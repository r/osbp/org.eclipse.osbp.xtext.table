/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.table.jvmmodel;

/**
 * <p>Container class for a Hashmap value. It carries look and feel information for table cells.</p>
 */
@SuppressWarnings("all")
public class FormatAttribute {
  public String formatter = null;
  
  public String tooltipPattern = null;
  
  public boolean hideLabelLookup = false;
  
  public boolean hideLabelInterval = false;
  
  public boolean hideImageLabel = false;
  
  public boolean collapsed = false;
  
  public boolean hasImage = false;
  
  public boolean hasImageParameter = false;
  
  public String imagePathParameter = null;
  
  public String imagePath = null;
  
  public boolean hasImageResize = false;
  
  public String imageResizeString = null;
  
  public String iconName = null;
}
