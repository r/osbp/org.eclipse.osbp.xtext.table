/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.table.jvmmodel;

import com.vaadin.server.ClientConnector;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;
import org.eclipse.e4.core.contexts.IContextFunction;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.workbench.IPresentationEngine;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.bpm.api.BPMTaskEventType;
import org.eclipse.osbp.bpm.api.BPMTaskSummary;
import org.eclipse.osbp.bpm.api.BPMTaskUserEvent;
import org.eclipse.osbp.bpm.api.IBPMTaskClient;
import org.eclipse.osbp.bpm.api.IBPMTaskEventNotification;
import org.eclipse.osbp.bpm.api.IBlipBPMWorkloadModifiableItem;
import org.eclipse.osbp.dsl.common.xtext.extensions.AnnotationExtension;
import org.eclipse.osbp.dsl.entity.xtext.extensions.ModelExtensions;
import org.eclipse.osbp.dsl.semantic.common.types.LDataType;
import org.eclipse.osbp.dsl.semantic.common.types.LKeyAndValue;
import org.eclipse.osbp.dsl.semantic.common.types.LScalarType;
import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityAttribute;
import org.eclipse.osbp.dsl.semantic.entity.LEntityReference;
import org.eclipse.osbp.infogrid.api.IGridSourceFacade;
import org.eclipse.osbp.osgi.hybrid.api.AbstractHybridVaaclipseView;
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent;
import org.eclipse.osbp.runtime.common.event.IEventDispatcher;
import org.eclipse.osbp.runtime.common.layouting.IPropertyConstants;
import org.eclipse.osbp.runtime.common.types.ITypeProviderService;
import org.eclipse.osbp.ui.api.contextfunction.IViewEmbeddedProvider;
import org.eclipse.osbp.ui.api.customfields.IBlobService;
import org.eclipse.osbp.ui.api.datamart.DatamartFilter;
import org.eclipse.osbp.ui.api.datamart.IDatamartFilterGenerator;
import org.eclipse.osbp.ui.api.e4.IE4Table;
import org.eclipse.osbp.ui.api.layout.IViewLayoutManager;
import org.eclipse.osbp.ui.api.metadata.IDSLMetadataService;
import org.eclipse.osbp.ui.api.table.ITableRowCheckListener;
import org.eclipse.osbp.ui.api.themes.IThemeResourceService;
import org.eclipse.osbp.ui.api.user.IUser;
import org.eclipse.osbp.ui.api.useraccess.IUserAccessService;
import org.eclipse.osbp.utils.common.EntityUtils;
import org.eclipse.osbp.vaaclipse.common.ecview.api.IECViewSessionHelper;
import org.eclipse.osbp.xtext.action.ActionPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeLevel;
import org.eclipse.osbp.xtext.datamart.common.DatamartDefinitionUtil;
import org.eclipse.osbp.xtext.datamart.common.olap.DerivedCellSet;
import org.eclipse.osbp.xtext.datamart.common.sql.OperativeDtoContainer;
import org.eclipse.osbp.xtext.datamartdsl.AxisEnum;
import org.eclipse.osbp.xtext.datamartdsl.DatamartAttribute;
import org.eclipse.osbp.xtext.datamartdsl.DatamartCube;
import org.eclipse.osbp.xtext.datamartdsl.DatamartDefinition;
import org.eclipse.osbp.xtext.datamartdsl.DatamartEntity;
import org.eclipse.osbp.xtext.datamartdsl.DatamartNavigation;
import org.eclipse.osbp.xtext.datamartdsl.DatamartPackage;
import org.eclipse.osbp.xtext.datamartdsl.DatamartSource;
import org.eclipse.osbp.xtext.datamartdsl.DatamartTask;
import org.eclipse.osbp.xtext.datamartdsl.jvmmodel.DatamartDSLJvmModelInferrer;
import org.eclipse.osbp.xtext.datamartdsl.util.DatamartAttributeUtil;
import org.eclipse.osbp.xtext.table.RowHeaderMode;
import org.eclipse.osbp.xtext.table.Table;
import org.eclipse.osbp.xtext.table.TableAggregation;
import org.eclipse.osbp.xtext.table.TableAllColumns;
import org.eclipse.osbp.xtext.table.TableAttribute;
import org.eclipse.osbp.xtext.table.TableAxis;
import org.eclipse.osbp.xtext.table.TableCellColor;
import org.eclipse.osbp.xtext.table.TableColumn;
import org.eclipse.osbp.xtext.table.TableDSLFactory;
import org.eclipse.osbp.xtext.table.TableDatamart;
import org.eclipse.osbp.xtext.table.TableDateDayInterval;
import org.eclipse.osbp.xtext.table.TableDateDayLookup;
import org.eclipse.osbp.xtext.table.TableDerived;
import org.eclipse.osbp.xtext.table.TableDtoDatasource;
import org.eclipse.osbp.xtext.table.TableElement;
import org.eclipse.osbp.xtext.table.TableEvent;
import org.eclipse.osbp.xtext.table.TableFormatter;
import org.eclipse.osbp.xtext.table.TableGrid;
import org.eclipse.osbp.xtext.table.TableHierarchy;
import org.eclipse.osbp.xtext.table.TableIcon;
import org.eclipse.osbp.xtext.table.TableIntInterval;
import org.eclipse.osbp.xtext.table.TableIntLookup;
import org.eclipse.osbp.xtext.table.TableInterval;
import org.eclipse.osbp.xtext.table.TableLookup;
import org.eclipse.osbp.xtext.table.TableMeasure;
import org.eclipse.osbp.xtext.table.TableNumberInterval;
import org.eclipse.osbp.xtext.table.TableNumberLookup;
import org.eclipse.osbp.xtext.table.TableOption;
import org.eclipse.osbp.xtext.table.TableOrdinal;
import org.eclipse.osbp.xtext.table.TablePackage;
import org.eclipse.osbp.xtext.table.TablePreorder;
import org.eclipse.osbp.xtext.table.TableRangeElement;
import org.eclipse.osbp.xtext.table.TableSelection;
import org.eclipse.osbp.xtext.table.TableStringLookup;
import org.eclipse.osbp.xtext.table.TableTable;
import org.eclipse.osbp.xtext.table.TableTextColor;
import org.eclipse.osbp.xtext.table.TableTooltip;
import org.eclipse.osbp.xtext.table.TableTooltipPattern;
import org.eclipse.osbp.xtext.table.TableTrend;
import org.eclipse.osbp.xtext.table.TableValue;
import org.eclipse.osbp.xtext.table.TableValueElement;
import org.eclipse.osbp.xtext.table.common.CellSetFilterTable;
import org.eclipse.osbp.xtext.table.common.CellSetIndexedContainer;
import org.eclipse.osbp.xtext.table.common.CheckboxSelectionCellSetFilterTable;
import org.eclipse.osbp.xtext.table.common.PropertyLookup;
import org.eclipse.osbp.xtext.table.common.TableFilterDecorator;
import org.eclipse.osbp.xtext.table.jvmmodel.DoubleComparator;
import org.eclipse.osbp.xtext.table.jvmmodel.FormatAttribute;
import org.eclipse.osbp.xtext.table.jvmmodel.IdAttribute;
import org.eclipse.osbp.xtext.table.jvmmodel.IntegerComparator;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.Constants;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.common.types.JvmAnnotationReference;
import org.eclipse.xtext.common.types.JvmConstructor;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.common.types.JvmField;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmMember;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.JvmVisibility;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.compiler.output.ITreeAppendable;
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer;
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor;
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;

/**
 * <p>Infers a JVM model from the source model.</p>
 * 
 * <p>The JVM model should contain all elements that would appear in the Java code
 * which is generated from the source model. Other models link against the JVM model rather than the source model.</p>
 */
@SuppressWarnings("all")
public class TableDSLJvmModelInferrer extends AbstractModelInferrer {
  @Inject
  @Extension
  private JvmTypesBuilder _jvmTypesBuilder;
  
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Inject
  @Extension
  private DatamartDSLJvmModelInferrer datamartInferrer;
  
  @Inject
  @Extension
  private ModelExtensions _modelExtensions;
  
  @Inject
  @Extension
  private AnnotationExtension _annotationExtension;
  
  @Inject
  @Named(Constants.FILE_EXTENSIONS)
  private String fileExtension;
  
  private int customformatCounter = 0;
  
  private final String CONVERT_METHOD_PREFIX = "getStringTo";
  
  private String binderClassName = "";
  
  public String getFileExtension() {
    return null;
  }
  
  /**
   * <p>infer method dispatches the necessary routines to build fields, setter, getter, constructors and methods of the generated code.</p>
   */
  protected void _infer(final TablePackage pkg, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    String _string = this._iQualifiedNameProvider.getFullyQualifiedName(pkg).toString();
    String _plus = (_string + ".");
    String _firstUpper = StringExtensions.toFirstUpper(this._iQualifiedNameProvider.getFullyQualifiedName(pkg).getLastSegment());
    String _plus_1 = (_plus + _firstUpper);
    String _plus_2 = (_plus_1 + "ServiceBinder");
    this.binderClassName = _plus_2;
    final JvmGenericType clsBinder = this._jvmTypesBuilder.toClass(pkg, pkg.getName().toString().concat("ServiceBinder"));
    clsBinder.setSimpleName(StringExtensions.toFirstUpper(clsBinder.getSimpleName()));
    final Procedure1<JvmGenericType> _function = (JvmGenericType it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(SuppressWarnings.class, "serial");
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      EList<JvmAnnotationReference> _annotations_1 = it.getAnnotations();
      JvmAnnotationReference _annotationRef_1 = this._annotationTypesBuilder.annotationRef(Component.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations_1, _annotationRef_1);
      it.setPackageName(this._iQualifiedNameProvider.getFullyQualifiedName(pkg).toString());
      this._jvmTypesBuilder.setFileHeader(it, this._jvmTypesBuilder.getDocumentation(pkg));
      this.toBinderFields(it, pkg);
      this.toBinderOperations(it, pkg);
    };
    acceptor.<JvmGenericType>accept(clsBinder, _function);
    EList<Table> _tables = pkg.getTables();
    for (final Table table : _tables) {
      {
        String clsName = "";
        boolean _isEmbedded = this.isEmbedded(table);
        if (_isEmbedded) {
          clsName = pkg.getName().toString().concat(".").concat(table.getName()).concat(StringExtensions.toFirstUpper(this.fileExtension)).concat("Embedded");
        } else {
          boolean _isGrid = this.isGrid(table);
          if (_isGrid) {
            clsName = pkg.getName().toString().concat(".").concat(table.getName()).concat("Grid");
          } else {
            boolean _isSelection = this.isSelection(table);
            if (_isSelection) {
              clsName = pkg.getName().toString().concat(".").concat(table.getName()).concat("Select");
            } else {
              clsName = pkg.getName().toString().concat(".").concat(table.getName()).concat(StringExtensions.toFirstUpper(this.fileExtension));
            }
          }
        }
        final String className = clsName;
        final JvmGenericType cls = this._jvmTypesBuilder.toClass(table, clsName);
        boolean _isEmbedded_1 = this.isEmbedded(table);
        if (_isEmbedded_1) {
          cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(IViewEmbeddedProvider.class));
          cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(ClientConnector.AttachListener.class));
        } else {
          cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(AbstractHybridVaaclipseView.class));
        }
        cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(IUser.UserLocaleListener.class));
        cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(IEventDispatcher.Receiver.class));
        cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(IE4Table.class));
        if (((!this.isGrid(table)) && (!this.isEmbedded(table)))) {
          cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(IDatamartFilterGenerator.FilterChangeListener.class));
        }
        boolean _isTask = this.isTask(table);
        if (_isTask) {
          cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(IBPMTaskEventNotification.class));
        }
        boolean _isSelection_1 = this.isSelection(table);
        if (_isSelection_1) {
          cls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(ITableRowCheckListener.CheckRecipient.class));
        }
        final Procedure1<JvmGenericType> _function_1 = (JvmGenericType it) -> {
          this.toFields(it, table);
          this.toConstructor(it, table);
          this.toGetterSetter(it, table);
          this.toOperations(it, pkg, table, className);
        };
        acceptor.<JvmGenericType>accept(cls, _function_1);
        boolean _isEmbedded_2 = this.isEmbedded(table);
        if (_isEmbedded_2) {
          String _concat = table.getName().concat(StringExtensions.toFirstUpper(this.fileExtension)).concat("Embedded");
          String contextClassName = (_concat + 
            "ContextFunction");
          final JvmGenericType contextCls = this._jvmTypesBuilder.toClass(pkg, contextClassName);
          final String tableClsName = clsName;
          contextCls.getSuperTypes().add(this._typeReferenceBuilder.typeRef(IContextFunction.class));
          contextCls.setSimpleName(StringExtensions.toFirstUpper(contextCls.getSimpleName()));
          final Procedure1<JvmGenericType> _function_2 = (JvmGenericType it) -> {
            JvmAnnotationReference annotationRef = this._annotationTypesBuilder.annotationRef(
              Component.class);
            JvmTypeReference _typeRef = this._typeReferenceBuilder.typeRef(IContextFunction.class);
            this._annotationExtension.addAnnAttr(annotationRef, table, "service", 
              ((JvmTypeReference) _typeRef));
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(IContextFunction.SERVICE_CONTEXT_KEY);
            _builder.append("=");
            String _firstUpper_1 = StringExtensions.toFirstUpper(table.getName());
            _builder.append(_firstUpper_1);
            String propContent = _builder.toString();
            this._annotationExtension.addAnnAttr(annotationRef, table, "property", propContent);
            EList<JvmAnnotationReference> _annotations = it.getAnnotations();
            this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, annotationRef);
            it.setPackageName(this._iQualifiedNameProvider.getFullyQualifiedName(pkg).toString());
            this.toContextOperations(it, table, tableClsName);
          };
          acceptor.<JvmGenericType>accept(contextCls, _function_2);
        }
      }
    }
  }
  
  /**
   * <p>create an injectable context function to be used by an e4 application.</p>
   */
  public void toContextOperations(final JvmGenericType type, final Table table, final String clsName) {
    EList<JvmMember> _members = type.getMembers();
    final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      it.setVisibility(JvmVisibility.PUBLIC);
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "eclipseContext", this._typeReferenceBuilder.typeRef(IEclipseContext.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      EList<JvmFormalParameter> _parameters_1 = it.getParameters();
      JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(table, "contextKey", this._typeReferenceBuilder.typeRef(String.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("MApplication application = eclipseContext.get(MApplication.class);");
        _builder.newLine();
        _builder.append("IEclipseContext appCtx = application.getContext();");
        _builder.newLine();
        _builder.append("IViewEmbeddedProvider provider = ContextInjectionFactory.make(");
        _builder.append(clsName);
        _builder.append(".class, appCtx);");
        _builder.newLineIfNotEmpty();
        _builder.append("appCtx.set(IViewEmbeddedProvider.class, provider);");
        _builder.newLine();
        _builder.append("return provider;");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_1);
    };
    JvmOperation _method = this._jvmTypesBuilder.toMethod(table, "compute", this._typeReferenceBuilder.typeRef(Object.class), _function);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members, _method);
  }
  
  /**
   * <p>build the constructors to be used by an e4 application.</p>
   */
  public void toConstructor(final JvmDeclaredType type, final Table table) {
    boolean _isEmbedded = this.isEmbedded(table);
    boolean _not = (!_isEmbedded);
    if (_not) {
      EList<JvmMember> _members = type.getMembers();
      final Procedure1<JvmConstructor> _function = (JvmConstructor it) -> {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "parent", this._typeReferenceBuilder.typeRef(VerticalLayout.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(table, "context", this._typeReferenceBuilder.typeRef(IEclipseContext.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        EList<JvmFormalParameter> _parameters_2 = it.getParameters();
        JvmFormalParameter _parameter_2 = this._jvmTypesBuilder.toParameter(table, "app", this._typeReferenceBuilder.typeRef(MApplication.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_2, _parameter_2);
        final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("super(parent,context,app);");
          _builder.newLine();
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_1);
      };
      JvmConstructor _constructor = this._jvmTypesBuilder.toConstructor(table, _function);
      this._jvmTypesBuilder.<JvmConstructor>operator_add(_members, _constructor);
    }
  }
  
  public TableDatamart getDataMartTable(final Table table) {
    return this.getDataMartTable(table.getTabletype());
  }
  
  public TableDatamart getDataMartTable(final TableOption option) {
    boolean _matched = false;
    if (option instanceof TableTable) {
      _matched=true;
      return ((TableTable)option).getSource();
    }
    return null;
  }
  
  public TableDtoDatasource getDtoGridTable(final Table table) {
    return this.getDtoGridTable(table.getTabletype());
  }
  
  public TableDtoDatasource getDtoGridTable(final TableOption option) {
    boolean _matched = false;
    if (option instanceof TableGrid) {
      _matched=true;
      TableDtoDatasource _source = ((TableGrid)option).getSource();
      return ((TableDtoDatasource) _source);
    }
    return null;
  }
  
  public String getSourceDataMartRefName(final Table table) {
    if ((((this.getDataMartTable(table) != null) && (this.getDataMartTable(table).getDatamartRef() != null)) && 
      (this.getDataMartTable(table).getDatamartRef().getName() != null))) {
      return this.getDataMartTable(table).getDatamartRef().getName().toString();
    } else {
      return null;
    }
  }
  
  /**
   * <p>build the class variables.</p>
   */
  public void toFields(final JvmDeclaredType type, final Table table) {
    JvmField field = null;
    final Procedure1<JvmField> _function = (JvmField it) -> {
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("org.slf4j.LoggerFactory.getLogger(\"tables\")");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setInitializer(it, _function_1);
    };
    field = this._jvmTypesBuilder.toField(table, "log", this._typeReferenceBuilder.typeRef(Logger.class), _function);
    field.setStatic(true);
    field.setVisibility(JvmVisibility.PRIVATE);
    EList<JvmMember> _members = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members, field);
    final Procedure1<JvmField> _function_1 = (JvmField it) -> {
      final Procedure1<ITreeAppendable> _function_2 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("false");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setInitializer(it, _function_2);
    };
    field = this._jvmTypesBuilder.toField(table, "initDone", this._typeReferenceBuilder.typeRef(boolean.class), _function_1);
    EList<JvmMember> _members_1 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_1, field);
    field = this._jvmTypesBuilder.toField(table, "parent", this._typeReferenceBuilder.typeRef(VerticalLayout.class));
    EList<JvmMember> _members_2 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_2, field);
    final Procedure1<JvmField> _function_2 = (JvmField it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
    };
    field = this._jvmTypesBuilder.toField(table, "eclipseContext", this._typeReferenceBuilder.typeRef(IEclipseContext.class), _function_2);
    EList<JvmMember> _members_3 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_3, field);
    final Procedure1<JvmField> _function_3 = (JvmField it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
    };
    field = this._jvmTypesBuilder.toField(table, "user", this._typeReferenceBuilder.typeRef(IUser.class), _function_3);
    EList<JvmMember> _members_4 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_4, field);
    final Procedure1<JvmField> _function_4 = (JvmField it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
    };
    field = this._jvmTypesBuilder.toField(table, "eventDispatcher", this._typeReferenceBuilder.typeRef(IEventDispatcher.class), _function_4);
    EList<JvmMember> _members_5 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_5, field);
    field = this._jvmTypesBuilder.toField(table, "coordinateSystem", this._typeReferenceBuilder.typeRef(ArrayList.class, this._typeReferenceBuilder.typeRef(Integer.class)));
    EList<JvmMember> _members_6 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_6, field);
    field = this._jvmTypesBuilder.toField(table, "layoutManager", this._typeReferenceBuilder.typeRef(IViewLayoutManager.class));
    EList<JvmMember> _members_7 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_7, field);
    final Procedure1<JvmField> _function_5 = (JvmField it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
    };
    field = this._jvmTypesBuilder.toField(table, "dslMetadataService", this._typeReferenceBuilder.typeRef(IDSLMetadataService.class), _function_5);
    EList<JvmMember> _members_8 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_8, field);
    final Procedure1<JvmField> _function_6 = (JvmField it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
    };
    field = this._jvmTypesBuilder.toField(table, "blobService", this._typeReferenceBuilder.typeRef(IBlobService.class), _function_6);
    EList<JvmMember> _members_9 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_9, field);
    String _sourceDataMartRefName = this.getSourceDataMartRefName(table);
    boolean _tripleNotEquals = (_sourceDataMartRefName != null);
    if (_tripleNotEquals) {
      EObject _eContainer = this.getDataMartTable(table).getDatamartRef().eContainer();
      String packageName = this._iQualifiedNameProvider.getFullyQualifiedName(((DatamartPackage) _eContainer)).toString();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(packageName);
      _builder.append(".");
      String _sourceDataMartRefName_1 = this.getSourceDataMartRefName(table);
      _builder.append(_sourceDataMartRefName_1);
      _builder.append("Datamart");
      field = this._jvmTypesBuilder.toField(table, "datamartInstance", 
        this._typeReferenceBuilder.typeRef(_builder.toString()));
      EList<JvmMember> _members_10 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_10, field);
      field = this._jvmTypesBuilder.toField(table, "filterGenerator", this._typeReferenceBuilder.typeRef(IDatamartFilterGenerator.class));
      EList<JvmMember> _members_11 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_11, field);
      boolean _isTask = this.isTask(table);
      boolean _not = (!_isTask);
      if (_not) {
        final Procedure1<JvmField> _function_7 = (JvmField it) -> {
          final Procedure1<ITreeAppendable> _function_8 = (ITreeAppendable it_1) -> {
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("false");
            it_1.append(_builder_1);
          };
          this._jvmTypesBuilder.setInitializer(it, _function_8);
        };
        field = this._jvmTypesBuilder.toField(table, "moreToLoad", this._typeReferenceBuilder.typeRef(boolean.class), _function_7);
        EList<JvmMember> _members_12 = type.getMembers();
        this._jvmTypesBuilder.<JvmField>operator_add(_members_12, field);
      }
    }
    boolean _isGrid = this.isGrid(table);
    if (_isGrid) {
      final Procedure1<JvmField> _function_8 = (JvmField it) -> {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      };
      field = this._jvmTypesBuilder.toField(table, "gridSourceService", this._typeReferenceBuilder.typeRef(IGridSourceFacade.class), _function_8);
      EList<JvmMember> _members_13 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_13, field);
      final Procedure1<JvmField> _function_9 = (JvmField it) -> {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      };
      field = this._jvmTypesBuilder.toField(table, "ecviewPropsProvider", this._typeReferenceBuilder.typeRef(IECViewSessionHelper.class), _function_9);
      EList<JvmMember> _members_14 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_14, field);
      final Procedure1<JvmField> _function_10 = (JvmField it) -> {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      };
      field = this._jvmTypesBuilder.toField(table, "bundleSpaceTypeProvider", this._typeReferenceBuilder.typeRef(ITypeProviderService.class), _function_10);
      EList<JvmMember> _members_15 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_15, field);
      field = this._jvmTypesBuilder.toField(table, "grid", this._typeReferenceBuilder.typeRef("org.eclipse.osbp.infogrid.vaaclipse.SingleInfoGridComponent"));
      EList<JvmMember> _members_16 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_16, field);
    }
    field = this._jvmTypesBuilder.toField(table, "dataComponent", this._typeReferenceBuilder.typeRef(com.vaadin.ui.Component.class));
    EList<JvmMember> _members_17 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_17, field);
    final Procedure1<JvmField> _function_11 = (JvmField it) -> {
      final Procedure1<ITreeAppendable> _function_12 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("false");
        it_1.append(_builder_1);
      };
      this._jvmTypesBuilder.setInitializer(it, _function_12);
    };
    field = this._jvmTypesBuilder.toField(table, "mustRefresh", this._typeReferenceBuilder.typeRef(boolean.class), _function_11);
    EList<JvmMember> _members_18 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_18, field);
    field = this._jvmTypesBuilder.toField(table, "attributeLookupMap", 
      this._typeReferenceBuilder.typeRef(HashMap.class, this._typeReferenceBuilder.typeRef(String.class), this._typeReferenceBuilder.typeRef(PropertyLookup.class)));
    EList<JvmMember> _members_19 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_19, field);
    field = this._jvmTypesBuilder.toField(table, "tabSheets", 
      this._typeReferenceBuilder.typeRef(HashMap.class, this._typeReferenceBuilder.typeRef(com.vaadin.ui.Component.class), this._typeReferenceBuilder.typeRef(ArrayList.class, this._typeReferenceBuilder.typeRef(ArrayList.class, this._typeReferenceBuilder.typeRef(Integer.class)))));
    EList<JvmMember> _members_20 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_20, field);
    field = this._jvmTypesBuilder.toField(table, "dataSourceContainer", this._typeReferenceBuilder.typeRef(CellSetIndexedContainer.class));
    EList<JvmMember> _members_21 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_21, field);
    field = this._jvmTypesBuilder.toField(table, "operativeDtoContainer", this._typeReferenceBuilder.typeRef(OperativeDtoContainer.class));
    EList<JvmMember> _members_22 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_22, field);
    field = this._jvmTypesBuilder.toField(table, "tables", this._typeReferenceBuilder.typeRef(ArrayList.class, this._typeReferenceBuilder.typeRef(CellSetFilterTable.class)));
    EList<JvmMember> _members_23 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_23, field);
    final Procedure1<JvmField> _function_12 = (JvmField it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
    };
    field = this._jvmTypesBuilder.toField(table, "renderingEngine", this._typeReferenceBuilder.typeRef(IPresentationEngine.class), _function_12);
    EList<JvmMember> _members_24 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_24, field);
    final Procedure1<JvmField> _function_13 = (JvmField it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
    };
    field = this._jvmTypesBuilder.toField(table, "themeResourceService", this._typeReferenceBuilder.typeRef(IThemeResourceService.class), _function_13);
    EList<JvmMember> _members_25 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_25, field);
    field = this._jvmTypesBuilder.toField(table, "tableFilterDecorator", this._typeReferenceBuilder.typeRef(TableFilterDecorator.class));
    EList<JvmMember> _members_26 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_26, field);
    boolean _isTask_1 = this.isTask(table);
    if (_isTask_1) {
      final Procedure1<JvmField> _function_14 = (JvmField it) -> {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        EList<JvmAnnotationReference> _annotations_1 = it.getAnnotations();
        JvmAnnotationReference _annotationRef_1 = this._annotationTypesBuilder.annotationRef(Optional.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations_1, _annotationRef_1);
      };
      field = this._jvmTypesBuilder.toField(table, "taskClient", this._typeReferenceBuilder.typeRef(IBPMTaskClient.class), _function_14);
      EList<JvmMember> _members_27 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_27, field);
      field = this._jvmTypesBuilder.toField(table, "currentTaskId", this._typeReferenceBuilder.typeRef(Long.class));
      EList<JvmMember> _members_28 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_28, field);
      field = this._jvmTypesBuilder.toField(table, "taskButtons", 
        this._typeReferenceBuilder.typeRef(ArrayList.class, this._typeReferenceBuilder.typeRef(Button.class)));
      EList<JvmMember> _members_29 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_29, field);
    }
    boolean _isSelection = this.isSelection(table);
    if (_isSelection) {
      final Procedure1<JvmField> _function_15 = (JvmField it) -> {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Inject.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        EList<JvmAnnotationReference> _annotations_1 = it.getAnnotations();
        JvmAnnotationReference _annotationRef_1 = this._annotationTypesBuilder.annotationRef(Optional.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations_1, _annotationRef_1);
      };
      field = this._jvmTypesBuilder.toField(table, "taskClient", this._typeReferenceBuilder.typeRef(IBPMTaskClient.class), _function_15);
      EList<JvmMember> _members_30 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_30, field);
      field = this._jvmTypesBuilder.toField(table, "selectTable", this._typeReferenceBuilder.typeRef(CheckboxSelectionCellSetFilterTable.class));
      EList<JvmMember> _members_31 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_31, field);
      field = this._jvmTypesBuilder.toField(table, "workLoadItem", this._typeReferenceBuilder.typeRef(IBlipBPMWorkloadModifiableItem.class));
      EList<JvmMember> _members_32 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_32, field);
      field = this._jvmTypesBuilder.toField(table, "taskSummary", this._typeReferenceBuilder.typeRef(BPMTaskSummary.class));
      EList<JvmMember> _members_33 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_33, field);
    }
    if ((((!this.isEmbedded(table)) && (table.getTabletype().getToolbar() != null)) && (table.getTabletype().getToolbar().eContainer() != null))) {
      EObject _eContainer_1 = table.getTabletype().getToolbar().eContainer();
      String packageName_1 = this._iQualifiedNameProvider.getFullyQualifiedName(((ActionPackage) _eContainer_1)).toString();
      field = this._jvmTypesBuilder.toField(table, "toolbarUUID", this._typeReferenceBuilder.typeRef(String.class));
      EList<JvmMember> _members_34 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_34, field);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(packageName_1);
      _builder_1.append(".");
      String _firstUpper = StringExtensions.toFirstUpper(table.getTabletype().getToolbar().getName().toString());
      _builder_1.append(_firstUpper);
      _builder_1.append("Toolbar");
      field = this._jvmTypesBuilder.toField(table, "toolbarClass", this._typeReferenceBuilder.typeRef(_builder_1.toString()));
      EList<JvmMember> _members_35 = type.getMembers();
      this._jvmTypesBuilder.<JvmField>operator_add(_members_35, field);
    }
  }
  
  /**
   * <p>build the getters and setters from class variables.</p>
   */
  public void toGetterSetter(final JvmDeclaredType type, final Table table) {
    JvmOperation operation = null;
    operation = this._jvmTypesBuilder.toGetter(table, "coordinateSystem", 
      this._typeReferenceBuilder.typeRef(ArrayList.class, this._typeReferenceBuilder.typeRef(Integer.class)));
    operation.setVisibility(JvmVisibility.PUBLIC);
    EList<JvmMember> _members = type.getMembers();
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members, operation);
  }
  
  /**
   * <p>build the methods.</p>
   */
  public void toOperations(final JvmDeclaredType type, final TablePackage pkg, final Table table, final String clsName) {
    EList<JvmMember> _members = type.getMembers();
    final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "limited", this._typeReferenceBuilder.typeRef(boolean.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("operativeDtoContainer = null;");
        _builder.newLine();
        {
          TableDatamart _dataMartTable = this.getDataMartTable(table);
          boolean _tripleNotEquals = (_dataMartTable != null);
          if (_tripleNotEquals) {
            _builder.append("DerivedCellSet cellSet = datamartInstance.getResults(");
            {
              if ((this.isTask(table) || this.isCube(table))) {
              } else {
                {
                  if ((this.isSelection(table) || this.isEmbedded(table))) {
                    _builder.append("limited, null, null");
                  } else {
                    _builder.append("limited, getTaskOperativeDtoClass(), getTaskInitialOperativeDtos()");
                  }
                }
              }
            }
            _builder.append(");");
            _builder.newLineIfNotEmpty();
            {
              if (((!this.isTask(table)) && (!this.isCube(table)))) {
                _builder.append("moreToLoad = datamartInstance.isMoreToLoad();");
              }
            }
            _builder.newLineIfNotEmpty();
          } else {
            _builder.append("DerivedCellSet cellSet = null;");
            _builder.newLine();
          }
        }
        _builder.append("if\t(cellSet != null) {");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("operativeDtoContainer = cellSet.getOperativeDtoContainer();");
        _builder.newLine();
        _builder.append("}");
        _builder.newLine();
        _builder.append("return cellSet;");
        _builder.newLine();
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_1);
    };
    JvmOperation _method = this._jvmTypesBuilder.toMethod(table, "getCellSet", this._typeReferenceBuilder.typeRef(DerivedCellSet.class), _function);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members, _method);
    EList<JvmMember> _members_1 = type.getMembers();
    final Procedure1<JvmOperation> _function_1 = (JvmOperation it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Focus.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      boolean _isGrid = this.isGrid(table);
      if (_isGrid) {
        final Procedure1<ITreeAppendable> _function_2 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("if(grid != null) {");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("grid.focus();");
          _builder.newLine();
          _builder.append("}");
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_2);
      } else {
        final Procedure1<ITreeAppendable> _function_3 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("if(tables != null && !tables.isEmpty()) {");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("tables.get(0).focus();");
          _builder.newLine();
          _builder.append("}");
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_3);
      }
    };
    JvmOperation _method_1 = this._jvmTypesBuilder.toMethod(table, "setFocus", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_1);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, _method_1);
    EList<JvmMember> _members_2 = type.getMembers();
    final Procedure1<JvmOperation> _function_2 = (JvmOperation it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(PostConstruct.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      final Procedure1<ITreeAppendable> _function_3 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        {
          boolean _isEmbedded = this.isEmbedded(table);
          boolean _not = (!_isEmbedded);
          if (_not) {
            _builder.append("super.initView();");
          }
        }
        _builder.newLineIfNotEmpty();
        _builder.append("user.addUserLocaleListener(this);");
        _builder.newLine();
        _builder.append("eventDispatcher.addEventReceiver(this);");
        _builder.newLine();
        {
          boolean _isTask = this.isTask(table);
          if (_isTask) {
            _builder.append("taskClient.subscribeTaskEventNotification(this);");
            _builder.newLine();
            _builder.append("taskClient.subscribeProcessCompletedEventNotification(this);");
            _builder.newLine();
          }
        }
        {
          if (((!this.isGrid(table)) && (!this.isEmbedded(table)))) {
            _builder.append("filterGenerator.addFilterChangeListener(this);");
            _builder.newLine();
          }
        }
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_3);
    };
    JvmOperation _method_2 = this._jvmTypesBuilder.toMethod(table, "activate", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_2);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_2, _method_2);
    EList<JvmMember> _members_3 = type.getMembers();
    final Procedure1<JvmOperation> _function_3 = (JvmOperation it) -> {
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(PreDestroy.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      final Procedure1<ITreeAppendable> _function_4 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("user.removeUserLocaleListener(this);");
        _builder.newLine();
        _builder.append("eventDispatcher.removeEventReceiver(this);");
        _builder.newLine();
        {
          boolean _isTask = this.isTask(table);
          if (_isTask) {
            _builder.append("taskClient.unsubscribeTaskEventNotification(this);");
            _builder.newLine();
            _builder.append("taskClient.unsubscribeProcessCompletedEventNotification(this);");
            _builder.newLine();
          }
        }
        {
          if (((!this.isGrid(table)) && (!this.isEmbedded(table)))) {
            _builder.append("filterGenerator.removeFilterChangeListener(this);");
            _builder.newLine();
          }
        }
        {
          boolean _isEmbedded = this.isEmbedded(table);
          boolean _not = (!_isEmbedded);
          if (_not) {
            _builder.append("super.destroyView();");
          }
        }
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_4);
    };
    JvmOperation _method_3 = this._jvmTypesBuilder.toMethod(table, "deactivate", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_3);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_3, _method_3);
    EList<JvmMember> _members_4 = type.getMembers();
    final Procedure1<JvmOperation> _function_4 = (JvmOperation it) -> {
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "parent", this._typeReferenceBuilder.typeRef(VerticalLayout.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      boolean _isEmbedded = this.isEmbedded(table);
      if (_isEmbedded) {
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      }
      final Procedure1<ITreeAppendable> _function_5 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        String _createView = this.createView(table, pkg);
        _builder.append(_createView);
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_5);
    };
    JvmOperation _method_4 = this._jvmTypesBuilder.toMethod(table, "createView", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_4);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_4, _method_4);
    EList<JvmMember> _members_5 = type.getMembers();
    final Procedure1<JvmOperation> _function_5 = (JvmOperation it) -> {
      final Procedure1<ITreeAppendable> _function_6 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        String _createComponents = this.createComponents(table, clsName);
        _builder.append(_createComponents);
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_6);
    };
    JvmOperation _method_5 = this._jvmTypesBuilder.toMethod(table, "createComponents", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_5);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_5, _method_5);
    EList<JvmMember> _members_6 = type.getMembers();
    final Procedure1<JvmOperation> _function_6 = (JvmOperation it) -> {
      this._jvmTypesBuilder.setDocumentation(it, this._jvmTypesBuilder.getDocumentation(table.getTabletype()));
      it.setVisibility(JvmVisibility.PUBLIC);
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "manualInput", this._typeReferenceBuilder.typeRef(boolean.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      final Procedure1<ITreeAppendable> _function_7 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_7);
    };
    JvmOperation _method_6 = this._jvmTypesBuilder.toMethod(table, "setEnableManualInput", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_6);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_6, _method_6);
    EList<JvmMember> _members_7 = type.getMembers();
    final Procedure1<JvmOperation> _function_7 = (JvmOperation it) -> {
      this._jvmTypesBuilder.setDocumentation(it, this._jvmTypesBuilder.getDocumentation(table.getTabletype()));
      it.setVisibility(JvmVisibility.PUBLIC);
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "dto", this._typeReferenceBuilder.typeRef(Object.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      final Procedure1<ITreeAppendable> _function_8 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_8);
    };
    JvmOperation _method_7 = this._jvmTypesBuilder.toMethod(table, "setInput", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_7);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_7, _method_7);
    EList<JvmMember> _members_8 = type.getMembers();
    final Procedure1<JvmOperation> _function_8 = (JvmOperation it) -> {
      this._jvmTypesBuilder.setDocumentation(it, this._jvmTypesBuilder.getDocumentation(table.getTabletype()));
      it.setVisibility(JvmVisibility.PRIVATE);
      final Procedure1<ITreeAppendable> _function_9 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("return ");
        {
          TableOption _tabletype = table.getTabletype();
          if ((_tabletype instanceof TableTable)) {
            TableOption _tabletype_1 = table.getTabletype();
            boolean _booleanValue = Boolean.valueOf(((TableTable) _tabletype_1).isSelectById()).booleanValue();
            _builder.append(_booleanValue);
          } else {
            _builder.append("false");
          }
        }
        _builder.append(";");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_9);
    };
    JvmOperation _method_8 = this._jvmTypesBuilder.toMethod(table, "getSelectById", this._typeReferenceBuilder.typeRef(boolean.class), _function_8);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_8, _method_8);
    TableOption _tabletype = table.getTabletype();
    boolean _matched = false;
    if (_tabletype instanceof TableTable) {
      _matched=true;
      EList<JvmMember> _members_9 = type.getMembers();
      final Procedure1<JvmOperation> _function_9 = (JvmOperation it) -> {
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "cellSet", this._typeReferenceBuilder.typeRef(DerivedCellSet.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(table, "axisNo", this._typeReferenceBuilder.typeRef(Integer.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        final Procedure1<ITreeAppendable> _function_10 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          String _createTabSheet = this.createTabSheet(table);
          _builder.append(_createTabSheet);
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_10);
      };
      JvmOperation _method_9 = this._jvmTypesBuilder.toMethod(table, "createTabSheet", this._typeReferenceBuilder.typeRef(com.vaadin.ui.Component.class), _function_9);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_9, _method_9);
      EList<JvmMember> _members_10 = type.getMembers();
      final Procedure1<JvmOperation> _function_10 = (JvmOperation it) -> {
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "cellSet", this._typeReferenceBuilder.typeRef(DerivedCellSet.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        final Procedure1<ITreeAppendable> _function_11 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          String _createTable = this.createTable(table);
          _builder.append(_createTable);
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_11);
      };
      JvmOperation _method_10 = this._jvmTypesBuilder.toMethod(table, "createTable", this._typeReferenceBuilder.typeRef(com.vaadin.ui.Component.class), _function_10);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_10, _method_10);
      EList<JvmMember> _members_11 = type.getMembers();
      final Procedure1<JvmOperation> _function_11 = (JvmOperation it) -> {
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "table", this._typeReferenceBuilder.typeRef(CellSetFilterTable.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(table, "cellSet", this._typeReferenceBuilder.typeRef(DerivedCellSet.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        final Procedure1<ITreeAppendable> _function_12 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          String _initTable = this.initTable(table);
          _builder.append(_initTable);
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_12);
      };
      JvmOperation _method_11 = this._jvmTypesBuilder.toMethod(table, "initTable", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_11);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_11, _method_11);
      EList<JvmMember> _members_12 = type.getMembers();
      final Procedure1<JvmOperation> _function_12 = (JvmOperation it) -> {
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "table", this._typeReferenceBuilder.typeRef(CellSetFilterTable.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        final Procedure1<ITreeAppendable> _function_13 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          CharSequence _tableSettingsByUserProperties = this.tableSettingsByUserProperties(table);
          _builder.append(_tableSettingsByUserProperties);
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_13);
      };
      JvmOperation _method_12 = this._jvmTypesBuilder.toMethod(table, "tableSettingsByUserProperties", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_12);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_12, _method_12);
      EList<JvmMember> _members_13 = type.getMembers();
      final Procedure1<JvmOperation> _function_13 = (JvmOperation it) -> {
        final Procedure1<ITreeAppendable> _function_14 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          String _dataRefresh = this.dataRefresh(table);
          _builder.append(_dataRefresh);
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_14);
      };
      JvmOperation _method_13 = this._jvmTypesBuilder.toMethod(table, "dataRefresh", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_13);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_13, _method_13);
      EList<JvmMember> _members_14 = type.getMembers();
      final Procedure1<JvmOperation> _function_14 = (JvmOperation it) -> {
        final Procedure1<ITreeAppendable> _function_15 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          CharSequence _renderAllowed = this.renderAllowed();
          _builder.append(_renderAllowed);
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_15);
      };
      JvmOperation _method_14 = this._jvmTypesBuilder.toMethod(table, "renderAllowed", this._typeReferenceBuilder.typeRef(boolean.class), _function_14);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_14, _method_14);
    }
    if (!_matched) {
      if (_tabletype instanceof TableGrid) {
        _matched=true;
        EList<JvmMember> _members_9 = type.getMembers();
        final Procedure1<JvmOperation> _function_9 = (JvmOperation it) -> {
          QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(this.getDtoGridTable(table).getDtoSource());
          String _string = null;
          if (_fullyQualifiedName!=null) {
            _string=_fullyQualifiedName.toString();
          }
          final String gridDtoFqn = _string;
          EObject _resolve = EcoreUtil2.resolve(this.getDtoGridTable(table).getDtoSource().getWrappedType(), table.eResource());
          final LEntity entity = ((LEntity) _resolve);
          final Procedure1<ITreeAppendable> _function_10 = (ITreeAppendable it_1) -> {
            StringConcatenation _builder = new StringConcatenation();
            CharSequence _createGridComponent = this.createGridComponent(pkg, table, entity, gridDtoFqn);
            _builder.append(_createGridComponent);
            _builder.newLineIfNotEmpty();
            _builder.append("return grid;");
            it_1.append(_builder);
          };
          this._jvmTypesBuilder.setBody(it, _function_10);
        };
        JvmOperation _method_9 = this._jvmTypesBuilder.toMethod(table, "createDtoGrid", this._typeReferenceBuilder.typeRef(com.vaadin.ui.Component.class), _function_9);
        this._jvmTypesBuilder.<JvmOperation>operator_add(_members_9, _method_9);
      }
    }
    EList<JvmMember> _members_9 = type.getMembers();
    final Procedure1<JvmOperation> _function_9 = (JvmOperation it) -> {
      it.setVisibility(JvmVisibility.PUBLIC);
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "locale", this._typeReferenceBuilder.typeRef(Locale.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      final Procedure1<ITreeAppendable> _function_10 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        CharSequence _localeChanged = this.localeChanged(table);
        _builder.append(_localeChanged);
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_10);
    };
    JvmOperation _method_9 = this._jvmTypesBuilder.toMethod(table, "localeChanged", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_9);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_9, _method_9);
    EList<JvmMember> _members_10 = type.getMembers();
    final Procedure1<JvmOperation> _function_10 = (JvmOperation it) -> {
      it.setVisibility(JvmVisibility.PUBLIC);
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "event", this._typeReferenceBuilder.typeRef(EventDispatcherEvent.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      final Procedure1<ITreeAppendable> _function_11 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        String _receiveEvent = this.receiveEvent(table);
        _builder.append(_receiveEvent);
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_11);
    };
    JvmOperation _method_10 = this._jvmTypesBuilder.toMethod(table, "receiveEvent", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_10);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_10, _method_10);
    if (((!this.isGrid(table)) && (!this.isEmbedded(table)))) {
      EList<JvmMember> _members_11 = type.getMembers();
      final Procedure1<JvmOperation> _function_11 = (JvmOperation it) -> {
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "changedFilter", this._typeReferenceBuilder.typeRef(DatamartFilter.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        it.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        final Procedure1<ITreeAppendable> _function_12 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          String _filterChanged = this.filterChanged(table);
          _builder.append(_filterChanged);
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_12);
      };
      JvmOperation _method_11 = this._jvmTypesBuilder.toMethod(table, "filterChanged", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_11);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_11, _method_11);
    }
    boolean _isTask = this.isTask(table);
    if (_isTask) {
      EList<JvmMember> _members_12 = type.getMembers();
      final Procedure1<JvmOperation> _function_12 = (JvmOperation it) -> {
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "type", this._typeReferenceBuilder.typeRef(BPMTaskEventType.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(table, "event", this._typeReferenceBuilder.typeRef(BPMTaskUserEvent.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        it.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        final Procedure1<ITreeAppendable> _function_13 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          CharSequence _notifyTaskEvent = this.notifyTaskEvent(table);
          _builder.append(_notifyTaskEvent);
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_13);
      };
      JvmOperation _method_12 = this._jvmTypesBuilder.toMethod(table, "notifyTaskEvent", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_12);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_12, _method_12);
      EList<JvmMember> _members_13 = type.getMembers();
      final Procedure1<JvmOperation> _function_13 = (JvmOperation it) -> {
        it.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        final Procedure1<ITreeAppendable> _function_14 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          CharSequence _notifyTaskEvent = this.notifyTaskEvent(table);
          _builder.append(_notifyTaskEvent);
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_14);
      };
      JvmOperation _method_13 = this._jvmTypesBuilder.toMethod(table, "notifyProcessCompletedEvent", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_13);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_13, _method_13);
    }
    boolean _isSelection = this.isSelection(table);
    if (_isSelection) {
      EList<JvmMember> _members_14 = type.getMembers();
      final Procedure1<JvmOperation> _function_14 = (JvmOperation it) -> {
        it.setVisibility(JvmVisibility.PUBLIC);
        final Procedure1<ITreeAppendable> _function_15 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          CharSequence _selectItemsFromWorkloadDto = this.selectItemsFromWorkloadDto(table);
          _builder.append(_selectItemsFromWorkloadDto);
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_15);
      };
      JvmOperation _method_14 = this._jvmTypesBuilder.toMethod(table, "selectItemsFromWorkloadDto", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_14);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_14, _method_14);
      EList<JvmMember> _members_15 = type.getMembers();
      final Procedure1<JvmOperation> _function_15 = (JvmOperation it) -> {
        it.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "selection", this._typeReferenceBuilder.typeRef(Object.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        EList<JvmFormalParameter> _parameters_1 = it.getParameters();
        JvmFormalParameter _parameter_1 = this._jvmTypesBuilder.toParameter(table, "selected", this._typeReferenceBuilder.typeRef(boolean.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters_1, _parameter_1);
        final Procedure1<ITreeAppendable> _function_16 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          CharSequence _rowChecked = this.rowChecked(table);
          _builder.append(_rowChecked);
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_16);
      };
      JvmOperation _method_15 = this._jvmTypesBuilder.toMethod(table, "rowChecked", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_15);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_15, _method_15);
    }
    boolean _isEmbedded = this.isEmbedded(table);
    if (_isEmbedded) {
      EList<JvmMember> _members_16 = type.getMembers();
      final Procedure1<JvmOperation> _function_16 = (JvmOperation it) -> {
        it.setVisibility(JvmVisibility.PUBLIC);
        EList<JvmAnnotationReference> _annotations = it.getAnnotations();
        JvmAnnotationReference _annotationRef = this._annotationTypesBuilder.annotationRef(Override.class);
        this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, _annotationRef);
        EList<JvmFormalParameter> _parameters = it.getParameters();
        JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(table, "event", this._typeReferenceBuilder.typeRef(ClientConnector.AttachEvent.class));
        this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
        final Procedure1<ITreeAppendable> _function_17 = (ITreeAppendable it_1) -> {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("if(mustRefresh){");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("dataRefresh();");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("mustRefresh = false;");
          _builder.newLine();
          _builder.append("}");
          it_1.append(_builder);
        };
        this._jvmTypesBuilder.setBody(it, _function_17);
      };
      JvmOperation _method_16 = this._jvmTypesBuilder.toMethod(table, "attach", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_16);
      this._jvmTypesBuilder.<JvmOperation>operator_add(_members_16, _method_16);
    }
  }
  
  public LEntity baseType(final LEntity entityRef) {
    if ((((entityRef.getSuperType() != null) && 
      (!entityRef.getSuperType().isMappedSuperclass())) && 
      (!entityRef.getSuperType().equals(entityRef)))) {
      return entityRef.getSuperType();
    }
    return entityRef;
  }
  
  private Map<String, String> getUniqueReferenceFQIdName(final LEntity entity) {
    Map<String, String> uniqueReferenceNames = new HashMap<String, String>();
    List<LEntityReference> _allReferences = entity.getAllReferences();
    for (final LEntityReference entityReference : _allReferences) {
      {
        String uniqueReferenceName = uniqueReferenceNames.get(this._iQualifiedNameProvider.getFullyQualifiedName(entityReference.getType()));
        if ((uniqueReferenceName == null)) {
          StringConcatenation _builder = new StringConcatenation();
          QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(entityReference.getType());
          _builder.append(_fullyQualifiedName);
          _builder.append(".");
          String _idAttributeName = entityReference.getType().getIdAttributeName();
          _builder.append(_idAttributeName);
          uniqueReferenceName = _builder.toString();
          uniqueReferenceNames.put(this._iQualifiedNameProvider.getFullyQualifiedName(entityReference.getType()).toString(), uniqueReferenceName);
        }
      }
    }
    return uniqueReferenceNames;
  }
  
  protected CharSequence createGridComponent(final TablePackage pkg, final Table table, final LEntity entity, final String gridDtoFqn) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("IEclipseContext childContext = eclipseContext.createChild();");
    _builder.newLine();
    _builder.append("childContext.set(Locale.class, user.getLocale());");
    _builder.newLine();
    _builder.append("childContext.set(\"gridSourceId\", \"");
    String _concat = pkg.getName().toString().concat(".").concat(table.getName()).concat("Grid");
    _builder.append(_concat);
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("LinkedHashMap<String, EventDispatcherEvent> selectionEvntList = new LinkedHashMap<>();");
    _builder.newLine();
    _builder.append("MPerspective perspective = eclipseContext.get(MPerspective.class);");
    _builder.newLine();
    _builder.append("EventDispatcherEvent evnt = new EventDispatcherEvent(perspective, EventDispatcherCommand.SELECT, \"");
    QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(this.baseType(entity));
    _builder.append(_fullyQualifiedName);
    _builder.append(".");
    String _idAttributeName = entity.getIdAttributeName();
    _builder.append(_idAttributeName);
    _builder.append("\", \"");
    QualifiedName _fullyQualifiedName_1 = this._iQualifiedNameProvider.getFullyQualifiedName(table);
    _builder.append(_fullyQualifiedName_1);
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("selectionEvntList.put(\"");
    QualifiedName _fullyQualifiedName_2 = this._iQualifiedNameProvider.getFullyQualifiedName(this.baseType(entity));
    _builder.append(_fullyQualifiedName_2);
    _builder.append(".");
    String _idAttributeName_1 = entity.getIdAttributeName();
    _builder.append(_idAttributeName_1);
    _builder.append("\", evnt);");
    _builder.newLineIfNotEmpty();
    {
      Collection<String> _values = this.getUniqueReferenceFQIdName(entity).values();
      for(final String entityReferenceFQIdName : _values) {
        _builder.append("evnt = new EventDispatcherEvent(perspective, EventDispatcherCommand.SELECT, \"");
        _builder.append(entityReferenceFQIdName);
        _builder.append("\", \"");
        QualifiedName _fullyQualifiedName_3 = this._iQualifiedNameProvider.getFullyQualifiedName(table);
        _builder.append(_fullyQualifiedName_3);
        _builder.append("\");");
        _builder.newLineIfNotEmpty();
        _builder.append("selectionEvntList.put(\"");
        _builder.append(entityReferenceFQIdName);
        _builder.append("\", evnt);");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("childContext.set(\"selectionEvnt\", selectionEvntList);");
    _builder.newLine();
    _builder.append("if (\"");
    _builder.append(gridDtoFqn);
    _builder.append("\".equals(getTaskOperativeDtoFqn())) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("childContext.set(\"inputdata\", getTaskInitialOperativeDtos());");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("grid = ContextInjectionFactory.make(SingleInfoGridComponent.class, childContext);");
    return _builder;
  }
  
  public String filterChanged(final Table table) {
    String body = "";
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(body);
    _builder.newLineIfNotEmpty();
    _builder.append("if(changedFilter != null) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("MPerspective perspective = eclipseContext.get(MPerspective.class);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("EventDispatcherEvent evnt = new EventDispatcherEvent(perspective, EventDispatcherCommand.SELECT, changedFilter.getName(), \"");
    QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(table);
    _builder.append(_fullyQualifiedName, "\t");
    _builder.append("\");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("evnt.addData(changedFilter.getSelectedData());");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("eventDispatcher.sendEvent(evnt);");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("dataRefresh();");
    _builder.newLine();
    body = _builder.toString();
    return body;
  }
  
  public String receiveEvent(final Table table) {
    String body = "";
    boolean _isTable = this.isTable(table);
    if (_isTable) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(body);
      _builder.newLineIfNotEmpty();
      _builder.append("switch(event.getCommand()) {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("case SELECT: ");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("MPerspective perspective = eclipseContext.get(MPerspective.class);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("if(perspective != null) {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("ArrayList<String> allowedSenderParts = (ArrayList<String>) eclipseContext.getActive(MPart.class).getTransientData().get(IPerspectiveProvider.EventDispatcherConstants.ACCEPTED_SENDERS);");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("if((event.getPerspective() == null || (perspective != null && event.getPerspective().equals(perspective))) && ((allowedSenderParts != null && allowedSenderParts.contains(event.getSender())) || allowedSenderParts == null)){");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("if(!event.getSender().equals(\"");
      QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(table);
      _builder.append(_fullyQualifiedName, "\t\t\t\t");
      _builder.append("\")) {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t\t\t");
      _builder.append("if(filterGenerator.selectItem(event, getSelectById())) {");
      _builder.newLine();
      {
        if ((this.isEmbedded(table) || this.isTask(table))) {
          _builder.append("\t\t\t\t\t\t");
          _builder.append("createComponents();");
          _builder.newLine();
        } else {
          _builder.append("\t\t\t\t\t\t");
          _builder.append("renderData();");
          _builder.newLine();
        }
      }
      _builder.append("\t\t\t\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("break;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("case SAVE:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("case DELETE:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("if(!event.getSender().equals(\"");
      QualifiedName _fullyQualifiedName_1 = this._iQualifiedNameProvider.getFullyQualifiedName(table);
      _builder.append(_fullyQualifiedName_1, "\t\t");
      _builder.append("\")) {");
      _builder.newLineIfNotEmpty();
      {
        Set<LEntity> _findRequestedEntities = this.findRequestedEntities(table);
        for(final LEntity e : _findRequestedEntities) {
          _builder.append("\t\t\t");
          _builder.append("if(event.getTopic().equals(\"");
          QualifiedName _fullyQualifiedName_2 = this._iQualifiedNameProvider.getFullyQualifiedName(e);
          _builder.append(_fullyQualifiedName_2, "\t\t\t");
          _builder.append("\")){");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t\t");
          _builder.append("\t");
          _builder.append("datamartInstance.clearCache();");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("\t");
          _builder.append("if(datamartInstance.contains(event.getData().get(EventDispatcherDataTag.ID))) {");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("\t\t");
          _builder.append("dataRefresh();");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("}");
          _builder.newLine();
        }
      }
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("break;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("case REFRESH:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("if(!event.getSender().equals(\"");
      QualifiedName _fullyQualifiedName_3 = this._iQualifiedNameProvider.getFullyQualifiedName(table);
      _builder.append(_fullyQualifiedName_3, "\t\t");
      _builder.append("\")) {");
      _builder.newLineIfNotEmpty();
      {
        Set<LEntity> _findRequestedEntities_1 = this.findRequestedEntities(table);
        for(final LEntity e_1 : _findRequestedEntities_1) {
          _builder.append("\t\t\t");
          _builder.append("if(event.getTopic().equals(\"");
          QualifiedName _fullyQualifiedName_4 = this._iQualifiedNameProvider.getFullyQualifiedName(e_1);
          _builder.append(_fullyQualifiedName_4, "\t\t\t");
          _builder.append("\")){");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t\t");
          _builder.append("\t");
          _builder.append("datamartInstance.clearCache();");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("\t");
          _builder.append("dataRefresh();");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("}");
          _builder.newLine();
        }
      }
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("/* must be activated if sure that the filter is to be refreshed");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("if (filterGenerator != null) {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("filterGenerator.updateFilter();");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}*/");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("break;");
      _builder.newLine();
      body = _builder.toString();
      if ((((!this.isEmbedded(table)) && (table.getTabletype().getToolbar() != null)) && (table.getTabletype().getToolbar().eContainer() != null))) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(body);
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("\t");
        _builder_1.append("case ACTION:");
        _builder_1.newLine();
        _builder_1.append("\t\t");
        _builder_1.append("if (toolbarUUID.equals(event.getTopic()) && event.getData().containsKey(EventDispatcherDataTag.BUTTON_ID)) {");
        _builder_1.newLine();
        _builder_1.append("\t\t\t");
        _builder_1.append("Object id = event.getData().get(EventDispatcherDataTag.BUTTON_ID);");
        _builder_1.newLine();
        _builder_1.append("\t\t\t");
        _builder_1.append("if (id instanceof TableActionEnum) {");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t");
        _builder_1.append("TableActionEnum enumId = (TableActionEnum) id;");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t");
        _builder_1.append("switch (enumId) {");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t");
        _builder_1.append("case EXPORT_EXCEL:");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t\t");
        _builder_1.append("for(CellSetFilterTable table:tables) {");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t\t\t");
        _builder_1.append("ExcelExport excelExport = new ExcelExport(table, themeResourceService);");
        _builder_1.newLine();
        _builder_1.append("\t\t                ");
        _builder_1.append("excelExport.excludeCollapsedColumns();");
        _builder_1.newLine();
        _builder_1.append("\t\t                ");
        _builder_1.append("excelExport.setReportTitle(layoutManager.getCaption());");
        _builder_1.newLine();
        _builder_1.append("\t\t                ");
        _builder_1.append("excelExport.export();");
        _builder_1.newLine();
        _builder_1.append("\t\t            ");
        _builder_1.append("}");
        _builder_1.newLine();
        _builder_1.append("\t                ");
        _builder_1.append("break;");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t");
        _builder_1.append("case EXPORT_CSV:");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t\t");
        _builder_1.append("for(CellSetFilterTable table:tables) {");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t\t\t");
        _builder_1.append("CsvExport csvExport = new CsvExport(table, themeResourceService);");
        _builder_1.newLine();
        _builder_1.append("\t\t                ");
        _builder_1.append("csvExport.excludeCollapsedColumns();");
        _builder_1.newLine();
        _builder_1.append("\t\t                ");
        _builder_1.append("csvExport.setReportTitle(layoutManager.getCaption());");
        _builder_1.newLine();
        _builder_1.append("\t\t                ");
        _builder_1.append("csvExport.export();");
        _builder_1.newLine();
        _builder_1.append("\t\t            ");
        _builder_1.append("}");
        _builder_1.newLine();
        _builder_1.append("\t                ");
        _builder_1.append("break;");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t");
        _builder_1.append("case EXPORT_PDF:");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t\t");
        _builder_1.append("for(CellSetFilterTable table:tables) {");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t\t\t");
        _builder_1.append("PdfExport pdfExport = new PdfExport(table, themeResourceService);");
        _builder_1.newLine();
        _builder_1.append("\t\t                ");
        _builder_1.append("pdfExport.excludeCollapsedColumns();");
        _builder_1.newLine();
        _builder_1.append("\t\t                ");
        _builder_1.append("pdfExport.setReportTitle(layoutManager.getCaption());");
        _builder_1.newLine();
        _builder_1.append("\t\t                ");
        _builder_1.append("pdfExport.export();");
        _builder_1.newLine();
        _builder_1.append("\t\t            ");
        _builder_1.append("}");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t\t");
        _builder_1.append("break;");
        _builder_1.newLine();
        _builder_1.append("\t\t\t\t");
        _builder_1.append("}");
        _builder_1.newLine();
        _builder_1.append("\t\t\t");
        _builder_1.append("}");
        _builder_1.newLine();
        _builder_1.append("\t\t");
        _builder_1.append("}");
        _builder_1.newLine();
        _builder_1.append("\t\t");
        _builder_1.append("break;");
        _builder_1.newLine();
        body = _builder_1.toString();
      }
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append(body);
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("\t");
      _builder_2.append("}");
      _builder_2.newLine();
      body = _builder_2.toString();
    } else {
      boolean _isGrid = this.isGrid(table);
      if (_isGrid) {
        StringConcatenation _builder_3 = new StringConcatenation();
        _builder_3.append(body);
        _builder_3.newLineIfNotEmpty();
        _builder_3.append("switch(event.getCommand()) {");
        _builder_3.newLine();
        _builder_3.append("\t");
        _builder_3.append("case SAVE:");
        _builder_3.newLine();
        _builder_3.append("\t");
        _builder_3.append("case DELETE:");
        _builder_3.newLine();
        _builder_3.append("\t");
        _builder_3.append("case REFRESH:");
        _builder_3.newLine();
        _builder_3.append("\t\t");
        _builder_3.append("if(!event.getSender().equals(\"");
        QualifiedName _fullyQualifiedName_5 = this._iQualifiedNameProvider.getFullyQualifiedName(table);
        _builder_3.append(_fullyQualifiedName_5, "\t\t");
        _builder_3.append("\")) {");
        _builder_3.newLineIfNotEmpty();
        {
          Set<LEntity> _findRequestedEntities_2 = this.findRequestedEntities(table);
          for(final LEntity e_2 : _findRequestedEntities_2) {
            _builder_3.append("\t\t\t");
            _builder_3.append("if(event.getTopic().equals(\"");
            QualifiedName _fullyQualifiedName_6 = this._iQualifiedNameProvider.getFullyQualifiedName(e_2);
            _builder_3.append(_fullyQualifiedName_6, "\t\t\t");
            _builder_3.append("\")){");
            _builder_3.newLineIfNotEmpty();
            _builder_3.append("\t\t\t");
            _builder_3.append("\t");
            _builder_3.append("// TODO: GridRefresh");
            _builder_3.newLine();
            _builder_3.append("\t\t\t");
            _builder_3.append("\t");
            _builder_3.append("grid.refreshData();");
            _builder_3.newLine();
            _builder_3.append("\t\t\t");
            _builder_3.append("}");
            _builder_3.newLine();
          }
        }
        _builder_3.append("\t\t");
        _builder_3.append("}");
        _builder_3.newLine();
        _builder_3.append("\t\t");
        _builder_3.append("break;");
        _builder_3.newLine();
        _builder_3.append("}");
        _builder_3.newLine();
        body = _builder_3.toString();
      }
    }
    return body;
  }
  
  public CharSequence refreshCondition(final LEntity entity) {
    String _name = entity.getName();
    return ((CharSequence) _name);
  }
  
  public String getCallDataRefreshMethod(final Table table) {
    String _switchResult = null;
    TableOption _tabletype = table.getTabletype();
    boolean _matched = false;
    if (_tabletype instanceof TableTable) {
      _matched=true;
      _switchResult = "dataRefresh();";
    }
    if (!_matched) {
      if (_tabletype instanceof TableGrid) {
        _matched=true;
        _switchResult = "";
      }
    }
    return _switchResult;
  }
  
  public CharSequence notifyTaskEvent(final Table table) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("mustRefresh = true;");
    _builder.newLine();
    return _builder;
  }
  
  /**
   * <p>build the main method to be called from e4.</p>
   * 
   * tablePageLabel = new Label();
   * tablePageLabel.setContentMode(ContentMode.HTML);
   * tablePageLabel.addStyleName("pagelabel");
   * tablePageLabel.setId("tablePageLabel");
   * 
   * «"Page".bindComponent("tablePageLabel", "Value")»
   * tableItemsLabel = new Label();
   * tableItemsLabel.setContentMode(ContentMode.HTML);
   * tableItemsLabel.addStyleName("pagelabel");
   * tableItemsLabel.setId("tableItemsLabel");
   * «"Items per page".bindComponent("tableItemsLabel", "Value")»
   */
  public String createView(final Table table, final TablePackage pkg) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _isEmbedded = this.isEmbedded(table);
      if (_isEmbedded) {
        _builder.append("eclipseContext");
      } else {
        _builder.append("getContext()");
      }
    }
    _builder.append(".set(IE4Table.class, this);");
    _builder.newLineIfNotEmpty();
    {
      boolean _isTask = this.isTask(table);
      if (_isTask) {
        _builder.append("currentTaskId = -1L;");
        _builder.newLine();
      }
    }
    _builder.append("tables = new ArrayList<CellSetFilterTable>();");
    _builder.newLine();
    _builder.append("tableFilterDecorator = new TableFilterDecorator(dslMetadataService, user.getLocale());");
    _builder.newLine();
    _builder.append("// the timeout to begin the filter process after the last key pressed");
    _builder.newLine();
    _builder.append("tableFilterDecorator.setTextChangeTimeout(500);");
    _builder.newLine();
    _builder.append("parent.setPrimaryStyleName(\"osbp\");");
    _builder.newLine();
    _builder.append("parent.setId(\"parent\");");
    _builder.newLine();
    _builder.append("parent.setSizeFull();");
    _builder.newLine();
    {
      boolean _isEmbedded_1 = this.isEmbedded(table);
      if (_isEmbedded_1) {
        _builder.append("parent.addAttachListener(this);");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("layoutManager = new ViewLayoutManager();");
    _builder.newLine();
    _builder.append("layoutManager.init(parent);");
    _builder.newLine();
    _builder.append("this.parent = parent;");
    _builder.newLine();
    String body = _builder.toString();
    boolean _isGrid = this.isGrid(table);
    boolean _not = (!_isGrid);
    if (_not) {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(body);
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("datamartInstance = new ");
      String _sourceDataMartRefName = this.getSourceDataMartRefName(table);
      _builder_1.append(_sourceDataMartRefName);
      _builder_1.append("Datamart();");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("datamartInstance.setUser(user);");
      _builder_1.newLine();
      _builder_1.append("filterGenerator = new DatamartFilterGenerator(datamartInstance, ");
      {
        boolean _isEmbedded_2 = this.isEmbedded(table);
        if (_isEmbedded_2) {
          _builder_1.append("eclipseContext");
        } else {
          _builder_1.append("getContext()");
        }
      }
      _builder_1.append(", ");
      boolean _isShowCaption = this.getDataMartTable(table).getDatamartRef().isShowCaption();
      _builder_1.append(_isShowCaption);
      _builder_1.append(", ");
      int _max = Integer.max(10, this.getDataMartTable(table).getDatamartRef().getNumMultiRows());
      _builder_1.append(_max);
      _builder_1.append(");");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("filterGenerator.createUIFilters(layoutManager);");
      _builder_1.newLine();
      body = _builder_1.toString();
    }
    boolean _isSelection = this.isSelection(table);
    if (_isSelection) {
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append(body);
      _builder_2.newLineIfNotEmpty();
      _builder_2.append("workLoadItem = (IBlipBPMWorkloadModifiableItem)getTransientDataVariable(IBlipBPMConstants.VARIABLE_TASK_START_INFO);");
      _builder_2.newLine();
      _builder_2.append("taskSummary = (BPMTaskSummary)getTransientDataVariable(IBlipBPMConstants.VARIABLE_TASK_JBPM_SUMMARY);");
      _builder_2.newLine();
      body = _builder_2.toString();
    }
    StringConcatenation _builder_3 = new StringConcatenation();
    _builder_3.append(body);
    _builder_3.newLineIfNotEmpty();
    _builder_3.append("coordinateSystem = new ArrayList<Integer>();");
    _builder_3.newLine();
    _builder_3.append("attributeLookupMap = new HashMap<>();");
    _builder_3.newLine();
    _builder_3.append("tabSheets = new HashMap<>();");
    body = _builder_3.toString();
    if (((!this.isEmbedded(table)) && (table.getTabletype().getToolbar() != null))) {
      StringConcatenation _builder_4 = new StringConcatenation();
      _builder_4.append(body);
      _builder_4.newLineIfNotEmpty();
      _builder_4.append("toolbarUUID = UUID.randomUUID().toString();");
      _builder_4.newLine();
      _builder_4.append("toolbarClass = new ");
      String _firstUpper = StringExtensions.toFirstUpper(table.getTabletype().getToolbar().getName().toString());
      _builder_4.append(_firstUpper);
      _builder_4.append("Toolbar();");
      _builder_4.newLineIfNotEmpty();
      _builder_4.append("toolbarClass.createToolbar(eclipseContext, renderingEngine, themeResourceService, toolbarUUID);");
      body = _builder_4.toString();
    }
    StringConcatenation _builder_5 = new StringConcatenation();
    _builder_5.append(body);
    _builder_5.newLineIfNotEmpty();
    _builder_5.append("initDone = true;");
    _builder_5.newLine();
    body = _builder_5.toString();
    return body;
  }
  
  public boolean isTask(final Table table) {
    return ((this.getDataMartTable(table) instanceof TableDatamart) && 
      (((TableDatamart) this.getDataMartTable(table)).getDatamartRef().getSource() instanceof DatamartTask));
  }
  
  public boolean isCube(final Table table) {
    return ((this.getDataMartTable(table) instanceof TableDatamart) && 
      (((TableDatamart) this.getDataMartTable(table)).getDatamartRef().getSource() instanceof DatamartCube));
  }
  
  public boolean isGrid(final Table table) {
    TableOption _tabletype = table.getTabletype();
    return (_tabletype instanceof TableGrid);
  }
  
  public boolean isTable(final Table table) {
    TableOption _tabletype = table.getTabletype();
    return (_tabletype instanceof TableTable);
  }
  
  public boolean isSelectIdOnly(final Table table) {
    return ((table.getTabletype() instanceof TableTable) && ((TableTable) table.getTabletype()).isSelectIdOnly());
  }
  
  public boolean isEmbedded(final Table table) {
    return table.getTabletype().isEmbedded();
  }
  
  public boolean isSelection(final Table table) {
    TableOption _tabletype = table.getTabletype();
    return (_tabletype instanceof TableSelection);
  }
  
  public CharSequence renderAllowed() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("if (getSelectById()) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("for (DatamartFilter filter : datamartInstance.getFilters()) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("if(filter.isOptional()) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return true;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("if(!filter.getSql().isEmpty()) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return true;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("} ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return false;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("return true;");
    return _builder;
  }
  
  public String dataRefresh(final Table table) {
    String body = "";
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(body);
    _builder.newLineIfNotEmpty();
    _builder.append("try {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// if no current ui found");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if (UI.getCurrent() == null) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("for(CellSetFilterTable table:tables) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("UI.setCurrent(table.getUI());");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("break;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if(dataSourceContainer== null || !dataSourceContainer.updateContainer(getCellSet(true))) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("log.debug(\"cannot update container - full refresh\");");
    _builder.newLine();
    {
      boolean _isEmbedded = this.isEmbedded(table);
      if (_isEmbedded) {
        _builder.append("\t\t");
        _builder.append("createComponents();");
        _builder.newLine();
      } else {
        _builder.append("\t\t");
        _builder.append("renderData();");
        _builder.newLine();
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("for(CellSetFilterTable table:tables) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("table.sanitizeSelection();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("} catch (Exception e) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("StringWriter sw = new StringWriter();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("e.printStackTrace(new PrintWriter(sw));");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("String exceptionDetails = sw.toString();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("log.error(\"table data refresh:{}\", exceptionDetails);");
    _builder.newLine();
    _builder.append("}");
    body = _builder.toString();
    return body;
  }
  
  /**
   * <p>build the data components.</p>
   */
  public String createComponents(final Table table, final String clsName) {
    String body = "";
    TableDatamart _dataMartTable = this.getDataMartTable(table);
    boolean _tripleNotEquals = (_dataMartTable != null);
    if (_tripleNotEquals) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("if (renderAllowed()){");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// get the results");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("final DerivedCellSet cellSet = getCellSet(true);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("if (cellSet == null) {");
      _builder.newLine();
      _builder.append("\t\t");
      {
        boolean _isEmbedded = this.isEmbedded(table);
        boolean _not = (!_isEmbedded);
        if (_not) {
          _builder.append("promptSecurityMessage(dslMetadataService.translate(user.getLocale().toLanguageTag(), \"securityMessage\"), layoutManager.getDataArea());");
        }
      }
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("return;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("} else {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("layoutManager.getDataArea().removeAllComponents();");
      _builder.newLine();
      {
        boolean _isSelection = this.isSelection(table);
        if (_isSelection) {
          _builder.append("\t\t");
          _builder.append("for(CellSetFilterTable table:tables) {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t");
          _builder.append("if(table instanceof CheckboxSelectionCellSetFilterTable) {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t");
          _builder.append("((CheckboxSelectionCellSetFilterTable)table).removeTableRowCheckListener(this);");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("}");
          _builder.newLine();
        }
      }
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("getCoordinateSystem().clear();");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("tables.clear();");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// generate a new result component");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("if (cellSet != null) {");
      _builder.newLine();
      {
        if (((!this.isTask(table)) && (!this.isCube(table)))) {
          _builder.append("\t\t");
          _builder.append("if(moreToLoad) {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t");
          _builder.append("Runnable backgroundFetch = new Runnable() {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t");
          _builder.append("public void run() {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t    ");
          _builder.append("operativeDtoContainer = null;");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t    ");
          _builder.append("final DerivedCellSet cellSetAll = datamartInstance.getResults(false, getTaskOperativeDtoClass(), getTaskInitialOperativeDtos());");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t    ");
          _builder.append("if\t(cellSet != null) {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t    \t");
          _builder.append("operativeDtoContainer = cellSet.getOperativeDtoContainer();");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t    ");
          _builder.append("}");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t\t");
          _builder.append("for(CellSetFilterTable table:tables) {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t\t    ");
          _builder.append("dataSourceContainer = new CellSetIndexedContainer(eclipseContext, attributeLookupMap, cellSetAll, coordinateSystem, false);");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t\t\t");
          _builder.append("UI.getCurrent().access(new Runnable() {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t\t\t\t");
          _builder.append("@Override");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t\t\t\t");
          _builder.append("public void run() {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t\t\t\t    ");
          _builder.append("table.setContainerDataSource(dataSourceContainer);");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t\t\t\t    ");
          _builder.append("initTable(table, cellSetAll);");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t\t\t\t    ");
          _builder.append("filterGenerator.addFilterChangeListener(");
          _builder.append(clsName, "\t\t\t\t\t\t\t    ");
          _builder.append(".this);");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t");
          _builder.append("\t\t\t\t\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t\t\t");
          _builder.append("});");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t");
          _builder.append("};");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t");
          _builder.append("new Thread(backgroundFetch).start();");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("\t");
          _builder.append("moreToLoad = false;");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("} else {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("    ");
          _builder.append("filterGenerator.addFilterChangeListener(");
          _builder.append(clsName, "\t\t    ");
          _builder.append(".this);");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t");
          _builder.append("}");
          _builder.newLine();
        }
      }
      _builder.append("\t\t");
      _builder.append("// create a multidimensional coordinate system against the cellSet");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("for\t(int axis = 0; axis < cellSet.getAxes().size(); axis++) {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("getCoordinateSystem().add(0);");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("// remove any previous component");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("if\t(dataComponent != null) {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("layoutManager.getDataArea().removeComponent(dataComponent);");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("dataComponent = null;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("if (cellSet.getAxes().size() < 2) {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("log.error(\"at least 2 axes from referenced datamart ");
      String _name = this.getDataMartTable(table).getDatamartRef().getName();
      _builder.append(_name, "\t\t\t");
      _builder.append(" are needed to render ");
      String _name_1 = table.getName();
      _builder.append(_name_1, "\t\t\t");
      _builder.append("\");");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("} else {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("dataComponent = createTabSheet(cellSet, cellSet.getAxes().size());");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("if\t(dataComponent != null) {");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("dataComponent.setSizeFull();");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("dataComponent.setId(\"dataComponent\");");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("layoutManager.getDataArea().addComponent(dataComponent);");
      _builder.newLine();
      {
        boolean _isSelection_1 = this.isSelection(table);
        if (_isSelection_1) {
          _builder.append("\t\t\t\t");
          _builder.append("selectItemsFromWorkloadDto();");
          _builder.newLine();
        }
      }
      _builder.append("\t\t\t\t");
      _builder.append("layoutManager.getDataArea().setExpandRatio(dataComponent, 1);");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      String _selectAlways = this.getSelectAlways(table);
      _builder.append(_selectAlways, "\t\t\t\t");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("} ");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("else {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("log.error(\"referenced datamart ");
      String _name_2 = this.getDataMartTable(table).getDatamartRef().getName();
      _builder.append(_name_2, "\t\t");
      _builder.append(" generates no results\");");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      body = _builder.toString();
    } else {
      boolean _isGrid = this.isGrid(table);
      if (_isGrid) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append(body);
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("// generate a new result component");
        _builder_1.newLine();
        _builder_1.append("// remove any previous component");
        _builder_1.newLine();
        _builder_1.append("if\t(dataComponent != null) {");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("layoutManager.getDataArea().removeComponent(dataComponent);");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append("dataComponent = null;");
        _builder_1.newLine();
        _builder_1.append("}");
        _builder_1.newLine();
        _builder_1.append("dataComponent = createDtoGrid();");
        _builder_1.newLine();
        _builder_1.append("dataComponent.setSizeFull();");
        _builder_1.newLine();
        _builder_1.append("dataComponent.setId(\"dataComponent\");");
        _builder_1.newLine();
        _builder_1.append("layoutManager.getDataArea().addComponent(dataComponent);");
        _builder_1.newLine();
        _builder_1.append("layoutManager.getDataArea().setExpandRatio(dataComponent, 1);");
        _builder_1.newLine();
        body = _builder_1.toString();
      }
    }
    return body;
  }
  
  public String getSelectAlways(final Table table) {
    String body = "";
    if ((this.isTable(table) && ((TableTable) table.getTabletype()).isSelectalways())) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(body);
      _builder.newLineIfNotEmpty();
      _builder.append("UI.getCurrent().access(new Runnable() {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("@Override");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("public void run() {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("for(CellSetFilterTable table:tables) {");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("if (table.getItemIds().size()>0) {");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("table.select(table.getItemIds().iterator().next());");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("});");
      body = _builder.toString();
    }
    return body;
  }
  
  /**
   * <p>if more than 2 axes (dimensions) are found in the cellSet, build recursive tabsheets as long as the remaining axes are more than 2.</p>
   */
  public String createTabSheet(final Table table) {
    String body = "";
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(body);
    _builder.newLineIfNotEmpty();
    _builder.append("// either create a recursive tabsheet or a table");
    _builder.newLine();
    _builder.append("Component component = null;");
    _builder.newLine();
    _builder.append("if\t(axisNo == 2) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("component = createTable(cellSet);");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("else {\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("Integer axis = axisNo-1;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("TabSheet tabsheet = new TabSheet();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("tabsheet.setSizeFull();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("tabsheet.addSelectedTabChangeListener(new SelectedTabChangeListener() {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("public void selectedTabChange(SelectedTabChangeEvent event) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if(tabSheets.containsKey(event.getTabSheet())) {");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("getCoordinateSystem().clear();");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("getCoordinateSystem().addAll(getCoordinates(event.getTabSheet()));");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("private Collection<? extends Integer> getCoordinates(TabSheet tabsheet) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if(!(tabsheet.getTab(tabsheet.getSelectedTab()) instanceof TabSheet)) {");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("int index = tabsheet.getTabPosition(tabsheet.getTab(tabsheet.getSelectedTab()));");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("return tabSheets.get(tabsheet).get(index);");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return getCoordinates((TabSheet)tabsheet.getTab(tabsheet.getSelectedTab()));");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("});");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("tabSheets.put(tabsheet, new ArrayList<>());");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("DerivedAxis tabAxis = cellSet.getAxes().get(axis);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// create a tab page for all tab axis members");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("int tabNo = 0;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("for\t(DerivedPosition column : tabAxis.getPositions()) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("// create the title for the axis");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("String title = null;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("for (DerivedMember member : column.getMembers()) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if\t(title == null) {");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("title = dslMetadataService.translate(user.getLocale().toLanguageTag(), member.getCaption());");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("else {");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("title += \" / \"+dslMetadataService.translate(user.getLocale().toLanguageTag(), member.getCaption());");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("// position the data to this coordinate");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("getCoordinateSystem().set(axis, tabNo);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("tabSheets.get(tabsheet).add(new ArrayList<Integer>(getCoordinateSystem()));");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("component = createTabSheet(cellSet, axis);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("// set the caption");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("if (component != null) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("component.setCaption(title);");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("tabsheet.addComponent(component);");
    _builder.newLine();
    _builder.append("    \t\t");
    _builder.append("if (component instanceof CellSetFilterTable) {");
    _builder.newLine();
    _builder.append("\t    \t\t");
    _builder.append("tabsheet.addComponent(((CellSetFilterTable)component));");
    _builder.newLine();
    _builder.append("    \t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("tabNo++;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("component = tabsheet;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("return component;");
    body = _builder.toString();
    return body;
  }
  
  /**
   * <p>build the table component.</p>
   */
  public String createTable(final Table table) {
    String _xblockexpression = null;
    {
      String body = "";
      String constructTable = "final CellSetFilterTable table = new CellSetFilterTable();";
      boolean multiSelect = false;
      boolean _isSelection = this.isSelection(table);
      if (_isSelection) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("final CheckboxSelectionCellSetFilterTable table = new CheckboxSelectionCellSetFilterTable();");
        _builder.newLine();
        _builder.append("table.addTableRowCheckListener(this);");
        _builder.newLine();
        _builder.append("selectTable = table;");
        _builder.newLine();
        constructTable = _builder.toString();
        TableOption _tabletype = table.getTabletype();
        multiSelect = ((TableSelection) _tabletype).isMultiSelection();
      }
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(body);
      _builder_1.append(constructTable);
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("VerticalLayout tableLayout = new VerticalLayout();");
      _builder_1.newLine();
      {
        if ((this.isEmbedded(table) && this.isTask(table))) {
          _builder_1.append("taskButtons = new ArrayList<Button>();");
          _builder_1.newLine();
          _builder_1.append("HorizontalLayout tools = new HorizontalLayout();");
          _builder_1.newLine();
          _builder_1.append("tools.addStyleName(\"os-embedded-toolbar\");");
          _builder_1.newLine();
          _builder_1.append("tools.setSpacing(false);");
          _builder_1.newLine();
          _builder_1.newLine();
          _builder_1.append("Button b = new Button(null, new Button.ClickListener() {");
          _builder_1.newLine();
          _builder_1.append("    ");
          _builder_1.append("public void buttonClick(ClickEvent event) {");
          _builder_1.newLine();
          _builder_1.append("    \t");
          _builder_1.append("taskClient.operations(dataSourceContainer.getTaskId((int)table.getValue()), user, null, BPMOperation.Start);");
          _builder_1.newLine();
          _builder_1.append("    \t");
          _builder_1.append("MPerspective perspective = eclipseContext.get(MPerspective.class);");
          _builder_1.newLine();
          _builder_1.append("\t\t");
          _builder_1.append("EventDispatcherEvent evnt = new EventDispatcherEvent(perspective, EventDispatcherCommand.CLOSE, \"UserMenu\", \"");
          QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(table);
          _builder_1.append(_fullyQualifiedName, "\t\t");
          _builder_1.append("\");");
          _builder_1.newLineIfNotEmpty();
          _builder_1.append("\t\t");
          _builder_1.append("eventDispatcher.sendEvent(evnt);");
          _builder_1.newLine();
          _builder_1.append("    ");
          _builder_1.append("}");
          _builder_1.newLine();
          _builder_1.append("});");
          _builder_1.newLine();
          _builder_1.append("b.setIcon(themeResourceService.getThemeResource(\"task_action_resume\", ThemeResourceType.ICON));");
          _builder_1.newLine();
          _builder_1.append("b.setDescription(null);");
          _builder_1.newLine();
          _builder_1.append("b.setEnabled(false);");
          _builder_1.newLine();
          _builder_1.append("b.addStyleName(\"v-button-small\");");
          _builder_1.newLine();
          _builder_1.append("tools.addComponent(b);");
          _builder_1.newLine();
          _builder_1.append("taskButtons.add(b);");
          _builder_1.newLine();
          _builder_1.append("tableLayout.addComponent(tools);");
          _builder_1.newLine();
        }
      }
      _builder_1.append("table.setImmediate(true);");
      _builder_1.newLine();
      _builder_1.append("table.setMultiSelect(");
      _builder_1.append(multiSelect);
      _builder_1.append(");");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("table.setColumnCollapsingAllowed(true);");
      _builder_1.newLine();
      _builder_1.append("table.setColumnReorderingAllowed(true);");
      _builder_1.newLine();
      _builder_1.append("table.setId(\"table\");");
      _builder_1.newLine();
      {
        boolean _isTable = this.isTable(table);
        if (_isTable) {
          _builder_1.append("table.setRowHeaderMode(RowHeaderMode.");
          {
            TableOption _tabletype_1 = table.getTabletype();
            RowHeaderMode _headerMode = ((TableTable) _tabletype_1).getHeaderMode();
            boolean _tripleNotEquals = (_headerMode != null);
            if (_tripleNotEquals) {
              TableOption _tabletype_2 = table.getTabletype();
              String _name = ((TableTable) _tabletype_2).getHeaderMode().name();
              _builder_1.append(_name);
            } else {
              _builder_1.append("HIDDEN");
            }
          }
          _builder_1.append(");");
          _builder_1.newLineIfNotEmpty();
        }
      }
      _builder_1.append("table.setFilterDecorator(tableFilterDecorator);");
      _builder_1.newLine();
      _builder_1.append("table.setFilterBarVisible(");
      boolean _booleanValue = Boolean.valueOf(table.getTabletype().isFiltering()).booleanValue();
      _builder_1.append(_booleanValue);
      _builder_1.append(");");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("table.setSelectable(true);");
      _builder_1.newLine();
      _builder_1.append("table.setSizeFull();");
      _builder_1.newLine();
      _builder_1.append("tableLayout.addComponent(table);");
      _builder_1.newLine();
      _builder_1.append("tables.add(table);");
      _builder_1.newLine();
      _builder_1.append("initTable(table, cellSet);");
      _builder_1.newLine();
      _builder_1.append("return tableLayout;");
      _xblockexpression = body = _builder_1.toString();
    }
    return _xblockexpression;
  }
  
  /**
   * <p>build the table component.</p>
   */
  public String initTable(final Table table) {
    String body = "";
    TableAxis category_axis = null;
    TableAxis data_axis = null;
    HashMap<String, IdAttribute> idMap = CollectionLiterals.<String, IdAttribute>newHashMap();
    HashMap<String, FormatAttribute> formatMap = CollectionLiterals.<String, FormatAttribute>newHashMap();
    HashMap<String, Map<Double, Map<String, String>>> intervalStyleMap = CollectionLiterals.<String, Map<Double, Map<String, String>>>newHashMap();
    HashMap<String, Map<Double, Map<String, String>>> intervalIconMap = CollectionLiterals.<String, Map<Double, Map<String, String>>>newHashMap();
    HashMap<String, Map<Double, Map<String, String>>> intervalTooltipMap = CollectionLiterals.<String, Map<Double, Map<String, String>>>newHashMap();
    HashMap<String, Map<Double, Map<String, String>>> numberLookupStyleMap = CollectionLiterals.<String, Map<Double, Map<String, String>>>newHashMap();
    HashMap<String, Map<Double, Map<String, String>>> numberLookupIconMap = CollectionLiterals.<String, Map<Double, Map<String, String>>>newHashMap();
    HashMap<String, Map<Double, Map<String, String>>> numberLookupTooltipMap = CollectionLiterals.<String, Map<Double, Map<String, String>>>newHashMap();
    HashMap<String, Map<Integer, Map<String, String>>> intIntervalStyleMap = CollectionLiterals.<String, Map<Integer, Map<String, String>>>newHashMap();
    HashMap<String, Map<Integer, Map<String, String>>> intIntervalIconMap = CollectionLiterals.<String, Map<Integer, Map<String, String>>>newHashMap();
    HashMap<String, Map<Integer, Map<String, String>>> intIntervalTooltipMap = CollectionLiterals.<String, Map<Integer, Map<String, String>>>newHashMap();
    HashMap<String, Map<Integer, Map<String, String>>> intLookupStyleMap = CollectionLiterals.<String, Map<Integer, Map<String, String>>>newHashMap();
    HashMap<String, Map<Integer, Map<String, String>>> intLookupIconMap = CollectionLiterals.<String, Map<Integer, Map<String, String>>>newHashMap();
    HashMap<String, Map<Integer, Map<String, String>>> intLookupTooltipMap = CollectionLiterals.<String, Map<Integer, Map<String, String>>>newHashMap();
    HashMap<String, Map<String, Map<String, String>>> stringLookupStyleMap = CollectionLiterals.<String, Map<String, Map<String, String>>>newHashMap();
    HashMap<String, Map<String, Map<String, String>>> stringLookupIconMap = CollectionLiterals.<String, Map<String, Map<String, String>>>newHashMap();
    HashMap<String, Map<String, Map<String, String>>> stringLookupTooltipMap = CollectionLiterals.<String, Map<String, Map<String, String>>>newHashMap();
    HashMap<String, Map<Double, Map<String, String>>> dateLookupStyleMap = CollectionLiterals.<String, Map<Double, Map<String, String>>>newHashMap();
    HashMap<String, Map<Double, Map<String, String>>> dateLookupIconMap = CollectionLiterals.<String, Map<Double, Map<String, String>>>newHashMap();
    HashMap<String, Map<Double, Map<String, String>>> dateLookupTooltipMap = CollectionLiterals.<String, Map<Double, Map<String, String>>>newHashMap();
    HashMap<String, String> blobLookupMap = CollectionLiterals.<String, String>newHashMap();
    HashMap<String, String> enumLookupMap = CollectionLiterals.<String, String>newHashMap();
    String rowStyle = "";
    EList<TableElement> _elements = this.getDataMartTable(table).getElements();
    for (final TableElement element : _elements) {
      if ((element instanceof TableAxis)) {
        TableAxis axis = ((TableAxis) element);
        boolean _equals = axis.getAxis().getName().equals("COLUMNS");
        if (_equals) {
          data_axis = axis;
        }
        boolean _equals_1 = axis.getAxis().getName().equals("ROWS");
        if (_equals_1) {
          category_axis = axis;
        }
      }
    }
    if ((category_axis == null)) {
      category_axis = TableDSLFactory.eINSTANCE.createTableAxis();
      category_axis.setAxis(AxisEnum.ROWS);
      this.getDataMartTable(table).getElements().add(category_axis);
    }
    if ((data_axis == null)) {
      data_axis = TableDSLFactory.eINSTANCE.createTableAxis();
      data_axis.setAxis(AxisEnum.COLUMNS);
      this.getDataMartTable(table).getElements().add(data_axis);
    }
    this.initBlobLookup(table, blobLookupMap);
    this.initEnumLookup(table, enumLookupMap);
    this.initIdMap(table, idMap);
    boolean _isHasDetails = category_axis.isHasDetails();
    if (_isHasDetails) {
      this.initPropertyLookups(category_axis, formatMap, intervalStyleMap, intervalIconMap, intervalTooltipMap, numberLookupStyleMap, numberLookupIconMap, numberLookupTooltipMap, intIntervalStyleMap, intIntervalIconMap, intIntervalTooltipMap, intLookupStyleMap, intLookupIconMap, intLookupTooltipMap, stringLookupStyleMap, stringLookupIconMap, stringLookupTooltipMap, dateLookupStyleMap, dateLookupIconMap, dateLookupTooltipMap);
    }
    boolean _isHasDetails_1 = data_axis.isHasDetails();
    if (_isHasDetails_1) {
      this.initPropertyLookups(data_axis, formatMap, intervalStyleMap, intervalIconMap, intervalTooltipMap, numberLookupStyleMap, numberLookupIconMap, numberLookupTooltipMap, intIntervalStyleMap, intIntervalIconMap, intIntervalTooltipMap, intLookupStyleMap, intLookupIconMap, intLookupTooltipMap, stringLookupStyleMap, stringLookupIconMap, stringLookupTooltipMap, dateLookupStyleMap, dateLookupIconMap, dateLookupTooltipMap);
    }
    if ((((((((intervalStyleMap.keySet().size() + numberLookupStyleMap.keySet().size()) + stringLookupStyleMap.keySet().size()) + intLookupStyleMap.keySet().size()) + intIntervalStyleMap.keySet().size()) + 
      dateLookupStyleMap.keySet().size()) > 0) || category_axis.isHasRowHeight())) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(body);
      _builder.append("// add styles to page for the axis\'s custom styles");
      _builder.newLineIfNotEmpty();
      _builder.append("Styles styles = Page.getCurrent().getStyles();");
      _builder.newLine();
      body = _builder.toString();
    }
    boolean _isHasRowHeight = category_axis.isHasRowHeight();
    if (_isHasRowHeight) {
      this.customformatCounter = (this.customformatCounter + 1);
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("rowcustomformat");
      _builder_1.append(this.customformatCounter);
      rowStyle = _builder_1.toString();
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append(body);
      _builder_2.append("styles.add(\".v-table-row-");
      _builder_2.append(rowStyle);
      _builder_2.append(" {height: ");
      String _rowHeight = category_axis.getRowHeight();
      _builder_2.append(_rowHeight);
      _builder_2.append("px;}\");");
      _builder_2.newLineIfNotEmpty();
      body = _builder_2.toString();
    }
    StringConcatenation _builder_3 = new StringConcatenation();
    _builder_3.append(body);
    String _createStyles = this.createStyles(intervalStyleMap, numberLookupStyleMap, intIntervalStyleMap, intLookupStyleMap, stringLookupStyleMap, dateLookupStyleMap);
    _builder_3.append(_createStyles);
    _builder_3.append(" ");
    body = _builder_3.toString();
    StringConcatenation _builder_4 = new StringConcatenation();
    _builder_4.append(body);
    String _generatePropertyLookups = this.generatePropertyLookups(idMap, formatMap, intervalStyleMap, intervalIconMap, intervalTooltipMap, numberLookupStyleMap, numberLookupIconMap, numberLookupTooltipMap, intIntervalStyleMap, intIntervalIconMap, intIntervalTooltipMap, intLookupStyleMap, intLookupIconMap, intLookupTooltipMap, stringLookupStyleMap, stringLookupIconMap, stringLookupTooltipMap, dateLookupStyleMap, dateLookupIconMap, dateLookupTooltipMap, blobLookupMap, enumLookupMap);
    _builder_4.append(_generatePropertyLookups);
    body = _builder_4.toString();
    StringConcatenation _builder_5 = new StringConcatenation();
    _builder_5.append(body);
    _builder_5.append("// DO NOT MOVE THESE LINES!!!!! The data source container creation has to be before the user properties readings");
    _builder_5.newLineIfNotEmpty();
    _builder_5.append("dataSourceContainer = new CellSetIndexedContainer(eclipseContext, attributeLookupMap, cellSet, coordinateSystem, ");
    boolean _isDatamartCrossJoined = this.isDatamartCrossJoined(table);
    _builder_5.append(_isDatamartCrossJoined);
    _builder_5.append(");");
    _builder_5.newLineIfNotEmpty();
    _builder_5.append("table.setContainerDataSource(dataSourceContainer);");
    _builder_5.newLine();
    _builder_5.append("dataSourceContainer.addExtras(table);");
    _builder_5.newLine();
    _builder_5.append("tableSettingsByUserProperties(table);");
    _builder_5.newLine();
    body = _builder_5.toString();
    StringConcatenation _builder_6 = new StringConcatenation();
    _builder_6.append(body);
    _builder_6.newLineIfNotEmpty();
    String _sendEvent = this.sendEvent(table);
    _builder_6.append(_sendEvent);
    _builder_6.newLineIfNotEmpty();
    CharSequence _columnCollapsedListener = this.columnCollapsedListener();
    _builder_6.append(_columnCollapsedListener);
    _builder_6.newLineIfNotEmpty();
    CharSequence _columnReorderListener = this.columnReorderListener();
    _builder_6.append(_columnReorderListener);
    _builder_6.newLineIfNotEmpty();
    CharSequence _columnResizeListener = this.columnResizeListener();
    _builder_6.append(_columnResizeListener);
    _builder_6.newLineIfNotEmpty();
    body = _builder_6.toString();
    boolean _isHasRowHeight_1 = category_axis.isHasRowHeight();
    if (_isHasRowHeight_1) {
      StringConcatenation _builder_7 = new StringConcatenation();
      _builder_7.append(body);
      _builder_7.append("dataSourceContainer.setRowStyle(\"");
      _builder_7.append(rowStyle);
      _builder_7.append("\");");
      _builder_7.newLineIfNotEmpty();
      body = _builder_7.toString();
    }
    boolean _isTable = this.isTable(table);
    if (_isTable) {
      TableDatamart _dataMartTable = this.getDataMartTable(table);
      if ((_dataMartTable instanceof TableDatamart)) {
        TableDatamart _dataMartTable_1 = this.getDataMartTable(table);
        EList<TableElement> _elements_1 = ((TableDatamart) _dataMartTable_1).getElements();
        for (final TableElement element_1 : _elements_1) {
          if ((element_1 instanceof TableAxis)) {
            TablePreorder _preOrder = ((TableAxis) element_1).getPreOrder();
            boolean _tripleNotEquals = (_preOrder != null);
            if (_tripleNotEquals) {
              StringConcatenation _builder_8 = new StringConcatenation();
              _builder_8.append(body);
              String _preOrder_1 = this.preOrder(((TableAxis) element_1).getPreOrder(), ((TableAxis) element_1));
              _builder_8.append(_preOrder_1);
              body = _builder_8.toString();
            }
          }
        }
      }
    }
    boolean _isSelection = this.isSelection(table);
    if (_isSelection) {
      StringConcatenation _builder_9 = new StringConcatenation();
      _builder_9.append(body);
      _builder_9.newLineIfNotEmpty();
      _builder_9.append("selectItemsFromWorkloadDto();");
      _builder_9.newLine();
      body = _builder_9.toString();
    }
    return body;
  }
  
  public boolean isDatamartCrossJoined(final Table table) {
    TableDatamart _dataMartTable = this.getDataMartTable(table);
    return this.datamartInferrer.isCrossJoined(((TableDatamart) _dataMartTable).getDatamartRef());
  }
  
  public String modifyOddStyle(final String style) {
    int start = style.indexOf("background-color:");
    int end = 0;
    if ((start >= 0)) {
      int pos = style.indexOf("(", start);
      if ((pos >= 0)) {
        String newrgb = "";
        end = style.indexOf(")", pos);
        String rgb = style.substring((pos + 1), end);
        String[] colors = rgb.split(",");
        for (final String color : colors) {
          {
            int colorValue = Integer.parseInt(color);
            int diff = 15;
            if ((diff > colorValue)) {
              diff = colorValue;
            }
            colorValue = (colorValue - diff);
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(newrgb);
            {
              int _length = newrgb.length();
              boolean _greaterThan = (_length > 0);
              if (_greaterThan) {
                _builder.append(",");
              }
            }
            String _string = Integer.valueOf(colorValue).toString();
            _builder.append(_string);
            newrgb = _builder.toString();
          }
        }
        StringConcatenation _builder = new StringConcatenation();
        String _substring = style.substring(0, start);
        _builder.append(_substring);
        _builder.append("background-color: rgb(");
        _builder.append(newrgb);
        _builder.append(")");
        String _substring_1 = style.substring((end + 1));
        _builder.append(_substring_1);
        return _builder.toString();
      }
    }
    return style;
  }
  
  /**
   * <p>collected styles from the model are applied to code. Styles aside from the CSS are created at runtime.</p>
   */
  public String createStyles(final HashMap<String, Map<Double, Map<String, String>>> intervalStyleMap, final HashMap<String, Map<Double, Map<String, String>>> numberLookupStyleMap, final HashMap<String, Map<Integer, Map<String, String>>> intIntervalStyleMap, final HashMap<String, Map<Integer, Map<String, String>>> intLookupStyleMap, final HashMap<String, Map<String, Map<String, String>>> stringLookupStyleMap, final HashMap<String, Map<Double, Map<String, String>>> dateLookupStyleMap) {
    String stylePrefix = ".v-table-cell-content-";
    String body = "";
    Set<String> _keySet = intervalStyleMap.keySet();
    for (final String format : _keySet) {
      Set<Double> _keySet_1 = intervalStyleMap.get(format).keySet();
      for (final Double interval : _keySet_1) {
        Set<String> _keySet_2 = intervalStyleMap.get(format).get(interval).keySet();
        for (final String style : _keySet_2) {
          {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(body);
            _builder.append("styles.add(\"");
            _builder.append(stylePrefix);
            _builder.append(style);
            _builder.append(" {");
            String _get = intervalStyleMap.get(format).get(interval).get(style);
            _builder.append(_get);
            _builder.append("}\");");
            _builder.newLineIfNotEmpty();
            body = _builder.toString();
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append(body);
            _builder_1.append("styles.add(\"");
            _builder_1.append(stylePrefix);
            _builder_1.append(style);
            _builder_1.append("-odd {");
            String _modifyOddStyle = this.modifyOddStyle(
              intervalStyleMap.get(format).get(interval).get(style));
            _builder_1.append(_modifyOddStyle);
            _builder_1.append("}\");");
            _builder_1.newLineIfNotEmpty();
            body = _builder_1.toString();
          }
        }
      }
    }
    Set<String> _keySet_3 = numberLookupStyleMap.keySet();
    for (final String format_1 : _keySet_3) {
      Set<Double> _keySet_4 = numberLookupStyleMap.get(format_1).keySet();
      for (final Double interval_1 : _keySet_4) {
        Set<String> _keySet_5 = numberLookupStyleMap.get(format_1).get(interval_1).keySet();
        for (final String style_1 : _keySet_5) {
          {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(body);
            _builder.append("styles.add(\"");
            _builder.append(stylePrefix);
            _builder.append(style_1);
            _builder.append(" {");
            String _get = numberLookupStyleMap.get(format_1).get(interval_1).get(style_1);
            _builder.append(_get);
            _builder.append("}\");");
            _builder.newLineIfNotEmpty();
            body = _builder.toString();
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append(body);
            _builder_1.append("styles.add(\"");
            _builder_1.append(stylePrefix);
            _builder_1.append(style_1);
            _builder_1.append("-odd {");
            String _modifyOddStyle = this.modifyOddStyle(
              numberLookupStyleMap.get(format_1).get(interval_1).get(style_1));
            _builder_1.append(_modifyOddStyle);
            _builder_1.append("}\");");
            _builder_1.newLineIfNotEmpty();
            body = _builder_1.toString();
          }
        }
      }
    }
    Set<String> _keySet_6 = intIntervalStyleMap.keySet();
    for (final String format_2 : _keySet_6) {
      Set<Integer> _keySet_7 = intIntervalStyleMap.get(format_2).keySet();
      for (final Integer interval_2 : _keySet_7) {
        Set<String> _keySet_8 = intIntervalStyleMap.get(format_2).get(interval_2).keySet();
        for (final String style_2 : _keySet_8) {
          {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(body);
            _builder.append("styles.add(\"");
            _builder.append(stylePrefix);
            _builder.append(style_2);
            _builder.append(" {");
            String _get = intIntervalStyleMap.get(format_2).get(interval_2).get(style_2);
            _builder.append(_get);
            _builder.append("}\");");
            _builder.newLineIfNotEmpty();
            body = _builder.toString();
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append(body);
            _builder_1.append("styles.add(\"");
            _builder_1.append(stylePrefix);
            _builder_1.append(style_2);
            _builder_1.append("-odd {");
            String _modifyOddStyle = this.modifyOddStyle(
              intIntervalStyleMap.get(format_2).get(interval_2).get(style_2));
            _builder_1.append(_modifyOddStyle);
            _builder_1.append("}\");");
            _builder_1.newLineIfNotEmpty();
            body = _builder_1.toString();
          }
        }
      }
    }
    Set<String> _keySet_9 = intLookupStyleMap.keySet();
    for (final String format_3 : _keySet_9) {
      Set<Integer> _keySet_10 = intLookupStyleMap.get(format_3).keySet();
      for (final Integer interval_3 : _keySet_10) {
        Set<String> _keySet_11 = intLookupStyleMap.get(format_3).get(interval_3).keySet();
        for (final String style_3 : _keySet_11) {
          {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(body);
            _builder.append("styles.add(\"");
            _builder.append(stylePrefix);
            _builder.append(style_3);
            _builder.append(" {");
            String _get = intLookupStyleMap.get(format_3).get(interval_3).get(style_3);
            _builder.append(_get);
            _builder.append("}\");");
            _builder.newLineIfNotEmpty();
            body = _builder.toString();
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append(body);
            _builder_1.append("styles.add(\"");
            _builder_1.append(stylePrefix);
            _builder_1.append(style_3);
            _builder_1.append("-odd {");
            String _modifyOddStyle = this.modifyOddStyle(
              intLookupStyleMap.get(format_3).get(interval_3).get(style_3));
            _builder_1.append(_modifyOddStyle);
            _builder_1.append("}\");");
            _builder_1.newLineIfNotEmpty();
            body = _builder_1.toString();
          }
        }
      }
    }
    Set<String> _keySet_12 = stringLookupStyleMap.keySet();
    for (final String format_4 : _keySet_12) {
      Set<String> _keySet_13 = stringLookupStyleMap.get(format_4).keySet();
      for (final String interval_4 : _keySet_13) {
        Set<String> _keySet_14 = stringLookupStyleMap.get(format_4).get(interval_4).keySet();
        for (final String style_4 : _keySet_14) {
          {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(body);
            _builder.append("styles.add(\"");
            _builder.append(stylePrefix);
            _builder.append(style_4);
            _builder.append(" {");
            String _get = stringLookupStyleMap.get(format_4).get(interval_4).get(style_4);
            _builder.append(_get);
            _builder.append("}\");");
            _builder.newLineIfNotEmpty();
            body = _builder.toString();
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append(body);
            _builder_1.append("styles.add(\"");
            _builder_1.append(stylePrefix);
            _builder_1.append(style_4);
            _builder_1.append("-odd {");
            String _modifyOddStyle = this.modifyOddStyle(
              stringLookupStyleMap.get(format_4).get(interval_4).get(style_4));
            _builder_1.append(_modifyOddStyle);
            _builder_1.append("}\");");
            _builder_1.newLineIfNotEmpty();
            body = _builder_1.toString();
          }
        }
      }
    }
    Set<String> _keySet_15 = dateLookupStyleMap.keySet();
    for (final String format_5 : _keySet_15) {
      Set<Double> _keySet_16 = dateLookupStyleMap.get(format_5).keySet();
      for (final Double interval_5 : _keySet_16) {
        Set<String> _keySet_17 = dateLookupStyleMap.get(format_5).get(interval_5).keySet();
        for (final String style_5 : _keySet_17) {
          {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(body);
            _builder.append("styles.add(\"");
            _builder.append(stylePrefix);
            _builder.append(style_5);
            _builder.append(" {");
            String _get = dateLookupStyleMap.get(format_5).get(interval_5).get(style_5);
            _builder.append(_get);
            _builder.append("}\");");
            _builder.newLineIfNotEmpty();
            body = _builder.toString();
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append(body);
            _builder_1.append("styles.add(\"");
            _builder_1.append(stylePrefix);
            _builder_1.append(style_5);
            _builder_1.append("-odd {");
            String _modifyOddStyle = this.modifyOddStyle(
              dateLookupStyleMap.get(format_5).get(interval_5).get(style_5));
            _builder_1.append(_modifyOddStyle);
            _builder_1.append("}\");");
            _builder_1.newLineIfNotEmpty();
            body = _builder_1.toString();
          }
        }
      }
    }
    return body;
  }
  
  /**
   * <p>In order to customize the cellGenerator, methods must be called inferred from the model.</p>
   */
  public String generatePropertyLookups(final HashMap<String, IdAttribute> idMap, final HashMap<String, FormatAttribute> formatMap, final HashMap<String, Map<Double, Map<String, String>>> intervalStyleMap, final HashMap<String, Map<Double, Map<String, String>>> intervalIconMap, final HashMap<String, Map<Double, Map<String, String>>> intervalTooltipMap, final HashMap<String, Map<Double, Map<String, String>>> numberLookupStyleMap, final HashMap<String, Map<Double, Map<String, String>>> numberLookupIconMap, final HashMap<String, Map<Double, Map<String, String>>> numberLookupTooltipMap, final HashMap<String, Map<Integer, Map<String, String>>> intIntervalStyleMap, final HashMap<String, Map<Integer, Map<String, String>>> intIntervalIconMap, final HashMap<String, Map<Integer, Map<String, String>>> intIntervalTooltipMap, final HashMap<String, Map<Integer, Map<String, String>>> intLookupStyleMap, final HashMap<String, Map<Integer, Map<String, String>>> intLookupIconMap, final HashMap<String, Map<Integer, Map<String, String>>> intLookupTooltipMap, final HashMap<String, Map<String, Map<String, String>>> stringLookupStyleMap, final HashMap<String, Map<String, Map<String, String>>> stringLookupIconMap, final HashMap<String, Map<String, Map<String, String>>> stringLookupTooltipMap, final HashMap<String, Map<Double, Map<String, String>>> dateLookupStyleMap, final HashMap<String, Map<Double, Map<String, String>>> dateLookupIconMap, final HashMap<String, Map<Double, Map<String, String>>> dateLookupTooltipMap, final HashMap<String, String> blobLookupMap, final HashMap<String, String> enumLookupMap) {
    HashSet<String> attributes = CollectionLiterals.<String>newHashSet();
    attributes.addAll(formatMap.keySet());
    attributes.addAll(intervalStyleMap.keySet());
    attributes.addAll(intervalIconMap.keySet());
    attributes.addAll(intervalTooltipMap.keySet());
    attributes.addAll(numberLookupStyleMap.keySet());
    attributes.addAll(numberLookupIconMap.keySet());
    attributes.addAll(numberLookupTooltipMap.keySet());
    attributes.addAll(intIntervalStyleMap.keySet());
    attributes.addAll(intIntervalIconMap.keySet());
    attributes.addAll(intIntervalTooltipMap.keySet());
    attributes.addAll(intLookupStyleMap.keySet());
    attributes.addAll(intLookupIconMap.keySet());
    attributes.addAll(intLookupTooltipMap.keySet());
    attributes.addAll(stringLookupStyleMap.keySet());
    attributes.addAll(stringLookupIconMap.keySet());
    attributes.addAll(stringLookupTooltipMap.keySet());
    attributes.addAll(dateLookupStyleMap.keySet());
    attributes.addAll(dateLookupIconMap.keySet());
    attributes.addAll(dateLookupTooltipMap.keySet());
    attributes.addAll(blobLookupMap.keySet());
    attributes.addAll(enumLookupMap.keySet());
    attributes.addAll(idMap.keySet());
    String body = "";
    int _size = attributes.size();
    boolean _greaterThan = (_size > 0);
    if (_greaterThan) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(body);
      _builder.append("// add attributeLookups for column and row attribute supplements");
      _builder.newLineIfNotEmpty();
      body = _builder.toString();
    }
    for (final String attribute : attributes) {
      {
        boolean hadAttribute = false;
        if ((attribute != null)) {
          hadAttribute = true;
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append(body);
          _builder_1.append("attributeLookupMap.put(\"");
          String _upperCase = attribute.toString().toUpperCase();
          _builder_1.append(_upperCase);
          _builder_1.append("\", new PropertyLookup(themeResourceService, dslMetadataService, blobService, user.getLocale())");
          body = _builder_1.toString();
          boolean _containsKey = idMap.containsKey(attribute);
          if (_containsKey) {
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append(body);
            _builder_2.newLineIfNotEmpty();
            _builder_2.append("\t");
            _builder_2.append(".setCollapseColumn(");
            String _string = Boolean.valueOf(Boolean.valueOf(idMap.get(attribute).collapsed).booleanValue()).toString();
            _builder_2.append(_string, "\t");
            _builder_2.append(")");
            _builder_2.newLineIfNotEmpty();
            body = _builder_2.toString();
          }
          boolean _containsKey_1 = formatMap.containsKey(attribute);
          if (_containsKey_1) {
            if ((formatMap.get(attribute).formatter != null)) {
              StringConcatenation _builder_3 = new StringConcatenation();
              _builder_3.append(body);
              _builder_3.newLineIfNotEmpty();
              _builder_3.append("\t");
              _builder_3.append(".setFormat(\"");
              _builder_3.append(formatMap.get(attribute).formatter, "\t");
              _builder_3.append("\")");
              _builder_3.newLineIfNotEmpty();
              body = _builder_3.toString();
            }
            if ((formatMap.get(attribute).tooltipPattern != null)) {
              StringConcatenation _builder_4 = new StringConcatenation();
              _builder_4.append(body);
              _builder_4.newLineIfNotEmpty();
              _builder_4.append("\t");
              _builder_4.append(".setTooltipPattern(\"");
              _builder_4.append(formatMap.get(attribute).tooltipPattern, "\t");
              _builder_4.append("\")");
              _builder_4.newLineIfNotEmpty();
              body = _builder_4.toString();
            }
            int _size_1 = intervalIconMap.size();
            boolean _greaterThan_1 = (_size_1 > 0);
            if (_greaterThan_1) {
              StringConcatenation _builder_5 = new StringConcatenation();
              _builder_5.append(body);
              _builder_5.newLineIfNotEmpty();
              _builder_5.append("\t");
              _builder_5.append(".setHideLabel(");
              String _string_1 = Boolean.valueOf(Boolean.valueOf(formatMap.get(attribute).hideLabelInterval).booleanValue()).toString();
              _builder_5.append(_string_1, "\t");
              _builder_5.append(")");
              _builder_5.newLineIfNotEmpty();
              body = _builder_5.toString();
            } else {
              int _size_2 = numberLookupIconMap.size();
              int _size_3 = intLookupIconMap.size();
              int _plus = (_size_2 + _size_3);
              int _size_4 = stringLookupIconMap.size();
              int _plus_1 = (_plus + _size_4);
              int _size_5 = dateLookupIconMap.size();
              int _plus_2 = (_plus_1 + _size_5);
              boolean _greaterThan_2 = (_plus_2 > 0);
              if (_greaterThan_2) {
                StringConcatenation _builder_6 = new StringConcatenation();
                _builder_6.append(body);
                _builder_6.newLineIfNotEmpty();
                _builder_6.append("\t");
                _builder_6.append(".setHideLabel(");
                String _string_2 = Boolean.valueOf(Boolean.valueOf(formatMap.get(attribute).hideLabelLookup).booleanValue()).toString();
                _builder_6.append(_string_2, "\t");
                _builder_6.append(")");
                _builder_6.newLineIfNotEmpty();
                body = _builder_6.toString();
              } else {
                StringConcatenation _builder_7 = new StringConcatenation();
                _builder_7.append(body);
                _builder_7.newLineIfNotEmpty();
                _builder_7.append("\t");
                _builder_7.append(".setHideLabel(");
                String _string_3 = Boolean.valueOf(Boolean.valueOf(formatMap.get(attribute).hideImageLabel).booleanValue()).toString();
                _builder_7.append(_string_3, "\t");
                _builder_7.append(")");
                _builder_7.newLineIfNotEmpty();
                body = _builder_7.toString();
              }
            }
            StringConcatenation _builder_8 = new StringConcatenation();
            _builder_8.append(body);
            _builder_8.newLineIfNotEmpty();
            _builder_8.append("\t");
            _builder_8.append(".setCollapseColumn(");
            String _string_4 = Boolean.valueOf(Boolean.valueOf(formatMap.get(attribute).collapsed).booleanValue()).toString();
            _builder_8.append(_string_4, "\t");
            _builder_8.append(")");
            _builder_8.newLineIfNotEmpty();
            body = _builder_8.toString();
            if (formatMap.get(attribute).hasImage) {
              StringConcatenation _builder_9 = new StringConcatenation();
              _builder_9.append(body);
              _builder_9.newLineIfNotEmpty();
              _builder_9.append("\t");
              _builder_9.append(".setImageService(\"");
              _builder_9.append(formatMap.get(attribute).imagePath, "\t");
              _builder_9.append("\", ");
              String _string_5 = Boolean.valueOf(Boolean.valueOf(formatMap.get(attribute).hasImageParameter).booleanValue()).toString();
              _builder_9.append(_string_5, "\t");
              _builder_9.append(")");
              _builder_9.newLineIfNotEmpty();
              body = _builder_9.toString();
              if (formatMap.get(attribute).hasImageResize) {
                StringConcatenation _builder_10 = new StringConcatenation();
                _builder_10.append(body);
                _builder_10.newLineIfNotEmpty();
                _builder_10.append("\t");
                _builder_10.append(".setResizeString(\"");
                _builder_10.append(formatMap.get(attribute).imageResizeString, "\t");
                _builder_10.append("\")");
                body = _builder_10.toString();
              }
            }
            if ((formatMap.get(attribute).iconName != null)) {
              StringConcatenation _builder_11 = new StringConcatenation();
              _builder_11.append(body);
              _builder_11.newLineIfNotEmpty();
              _builder_11.append("\t");
              _builder_11.append(".setColumnIcon(\"");
              _builder_11.append(formatMap.get(attribute).iconName, "\t");
              _builder_11.append("\")");
              _builder_11.newLineIfNotEmpty();
              body = _builder_11.toString();
            }
          }
          boolean _containsKey_2 = intervalStyleMap.containsKey(attribute);
          if (_containsKey_2) {
            Set<Double> _keySet = intervalStyleMap.get(attribute).keySet();
            for (final Double interval : _keySet) {
              Set<String> _keySet_1 = intervalStyleMap.get(attribute).get(interval).keySet();
              for (final String style : _keySet_1) {
                StringConcatenation _builder_12 = new StringConcatenation();
                _builder_12.append(body);
                _builder_12.newLineIfNotEmpty();
                _builder_12.append("\t");
                _builder_12.append(".addStyleInterval(");
                {
                  boolean _startsWith = style.startsWith("date");
                  if (_startsWith) {
                    _builder_12.append("ExtendedDate.add(new Date(), (long)");
                    _builder_12.append(((interval).doubleValue() * (-1)), "\t");
                    _builder_12.append(", TimeUnit.DAYS)");
                  } else {
                    _builder_12.append(interval, "\t");
                  }
                }
                _builder_12.append(", \"");
                _builder_12.append(style, "\t");
                _builder_12.append("\")");
                _builder_12.newLineIfNotEmpty();
                body = _builder_12.toString();
              }
            }
          }
          boolean _containsKey_3 = intervalIconMap.containsKey(attribute);
          if (_containsKey_3) {
            Set<Double> _keySet_2 = intervalIconMap.get(attribute).keySet();
            for (final Double interval_1 : _keySet_2) {
              Set<String> _keySet_3 = intervalIconMap.get(attribute).get(interval_1).keySet();
              for (final String style_1 : _keySet_3) {
                StringConcatenation _builder_13 = new StringConcatenation();
                _builder_13.append(body);
                _builder_13.newLineIfNotEmpty();
                _builder_13.append("\t");
                _builder_13.append(".addResourceInterval(");
                {
                  boolean _startsWith_1 = style_1.startsWith("date");
                  if (_startsWith_1) {
                    _builder_13.append("ExtendedDate.add(new Date(), (long)");
                    _builder_13.append(((interval_1).doubleValue() * (-1)), "\t");
                    _builder_13.append(", TimeUnit.DAYS)");
                  } else {
                    _builder_13.append(interval_1, "\t");
                  }
                }
                _builder_13.append(", \"");
                String _get = intervalIconMap.get(attribute).get(interval_1).get(style_1);
                _builder_13.append(_get, "\t");
                _builder_13.append("\")");
                _builder_13.newLineIfNotEmpty();
                body = _builder_13.toString();
              }
            }
          }
          boolean _containsKey_4 = intervalTooltipMap.containsKey(attribute);
          if (_containsKey_4) {
            Set<Double> _keySet_4 = intervalTooltipMap.get(attribute).keySet();
            for (final Double interval_2 : _keySet_4) {
              Set<String> _keySet_5 = intervalTooltipMap.get(attribute).get(interval_2).keySet();
              for (final String style_2 : _keySet_5) {
                StringConcatenation _builder_14 = new StringConcatenation();
                _builder_14.append(body);
                _builder_14.newLineIfNotEmpty();
                _builder_14.append("\t");
                _builder_14.append(".addTooltipInterval(");
                {
                  boolean _startsWith_2 = style_2.startsWith("date");
                  if (_startsWith_2) {
                    _builder_14.append("ExtendedDate.add(new Date(), (long)");
                    _builder_14.append(((interval_2).doubleValue() * (-1)), "\t");
                    _builder_14.append(", TimeUnit.DAYS)");
                  } else {
                    _builder_14.append(interval_2, "\t");
                  }
                }
                _builder_14.append(", \"");
                String _get_1 = intervalTooltipMap.get(attribute).get(interval_2).get(style_2);
                _builder_14.append(_get_1, "\t");
                _builder_14.append("\")");
                _builder_14.newLineIfNotEmpty();
                body = _builder_14.toString();
              }
            }
          }
          boolean _containsKey_5 = numberLookupStyleMap.containsKey(attribute);
          if (_containsKey_5) {
            Set<Double> _keySet_6 = numberLookupStyleMap.get(attribute).keySet();
            for (final Double interval_3 : _keySet_6) {
              Set<String> _keySet_7 = numberLookupStyleMap.get(attribute).get(interval_3).keySet();
              for (final String style_3 : _keySet_7) {
                StringConcatenation _builder_15 = new StringConcatenation();
                _builder_15.append(body);
                _builder_15.newLineIfNotEmpty();
                _builder_15.append("\t");
                _builder_15.append(".addStyleLookup(");
                _builder_15.append(interval_3, "\t");
                _builder_15.append(", \"");
                _builder_15.append(style_3, "\t");
                _builder_15.append("\")");
                _builder_15.newLineIfNotEmpty();
                body = _builder_15.toString();
              }
            }
          }
          boolean _containsKey_6 = numberLookupIconMap.containsKey(attribute);
          if (_containsKey_6) {
            Set<Double> _keySet_8 = numberLookupIconMap.get(attribute).keySet();
            for (final Double interval_4 : _keySet_8) {
              Set<String> _keySet_9 = numberLookupIconMap.get(attribute).get(interval_4).keySet();
              for (final String style_4 : _keySet_9) {
                StringConcatenation _builder_16 = new StringConcatenation();
                _builder_16.append(body);
                _builder_16.newLineIfNotEmpty();
                _builder_16.append("\t");
                _builder_16.append(".addResourceLookup(");
                _builder_16.append(interval_4, "\t");
                _builder_16.append(", \"");
                String _get_2 = numberLookupIconMap.get(attribute).get(interval_4).get(style_4);
                _builder_16.append(_get_2, "\t");
                _builder_16.append("\")");
                _builder_16.newLineIfNotEmpty();
                body = _builder_16.toString();
              }
            }
          }
          boolean _containsKey_7 = numberLookupTooltipMap.containsKey(attribute);
          if (_containsKey_7) {
            Set<Double> _keySet_10 = numberLookupTooltipMap.get(attribute).keySet();
            for (final Double interval_5 : _keySet_10) {
              Set<String> _keySet_11 = numberLookupTooltipMap.get(attribute).get(interval_5).keySet();
              for (final String style_5 : _keySet_11) {
                StringConcatenation _builder_17 = new StringConcatenation();
                _builder_17.append(body);
                _builder_17.newLineIfNotEmpty();
                _builder_17.append("\t");
                _builder_17.append(".addTooltipLookup(");
                _builder_17.append(interval_5, "\t");
                _builder_17.append(", \"");
                String _get_3 = numberLookupTooltipMap.get(attribute).get(interval_5).get(style_5);
                _builder_17.append(_get_3, "\t");
                _builder_17.append("\")");
                _builder_17.newLineIfNotEmpty();
                body = _builder_17.toString();
              }
            }
          }
          boolean _containsKey_8 = intIntervalStyleMap.containsKey(attribute);
          if (_containsKey_8) {
            Set<Integer> _keySet_12 = intIntervalStyleMap.get(attribute).keySet();
            for (final Integer intInterval : _keySet_12) {
              Set<String> _keySet_13 = intIntervalStyleMap.get(attribute).get(intInterval).keySet();
              for (final String style_6 : _keySet_13) {
                StringConcatenation _builder_18 = new StringConcatenation();
                _builder_18.append(body);
                _builder_18.newLineIfNotEmpty();
                _builder_18.append("\t");
                _builder_18.append(".addStyleInterval(");
                {
                  boolean _startsWith_3 = style_6.startsWith("date");
                  if (_startsWith_3) {
                    _builder_18.append("ExtendedDate.add(new Date(), (long)");
                    _builder_18.append(((intInterval).intValue() * (-1)), "\t");
                    _builder_18.append(", TimeUnit.DAYS)");
                  } else {
                    _builder_18.append(intInterval, "\t");
                  }
                }
                _builder_18.append(", \"");
                _builder_18.append(style_6, "\t");
                _builder_18.append("\")");
                _builder_18.newLineIfNotEmpty();
                body = _builder_18.toString();
              }
            }
          }
          boolean _containsKey_9 = intIntervalIconMap.containsKey(attribute);
          if (_containsKey_9) {
            Set<Integer> _keySet_14 = intIntervalIconMap.get(attribute).keySet();
            for (final Integer intInterval_1 : _keySet_14) {
              Set<String> _keySet_15 = intIntervalIconMap.get(attribute).get(intInterval_1).keySet();
              for (final String style_7 : _keySet_15) {
                StringConcatenation _builder_19 = new StringConcatenation();
                _builder_19.append(body);
                _builder_19.newLineIfNotEmpty();
                _builder_19.append("\t");
                _builder_19.append(".addResourceInterval(");
                {
                  boolean _startsWith_4 = style_7.startsWith("date");
                  if (_startsWith_4) {
                    _builder_19.append("ExtendedDate.add(new Date(), (long)");
                    _builder_19.append(((intInterval_1).intValue() * (-1)), "\t");
                    _builder_19.append(", TimeUnit.DAYS)");
                  } else {
                    _builder_19.append(intInterval_1, "\t");
                  }
                }
                _builder_19.append(", \"");
                String _get_4 = intIntervalIconMap.get(attribute).get(intInterval_1).get(style_7);
                _builder_19.append(_get_4, "\t");
                _builder_19.append("\")");
                _builder_19.newLineIfNotEmpty();
                body = _builder_19.toString();
              }
            }
          }
          boolean _containsKey_10 = intIntervalTooltipMap.containsKey(attribute);
          if (_containsKey_10) {
            Set<Integer> _keySet_16 = intIntervalTooltipMap.get(attribute).keySet();
            for (final Integer intInterval_2 : _keySet_16) {
              Set<String> _keySet_17 = intIntervalTooltipMap.get(attribute).get(intInterval_2).keySet();
              for (final String style_8 : _keySet_17) {
                StringConcatenation _builder_20 = new StringConcatenation();
                _builder_20.append(body);
                _builder_20.newLineIfNotEmpty();
                _builder_20.append("\t");
                _builder_20.append(".addTooltipInterval(");
                {
                  boolean _startsWith_5 = style_8.startsWith("date");
                  if (_startsWith_5) {
                    _builder_20.append("ExtendedDate.add(new Date(), (long)");
                    _builder_20.append(((intInterval_2).intValue() * (-1)), "\t");
                    _builder_20.append(", TimeUnit.DAYS)");
                  } else {
                    _builder_20.append(intInterval_2, "\t");
                  }
                }
                _builder_20.append(", \"");
                String _get_5 = intIntervalTooltipMap.get(attribute).get(intInterval_2).get(style_8);
                _builder_20.append(_get_5, "\t");
                _builder_20.append("\")");
                _builder_20.newLineIfNotEmpty();
                body = _builder_20.toString();
              }
            }
          }
          boolean _containsKey_11 = intLookupStyleMap.containsKey(attribute);
          if (_containsKey_11) {
            Set<Integer> _keySet_18 = intLookupStyleMap.get(attribute).keySet();
            for (final Integer interval_6 : _keySet_18) {
              Set<String> _keySet_19 = intLookupStyleMap.get(attribute).get(interval_6).keySet();
              for (final String style_9 : _keySet_19) {
                StringConcatenation _builder_21 = new StringConcatenation();
                _builder_21.append(body);
                _builder_21.newLineIfNotEmpty();
                _builder_21.append("\t");
                _builder_21.append(".addStyleLookup(");
                _builder_21.append(interval_6, "\t");
                _builder_21.append(", \"");
                _builder_21.append(style_9, "\t");
                _builder_21.append("\")");
                _builder_21.newLineIfNotEmpty();
                body = _builder_21.toString();
              }
            }
          }
          boolean _containsKey_12 = intLookupIconMap.containsKey(attribute);
          if (_containsKey_12) {
            Set<Integer> _keySet_20 = intLookupIconMap.get(attribute).keySet();
            for (final Integer interval_7 : _keySet_20) {
              Set<String> _keySet_21 = intLookupIconMap.get(attribute).get(interval_7).keySet();
              for (final String style_10 : _keySet_21) {
                StringConcatenation _builder_22 = new StringConcatenation();
                _builder_22.append(body);
                _builder_22.newLineIfNotEmpty();
                _builder_22.append("\t");
                _builder_22.append(".addResourceLookup(");
                _builder_22.append(interval_7, "\t");
                _builder_22.append(", \"");
                String _get_6 = intLookupIconMap.get(attribute).get(interval_7).get(style_10);
                _builder_22.append(_get_6, "\t");
                _builder_22.append("\")");
                _builder_22.newLineIfNotEmpty();
                body = _builder_22.toString();
              }
            }
          }
          boolean _containsKey_13 = intLookupTooltipMap.containsKey(attribute);
          if (_containsKey_13) {
            Set<Integer> _keySet_22 = intLookupTooltipMap.get(attribute).keySet();
            for (final Integer interval_8 : _keySet_22) {
              Set<String> _keySet_23 = intLookupTooltipMap.get(attribute).get(interval_8).keySet();
              for (final String style_11 : _keySet_23) {
                StringConcatenation _builder_23 = new StringConcatenation();
                _builder_23.append(body);
                _builder_23.newLineIfNotEmpty();
                _builder_23.append("\t");
                _builder_23.append(".addTooltipLookup(");
                _builder_23.append(interval_8, "\t");
                _builder_23.append(", \"");
                String _get_7 = intLookupTooltipMap.get(attribute).get(interval_8).get(style_11);
                _builder_23.append(_get_7, "\t");
                _builder_23.append("\")");
                _builder_23.newLineIfNotEmpty();
                body = _builder_23.toString();
              }
            }
          }
          boolean _containsKey_14 = stringLookupStyleMap.containsKey(attribute);
          if (_containsKey_14) {
            Set<String> _keySet_24 = stringLookupStyleMap.get(attribute).keySet();
            for (final String interval_9 : _keySet_24) {
              Set<String> _keySet_25 = stringLookupStyleMap.get(attribute).get(interval_9).keySet();
              for (final String style_12 : _keySet_25) {
                StringConcatenation _builder_24 = new StringConcatenation();
                _builder_24.append(body);
                _builder_24.newLineIfNotEmpty();
                _builder_24.append("\t");
                _builder_24.append(".addStyleLookup(\"");
                _builder_24.append(interval_9, "\t");
                _builder_24.append("\", \"");
                _builder_24.append(style_12, "\t");
                _builder_24.append("\")");
                _builder_24.newLineIfNotEmpty();
                body = _builder_24.toString();
              }
            }
          }
          boolean _containsKey_15 = stringLookupIconMap.containsKey(attribute);
          if (_containsKey_15) {
            Set<String> _keySet_26 = stringLookupIconMap.get(attribute).keySet();
            for (final String interval_10 : _keySet_26) {
              Set<String> _keySet_27 = stringLookupIconMap.get(attribute).get(interval_10).keySet();
              for (final String style_13 : _keySet_27) {
                StringConcatenation _builder_25 = new StringConcatenation();
                _builder_25.append(body);
                _builder_25.newLineIfNotEmpty();
                _builder_25.append("\t");
                _builder_25.append(".addResourceLookup(\"");
                _builder_25.append(interval_10, "\t");
                _builder_25.append("\", \"");
                String _get_8 = stringLookupIconMap.get(attribute).get(interval_10).get(style_13);
                _builder_25.append(_get_8, "\t");
                _builder_25.append("\")");
                _builder_25.newLineIfNotEmpty();
                body = _builder_25.toString();
              }
            }
          }
          boolean _containsKey_16 = stringLookupTooltipMap.containsKey(attribute);
          if (_containsKey_16) {
            Set<String> _keySet_28 = stringLookupTooltipMap.get(attribute).keySet();
            for (final String interval_11 : _keySet_28) {
              Set<String> _keySet_29 = stringLookupTooltipMap.get(attribute).get(interval_11).keySet();
              for (final String style_14 : _keySet_29) {
                StringConcatenation _builder_26 = new StringConcatenation();
                _builder_26.append(body);
                _builder_26.newLineIfNotEmpty();
                _builder_26.append("\t");
                _builder_26.append(".addTooltipLookup(\"");
                _builder_26.append(interval_11, "\t");
                _builder_26.append("\", \"");
                String _get_9 = stringLookupTooltipMap.get(attribute).get(interval_11).get(style_14);
                _builder_26.append(_get_9, "\t");
                _builder_26.append("\")");
                _builder_26.newLineIfNotEmpty();
                body = _builder_26.toString();
              }
            }
          }
          boolean _containsKey_17 = dateLookupStyleMap.containsKey(attribute);
          if (_containsKey_17) {
            Set<Double> _keySet_30 = dateLookupStyleMap.get(attribute).keySet();
            for (final Double interval_12 : _keySet_30) {
              Set<String> _keySet_31 = dateLookupStyleMap.get(attribute).get(interval_12).keySet();
              for (final String style_15 : _keySet_31) {
                StringConcatenation _builder_27 = new StringConcatenation();
                _builder_27.append(body);
                _builder_27.newLineIfNotEmpty();
                _builder_27.append("\t");
                _builder_27.append(".addStyleLookup(ExtendedDate.add(new Date(),");
                _builder_27.append(((interval_12).doubleValue() * (-1)), "\t");
                _builder_27.append(",TimeUnit.DAYS), \"");
                _builder_27.append(style_15, "\t");
                _builder_27.append("\")");
                _builder_27.newLineIfNotEmpty();
                body = _builder_27.toString();
              }
            }
          }
          boolean _containsKey_18 = dateLookupIconMap.containsKey(attribute);
          if (_containsKey_18) {
            Set<Double> _keySet_32 = dateLookupIconMap.get(attribute).keySet();
            for (final Double interval_13 : _keySet_32) {
              Set<String> _keySet_33 = dateLookupIconMap.get(attribute).get(interval_13).keySet();
              for (final String style_16 : _keySet_33) {
                StringConcatenation _builder_28 = new StringConcatenation();
                _builder_28.append(body);
                _builder_28.newLineIfNotEmpty();
                _builder_28.append("\t");
                _builder_28.append(".addResourceLookup(ExtendedDate.add(new Date(),");
                _builder_28.append(((interval_13).doubleValue() * (-1)), "\t");
                _builder_28.append(",TimeUnit.DAYS), \"");
                String _get_10 = dateLookupIconMap.get(attribute).get(interval_13).get(style_16);
                _builder_28.append(_get_10, "\t");
                _builder_28.append("\")");
                _builder_28.newLineIfNotEmpty();
                body = _builder_28.toString();
              }
            }
          }
          boolean _containsKey_19 = dateLookupTooltipMap.containsKey(attribute);
          if (_containsKey_19) {
            Set<Double> _keySet_34 = dateLookupTooltipMap.get(attribute).keySet();
            for (final Double interval_14 : _keySet_34) {
              Set<String> _keySet_35 = dateLookupTooltipMap.get(attribute).get(interval_14).keySet();
              for (final String style_17 : _keySet_35) {
                StringConcatenation _builder_29 = new StringConcatenation();
                _builder_29.append(body);
                _builder_29.newLineIfNotEmpty();
                _builder_29.append("\t");
                _builder_29.append(".addTooltipLookup(ExtendedDate.add(new Date(),");
                _builder_29.append(((interval_14).doubleValue() * (-1)), "\t");
                _builder_29.append(",TimeUnit.DAYS), \"");
                String _get_11 = dateLookupTooltipMap.get(attribute).get(interval_14).get(style_17);
                _builder_29.append(_get_11, "\t");
                _builder_29.append("\")");
                _builder_29.newLineIfNotEmpty();
                body = _builder_29.toString();
              }
            }
          }
          boolean _containsKey_20 = blobLookupMap.containsKey(attribute);
          if (_containsKey_20) {
            StringConcatenation _builder_30 = new StringConcatenation();
            _builder_30.append(body);
            _builder_30.newLineIfNotEmpty();
            _builder_30.append("\t");
            _builder_30.append(".setBlob(true, ");
            String _get_12 = blobLookupMap.get(attribute);
            _builder_30.append(_get_12, "\t");
            _builder_30.append(")");
            _builder_30.newLineIfNotEmpty();
            body = _builder_30.toString();
          }
          boolean _containsKey_21 = enumLookupMap.containsKey(attribute);
          if (_containsKey_21) {
            StringConcatenation _builder_31 = new StringConcatenation();
            _builder_31.append(body);
            _builder_31.newLineIfNotEmpty();
            _builder_31.append("\t");
            _builder_31.append(".setEnum(true, ");
            String _get_13 = enumLookupMap.get(attribute);
            _builder_31.append(_get_13, "\t");
            _builder_31.append(".class)");
            _builder_31.newLineIfNotEmpty();
            body = _builder_31.toString();
          }
        }
        if (hadAttribute) {
          StringConcatenation _builder_32 = new StringConcatenation();
          _builder_32.append(body);
          _builder_32.append(");");
          _builder_32.newLineIfNotEmpty();
          body = _builder_32.toString();
        }
      }
    }
    return body;
  }
  
  /**
   * <p>infer the model's setting for cell configuration including blobs.</p>
   */
  public void initBlobLookup(final Table table, final Map<String, String> blobMap) {
    DatamartSource datamartSource = this.getDataMartTable(table).getDatamartRef().getSource();
    if ((datamartSource instanceof DatamartEntity)) {
      List<DatamartAttribute> _allEntityAttributes = this.datamartInferrer.allEntityAttributes(this.getDataMartTable(table).getDatamartRef());
      for (final DatamartAttribute attribute : _allEntityAttributes) {
        {
          LKeyAndValue resolution = this.getResolution(attribute.getAttributeRef());
          if ((resolution != null)) {
            StringConcatenation _builder = new StringConcatenation();
            LEntityAttribute _attributeRef = null;
            if (attribute!=null) {
              _attributeRef=attribute.getAttributeRef();
            }
            String _name = null;
            if (_attributeRef!=null) {
              _name=_attributeRef.getName();
            }
            String _replace = null;
            if (_name!=null) {
              _replace=_name.replace("^", "");
            }
            _builder.append(_replace);
            String _value = null;
            if (resolution!=null) {
              _value=resolution.getValue();
            }
            blobMap.put(_builder.toString(), _value);
          }
        }
      }
      List<DatamartAttribute> _allEntityAttributes_1 = this.datamartInferrer.allEntityAttributes(this.getDataMartTable(table).getDatamartRef());
      for (final DatamartAttribute attribute_1 : _allEntityAttributes_1) {
        {
          LKeyAndValue resolution = this.getResolution(attribute_1.getAttributeRef());
          if ((resolution != null)) {
            String _aliasedAttributeName = DatamartAttributeUtil.getAliasedAttributeName(attribute_1);
            String _value = null;
            if (resolution!=null) {
              _value=resolution.getValue();
            }
            blobMap.put(_aliasedAttributeName, _value);
          }
        }
      }
    }
  }
  
  /**
   * <p>infer the model's setting for cell configuration including enums.</p>
   */
  public void initEnumLookup(final Table table, final Map<String, String> enumMap) {
    DatamartSource datamartSource = this.getDataMartTable(table).getDatamartRef().getSource();
    if ((datamartSource instanceof DatamartEntity)) {
      List<DatamartAttribute> _allEntityAttributes = this.datamartInferrer.allEntityAttributes(this.getDataMartTable(table).getDatamartRef());
      for (final DatamartAttribute attribute : _allEntityAttributes) {
        boolean _typeIsEnum = this._modelExtensions.typeIsEnum(attribute.getAttributeRef());
        if (_typeIsEnum) {
          StringConcatenation _builder = new StringConcatenation();
          LEntityAttribute _attributeRef = null;
          if (attribute!=null) {
            _attributeRef=attribute.getAttributeRef();
          }
          String _name = null;
          if (_attributeRef!=null) {
            _name=_attributeRef.getName();
          }
          String _replace = null;
          if (_name!=null) {
            _replace=_name.replace("^", "");
          }
          _builder.append(_replace);
          StringConcatenation _builder_1 = new StringConcatenation();
          LEntityAttribute _attributeRef_1 = null;
          if (attribute!=null) {
            _attributeRef_1=attribute.getAttributeRef();
          }
          LScalarType _type = null;
          if (_attributeRef_1!=null) {
            _type=_attributeRef_1.getType();
          }
          QualifiedName _fullyQualifiedName = null;
          if (_type!=null) {
            _fullyQualifiedName=this._iQualifiedNameProvider.getFullyQualifiedName(_type);
          }
          _builder_1.append(_fullyQualifiedName);
          enumMap.put(_builder.toString(), _builder_1.toString());
        }
      }
    }
  }
  
  public LKeyAndValue getResolution(final LEntityAttribute attribute) {
    final Function1<LKeyAndValue, Boolean> _function = (LKeyAndValue it) -> {
      return Boolean.valueOf(it.getKey().toLowerCase().equals(IPropertyConstants.PROPERTY_BLOB));
    };
    boolean _exists = IterableExtensions.<LKeyAndValue>exists(attribute.getProperties(), _function);
    if (_exists) {
      final Function1<LKeyAndValue, Boolean> _function_1 = (LKeyAndValue it) -> {
        return Boolean.valueOf(it.getKey().toLowerCase().equals(IPropertyConstants.PROPERTY_BLOB));
      };
      return IterableExtensions.<LKeyAndValue>findFirst(attribute.getProperties(), _function_1);
    } else {
      LScalarType _type = attribute.getType();
      if ((_type instanceof LDataType)) {
        LScalarType _type_1 = attribute.getType();
        final Function1<LKeyAndValue, Boolean> _function_2 = (LKeyAndValue it) -> {
          return Boolean.valueOf(it.getKey().toLowerCase().equals(IPropertyConstants.PROPERTY_BLOB));
        };
        boolean _exists_1 = IterableExtensions.<LKeyAndValue>exists(((LDataType) _type_1).getProperties(), _function_2);
        if (_exists_1) {
          LScalarType _type_2 = attribute.getType();
          final Function1<LKeyAndValue, Boolean> _function_3 = (LKeyAndValue it) -> {
            return Boolean.valueOf(it.getKey().toLowerCase().equals(IPropertyConstants.PROPERTY_BLOB));
          };
          return IterableExtensions.<LKeyAndValue>findFirst(((LDataType) _type_2).getProperties(), _function_3);
        }
      }
    }
    return null;
  }
  
  /**
   * <p>infer the model's setting for cell configuration including styles and images.</p>
   */
  public void initPropertyLookups(final TableAxis axis, final HashMap<String, FormatAttribute> formatMap, final HashMap<String, Map<Double, Map<String, String>>> intervalStyleMap, final HashMap<String, Map<Double, Map<String, String>>> intervalIconMap, final HashMap<String, Map<Double, Map<String, String>>> intervalTooltipMap, final HashMap<String, Map<Double, Map<String, String>>> numberLookupStyleMap, final HashMap<String, Map<Double, Map<String, String>>> numberLookupIconMap, final HashMap<String, Map<Double, Map<String, String>>> numberLookupTooltipMap, final HashMap<String, Map<Integer, Map<String, String>>> intIntervalStyleMap, final HashMap<String, Map<Integer, Map<String, String>>> intIntervalIconMap, final HashMap<String, Map<Integer, Map<String, String>>> intIntervalTooltipMap, final HashMap<String, Map<Integer, Map<String, String>>> intLookupStyleMap, final HashMap<String, Map<Integer, Map<String, String>>> intLookupIconMap, final HashMap<String, Map<Integer, Map<String, String>>> intLookupTooltipMap, final HashMap<String, Map<String, Map<String, String>>> stringLookupStyleMap, final HashMap<String, Map<String, Map<String, String>>> stringLookupIconMap, final HashMap<String, Map<String, Map<String, String>>> stringLookupTooltipMap, final HashMap<String, Map<Double, Map<String, String>>> dateLookupStyleMap, final HashMap<String, Map<Double, Map<String, String>>> dateLookupIconMap, final HashMap<String, Map<Double, Map<String, String>>> dateLookupTooltipMap) {
    EList<TableValue> _values = axis.getValues();
    for (final TableValue value : _values) {
      {
        FormatAttribute prop = new FormatAttribute();
        TableFormatter _formatter = value.getFormatter();
        boolean _tripleNotEquals = (_formatter != null);
        if (_tripleNotEquals) {
          prop.formatter = value.getFormatter().getFormat();
        }
        TableTooltipPattern _tooltipPattern = value.getTooltipPattern();
        boolean _tripleNotEquals_1 = (_tooltipPattern != null);
        if (_tripleNotEquals_1) {
          prop.tooltipPattern = value.getTooltipPattern().getTooltipPattern();
        }
        prop.hideLabelLookup = value.isHideLabelLookup();
        prop.collapsed = value.isCollapsed();
        prop.hasImage = value.isHasImage();
        prop.iconName = value.getIconName();
        if ((value.isHasImage() && (value.getImage() != null))) {
          prop.hasImageParameter = value.getImage().isHasParameter();
          prop.imagePath = value.getImage().getImagePathPattern();
          prop.hideImageLabel = value.getImage().isHideImageLabel();
          prop.hasImageResize = value.getImage().isResize();
          prop.imageResizeString = value.getImage().getResizeString();
          boolean _isHasParameter = value.getImage().isHasParameter();
          if (_isHasParameter) {
            TableValueElement _imagePathParameter = value.getImage().getImagePathParameter();
            boolean _matched = false;
            if (_imagePathParameter instanceof TableColumn) {
              _matched=true;
              TableValueElement _imagePathParameter_1 = value.getImage().getImagePathParameter();
              prop.imagePathParameter = ((TableColumn) _imagePathParameter_1).getValueRef().getColumnRef().getLiteral();
            }
            if (!_matched) {
              if (_imagePathParameter instanceof TableMeasure) {
                _matched=true;
                TableValueElement _imagePathParameter_1 = value.getImage().getImagePathParameter();
                prop.imagePathParameter = ((TableMeasure) _imagePathParameter_1).getValueRef().getMeasureRef().getName();
              }
            }
            if (!_matched) {
              if (_imagePathParameter instanceof TableDerived) {
                _matched=true;
                TableValueElement _imagePathParameter_1 = value.getImage().getImagePathParameter();
                prop.imagePathParameter = ((TableDerived) _imagePathParameter_1).getValueRef().getDerivedRef().getName();
              }
            }
            if (!_matched) {
              if (_imagePathParameter instanceof TableHierarchy) {
                _matched=true;
                TableValueElement _imagePathParameter_1 = value.getImage().getImagePathParameter();
                prop.imagePathParameter = this.getLevelName(((TableHierarchy) _imagePathParameter_1).getValueRef());
              }
            }
            if (!_matched) {
              if (_imagePathParameter instanceof TableAttribute) {
                _matched=true;
                TableValueElement _imagePathParameter_1 = value.getImage().getImagePathParameter();
                prop.imagePathParameter = DatamartAttributeUtil.getAliasedAttributeName(((TableAttribute) _imagePathParameter_1).getValueRef());
              }
            }
            if (!_matched) {
              if (_imagePathParameter instanceof TableAggregation) {
                _matched=true;
                TableValueElement _imagePathParameter_1 = value.getImage().getImagePathParameter();
                prop.imagePathParameter = ((TableAggregation) _imagePathParameter_1).getValueRef().getAggregation().name();
              }
            }
          }
        }
        HashMap<String, String> columnValueMap = CollectionLiterals.<String, String>newHashMap();
        TableValueElement _column = value.getColumn();
        boolean _matched_1 = false;
        if (_column instanceof TableOrdinal) {
          _matched_1=true;
          String _canonicalName = TableOrdinal.class.getCanonicalName();
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("#");
          String _literal = axis.getAxis().getLiteral();
          _builder.append(_literal);
          TableValueElement _column_1 = value.getColumn();
          String _string = ((TableOrdinal) _column_1).getValueRef().toString();
          _builder.append(_string);
          columnValueMap.put(_canonicalName, _builder.toString());
          formatMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), prop);
        }
        if (!_matched_1) {
          if (_column instanceof TableAllColumns) {
            _matched_1=true;
            String _canonicalName = TableAllColumns.class.getCanonicalName();
            StringConcatenation _builder = new StringConcatenation();
            _builder.append("?");
            String _literal = axis.getAxis().getLiteral();
            _builder.append(_literal);
            columnValueMap.put(_canonicalName, _builder.toString());
            formatMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), prop);
          }
        }
        if (!_matched_1) {
          if (_column instanceof TableMeasure) {
            _matched_1=true;
            String _canonicalName = TableMeasure.class.getCanonicalName();
            StringConcatenation _builder = new StringConcatenation();
            TableValueElement _column_1 = value.getColumn();
            String _name = ((TableMeasure) _column_1).getValueRef().getMeasureRef().getName();
            _builder.append(_name);
            columnValueMap.put(_canonicalName, _builder.toString());
            formatMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), prop);
          }
        }
        if (!_matched_1) {
          if (_column instanceof TableDerived) {
            _matched_1=true;
            String _canonicalName = TableDerived.class.getCanonicalName();
            StringConcatenation _builder = new StringConcatenation();
            TableValueElement _column_1 = value.getColumn();
            String _name = ((TableDerived) _column_1).getValueRef().getDerivedRef().getName();
            _builder.append(_name);
            columnValueMap.put(_canonicalName, _builder.toString());
            formatMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), prop);
          }
        }
        if (!_matched_1) {
          if (_column instanceof TableHierarchy) {
            _matched_1=true;
            String _canonicalName = TableHierarchy.class.getCanonicalName();
            StringConcatenation _builder = new StringConcatenation();
            TableValueElement _column_1 = value.getColumn();
            String _levelName = this.getLevelName(((TableHierarchy) _column_1).getValueRef());
            _builder.append(_levelName);
            columnValueMap.put(_canonicalName, _builder.toString());
            formatMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), prop);
          }
        }
        if (!_matched_1) {
          if (_column instanceof TableAttribute) {
            _matched_1=true;
            String _canonicalName = TableAttribute.class.getCanonicalName();
            StringConcatenation _builder = new StringConcatenation();
            TableValueElement _column_1 = value.getColumn();
            String _aliasedAttributeName = DatamartAttributeUtil.getAliasedAttributeName(((TableAttribute) _column_1).getValueRef());
            _builder.append(_aliasedAttributeName);
            columnValueMap.put(_canonicalName, _builder.toString());
            formatMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), prop);
          }
        }
        if (!_matched_1) {
          if (_column instanceof TableAggregation) {
            _matched_1=true;
            String _canonicalName = TableAggregation.class.getCanonicalName();
            StringConcatenation _builder = new StringConcatenation();
            TableValueElement _column_1 = value.getColumn();
            String _name = ((TableAggregation) _column_1).getValueRef().getAggregation().name();
            _builder.append(_name);
            columnValueMap.put(_canonicalName, _builder.toString());
            formatMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), prop);
          }
        }
        if (!_matched_1) {
          if (_column instanceof TableColumn) {
            _matched_1=true;
            String _canonicalName = TableColumn.class.getCanonicalName();
            StringConcatenation _builder = new StringConcatenation();
            TableValueElement _column_1 = value.getColumn();
            String _literal = ((TableColumn) _column_1).getValueRef().getColumnRef().getLiteral();
            _builder.append(_literal);
            columnValueMap.put(_canonicalName, _builder.toString());
            formatMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), prop);
          }
        }
        DoubleComparator _doubleComparator = new DoubleComparator();
        TreeMap<Double, Map<String, String>> valueMap = CollectionLiterals.<Double, Map<String, String>>newTreeMap(_doubleComparator);
        IntegerComparator _integerComparator = new IntegerComparator();
        TreeMap<Integer, Map<String, String>> intMap = CollectionLiterals.<Integer, Map<String, String>>newTreeMap(_integerComparator);
        EList<TableInterval> _intervals = value.getIntervals();
        for (final TableInterval interval : _intervals) {
          boolean _matched_2 = false;
          if (interval instanceof TableNumberInterval) {
            _matched_2=true;
            if (((((TableNumberInterval) interval).getNumberRange() instanceof TableTextColor) || 
              (((TableNumberInterval) interval).getNumberRange() instanceof TableCellColor))) {
              double _numberIntervalValue = ((TableNumberInterval) interval).getNumberIntervalValue();
              Double key = new Double(_numberIntervalValue);
              String styleAspect = "";
              Map<String, String> styleMap = null;
              boolean _containsKey = valueMap.containsKey(key);
              if (_containsKey) {
                styleMap = valueMap.get(key);
                styleAspect = styleMap.get(((Object[])Conversions.unwrapArray(styleMap.keySet(), Object.class))[0]);
              } else {
                styleMap = CollectionLiterals.<String, String>newHashMap();
                styleAspect = "";
                this.customformatCounter = (this.customformatCounter + 1);
              }
              TableRangeElement _numberRange = ((TableNumberInterval) interval).getNumberRange();
              boolean _matched_3 = false;
              if (_numberRange instanceof TableTextColor) {
                _matched_3=true;
                StringConcatenation _builder = new StringConcatenation();
                _builder.append(styleAspect);
                _builder.append("color: ");
                TableRangeElement _numberRange_1 = ((TableNumberInterval) interval).getNumberRange();
                String _rGB = this.toRGB(((TableTextColor) _numberRange_1).getRgb());
                _builder.append(_rGB);
                _builder.append(";");
                styleAspect = _builder.toString();
              }
              if (!_matched_3) {
                if (_numberRange instanceof TableCellColor) {
                  _matched_3=true;
                  StringConcatenation _builder = new StringConcatenation();
                  _builder.append(styleAspect);
                  _builder.append("background-color: ");
                  TableRangeElement _numberRange_1 = ((TableNumberInterval) interval).getNumberRange();
                  String _rGB = this.toRGB(((TableCellColor) _numberRange_1).getRgb());
                  _builder.append(_rGB);
                  _builder.append(";");
                  styleAspect = _builder.toString();
                }
              }
              int _length = styleAspect.length();
              boolean _greaterThan = (_length > 0);
              if (_greaterThan) {
                StringConcatenation _builder = new StringConcatenation();
                _builder.append("numbercustomformat");
                _builder.append(this.customformatCounter);
                styleMap.put(_builder.toString(), styleAspect);
                valueMap.put(key, styleMap);
              }
            }
          }
          if (!_matched_2) {
            if (interval instanceof TableIntInterval) {
              _matched_2=true;
              if (((((TableIntInterval) interval).getIntRange() instanceof TableTextColor) || 
                (((TableIntInterval) interval).getIntRange() instanceof TableCellColor))) {
                int _intIntervalValue = ((TableIntInterval) interval).getIntIntervalValue();
                Integer key = new Integer(_intIntervalValue);
                String styleAspect = "";
                Map<String, String> styleMap = null;
                boolean _containsKey = valueMap.containsKey(key);
                if (_containsKey) {
                  styleMap = valueMap.get(key);
                  styleAspect = styleMap.get(((Object[])Conversions.unwrapArray(styleMap.keySet(), Object.class))[0]);
                } else {
                  styleMap = CollectionLiterals.<String, String>newHashMap();
                  styleAspect = "";
                  this.customformatCounter = (this.customformatCounter + 1);
                }
                TableRangeElement _intRange = ((TableIntInterval) interval).getIntRange();
                boolean _matched_3 = false;
                if (_intRange instanceof TableTextColor) {
                  _matched_3=true;
                  StringConcatenation _builder = new StringConcatenation();
                  _builder.append(styleAspect);
                  _builder.append("color: ");
                  TableRangeElement _intRange_1 = ((TableIntInterval) interval).getIntRange();
                  String _rGB = this.toRGB(((TableTextColor) _intRange_1).getRgb());
                  _builder.append(_rGB);
                  _builder.append(";");
                  styleAspect = _builder.toString();
                }
                if (!_matched_3) {
                  if (_intRange instanceof TableCellColor) {
                    _matched_3=true;
                    StringConcatenation _builder = new StringConcatenation();
                    _builder.append(styleAspect);
                    _builder.append("background-color: ");
                    TableRangeElement _intRange_1 = ((TableIntInterval) interval).getIntRange();
                    String _rGB = this.toRGB(((TableCellColor) _intRange_1).getRgb());
                    _builder.append(_rGB);
                    _builder.append(";");
                    styleAspect = _builder.toString();
                  }
                }
                int _length = styleAspect.length();
                boolean _greaterThan = (_length > 0);
                if (_greaterThan) {
                  StringConcatenation _builder = new StringConcatenation();
                  _builder.append("numbercustomformat");
                  _builder.append(this.customformatCounter);
                  styleMap.put(_builder.toString(), styleAspect);
                  intMap.put(key, styleMap);
                }
              }
            }
          }
          if (!_matched_2) {
            if (interval instanceof TableDateDayInterval) {
              _matched_2=true;
              if (((((TableDateDayInterval) interval).getDateRange() instanceof TableTextColor) || 
                (((TableDateDayInterval) interval).getDateRange() instanceof TableCellColor))) {
                int _dateIntervalValue = ((TableDateDayInterval) interval).getDateIntervalValue();
                Double key = new Double(_dateIntervalValue);
                String styleAspect = "";
                Map<String, String> styleMap = null;
                boolean _containsKey = valueMap.containsKey(key);
                if (_containsKey) {
                  styleMap = valueMap.get(key);
                  styleAspect = styleMap.get(((Object[])Conversions.unwrapArray(styleMap.keySet(), Object.class))[0]);
                } else {
                  styleMap = CollectionLiterals.<String, String>newHashMap();
                  styleAspect = "";
                  this.customformatCounter = (this.customformatCounter + 1);
                }
                TableRangeElement _dateRange = ((TableDateDayInterval) interval).getDateRange();
                boolean _matched_3 = false;
                if (_dateRange instanceof TableTextColor) {
                  _matched_3=true;
                  StringConcatenation _builder = new StringConcatenation();
                  _builder.append(styleAspect);
                  _builder.append("color: ");
                  TableRangeElement _dateRange_1 = ((TableDateDayInterval) interval).getDateRange();
                  String _rGB = this.toRGB(((TableTextColor) _dateRange_1).getRgb());
                  _builder.append(_rGB);
                  _builder.append(";");
                  styleAspect = _builder.toString();
                }
                if (!_matched_3) {
                  if (_dateRange instanceof TableCellColor) {
                    _matched_3=true;
                    StringConcatenation _builder = new StringConcatenation();
                    _builder.append(styleAspect);
                    _builder.append("background-color: ");
                    TableRangeElement _dateRange_1 = ((TableDateDayInterval) interval).getDateRange();
                    String _rGB = this.toRGB(((TableCellColor) _dateRange_1).getRgb());
                    _builder.append(_rGB);
                    _builder.append(";");
                    styleAspect = _builder.toString();
                  }
                }
                int _length = styleAspect.length();
                boolean _greaterThan = (_length > 0);
                if (_greaterThan) {
                  StringConcatenation _builder = new StringConcatenation();
                  _builder.append("datecustomformat");
                  _builder.append(this.customformatCounter);
                  styleMap.put(_builder.toString(), styleAspect);
                  valueMap.put(key, styleMap);
                }
              }
            }
          }
        }
        int _size = valueMap.size();
        boolean _greaterThan = (_size > 0);
        if (_greaterThan) {
          TableValueElement _column_1 = value.getColumn();
          boolean _matched_3 = false;
          if (_column_1 instanceof TableOrdinal) {
            _matched_3=true;
            intervalStyleMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), valueMap);
          }
          if (!_matched_3) {
            if (_column_1 instanceof TableAllColumns) {
              _matched_3=true;
              intervalStyleMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), valueMap);
            }
          }
          if (!_matched_3) {
            if (_column_1 instanceof TableMeasure) {
              _matched_3=true;
              intervalStyleMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), valueMap);
            }
          }
          if (!_matched_3) {
            if (_column_1 instanceof TableDerived) {
              _matched_3=true;
              intervalStyleMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), valueMap);
            }
          }
          if (!_matched_3) {
            if (_column_1 instanceof TableHierarchy) {
              _matched_3=true;
              intervalStyleMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), valueMap);
            }
          }
          if (!_matched_3) {
            if (_column_1 instanceof TableAttribute) {
              _matched_3=true;
              intervalStyleMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), valueMap);
            }
          }
          if (!_matched_3) {
            if (_column_1 instanceof TableAggregation) {
              _matched_3=true;
              intervalStyleMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), valueMap);
            }
          }
          if (!_matched_3) {
            if (_column_1 instanceof TableColumn) {
              _matched_3=true;
              intervalStyleMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), valueMap);
            }
          }
        }
        int _size_1 = intMap.size();
        boolean _greaterThan_1 = (_size_1 > 0);
        if (_greaterThan_1) {
          TableValueElement _column_2 = value.getColumn();
          boolean _matched_4 = false;
          if (_column_2 instanceof TableOrdinal) {
            _matched_4=true;
            intIntervalStyleMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), intMap);
          }
          if (!_matched_4) {
            if (_column_2 instanceof TableAllColumns) {
              _matched_4=true;
              intIntervalStyleMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), intMap);
            }
          }
          if (!_matched_4) {
            if (_column_2 instanceof TableMeasure) {
              _matched_4=true;
              intIntervalStyleMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), intMap);
            }
          }
          if (!_matched_4) {
            if (_column_2 instanceof TableDerived) {
              _matched_4=true;
              intIntervalStyleMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), intMap);
            }
          }
          if (!_matched_4) {
            if (_column_2 instanceof TableHierarchy) {
              _matched_4=true;
              intIntervalStyleMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), intMap);
            }
          }
          if (!_matched_4) {
            if (_column_2 instanceof TableAttribute) {
              _matched_4=true;
              intIntervalStyleMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), intMap);
            }
          }
          if (!_matched_4) {
            if (_column_2 instanceof TableAggregation) {
              _matched_4=true;
              intIntervalStyleMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), intMap);
            }
          }
          if (!_matched_4) {
            if (_column_2 instanceof TableColumn) {
              _matched_4=true;
              intIntervalStyleMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), intMap);
            }
          }
        }
        DoubleComparator _doubleComparator_1 = new DoubleComparator();
        TreeMap<Double, Map<String, String>> valueIconMap = CollectionLiterals.<Double, Map<String, String>>newTreeMap(_doubleComparator_1);
        IntegerComparator _integerComparator_1 = new IntegerComparator();
        TreeMap<Integer, Map<String, String>> intIconMap = CollectionLiterals.<Integer, Map<String, String>>newTreeMap(_integerComparator_1);
        EList<TableInterval> _intervals_1 = value.getIntervals();
        for (final TableInterval interval_1 : _intervals_1) {
          boolean _matched_5 = false;
          if (interval_1 instanceof TableNumberInterval) {
            _matched_5=true;
            double _numberIntervalValue = ((TableNumberInterval) interval_1).getNumberIntervalValue();
            Double key = new Double(_numberIntervalValue);
            String iconAspect = "";
            Map<String, String> iMap = CollectionLiterals.<String, String>newHashMap();
            TableRangeElement _numberRange = ((TableNumberInterval) interval_1).getNumberRange();
            boolean _matched_6 = false;
            if (_numberRange instanceof TableIcon) {
              _matched_6=true;
              StringConcatenation _builder = new StringConcatenation();
              TableRangeElement _numberRange_1 = ((TableNumberInterval) interval_1).getNumberRange();
              String _icon = ((TableIcon) _numberRange_1).getIcon();
              _builder.append(_icon);
              iconAspect = _builder.toString();
            }
            if (!_matched_6) {
              if (_numberRange instanceof TableTrend) {
                _matched_6=true;
                StringConcatenation _builder = new StringConcatenation();
                _builder.append("trend_");
                TableRangeElement _numberRange_1 = ((TableNumberInterval) interval_1).getNumberRange();
                String _literal = ((TableTrend) _numberRange_1).getIcon().getLiteral();
                _builder.append(_literal);
                iconAspect = _builder.toString();
              }
            }
            int _length = iconAspect.length();
            boolean _greaterThan_2 = (_length > 0);
            if (_greaterThan_2) {
              iMap.put("numbericon", iconAspect);
              valueIconMap.put(key, iMap);
            }
          }
          if (!_matched_5) {
            if (interval_1 instanceof TableIntInterval) {
              _matched_5=true;
              int _intIntervalValue = ((TableIntInterval) interval_1).getIntIntervalValue();
              Integer key = new Integer(_intIntervalValue);
              String iconAspect = "";
              Map<String, String> iMap = CollectionLiterals.<String, String>newHashMap();
              TableRangeElement _intRange = ((TableIntInterval) interval_1).getIntRange();
              boolean _matched_6 = false;
              if (_intRange instanceof TableIcon) {
                _matched_6=true;
                StringConcatenation _builder = new StringConcatenation();
                TableRangeElement _intRange_1 = ((TableIntInterval) interval_1).getIntRange();
                String _icon = ((TableIcon) _intRange_1).getIcon();
                _builder.append(_icon);
                iconAspect = _builder.toString();
              }
              if (!_matched_6) {
                if (_intRange instanceof TableTrend) {
                  _matched_6=true;
                  StringConcatenation _builder = new StringConcatenation();
                  _builder.append("trend_");
                  TableRangeElement _intRange_1 = ((TableIntInterval) interval_1).getIntRange();
                  String _literal = ((TableTrend) _intRange_1).getIcon().getLiteral();
                  _builder.append(_literal);
                  iconAspect = _builder.toString();
                }
              }
              int _length = iconAspect.length();
              boolean _greaterThan_2 = (_length > 0);
              if (_greaterThan_2) {
                iMap.put("numbericon", iconAspect);
                intIconMap.put(key, iMap);
              }
            }
          }
          if (!_matched_5) {
            if (interval_1 instanceof TableDateDayInterval) {
              _matched_5=true;
              int _dateIntervalValue = ((TableDateDayInterval) interval_1).getDateIntervalValue();
              Double key = new Double(_dateIntervalValue);
              String iconAspect = "";
              Map<String, String> iMap = CollectionLiterals.<String, String>newHashMap();
              TableRangeElement _dateRange = ((TableDateDayInterval) interval_1).getDateRange();
              boolean _matched_6 = false;
              if (_dateRange instanceof TableIcon) {
                _matched_6=true;
                StringConcatenation _builder = new StringConcatenation();
                TableRangeElement _dateRange_1 = ((TableDateDayInterval) interval_1).getDateRange();
                String _icon = ((TableIcon) _dateRange_1).getIcon();
                _builder.append(_icon);
                iconAspect = _builder.toString();
              }
              if (!_matched_6) {
                if (_dateRange instanceof TableTrend) {
                  _matched_6=true;
                  StringConcatenation _builder = new StringConcatenation();
                  _builder.append("trend_");
                  TableRangeElement _dateRange_1 = ((TableDateDayInterval) interval_1).getDateRange();
                  String _literal = ((TableTrend) _dateRange_1).getIcon().getLiteral();
                  _builder.append(_literal);
                  iconAspect = _builder.toString();
                }
              }
              int _length = iconAspect.length();
              boolean _greaterThan_2 = (_length > 0);
              if (_greaterThan_2) {
                iMap.put("dateicon", iconAspect);
                valueIconMap.put(key, iMap);
              }
            }
          }
        }
        int _size_2 = valueIconMap.size();
        boolean _greaterThan_2 = (_size_2 > 0);
        if (_greaterThan_2) {
          TableValueElement _column_3 = value.getColumn();
          boolean _matched_6 = false;
          if (_column_3 instanceof TableOrdinal) {
            _matched_6=true;
            intervalIconMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), valueIconMap);
          }
          if (!_matched_6) {
            if (_column_3 instanceof TableAllColumns) {
              _matched_6=true;
              intervalIconMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), valueIconMap);
            }
          }
          if (!_matched_6) {
            if (_column_3 instanceof TableMeasure) {
              _matched_6=true;
              intervalIconMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), valueIconMap);
            }
          }
          if (!_matched_6) {
            if (_column_3 instanceof TableDerived) {
              _matched_6=true;
              intervalIconMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), valueIconMap);
            }
          }
          if (!_matched_6) {
            if (_column_3 instanceof TableHierarchy) {
              _matched_6=true;
              intervalIconMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), valueIconMap);
            }
          }
          if (!_matched_6) {
            if (_column_3 instanceof TableAttribute) {
              _matched_6=true;
              intervalIconMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), valueIconMap);
            }
          }
          if (!_matched_6) {
            if (_column_3 instanceof TableAggregation) {
              _matched_6=true;
              intervalIconMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), valueIconMap);
            }
          }
          if (!_matched_6) {
            if (_column_3 instanceof TableColumn) {
              _matched_6=true;
              intervalIconMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), valueIconMap);
            }
          }
        }
        int _size_3 = intIconMap.size();
        boolean _greaterThan_3 = (_size_3 > 0);
        if (_greaterThan_3) {
          TableValueElement _column_4 = value.getColumn();
          boolean _matched_7 = false;
          if (_column_4 instanceof TableOrdinal) {
            _matched_7=true;
            intIntervalIconMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), intIconMap);
          }
          if (!_matched_7) {
            if (_column_4 instanceof TableAllColumns) {
              _matched_7=true;
              intIntervalIconMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), intIconMap);
            }
          }
          if (!_matched_7) {
            if (_column_4 instanceof TableMeasure) {
              _matched_7=true;
              intIntervalIconMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), intIconMap);
            }
          }
          if (!_matched_7) {
            if (_column_4 instanceof TableDerived) {
              _matched_7=true;
              intIntervalIconMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), intIconMap);
            }
          }
          if (!_matched_7) {
            if (_column_4 instanceof TableHierarchy) {
              _matched_7=true;
              intIntervalIconMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), intIconMap);
            }
          }
          if (!_matched_7) {
            if (_column_4 instanceof TableAttribute) {
              _matched_7=true;
              intIntervalIconMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), intIconMap);
            }
          }
          if (!_matched_7) {
            if (_column_4 instanceof TableAggregation) {
              _matched_7=true;
              intIntervalIconMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), intIconMap);
            }
          }
          if (!_matched_7) {
            if (_column_4 instanceof TableColumn) {
              _matched_7=true;
              intIntervalIconMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), intIconMap);
            }
          }
        }
        DoubleComparator _doubleComparator_2 = new DoubleComparator();
        TreeMap<Double, Map<String, String>> valueTooltipMap = CollectionLiterals.<Double, Map<String, String>>newTreeMap(_doubleComparator_2);
        IntegerComparator _integerComparator_2 = new IntegerComparator();
        TreeMap<Integer, Map<String, String>> intTooltipMap = CollectionLiterals.<Integer, Map<String, String>>newTreeMap(_integerComparator_2);
        EList<TableInterval> _intervals_2 = value.getIntervals();
        for (final TableInterval interval_2 : _intervals_2) {
          boolean _matched_8 = false;
          if (interval_2 instanceof TableNumberInterval) {
            _matched_8=true;
            double _numberIntervalValue = ((TableNumberInterval) interval_2).getNumberIntervalValue();
            Double key = new Double(_numberIntervalValue);
            String tooltipAspect = "";
            Map<String, String> tMap = CollectionLiterals.<String, String>newHashMap();
            TableRangeElement _numberRange = ((TableNumberInterval) interval_2).getNumberRange();
            boolean _matched_9 = false;
            if (_numberRange instanceof TableTooltip) {
              _matched_9=true;
              StringConcatenation _builder = new StringConcatenation();
              TableRangeElement _numberRange_1 = ((TableNumberInterval) interval_2).getNumberRange();
              String _tooltip = ((TableTooltip) _numberRange_1).getTooltip();
              _builder.append(_tooltip);
              tooltipAspect = _builder.toString();
            }
            int _length = tooltipAspect.length();
            boolean _greaterThan_4 = (_length > 0);
            if (_greaterThan_4) {
              tMap.put("numbertooltip", tooltipAspect);
              valueTooltipMap.put(key, tMap);
            }
          }
          if (!_matched_8) {
            if (interval_2 instanceof TableIntInterval) {
              _matched_8=true;
              int _intIntervalValue = ((TableIntInterval) interval_2).getIntIntervalValue();
              Integer key = new Integer(_intIntervalValue);
              String tooltipAspect = "";
              Map<String, String> tMap = CollectionLiterals.<String, String>newHashMap();
              TableRangeElement _intRange = ((TableIntInterval) interval_2).getIntRange();
              boolean _matched_9 = false;
              if (_intRange instanceof TableTooltip) {
                _matched_9=true;
                StringConcatenation _builder = new StringConcatenation();
                TableRangeElement _intRange_1 = ((TableIntInterval) interval_2).getIntRange();
                String _tooltip = ((TableTooltip) _intRange_1).getTooltip();
                _builder.append(_tooltip);
                tooltipAspect = _builder.toString();
              }
              int _length = tooltipAspect.length();
              boolean _greaterThan_4 = (_length > 0);
              if (_greaterThan_4) {
                tMap.put("numbertooltip", tooltipAspect);
                intTooltipMap.put(key, tMap);
              }
            }
          }
          if (!_matched_8) {
            if (interval_2 instanceof TableDateDayInterval) {
              _matched_8=true;
              int _dateIntervalValue = ((TableDateDayInterval) interval_2).getDateIntervalValue();
              Double key = new Double(_dateIntervalValue);
              String tooltipAspect = "";
              Map<String, String> tMap = CollectionLiterals.<String, String>newHashMap();
              TableRangeElement _dateRange = ((TableDateDayInterval) interval_2).getDateRange();
              boolean _matched_9 = false;
              if (_dateRange instanceof TableTooltip) {
                _matched_9=true;
                StringConcatenation _builder = new StringConcatenation();
                TableRangeElement _dateRange_1 = ((TableDateDayInterval) interval_2).getDateRange();
                String _tooltip = ((TableTooltip) _dateRange_1).getTooltip();
                _builder.append(_tooltip);
                tooltipAspect = _builder.toString();
              }
              int _length = tooltipAspect.length();
              boolean _greaterThan_4 = (_length > 0);
              if (_greaterThan_4) {
                tMap.put("datetooltip", tooltipAspect);
                valueTooltipMap.put(key, tMap);
              }
            }
          }
        }
        int _size_4 = valueTooltipMap.size();
        boolean _greaterThan_4 = (_size_4 > 0);
        if (_greaterThan_4) {
          TableValueElement _column_5 = value.getColumn();
          boolean _matched_9 = false;
          if (_column_5 instanceof TableOrdinal) {
            _matched_9=true;
            intervalTooltipMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), valueTooltipMap);
          }
          if (!_matched_9) {
            if (_column_5 instanceof TableAllColumns) {
              _matched_9=true;
              intervalTooltipMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), valueTooltipMap);
            }
          }
          if (!_matched_9) {
            if (_column_5 instanceof TableMeasure) {
              _matched_9=true;
              intervalTooltipMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), valueTooltipMap);
            }
          }
          if (!_matched_9) {
            if (_column_5 instanceof TableDerived) {
              _matched_9=true;
              intervalTooltipMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), valueTooltipMap);
            }
          }
          if (!_matched_9) {
            if (_column_5 instanceof TableHierarchy) {
              _matched_9=true;
              intervalTooltipMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), valueTooltipMap);
            }
          }
          if (!_matched_9) {
            if (_column_5 instanceof TableAttribute) {
              _matched_9=true;
              intervalTooltipMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), valueTooltipMap);
            }
          }
          if (!_matched_9) {
            if (_column_5 instanceof TableAggregation) {
              _matched_9=true;
              intervalTooltipMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), valueTooltipMap);
            }
          }
          if (!_matched_9) {
            if (_column_5 instanceof TableColumn) {
              _matched_9=true;
              intervalTooltipMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), valueTooltipMap);
            }
          }
        }
        int _size_5 = intTooltipMap.size();
        boolean _greaterThan_5 = (_size_5 > 0);
        if (_greaterThan_5) {
          TableValueElement _column_6 = value.getColumn();
          boolean _matched_10 = false;
          if (_column_6 instanceof TableOrdinal) {
            _matched_10=true;
            intIntervalTooltipMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), intTooltipMap);
          }
          if (!_matched_10) {
            if (_column_6 instanceof TableAllColumns) {
              _matched_10=true;
              intIntervalTooltipMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), intTooltipMap);
            }
          }
          if (!_matched_10) {
            if (_column_6 instanceof TableMeasure) {
              _matched_10=true;
              intIntervalTooltipMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), intTooltipMap);
            }
          }
          if (!_matched_10) {
            if (_column_6 instanceof TableDerived) {
              _matched_10=true;
              intIntervalTooltipMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), intTooltipMap);
            }
          }
          if (!_matched_10) {
            if (_column_6 instanceof TableHierarchy) {
              _matched_10=true;
              intIntervalTooltipMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), intTooltipMap);
            }
          }
          if (!_matched_10) {
            if (_column_6 instanceof TableAttribute) {
              _matched_10=true;
              intIntervalTooltipMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), intTooltipMap);
            }
          }
          if (!_matched_10) {
            if (_column_6 instanceof TableAggregation) {
              _matched_10=true;
              intIntervalTooltipMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), intTooltipMap);
            }
          }
          if (!_matched_10) {
            if (_column_6 instanceof TableColumn) {
              _matched_10=true;
              intIntervalTooltipMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), intTooltipMap);
            }
          }
        }
        DoubleComparator _doubleComparator_3 = new DoubleComparator();
        TreeMap<Double, Map<String, String>> numberValueLookupStyleMap = CollectionLiterals.<Double, Map<String, String>>newTreeMap(_doubleComparator_3);
        IntegerComparator _integerComparator_3 = new IntegerComparator();
        TreeMap<Integer, Map<String, String>> intValueLookupStyleMap = CollectionLiterals.<Integer, Map<String, String>>newTreeMap(_integerComparator_3);
        HashMap<String, Map<String, String>> stringValueLookupStyleMap = CollectionLiterals.<String, Map<String, String>>newHashMap();
        DoubleComparator _doubleComparator_4 = new DoubleComparator();
        TreeMap<Double, Map<String, String>> dateValueLookupStyleMap = CollectionLiterals.<Double, Map<String, String>>newTreeMap(_doubleComparator_4);
        EList<TableLookup> _lookups = value.getLookups();
        for (final TableLookup lookup : _lookups) {
          {
            if (((lookup instanceof TableNumberLookup) && ((((TableNumberLookup) lookup).getDiscrete() instanceof TableTextColor) || 
              (((TableNumberLookup) lookup).getDiscrete() instanceof TableCellColor)))) {
              double _lookupValue = ((TableNumberLookup) lookup).getLookupValue();
              Double key = new Double(_lookupValue);
              String styleAspect = "";
              Map<String, String> styleMap = null;
              boolean _containsKey = numberValueLookupStyleMap.containsKey(key);
              if (_containsKey) {
                styleMap = numberValueLookupStyleMap.get(key);
                styleAspect = styleMap.get(((Object[])Conversions.unwrapArray(styleMap.keySet(), Object.class))[0]);
              } else {
                styleMap = CollectionLiterals.<String, String>newHashMap();
                styleAspect = "";
                this.customformatCounter = (this.customformatCounter + 1);
              }
              TableRangeElement _discrete = ((TableNumberLookup) lookup).getDiscrete();
              boolean _matched_11 = false;
              if (_discrete instanceof TableTextColor) {
                _matched_11=true;
                StringConcatenation _builder = new StringConcatenation();
                _builder.append(styleAspect);
                _builder.append("color: ");
                TableRangeElement _discrete_1 = ((TableNumberLookup) lookup).getDiscrete();
                String _rGB = this.toRGB(((TableTextColor) _discrete_1).getRgb());
                _builder.append(_rGB);
                _builder.append(";");
                styleAspect = _builder.toString();
              }
              if (!_matched_11) {
                if (_discrete instanceof TableCellColor) {
                  _matched_11=true;
                  StringConcatenation _builder = new StringConcatenation();
                  _builder.append(styleAspect);
                  _builder.append("background-color: ");
                  TableRangeElement _discrete_1 = ((TableNumberLookup) lookup).getDiscrete();
                  String _rGB = this.toRGB(((TableCellColor) _discrete_1).getRgb());
                  _builder.append(_rGB);
                  _builder.append(";");
                  styleAspect = _builder.toString();
                }
              }
              int _length = styleAspect.length();
              boolean _greaterThan_6 = (_length > 0);
              if (_greaterThan_6) {
                StringConcatenation _builder = new StringConcatenation();
                _builder.append("customformat");
                _builder.append(this.customformatCounter);
                styleMap.put(_builder.toString(), styleAspect);
                numberValueLookupStyleMap.put(key, styleMap);
              }
            }
            if (((lookup instanceof TableIntLookup) && ((((TableIntLookup) lookup).getDiscrete() instanceof TableTextColor) || 
              (((TableIntLookup) lookup).getDiscrete() instanceof TableCellColor)))) {
              int _lookupValue_1 = ((TableIntLookup) lookup).getLookupValue();
              Integer key_1 = new Integer(_lookupValue_1);
              String styleAspect_1 = "";
              Map<String, String> styleMap_1 = null;
              boolean _containsKey_1 = intValueLookupStyleMap.containsKey(key_1);
              if (_containsKey_1) {
                styleMap_1 = intValueLookupStyleMap.get(key_1);
                styleAspect_1 = styleMap_1.get(((Object[])Conversions.unwrapArray(styleMap_1.keySet(), Object.class))[0]);
              } else {
                styleMap_1 = CollectionLiterals.<String, String>newHashMap();
                styleAspect_1 = "";
                this.customformatCounter = (this.customformatCounter + 1);
              }
              TableRangeElement _discrete_1 = ((TableIntLookup) lookup).getDiscrete();
              boolean _matched_12 = false;
              if (_discrete_1 instanceof TableTextColor) {
                _matched_12=true;
                StringConcatenation _builder_1 = new StringConcatenation();
                _builder_1.append(styleAspect_1);
                _builder_1.append("color: ");
                TableRangeElement _discrete_2 = ((TableIntLookup) lookup).getDiscrete();
                String _rGB = this.toRGB(((TableTextColor) _discrete_2).getRgb());
                _builder_1.append(_rGB);
                _builder_1.append(";");
                styleAspect_1 = _builder_1.toString();
              }
              if (!_matched_12) {
                if (_discrete_1 instanceof TableCellColor) {
                  _matched_12=true;
                  StringConcatenation _builder_1 = new StringConcatenation();
                  _builder_1.append(styleAspect_1);
                  _builder_1.append("background-color: ");
                  TableRangeElement _discrete_2 = ((TableIntLookup) lookup).getDiscrete();
                  String _rGB = this.toRGB(((TableCellColor) _discrete_2).getRgb());
                  _builder_1.append(_rGB);
                  _builder_1.append(";");
                  styleAspect_1 = _builder_1.toString();
                }
              }
              int _length_1 = styleAspect_1.length();
              boolean _greaterThan_7 = (_length_1 > 0);
              if (_greaterThan_7) {
                StringConcatenation _builder_1 = new StringConcatenation();
                _builder_1.append("customformat");
                _builder_1.append(this.customformatCounter);
                styleMap_1.put(_builder_1.toString(), styleAspect_1);
                intValueLookupStyleMap.put(key_1, styleMap_1);
              }
            }
            if ((lookup instanceof TableStringLookup)) {
              String _lookupValue_2 = ((TableStringLookup) lookup).getLookupValue();
              String key_2 = new String(_lookupValue_2);
              String styleAspect_2 = "";
              Map<String, String> styleMap_2 = null;
              boolean _containsKey_2 = stringValueLookupStyleMap.containsKey(key_2);
              if (_containsKey_2) {
                styleMap_2 = stringValueLookupStyleMap.get(key_2);
                styleAspect_2 = styleMap_2.get(((Object[])Conversions.unwrapArray(styleMap_2.keySet(), Object.class))[0]);
              } else {
                styleMap_2 = CollectionLiterals.<String, String>newHashMap();
                styleAspect_2 = "";
                this.customformatCounter = (this.customformatCounter + 1);
              }
              TableRangeElement _discrete_2 = ((TableStringLookup)lookup).getDiscrete();
              boolean _matched_13 = false;
              if (_discrete_2 instanceof TableTextColor) {
                _matched_13=true;
                StringConcatenation _builder_2 = new StringConcatenation();
                _builder_2.append(styleAspect_2);
                _builder_2.append("color: ");
                TableRangeElement _discrete_3 = ((TableStringLookup)lookup).getDiscrete();
                String _rGB = this.toRGB(((TableTextColor) _discrete_3).getRgb());
                _builder_2.append(_rGB);
                _builder_2.append(";");
                styleAspect_2 = _builder_2.toString();
              }
              if (!_matched_13) {
                if (_discrete_2 instanceof TableCellColor) {
                  _matched_13=true;
                  StringConcatenation _builder_2 = new StringConcatenation();
                  _builder_2.append(styleAspect_2);
                  _builder_2.append("background-color: ");
                  TableRangeElement _discrete_3 = ((TableStringLookup)lookup).getDiscrete();
                  String _rGB = this.toRGB(((TableCellColor) _discrete_3).getRgb());
                  _builder_2.append(_rGB);
                  _builder_2.append(";");
                  styleAspect_2 = _builder_2.toString();
                }
              }
              int _length_2 = styleAspect_2.length();
              boolean _greaterThan_8 = (_length_2 > 0);
              if (_greaterThan_8) {
                StringConcatenation _builder_2 = new StringConcatenation();
                _builder_2.append("customformat");
                _builder_2.append(this.customformatCounter);
                styleMap_2.put(_builder_2.toString(), styleAspect_2);
                stringValueLookupStyleMap.put(key_2, styleMap_2);
              }
            }
            if (((lookup instanceof TableDateDayLookup) && ((((TableDateDayLookup) lookup).getDiscrete() instanceof TableTextColor) || 
              (((TableDateDayLookup) lookup).getDiscrete() instanceof TableCellColor)))) {
              int _lookupValue_3 = ((TableDateDayLookup) lookup).getLookupValue();
              Double key_3 = new Double(_lookupValue_3);
              String styleAspect_3 = "";
              Map<String, String> styleMap_3 = null;
              boolean _containsKey_3 = dateValueLookupStyleMap.containsKey(key_3);
              if (_containsKey_3) {
                styleMap_3 = dateValueLookupStyleMap.get(key_3);
                styleAspect_3 = styleMap_3.get(((Object[])Conversions.unwrapArray(styleMap_3.keySet(), Object.class))[0]);
              } else {
                styleMap_3 = CollectionLiterals.<String, String>newHashMap();
                styleAspect_3 = "";
                this.customformatCounter = (this.customformatCounter + 1);
              }
              TableRangeElement _discrete_3 = ((TableDateDayLookup) lookup).getDiscrete();
              boolean _matched_14 = false;
              if (_discrete_3 instanceof TableTextColor) {
                _matched_14=true;
                StringConcatenation _builder_3 = new StringConcatenation();
                _builder_3.append(styleAspect_3);
                _builder_3.append("color: ");
                TableRangeElement _discrete_4 = ((TableDateDayLookup) lookup).getDiscrete();
                String _rGB = this.toRGB(((TableTextColor) _discrete_4).getRgb());
                _builder_3.append(_rGB);
                _builder_3.append(";");
                styleAspect_3 = _builder_3.toString();
              }
              if (!_matched_14) {
                if (_discrete_3 instanceof TableCellColor) {
                  _matched_14=true;
                  StringConcatenation _builder_3 = new StringConcatenation();
                  _builder_3.append(styleAspect_3);
                  _builder_3.append("background-color: ");
                  TableRangeElement _discrete_4 = ((TableDateDayLookup) lookup).getDiscrete();
                  String _rGB = this.toRGB(((TableCellColor) _discrete_4).getRgb());
                  _builder_3.append(_rGB);
                  _builder_3.append(";");
                  styleAspect_3 = _builder_3.toString();
                }
              }
              int _length_3 = styleAspect_3.length();
              boolean _greaterThan_9 = (_length_3 > 0);
              if (_greaterThan_9) {
                StringConcatenation _builder_3 = new StringConcatenation();
                _builder_3.append("customformat");
                _builder_3.append(this.customformatCounter);
                styleMap_3.put(_builder_3.toString(), styleAspect_3);
                dateValueLookupStyleMap.put(key_3, styleMap_3);
              }
            }
          }
        }
        int _size_6 = numberValueLookupStyleMap.size();
        boolean _greaterThan_6 = (_size_6 > 0);
        if (_greaterThan_6) {
          TableValueElement _column_7 = value.getColumn();
          boolean _matched_11 = false;
          if (_column_7 instanceof TableOrdinal) {
            _matched_11=true;
            numberLookupStyleMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), numberValueLookupStyleMap);
          }
          if (!_matched_11) {
            if (_column_7 instanceof TableAllColumns) {
              _matched_11=true;
              numberLookupStyleMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), numberValueLookupStyleMap);
            }
          }
          if (!_matched_11) {
            if (_column_7 instanceof TableMeasure) {
              _matched_11=true;
              numberLookupStyleMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), numberValueLookupStyleMap);
            }
          }
          if (!_matched_11) {
            if (_column_7 instanceof TableDerived) {
              _matched_11=true;
              numberLookupStyleMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), numberValueLookupStyleMap);
            }
          }
          if (!_matched_11) {
            if (_column_7 instanceof TableHierarchy) {
              _matched_11=true;
              numberLookupStyleMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), numberValueLookupStyleMap);
            }
          }
          if (!_matched_11) {
            if (_column_7 instanceof TableAttribute) {
              _matched_11=true;
              numberLookupStyleMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), numberValueLookupStyleMap);
            }
          }
          if (!_matched_11) {
            if (_column_7 instanceof TableAggregation) {
              _matched_11=true;
              numberLookupStyleMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), numberValueLookupStyleMap);
            }
          }
          if (!_matched_11) {
            if (_column_7 instanceof TableColumn) {
              _matched_11=true;
              numberLookupStyleMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), numberValueLookupStyleMap);
            }
          }
        }
        int _size_7 = intValueLookupStyleMap.size();
        boolean _greaterThan_7 = (_size_7 > 0);
        if (_greaterThan_7) {
          TableValueElement _column_8 = value.getColumn();
          boolean _matched_12 = false;
          if (_column_8 instanceof TableOrdinal) {
            _matched_12=true;
            intLookupStyleMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), intValueLookupStyleMap);
          }
          if (!_matched_12) {
            if (_column_8 instanceof TableAllColumns) {
              _matched_12=true;
              intLookupStyleMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), intValueLookupStyleMap);
            }
          }
          if (!_matched_12) {
            if (_column_8 instanceof TableMeasure) {
              _matched_12=true;
              intLookupStyleMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), intValueLookupStyleMap);
            }
          }
          if (!_matched_12) {
            if (_column_8 instanceof TableDerived) {
              _matched_12=true;
              intLookupStyleMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), intValueLookupStyleMap);
            }
          }
          if (!_matched_12) {
            if (_column_8 instanceof TableHierarchy) {
              _matched_12=true;
              intLookupStyleMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), intValueLookupStyleMap);
            }
          }
          if (!_matched_12) {
            if (_column_8 instanceof TableAttribute) {
              _matched_12=true;
              intLookupStyleMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), intValueLookupStyleMap);
            }
          }
          if (!_matched_12) {
            if (_column_8 instanceof TableAggregation) {
              _matched_12=true;
              intLookupStyleMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), intValueLookupStyleMap);
            }
          }
          if (!_matched_12) {
            if (_column_8 instanceof TableColumn) {
              _matched_12=true;
              intLookupStyleMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), intValueLookupStyleMap);
            }
          }
        }
        int _size_8 = stringValueLookupStyleMap.size();
        boolean _greaterThan_8 = (_size_8 > 0);
        if (_greaterThan_8) {
          TableValueElement _column_9 = value.getColumn();
          boolean _matched_13 = false;
          if (_column_9 instanceof TableOrdinal) {
            _matched_13=true;
            stringLookupStyleMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), stringValueLookupStyleMap);
          }
          if (!_matched_13) {
            if (_column_9 instanceof TableAllColumns) {
              _matched_13=true;
              stringLookupStyleMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), stringValueLookupStyleMap);
            }
          }
          if (!_matched_13) {
            if (_column_9 instanceof TableMeasure) {
              _matched_13=true;
              stringLookupStyleMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), stringValueLookupStyleMap);
            }
          }
          if (!_matched_13) {
            if (_column_9 instanceof TableDerived) {
              _matched_13=true;
              stringLookupStyleMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), stringValueLookupStyleMap);
            }
          }
          if (!_matched_13) {
            if (_column_9 instanceof TableHierarchy) {
              _matched_13=true;
              stringLookupStyleMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), stringValueLookupStyleMap);
            }
          }
          if (!_matched_13) {
            if (_column_9 instanceof TableAttribute) {
              _matched_13=true;
              stringLookupStyleMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), stringValueLookupStyleMap);
            }
          }
          if (!_matched_13) {
            if (_column_9 instanceof TableAggregation) {
              _matched_13=true;
              stringLookupStyleMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), stringValueLookupStyleMap);
            }
          }
          if (!_matched_13) {
            if (_column_9 instanceof TableColumn) {
              _matched_13=true;
              stringLookupStyleMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), stringValueLookupStyleMap);
            }
          }
        }
        int _size_9 = dateValueLookupStyleMap.size();
        boolean _greaterThan_9 = (_size_9 > 0);
        if (_greaterThan_9) {
          TableValueElement _column_10 = value.getColumn();
          boolean _matched_14 = false;
          if (_column_10 instanceof TableOrdinal) {
            _matched_14=true;
            dateLookupStyleMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), dateValueLookupStyleMap);
          }
          if (!_matched_14) {
            if (_column_10 instanceof TableAllColumns) {
              _matched_14=true;
              dateLookupStyleMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), dateValueLookupStyleMap);
            }
          }
          if (!_matched_14) {
            if (_column_10 instanceof TableMeasure) {
              _matched_14=true;
              dateLookupStyleMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), dateValueLookupStyleMap);
            }
          }
          if (!_matched_14) {
            if (_column_10 instanceof TableDerived) {
              _matched_14=true;
              dateLookupStyleMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), dateValueLookupStyleMap);
            }
          }
          if (!_matched_14) {
            if (_column_10 instanceof TableHierarchy) {
              _matched_14=true;
              dateLookupStyleMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), dateValueLookupStyleMap);
            }
          }
          if (!_matched_14) {
            if (_column_10 instanceof TableAttribute) {
              _matched_14=true;
              dateLookupStyleMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), dateValueLookupStyleMap);
            }
          }
          if (!_matched_14) {
            if (_column_10 instanceof TableAggregation) {
              _matched_14=true;
              dateLookupStyleMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), dateValueLookupStyleMap);
            }
          }
          if (!_matched_14) {
            if (_column_10 instanceof TableColumn) {
              _matched_14=true;
              dateLookupStyleMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), dateValueLookupStyleMap);
            }
          }
        }
        DoubleComparator _doubleComparator_5 = new DoubleComparator();
        TreeMap<Double, Map<String, String>> numberValueLookupIconMap = CollectionLiterals.<Double, Map<String, String>>newTreeMap(_doubleComparator_5);
        IntegerComparator _integerComparator_4 = new IntegerComparator();
        TreeMap<Integer, Map<String, String>> intValueLookupIconMap = CollectionLiterals.<Integer, Map<String, String>>newTreeMap(_integerComparator_4);
        HashMap<String, Map<String, String>> stringValueLookupIconMap = CollectionLiterals.<String, Map<String, String>>newHashMap();
        DoubleComparator _doubleComparator_6 = new DoubleComparator();
        TreeMap<Double, Map<String, String>> dateValueLookupIconMap = CollectionLiterals.<Double, Map<String, String>>newTreeMap(_doubleComparator_6);
        EList<TableLookup> _lookups_1 = value.getLookups();
        for (final TableLookup lookup_1 : _lookups_1) {
          {
            if ((lookup_1 instanceof TableNumberLookup)) {
              double _lookupValue = ((TableNumberLookup) lookup_1).getLookupValue();
              Double key = new Double(_lookupValue);
              String iconAspect = "";
              Map<String, String> iMap = CollectionLiterals.<String, String>newHashMap();
              TableRangeElement _discrete = ((TableNumberLookup)lookup_1).getDiscrete();
              boolean _matched_15 = false;
              if (_discrete instanceof TableIcon) {
                _matched_15=true;
                StringConcatenation _builder = new StringConcatenation();
                TableRangeElement _discrete_1 = ((TableNumberLookup)lookup_1).getDiscrete();
                String _icon = ((TableIcon) _discrete_1).getIcon();
                _builder.append(_icon);
                iconAspect = _builder.toString();
              }
              if (!_matched_15) {
                if (_discrete instanceof TableTrend) {
                  _matched_15=true;
                  StringConcatenation _builder = new StringConcatenation();
                  _builder.append("trend_");
                  TableRangeElement _discrete_1 = ((TableNumberLookup)lookup_1).getDiscrete();
                  String _literal = ((TableTrend) _discrete_1).getIcon().getLiteral();
                  _builder.append(_literal);
                  iconAspect = _builder.toString();
                }
              }
              int _length = iconAspect.length();
              boolean _greaterThan_10 = (_length > 0);
              if (_greaterThan_10) {
                iMap.put("icon", iconAspect);
                numberValueLookupIconMap.put(key, iMap);
              }
            }
            if ((lookup_1 instanceof TableIntLookup)) {
              int _lookupValue_1 = ((TableIntLookup) lookup_1).getLookupValue();
              Integer key_1 = new Integer(_lookupValue_1);
              String iconAspect_1 = "";
              Map<String, String> iMap_1 = CollectionLiterals.<String, String>newHashMap();
              TableRangeElement _discrete_1 = ((TableIntLookup)lookup_1).getDiscrete();
              boolean _matched_16 = false;
              if (_discrete_1 instanceof TableIcon) {
                _matched_16=true;
                StringConcatenation _builder = new StringConcatenation();
                TableRangeElement _discrete_2 = ((TableIntLookup)lookup_1).getDiscrete();
                String _icon = ((TableIcon) _discrete_2).getIcon();
                _builder.append(_icon);
                iconAspect_1 = _builder.toString();
              }
              if (!_matched_16) {
                if (_discrete_1 instanceof TableTrend) {
                  _matched_16=true;
                  StringConcatenation _builder = new StringConcatenation();
                  _builder.append("trend_");
                  TableRangeElement _discrete_2 = ((TableIntLookup)lookup_1).getDiscrete();
                  String _literal = ((TableTrend) _discrete_2).getIcon().getLiteral();
                  _builder.append(_literal);
                  iconAspect_1 = _builder.toString();
                }
              }
              int _length_1 = iconAspect_1.length();
              boolean _greaterThan_11 = (_length_1 > 0);
              if (_greaterThan_11) {
                iMap_1.put("icon", iconAspect_1);
                intValueLookupIconMap.put(key_1, iMap_1);
              }
            }
            if ((lookup_1 instanceof TableStringLookup)) {
              String key_2 = ((TableStringLookup) lookup_1).getLookupValue();
              String iconAspect_2 = "";
              Map<String, String> iMap_2 = CollectionLiterals.<String, String>newHashMap();
              TableRangeElement _discrete_2 = ((TableStringLookup)lookup_1).getDiscrete();
              boolean _matched_17 = false;
              if (_discrete_2 instanceof TableIcon) {
                _matched_17=true;
                StringConcatenation _builder = new StringConcatenation();
                TableRangeElement _discrete_3 = ((TableStringLookup)lookup_1).getDiscrete();
                String _icon = ((TableIcon) _discrete_3).getIcon();
                _builder.append(_icon);
                iconAspect_2 = _builder.toString();
              }
              int _length_2 = iconAspect_2.length();
              boolean _greaterThan_12 = (_length_2 > 0);
              if (_greaterThan_12) {
                iMap_2.put("icon", iconAspect_2);
                stringValueLookupIconMap.put(key_2, iMap_2);
              }
            }
            if ((lookup_1 instanceof TableDateDayLookup)) {
              int _lookupValue_2 = ((TableDateDayLookup) lookup_1).getLookupValue();
              Double key_3 = new Double(_lookupValue_2);
              String iconAspect_3 = "";
              Map<String, String> iMap_3 = CollectionLiterals.<String, String>newHashMap();
              TableRangeElement _discrete_3 = ((TableDateDayLookup)lookup_1).getDiscrete();
              boolean _matched_18 = false;
              if (_discrete_3 instanceof TableIcon) {
                _matched_18=true;
                StringConcatenation _builder = new StringConcatenation();
                TableRangeElement _discrete_4 = ((TableDateDayLookup)lookup_1).getDiscrete();
                String _icon = ((TableIcon) _discrete_4).getIcon();
                _builder.append(_icon);
                iconAspect_3 = _builder.toString();
              }
              if (!_matched_18) {
                if (_discrete_3 instanceof TableTrend) {
                  _matched_18=true;
                  StringConcatenation _builder = new StringConcatenation();
                  _builder.append("trend_");
                  TableRangeElement _discrete_4 = ((TableDateDayLookup)lookup_1).getDiscrete();
                  String _literal = ((TableTrend) _discrete_4).getIcon().getLiteral();
                  _builder.append(_literal);
                  iconAspect_3 = _builder.toString();
                }
              }
              int _length_3 = iconAspect_3.length();
              boolean _greaterThan_13 = (_length_3 > 0);
              if (_greaterThan_13) {
                iMap_3.put("icon", iconAspect_3);
                dateValueLookupIconMap.put(key_3, iMap_3);
              }
            }
          }
        }
        int _size_10 = numberValueLookupIconMap.size();
        boolean _greaterThan_10 = (_size_10 > 0);
        if (_greaterThan_10) {
          TableValueElement _column_11 = value.getColumn();
          boolean _matched_15 = false;
          if (_column_11 instanceof TableOrdinal) {
            _matched_15=true;
            numberLookupIconMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), numberValueLookupIconMap);
          }
          if (!_matched_15) {
            if (_column_11 instanceof TableAllColumns) {
              _matched_15=true;
              numberLookupIconMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), numberValueLookupIconMap);
            }
          }
          if (!_matched_15) {
            if (_column_11 instanceof TableMeasure) {
              _matched_15=true;
              numberLookupIconMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), numberValueLookupIconMap);
            }
          }
          if (!_matched_15) {
            if (_column_11 instanceof TableDerived) {
              _matched_15=true;
              numberLookupIconMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), numberValueLookupIconMap);
            }
          }
          if (!_matched_15) {
            if (_column_11 instanceof TableHierarchy) {
              _matched_15=true;
              numberLookupIconMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), numberValueLookupIconMap);
            }
          }
          if (!_matched_15) {
            if (_column_11 instanceof TableAttribute) {
              _matched_15=true;
              numberLookupIconMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), numberValueLookupIconMap);
            }
          }
          if (!_matched_15) {
            if (_column_11 instanceof TableAggregation) {
              _matched_15=true;
              numberLookupIconMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), numberValueLookupIconMap);
            }
          }
          if (!_matched_15) {
            if (_column_11 instanceof TableColumn) {
              _matched_15=true;
              numberLookupIconMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), numberValueLookupIconMap);
            }
          }
        }
        int _size_11 = intValueLookupIconMap.size();
        boolean _greaterThan_11 = (_size_11 > 0);
        if (_greaterThan_11) {
          TableValueElement _column_12 = value.getColumn();
          boolean _matched_16 = false;
          if (_column_12 instanceof TableOrdinal) {
            _matched_16=true;
            intLookupIconMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), intValueLookupIconMap);
          }
          if (!_matched_16) {
            if (_column_12 instanceof TableAllColumns) {
              _matched_16=true;
              intLookupIconMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), intValueLookupIconMap);
            }
          }
          if (!_matched_16) {
            if (_column_12 instanceof TableMeasure) {
              _matched_16=true;
              intLookupIconMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), intValueLookupIconMap);
            }
          }
          if (!_matched_16) {
            if (_column_12 instanceof TableDerived) {
              _matched_16=true;
              intLookupIconMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), intValueLookupIconMap);
            }
          }
          if (!_matched_16) {
            if (_column_12 instanceof TableHierarchy) {
              _matched_16=true;
              intLookupIconMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), intValueLookupIconMap);
            }
          }
          if (!_matched_16) {
            if (_column_12 instanceof TableAttribute) {
              _matched_16=true;
              intLookupIconMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), intValueLookupIconMap);
            }
          }
          if (!_matched_16) {
            if (_column_12 instanceof TableAggregation) {
              _matched_16=true;
              intLookupIconMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), intValueLookupIconMap);
            }
          }
          if (!_matched_16) {
            if (_column_12 instanceof TableColumn) {
              _matched_16=true;
              intLookupIconMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), intValueLookupIconMap);
            }
          }
        }
        int _size_12 = stringValueLookupIconMap.size();
        boolean _greaterThan_12 = (_size_12 > 0);
        if (_greaterThan_12) {
          TableValueElement _column_13 = value.getColumn();
          boolean _matched_17 = false;
          if (_column_13 instanceof TableOrdinal) {
            _matched_17=true;
            stringLookupIconMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), stringValueLookupIconMap);
          }
          if (!_matched_17) {
            if (_column_13 instanceof TableAllColumns) {
              _matched_17=true;
              stringLookupIconMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), stringValueLookupIconMap);
            }
          }
          if (!_matched_17) {
            if (_column_13 instanceof TableMeasure) {
              _matched_17=true;
              stringLookupIconMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), stringValueLookupIconMap);
            }
          }
          if (!_matched_17) {
            if (_column_13 instanceof TableDerived) {
              _matched_17=true;
              stringLookupIconMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), stringValueLookupIconMap);
            }
          }
          if (!_matched_17) {
            if (_column_13 instanceof TableHierarchy) {
              _matched_17=true;
              stringLookupIconMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), stringValueLookupIconMap);
            }
          }
          if (!_matched_17) {
            if (_column_13 instanceof TableAttribute) {
              _matched_17=true;
              stringLookupIconMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), stringValueLookupIconMap);
            }
          }
          if (!_matched_17) {
            if (_column_13 instanceof TableAggregation) {
              _matched_17=true;
              stringLookupIconMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), stringValueLookupIconMap);
            }
          }
          if (!_matched_17) {
            if (_column_13 instanceof TableColumn) {
              _matched_17=true;
              stringLookupIconMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), stringValueLookupIconMap);
            }
          }
        }
        int _size_13 = dateValueLookupIconMap.size();
        boolean _greaterThan_13 = (_size_13 > 0);
        if (_greaterThan_13) {
          TableValueElement _column_14 = value.getColumn();
          boolean _matched_18 = false;
          if (_column_14 instanceof TableOrdinal) {
            _matched_18=true;
            dateLookupIconMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), dateValueLookupIconMap);
          }
          if (!_matched_18) {
            if (_column_14 instanceof TableAllColumns) {
              _matched_18=true;
              dateLookupIconMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), dateValueLookupIconMap);
            }
          }
          if (!_matched_18) {
            if (_column_14 instanceof TableMeasure) {
              _matched_18=true;
              dateLookupIconMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), dateValueLookupIconMap);
            }
          }
          if (!_matched_18) {
            if (_column_14 instanceof TableDerived) {
              _matched_18=true;
              dateLookupIconMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), dateValueLookupIconMap);
            }
          }
          if (!_matched_18) {
            if (_column_14 instanceof TableHierarchy) {
              _matched_18=true;
              dateLookupIconMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), dateValueLookupIconMap);
            }
          }
          if (!_matched_18) {
            if (_column_14 instanceof TableAttribute) {
              _matched_18=true;
              dateLookupIconMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), dateValueLookupIconMap);
            }
          }
          if (!_matched_18) {
            if (_column_14 instanceof TableAggregation) {
              _matched_18=true;
              dateLookupIconMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), dateValueLookupIconMap);
            }
          }
          if (!_matched_18) {
            if (_column_14 instanceof TableColumn) {
              _matched_18=true;
              dateLookupIconMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), dateValueLookupIconMap);
            }
          }
        }
        DoubleComparator _doubleComparator_7 = new DoubleComparator();
        TreeMap<Double, Map<String, String>> numberValueLookupTooltipMap = CollectionLiterals.<Double, Map<String, String>>newTreeMap(_doubleComparator_7);
        IntegerComparator _integerComparator_5 = new IntegerComparator();
        TreeMap<Integer, Map<String, String>> intValueLookupTooltipMap = CollectionLiterals.<Integer, Map<String, String>>newTreeMap(_integerComparator_5);
        HashMap<String, Map<String, String>> stringValueLookupTooltipMap = CollectionLiterals.<String, Map<String, String>>newHashMap();
        DoubleComparator _doubleComparator_8 = new DoubleComparator();
        TreeMap<Double, Map<String, String>> dateValueLookupTooltipMap = CollectionLiterals.<Double, Map<String, String>>newTreeMap(_doubleComparator_8);
        EList<TableLookup> _lookups_2 = value.getLookups();
        for (final TableLookup lookup_2 : _lookups_2) {
          {
            if ((lookup_2 instanceof TableNumberLookup)) {
              double _lookupValue = ((TableNumberLookup) lookup_2).getLookupValue();
              Double key = new Double(_lookupValue);
              String tooltipAspect = "";
              Map<String, String> tMap = CollectionLiterals.<String, String>newHashMap();
              TableRangeElement _discrete = ((TableNumberLookup)lookup_2).getDiscrete();
              boolean _matched_19 = false;
              if (_discrete instanceof TableTooltip) {
                _matched_19=true;
                StringConcatenation _builder = new StringConcatenation();
                TableRangeElement _discrete_1 = ((TableNumberLookup)lookup_2).getDiscrete();
                String _tooltip = ((TableTooltip) _discrete_1).getTooltip();
                _builder.append(_tooltip);
                tooltipAspect = _builder.toString();
              }
              int _length = tooltipAspect.length();
              boolean _greaterThan_14 = (_length > 0);
              if (_greaterThan_14) {
                tMap.put("tooltip", tooltipAspect);
                numberValueLookupTooltipMap.put(key, tMap);
              }
            }
            if ((lookup_2 instanceof TableIntLookup)) {
              int _lookupValue_1 = ((TableIntLookup) lookup_2).getLookupValue();
              Integer key_1 = new Integer(_lookupValue_1);
              String tooltipAspect_1 = "";
              Map<String, String> tMap_1 = CollectionLiterals.<String, String>newHashMap();
              TableRangeElement _discrete_1 = ((TableIntLookup)lookup_2).getDiscrete();
              boolean _matched_20 = false;
              if (_discrete_1 instanceof TableTooltip) {
                _matched_20=true;
                StringConcatenation _builder = new StringConcatenation();
                TableRangeElement _discrete_2 = ((TableIntLookup)lookup_2).getDiscrete();
                String _tooltip = ((TableTooltip) _discrete_2).getTooltip();
                _builder.append(_tooltip);
                tooltipAspect_1 = _builder.toString();
              }
              int _length_1 = tooltipAspect_1.length();
              boolean _greaterThan_15 = (_length_1 > 0);
              if (_greaterThan_15) {
                tMap_1.put("tooltip", tooltipAspect_1);
                intValueLookupTooltipMap.put(key_1, tMap_1);
              }
            }
            if ((lookup_2 instanceof TableStringLookup)) {
              String key_2 = ((TableStringLookup) lookup_2).getLookupValue();
              String tooltipAspect_2 = "";
              Map<String, String> tMap_2 = CollectionLiterals.<String, String>newHashMap();
              TableRangeElement _discrete_2 = ((TableStringLookup)lookup_2).getDiscrete();
              boolean _matched_21 = false;
              if (_discrete_2 instanceof TableTooltip) {
                _matched_21=true;
                StringConcatenation _builder = new StringConcatenation();
                TableRangeElement _discrete_3 = ((TableStringLookup)lookup_2).getDiscrete();
                String _tooltip = ((TableTooltip) _discrete_3).getTooltip();
                _builder.append(_tooltip);
                tooltipAspect_2 = _builder.toString();
              }
              int _length_2 = tooltipAspect_2.length();
              boolean _greaterThan_16 = (_length_2 > 0);
              if (_greaterThan_16) {
                tMap_2.put("tooltip", tooltipAspect_2);
                stringValueLookupTooltipMap.put(key_2, tMap_2);
              }
            }
            if ((lookup_2 instanceof TableDateDayLookup)) {
              int _lookupValue_2 = ((TableDateDayLookup) lookup_2).getLookupValue();
              Double key_3 = new Double(_lookupValue_2);
              String tooltipAspect_3 = "";
              Map<String, String> tMap_3 = CollectionLiterals.<String, String>newHashMap();
              TableRangeElement _discrete_3 = ((TableDateDayLookup)lookup_2).getDiscrete();
              boolean _matched_22 = false;
              if (_discrete_3 instanceof TableTooltip) {
                _matched_22=true;
                StringConcatenation _builder = new StringConcatenation();
                TableRangeElement _discrete_4 = ((TableDateDayLookup)lookup_2).getDiscrete();
                String _tooltip = ((TableTooltip) _discrete_4).getTooltip();
                _builder.append(_tooltip);
                tooltipAspect_3 = _builder.toString();
              }
              int _length_3 = tooltipAspect_3.length();
              boolean _greaterThan_17 = (_length_3 > 0);
              if (_greaterThan_17) {
                tMap_3.put("tooltip", tooltipAspect_3);
                dateValueLookupTooltipMap.put(key_3, tMap_3);
              }
            }
          }
        }
        int _size_14 = numberValueLookupTooltipMap.size();
        boolean _greaterThan_14 = (_size_14 > 0);
        if (_greaterThan_14) {
          TableValueElement _column_15 = value.getColumn();
          boolean _matched_19 = false;
          if (_column_15 instanceof TableOrdinal) {
            _matched_19=true;
            numberLookupTooltipMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), numberValueLookupTooltipMap);
          }
          if (!_matched_19) {
            if (_column_15 instanceof TableAllColumns) {
              _matched_19=true;
              numberLookupTooltipMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), numberValueLookupTooltipMap);
            }
          }
          if (!_matched_19) {
            if (_column_15 instanceof TableMeasure) {
              _matched_19=true;
              numberLookupTooltipMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), numberValueLookupTooltipMap);
            }
          }
          if (!_matched_19) {
            if (_column_15 instanceof TableDerived) {
              _matched_19=true;
              numberLookupTooltipMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), numberValueLookupTooltipMap);
            }
          }
          if (!_matched_19) {
            if (_column_15 instanceof TableHierarchy) {
              _matched_19=true;
              numberLookupTooltipMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), numberValueLookupTooltipMap);
            }
          }
          if (!_matched_19) {
            if (_column_15 instanceof TableAttribute) {
              _matched_19=true;
              numberLookupTooltipMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), numberValueLookupTooltipMap);
            }
          }
          if (!_matched_19) {
            if (_column_15 instanceof TableAggregation) {
              _matched_19=true;
              numberLookupTooltipMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), numberValueLookupTooltipMap);
            }
          }
          if (!_matched_19) {
            if (_column_15 instanceof TableColumn) {
              _matched_19=true;
              numberLookupTooltipMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), numberValueLookupTooltipMap);
            }
          }
        }
        int _size_15 = intValueLookupTooltipMap.size();
        boolean _greaterThan_15 = (_size_15 > 0);
        if (_greaterThan_15) {
          TableValueElement _column_16 = value.getColumn();
          boolean _matched_20 = false;
          if (_column_16 instanceof TableOrdinal) {
            _matched_20=true;
            intLookupStyleMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), intValueLookupTooltipMap);
          }
          if (!_matched_20) {
            if (_column_16 instanceof TableAllColumns) {
              _matched_20=true;
              intLookupStyleMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), intValueLookupTooltipMap);
            }
          }
          if (!_matched_20) {
            if (_column_16 instanceof TableMeasure) {
              _matched_20=true;
              intLookupStyleMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), intValueLookupTooltipMap);
            }
          }
          if (!_matched_20) {
            if (_column_16 instanceof TableDerived) {
              _matched_20=true;
              intLookupStyleMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), intValueLookupTooltipMap);
            }
          }
          if (!_matched_20) {
            if (_column_16 instanceof TableHierarchy) {
              _matched_20=true;
              intLookupStyleMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), intValueLookupTooltipMap);
            }
          }
          if (!_matched_20) {
            if (_column_16 instanceof TableAttribute) {
              _matched_20=true;
              intLookupStyleMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), intValueLookupTooltipMap);
            }
          }
          if (!_matched_20) {
            if (_column_16 instanceof TableAggregation) {
              _matched_20=true;
              intLookupStyleMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), intValueLookupTooltipMap);
            }
          }
          if (!_matched_20) {
            if (_column_16 instanceof TableColumn) {
              _matched_20=true;
              intLookupStyleMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), intValueLookupTooltipMap);
            }
          }
        }
        int _size_16 = stringValueLookupTooltipMap.size();
        boolean _greaterThan_16 = (_size_16 > 0);
        if (_greaterThan_16) {
          TableValueElement _column_17 = value.getColumn();
          boolean _matched_21 = false;
          if (_column_17 instanceof TableOrdinal) {
            _matched_21=true;
            stringLookupTooltipMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), stringValueLookupTooltipMap);
          }
          if (!_matched_21) {
            if (_column_17 instanceof TableAllColumns) {
              _matched_21=true;
              stringLookupTooltipMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), stringValueLookupTooltipMap);
            }
          }
          if (!_matched_21) {
            if (_column_17 instanceof TableMeasure) {
              _matched_21=true;
              stringLookupTooltipMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), stringValueLookupTooltipMap);
            }
          }
          if (!_matched_21) {
            if (_column_17 instanceof TableDerived) {
              _matched_21=true;
              stringLookupTooltipMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), stringValueLookupTooltipMap);
            }
          }
          if (!_matched_21) {
            if (_column_17 instanceof TableHierarchy) {
              _matched_21=true;
              stringLookupTooltipMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), stringValueLookupTooltipMap);
            }
          }
          if (!_matched_21) {
            if (_column_17 instanceof TableAttribute) {
              _matched_21=true;
              stringLookupTooltipMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), stringValueLookupTooltipMap);
            }
          }
          if (!_matched_21) {
            if (_column_17 instanceof TableAggregation) {
              _matched_21=true;
              stringLookupTooltipMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), stringValueLookupTooltipMap);
            }
          }
          if (!_matched_21) {
            if (_column_17 instanceof TableColumn) {
              _matched_21=true;
              stringLookupTooltipMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), stringValueLookupTooltipMap);
            }
          }
        }
        int _size_17 = dateValueLookupTooltipMap.size();
        boolean _greaterThan_17 = (_size_17 > 0);
        if (_greaterThan_17) {
          TableValueElement _column_18 = value.getColumn();
          boolean _matched_22 = false;
          if (_column_18 instanceof TableOrdinal) {
            _matched_22=true;
            dateLookupTooltipMap.put(columnValueMap.get(TableOrdinal.class.getCanonicalName()), dateValueLookupTooltipMap);
          }
          if (!_matched_22) {
            if (_column_18 instanceof TableAllColumns) {
              _matched_22=true;
              dateLookupTooltipMap.put(columnValueMap.get(TableAllColumns.class.getCanonicalName()), dateValueLookupTooltipMap);
            }
          }
          if (!_matched_22) {
            if (_column_18 instanceof TableMeasure) {
              _matched_22=true;
              dateLookupTooltipMap.put(columnValueMap.get(TableMeasure.class.getCanonicalName()), dateValueLookupTooltipMap);
            }
          }
          if (!_matched_22) {
            if (_column_18 instanceof TableDerived) {
              _matched_22=true;
              dateLookupTooltipMap.put(columnValueMap.get(TableDerived.class.getCanonicalName()), dateValueLookupTooltipMap);
            }
          }
          if (!_matched_22) {
            if (_column_18 instanceof TableHierarchy) {
              _matched_22=true;
              dateLookupTooltipMap.put(columnValueMap.get(TableHierarchy.class.getCanonicalName()), dateValueLookupTooltipMap);
            }
          }
          if (!_matched_22) {
            if (_column_18 instanceof TableAttribute) {
              _matched_22=true;
              dateLookupTooltipMap.put(columnValueMap.get(TableAttribute.class.getCanonicalName()), dateValueLookupTooltipMap);
            }
          }
          if (!_matched_22) {
            if (_column_18 instanceof TableAggregation) {
              _matched_22=true;
              dateLookupTooltipMap.put(columnValueMap.get(TableAggregation.class.getCanonicalName()), dateValueLookupTooltipMap);
            }
          }
          if (!_matched_22) {
            if (_column_18 instanceof TableColumn) {
              _matched_22=true;
              dateLookupTooltipMap.put(columnValueMap.get(TableColumn.class.getCanonicalName()), dateValueLookupTooltipMap);
            }
          }
        }
      }
    }
  }
  
  /**
   * <p>helper method to get a attribute name or its alias if present of a entity's attribute object.</p>
   */
  public String attributeName(final DatamartAttribute attribute) {
    return this.datamartInferrer.getAttributeName(attribute, null);
  }
  
  /**
   * <p>helper method to get a level name of a hierarchy's level object.</p>
   */
  public String getLevelName(final CubeLevel level) {
    return level.getName();
  }
  
  /**
   * <p>generate code for the pre-ordering refresh event.</p>
   */
  public String preOrder(final TablePreorder preorder, final TableAxis axis) {
    String name = "";
    TableValueElement _column = preorder.getColumn();
    boolean _matched = false;
    if (_column instanceof TableOrdinal) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("#");
      String _literal = axis.getAxis().getLiteral();
      _builder.append(_literal);
      TableValueElement _column_1 = preorder.getColumn();
      String _string = ((TableOrdinal) _column_1).getValueRef().toString();
      _builder.append(_string);
      name = _builder.toString();
    }
    if (!_matched) {
      if (_column instanceof TableAllColumns) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("?");
        String _literal = axis.getAxis().getLiteral();
        _builder.append(_literal);
        name = _builder.toString();
      }
    }
    if (!_matched) {
      if (_column instanceof TableMeasure) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        TableValueElement _column_1 = preorder.getColumn();
        String _name = ((TableMeasure) _column_1).getValueRef().getMeasureRef().getName();
        _builder.append(_name);
        name = _builder.toString();
      }
    }
    if (!_matched) {
      if (_column instanceof TableDerived) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        TableValueElement _column_1 = preorder.getColumn();
        String _name = ((TableDerived) _column_1).getValueRef().getDerivedRef().getName();
        _builder.append(_name);
        name = _builder.toString();
      }
    }
    if (!_matched) {
      if (_column instanceof TableHierarchy) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        TableValueElement _column_1 = preorder.getColumn();
        String _levelName = this.getLevelName(((TableHierarchy) _column_1).getValueRef());
        _builder.append(_levelName);
        name = _builder.toString();
      }
    }
    if (!_matched) {
      if (_column instanceof TableAttribute) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        TableValueElement _column_1 = preorder.getColumn();
        String _aliasedAttributeName = DatamartAttributeUtil.getAliasedAttributeName(((TableAttribute) _column_1).getValueRef());
        _builder.append(_aliasedAttributeName);
        name = _builder.toString();
      }
    }
    if (!_matched) {
      if (_column instanceof TableAggregation) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        TableValueElement _column_1 = preorder.getColumn();
        String _name = ((TableAggregation) _column_1).getValueRef().getAggregation().name();
        _builder.append(_name);
        name = _builder.toString();
      }
    }
    if (!_matched) {
      if (_column instanceof TableColumn) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        TableValueElement _column_1 = preorder.getColumn();
        String _literal = ((TableColumn) _column_1).getValueRef().getColumnRef().getLiteral();
        _builder.append(_literal);
        name = _builder.toString();
      }
    }
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("dataSourceContainer.setSort(table, \"");
    _builder.append(name);
    _builder.append("\", ");
    String _string = Boolean.valueOf(Boolean.valueOf(preorder.isAscending()).booleanValue()).toString();
    _builder.append(_string);
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    String body = _builder.toString();
    return body;
  }
  
  public Set<LEntity> findRequestedEntities(final Table table) {
    HashSet<LEntity> entities = new HashSet<LEntity>();
    TableOption _tabletype = table.getTabletype();
    boolean _matched = false;
    if (_tabletype instanceof TableTable) {
      _matched=true;
      TableOption _tabletype_1 = table.getTabletype();
      entities.addAll(this.datamartInferrer.findAllEntities(((TableTable) _tabletype_1).getSource().getDatamartRef()));
    }
    if (!_matched) {
      if (_tabletype instanceof TableGrid) {
        _matched=true;
        EObject _resolve = EcoreUtil2.resolve(this.getDtoGridTable(table).getDtoSource().getWrappedType(), table.eResource());
        final LEntity entity = ((LEntity) _resolve);
        entities.add(entity);
      }
    }
    return entities;
  }
  
  public String sendEvent(final Table table) {
    String body = "";
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(body);
    _builder.newLineIfNotEmpty();
    _builder.append("table.addValueChangeListener(new ValueChangeListener() {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("Object msg = null;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("Object id = null;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("String target = null;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("Object value = table.getValue();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("int selection = -1;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("if\t(value instanceof Integer) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("selection = (Integer)value;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("else {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if ((value instanceof Collection<?>) && (((Collection<?>)value).size() == 1)) {");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("value = ((Collection<?>)value).iterator().next();");
    _builder.newLine();
    _builder.append("    \t\t\t");
    _builder.append("if\t(value instanceof Integer) {");
    _builder.newLine();
    _builder.append("    \t\t\t\t");
    _builder.append("selection = (Integer)value;");
    _builder.newLine();
    _builder.append("    \t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    {
      if ((this.isEmbedded(table) && this.isTask(table))) {
        _builder.append("\t\t");
        _builder.append("for(Button b:taskButtons) {");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("b.setEnabled(table.getValue() != null && ");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("\t\t");
        _builder.append("(dataSourceContainer.getTaskStatus(selection).equals(BPMStatus.Created) ||");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("\t\t");
        _builder.append("dataSourceContainer.getTaskStatus(selection).equals(BPMStatus.Ready) ||");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("\t\t");
        _builder.append("dataSourceContainer.getTaskStatus(selection).equals(BPMStatus.Suspended)));");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("}");
        _builder.newLine();
      }
    }
    body = _builder.toString();
    boolean _isTable = this.isTable(table);
    if (_isTable) {
      TableDatamart _dataMartTable = this.getDataMartTable(table);
      if ((_dataMartTable instanceof TableDatamart)) {
        TableDatamart _dataMartTable_1 = this.getDataMartTable(table);
        DatamartSource _source = ((TableDatamart) _dataMartTable_1).getDatamartRef().getSource();
        boolean _matched = false;
        if (_source instanceof DatamartTask) {
          _matched=true;
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append(body);
          _builder_1.newLineIfNotEmpty();
          _builder_1.append("\t\t");
          _builder_1.append("if (table.getValue() != null && taskClient != null) {");
          _builder_1.newLine();
          _builder_1.append("\t\t\t");
          _builder_1.append("eclipseContext.set(BPMTaskSummary.class, taskClient.getUserTask(dataSourceContainer.getTaskId(selection), user));");
          _builder_1.newLine();
          _builder_1.append("\t\t");
          _builder_1.append("} else {");
          _builder_1.newLine();
          _builder_1.append("\t\t\t");
          _builder_1.append("eclipseContext.set(BPMTaskSummary.class, null);");
          _builder_1.newLine();
          _builder_1.append("\t\t");
          _builder_1.append("}");
          _builder_1.newLine();
          body = _builder_1.toString();
        }
        if (!_matched) {
          if (_source instanceof DatamartEntity) {
            _matched=true;
            TableDatamart _dataMartTable_2 = this.getDataMartTable(table);
            DatamartSource _source_1 = ((TableDatamart) _dataMartTable_2).getDatamartRef().getSource();
            DatamartEntity datamartEntity = ((DatamartEntity) _source_1);
            EObject _resolve = EcoreUtil2.resolve(datamartEntity.getEntityRef(), table.eResource());
            LEntity entity = ((LEntity) _resolve);
            String alias = DatamartDefinitionUtil.getEntityIdAliasName(entity);
            if ((alias != null)) {
              StringConcatenation _builder_1 = new StringConcatenation();
              _builder_1.append(body);
              _builder_1.newLineIfNotEmpty();
              {
                LEntityAttribute _primaryKeyAttribute = entity.getPrimaryKeyAttribute();
                boolean _tripleNotEquals = (_primaryKeyAttribute != null);
                if (_tripleNotEquals) {
                  _builder_1.append("\t\t");
                  _builder_1.append("if(selection != -1) {");
                  _builder_1.newLine();
                  _builder_1.append("\t\t");
                  _builder_1.append("\t");
                  _builder_1.append("Object idObj = datamartInstance.getPrimaryListKeys(\"");
                  _builder_1.append(alias, "\t\t\t");
                  _builder_1.append("\").get(selection);");
                  _builder_1.newLineIfNotEmpty();
                  _builder_1.append("\t\t");
                  _builder_1.append("\t");
                  _builder_1.append("SelectionStore.putSelectionToPerspectiveContext(getPart(), \"");
                  QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(this.baseType(entity));
                  _builder_1.append(_fullyQualifiedName, "\t\t\t");
                  _builder_1.append(".");
                  String _idAttributeName = entity.getIdAttributeName();
                  _builder_1.append(_idAttributeName, "\t\t\t");
                  _builder_1.append("\", idObj);");
                  _builder_1.newLineIfNotEmpty();
                  _builder_1.append("\t\t");
                  _builder_1.append("\t");
                  _builder_1.newLine();
                  _builder_1.append("\t\t");
                  _builder_1.append("\t");
                  _builder_1.append("EventDispatcherEvent evnt = new EventDispatcherEvent(getContext().get(MPerspective.class), EventDispatcherCommand.SELECT, \"");
                  QualifiedName _fullyQualifiedName_1 = this._iQualifiedNameProvider.getFullyQualifiedName(this.baseType(entity));
                  _builder_1.append(_fullyQualifiedName_1, "\t\t\t");
                  _builder_1.append(".");
                  String _idAttributeName_1 = entity.getIdAttributeName();
                  _builder_1.append(_idAttributeName_1, "\t\t\t");
                  _builder_1.append("\", \"");
                  QualifiedName _fullyQualifiedName_2 = this._iQualifiedNameProvider.getFullyQualifiedName(table);
                  _builder_1.append(_fullyQualifiedName_2, "\t\t\t");
                  _builder_1.append("\");");
                  _builder_1.newLineIfNotEmpty();
                  _builder_1.append("\t\t");
                  _builder_1.append("\t");
                  _builder_1.append("evnt.addItem(EventDispatcherDataTag.ID, idObj);");
                  _builder_1.newLine();
                  _builder_1.append("\t\t");
                  _builder_1.append("\t");
                  _builder_1.append("eventDispatcher.sendEvent(evnt);");
                  _builder_1.newLine();
                  {
                    boolean _isSelectIdOnly = this.isSelectIdOnly(table);
                    boolean _not = (!_isSelectIdOnly);
                    if (_not) {
                      _builder_1.append("\t\t");
                      _builder_1.append("\t");
                      _builder_1.append("// send all refs too");
                      _builder_1.newLine();
                      _builder_1.append("\t\t");
                      _builder_1.append("\t");
                      _builder_1.append("for (String alias : datamartInstance.getPrimaryList().keySet()){");
                      _builder_1.newLine();
                      _builder_1.append("\t\t");
                      _builder_1.append("\t");
                      _builder_1.append("\t");
                      _builder_1.append("if (!\"");
                      _builder_1.append(alias, "\t\t\t\t");
                      _builder_1.append("\".equals(alias)){");
                      _builder_1.newLineIfNotEmpty();
                      _builder_1.append("\t\t");
                      _builder_1.append("\t");
                      _builder_1.append("\t\t");
                      _builder_1.append("evnt = new EventDispatcherEvent(getContext().get(MPerspective.class), EventDispatcherCommand.SELECT, datamartInstance.getPrimaryList().get(alias).getEntityName() + \".\" + datamartInstance.getPrimaryList().get(alias).getAttributeName(), \"");
                      QualifiedName _fullyQualifiedName_3 = this._iQualifiedNameProvider.getFullyQualifiedName(table);
                      _builder_1.append(_fullyQualifiedName_3, "\t\t\t\t\t");
                      _builder_1.append("\");");
                      _builder_1.newLineIfNotEmpty();
                      _builder_1.append("\t\t");
                      _builder_1.append("\t");
                      _builder_1.append("\t\t");
                      _builder_1.append("evnt.addItem(EventDispatcherDataTag.ID, datamartInstance.getPrimaryListKeys(alias).get(selection));");
                      _builder_1.newLine();
                      _builder_1.append("\t\t");
                      _builder_1.append("\t");
                      _builder_1.append("\t\t");
                      _builder_1.append("eventDispatcher.sendEvent(evnt);");
                      _builder_1.newLine();
                      _builder_1.append("\t\t");
                      _builder_1.append("\t");
                      _builder_1.append("\t");
                      _builder_1.append("}");
                      _builder_1.newLine();
                      _builder_1.append("\t\t");
                      _builder_1.append("\t");
                      _builder_1.append("}");
                      _builder_1.newLine();
                    }
                  }
                  _builder_1.append("\t\t");
                  _builder_1.append("}");
                  _builder_1.newLine();
                }
              }
              body = _builder_1.toString();
            }
          }
        }
      }
    }
    EList<TableElement> _elements = this.getDataMartTable(table).getElements();
    for (final TableElement element : _elements) {
      if ((element instanceof TableAxis)) {
        TableAxis axis = ((TableAxis) element);
        int evntCnt = 0;
        EList<TableEvent> _events = axis.getEvents();
        for (final TableEvent event : _events) {
          {
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append(body);
            _builder_1.newLineIfNotEmpty();
            _builder_1.append("\t\t");
            _builder_1.append("if(selection != -1) {");
            _builder_1.newLine();
            {
              TableValueElement _source_1 = event.getSource();
              if ((_source_1 instanceof TableOrdinal)) {
                _builder_1.append("\t\t\t");
                _builder_1.append("EventDispatcherEvent evnt");
                _builder_1.append(evntCnt, "\t\t\t");
                _builder_1.append(" = new EventDispatcherEvent(getContext().get(MPerspective.class), EventDispatcherCommand.SELECT, \"");
                TableValueElement _source_2 = event.getSource();
                String _valueRef = ((TableOrdinal) _source_2).getValueRef();
                _builder_1.append(_valueRef, "\t\t\t");
                _builder_1.append("\", \"");
                QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(table);
                _builder_1.append(_fullyQualifiedName, "\t\t\t");
                _builder_1.append("\");");
                _builder_1.newLineIfNotEmpty();
              } else {
                TableValueElement _source_3 = event.getSource();
                if ((_source_3 instanceof TableAttribute)) {
                  _builder_1.append("\t\t\t");
                  _builder_1.append("EventDispatcherEvent evnt");
                  _builder_1.append(evntCnt, "\t\t\t");
                  _builder_1.append(" = new EventDispatcherEvent(getContext().get(MPerspective.class), EventDispatcherCommand.SELECT, \"");
                  TableValueElement _source_4 = event.getSource();
                  String _name = ((TableAttribute) _source_4).getValueRef().getAttributeRef().getName();
                  _builder_1.append(_name, "\t\t\t");
                  _builder_1.append("\", \"");
                  QualifiedName _fullyQualifiedName_1 = this._iQualifiedNameProvider.getFullyQualifiedName(table);
                  _builder_1.append(_fullyQualifiedName_1, "\t\t\t");
                  _builder_1.append("\");");
                  _builder_1.newLineIfNotEmpty();
                }
              }
            }
            {
              TableValueElement _source_5 = event.getSource();
              if ((_source_5 instanceof TableOrdinal)) {
                _builder_1.append("\t\t\t");
                _builder_1.append("evnt");
                _builder_1.append(evntCnt, "\t\t\t");
                _builder_1.append(".addItem(EventDispatcherDataTag.LIST, dataSourceContainer.getValueByProperty(selection, \"");
                TableValueElement _source_6 = event.getSource();
                String _valueRef_1 = ((TableOrdinal) _source_6).getValueRef();
                _builder_1.append(_valueRef_1, "\t\t\t");
                _builder_1.append("\"));");
                _builder_1.newLineIfNotEmpty();
                _builder_1.append("\t\t\t");
                _builder_1.append("eventDispatcher.sendEvent(evnt");
                _builder_1.append(evntCnt, "\t\t\t");
                _builder_1.append(");");
                _builder_1.newLineIfNotEmpty();
              } else {
                TableValueElement _source_7 = event.getSource();
                if ((_source_7 instanceof TableAttribute)) {
                  _builder_1.append("\t\t\t");
                  _builder_1.append("evnt");
                  _builder_1.append(evntCnt, "\t\t\t");
                  _builder_1.append(".addItem(EventDispatcherDataTag.LIST, dataSourceContainer.getValueByProperty(selection, \"");
                  TableValueElement _source_8 = event.getSource();
                  String _name_1 = ((TableAttribute) _source_8).getValueRef().getAttributeRef().getName();
                  _builder_1.append(_name_1, "\t\t\t");
                  _builder_1.append("\"));");
                  _builder_1.newLineIfNotEmpty();
                  _builder_1.append("\t\t\t");
                  _builder_1.append("eventDispatcher.sendEvent(evnt");
                  _builder_1.append(evntCnt, "\t\t\t");
                  _builder_1.append(");");
                  _builder_1.newLineIfNotEmpty();
                }
              }
            }
            _builder_1.append("\t\t");
            _builder_1.append("} ");
            _builder_1.newLine();
            body = _builder_1.toString();
            evntCnt = (evntCnt + 1);
          }
        }
      }
    }
    StringConcatenation _builder_1 = new StringConcatenation();
    _builder_1.append(body);
    _builder_1.newLineIfNotEmpty();
    _builder_1.append("\t");
    _builder_1.append("}");
    _builder_1.newLine();
    _builder_1.append("});");
    _builder_1.newLine();
    body = _builder_1.toString();
    return body;
  }
  
  public CharSequence columnResizeListener() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("table.addColumnResizeListener(new ColumnResizeListener(){");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public void columnResize(ColumnResizeEvent event) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("// Get the new width of the resized column");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("int width = event.getCurrentWidth();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("// Get the property ID of the resized column");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("Object columnId = event.getPropertyId();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("user.addToProperties(this.getClass().getEnclosingClass().getCanonicalName() + \".\" + columnId.toString() + \".width\", String.valueOf(width));");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("});");
    _builder.newLine();
    _builder.append("// Must be immediate to send the resize events immediately");
    _builder.newLine();
    _builder.append("table.setImmediate(true);");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence columnReorderListener() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("table.addColumnReorderListener(new ColumnReorderListener(){");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public void columnReorder(ColumnReorderEvent event) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("Object source = event.getSource();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("String propertyKey = this.getClass().getEnclosingClass().getCanonicalName() + \".columns\";");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("if (source instanceof CellSetFilterTable){");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("Map<Object, Integer> visibleColumnsPositions = new HashMap<>();");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("Object[] visibleColumns = ((CellSetFilterTable)source).getVisibleColumns();");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("for (int i = 0; i < visibleColumns.length; i++) {");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("CellSetIndexedContainer container = ((CellSetFilterTable)source).getCellSetIndexedContainer();");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("Object visibleColumnKey = visibleColumns[i];");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    CharSequence _positionMembersHashCode = this.getPositionMembersHashCode();
    _builder.append(_positionMembersHashCode, "\t\t\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t");
    _builder.append("visibleColumnsPositions.put(visibleColumnKey, membersHash);");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if (visibleColumns!=null){");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("user.addToProperties(propertyKey, visibleColumnsPositions);");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("});");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence getPositionMembersHashCode() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// creates a hash code by the column position members\t\t");
    _builder.newLine();
    _builder.append("int membersHash = 0;");
    _builder.newLine();
    _builder.append("if (container != null) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("DerivedPosition pos = container.getColumnPosition(visibleColumnKey);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if ( pos != null ) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("List<DerivedMember> members = pos.getMembers();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("for (DerivedMember member : members) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("membersHash += (member.getUniqueName() != null) ? member.getUniqueName().hashCode() : member.hashCode();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence columnCollapsedListener() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("table.addColumnCollapseListener(new ColumnCollapseListener(){");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public void columnCollapseStateChange(ColumnCollapseEvent event) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("// Get the ID of the column collapse event");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("Object columnId = event.getPropertyId();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("Object source = event.getSource();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("String propertyKey = this.getClass().getEnclosingClass().getCanonicalName() + \".\" + columnId.toString() + \".collapsed\";");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("if (source instanceof CustomTable){");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if (((CustomTable)source).isColumnCollapsed(columnId)) {");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("user.addToProperties(propertyKey, \"true\");");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("} else {");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("user.removeFromProperties(propertyKey);");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("});");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence tableSettingsByUserProperties(final Table table) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("// Setting the order of the columns via the visibleColumns list");
    _builder.newLine();
    _builder.append("Map<Object, Integer> visibleColumnsMap = user.getColumnUtil().getVisibleColumns(this.getClass().getCanonicalName() + \".columns\");");
    _builder.newLine();
    _builder.append("if (visibleColumnsMap != null){");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("boolean unmodifiedVisibleColumns = true;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("for (Object visibleColumnKey : visibleColumnsMap.keySet()) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("CellSetIndexedContainer container = dataSourceContainer;");
    _builder.newLine();
    _builder.append("\t\t");
    CharSequence _positionMembersHashCode = this.getPositionMembersHashCode();
    _builder.append(_positionMembersHashCode, "\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("if (membersHash != visibleColumnsMap.get(visibleColumnKey)) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("unmodifiedVisibleColumns = false;");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("break;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if (unmodifiedVisibleColumns) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("try {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("Object[] visibleColumns = visibleColumnsMap.values().toArray();");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("table.setVisibleColumns(visibleColumns );");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("} catch (IllegalArgumentException e) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("// this occurs if a saved column is no longer existing due to model changes");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("// Setting table columns width from the persisted user properties");
    _builder.newLine();
    _builder.append("for (Object visibleId : table.getVisibleColumns()) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("String widthStr = user.getColumnUtil().getColumnWidth(this.getClass().getCanonicalName() + \".\" + visibleId.toString() + \".width\");");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if (widthStr!= null) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("try {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("table.setColumnWidth(visibleId, Integer.parseInt(widthStr));");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("} catch (IllegalArgumentException e) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("// this occurs if a saved column is no longer existing due to model changes");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("// Setting table collapsed columns from the persisted user properties");
    _builder.newLine();
    _builder.append("for (Object propertyId : table.getContainerDataSource().getContainerPropertyIds()) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("String collapsedColumn = user.getColumnUtil().getColumnCollapsed(this.getClass().getCanonicalName() + \".\" + propertyId + \".collapsed\");");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("try {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("if (collapsedColumn != null){");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("table.setColumnCollapsed(propertyId, true);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("} catch (IllegalArgumentException e) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("// this occurs if a saved column is no longer existing due to model changes");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence getMsgValue(final LEntityAttribute attribute, final String string) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(this.CONVERT_METHOD_PREFIX);
    String _firstUpper = StringExtensions.toFirstUpper(this._modelExtensions.typeName(attribute));
    _builder.append(_firstUpper);
    _builder.append("(");
    _builder.append(string);
    _builder.append(")");
    return _builder;
  }
  
  public String buildTopic(final DatamartDefinition datamart, final String referenceName) {
    String topic = "";
    DatamartSource _source = datamart.getSource();
    if ((_source instanceof DatamartEntity)) {
      DatamartSource _source_1 = datamart.getSource();
      LEntity entity = ((DatamartEntity) _source_1).getEntityRef();
      EObject eObj = datamart.eContainer();
      while ((!(eObj instanceof DatamartPackage))) {
        eObj = eObj.eContainer();
      }
      if ((eObj != null)) {
        String pckName = this._iQualifiedNameProvider.getFullyQualifiedName(((DatamartPackage) eObj)).toString().replace(".", "/");
        topic = pckName.concat("/").concat(datamart.getName()).concat("/").concat(entity.getName()).concat("/").concat(referenceName);
      }
    }
    return topic;
  }
  
  /**
   * <p>helper method to convert a rgb string to a hex string.</p>
   */
  public String StringtoHex(final String rgb) {
    String colorHex = "#";
    String[] _split = rgb.split(",");
    for (final String color : _split) {
      {
        Integer i = new Integer(color);
        StringConcatenation _builder = new StringConcatenation();
        _builder.append(colorHex);
        String _format = String.format("%02x", i);
        _builder.append(_format);
        colorHex = _builder.toString();
      }
    }
    return colorHex;
  }
  
  /**
   * <p>helper method to convert a rgb string to a rgb syntax.</p>
   */
  public String toRGB(final String rgb) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("rgb(");
    _builder.append(rgb);
    _builder.append(")");
    return _builder.toString();
  }
  
  public void initIdMap(final Table table, final HashMap<String, IdAttribute> map) {
    map.clear();
    TableDatamart _dataMartTable = this.getDataMartTable(table);
    if ((_dataMartTable instanceof TableDatamart)) {
      TableDatamart _dataMartTable_1 = this.getDataMartTable(table);
      DatamartDefinition datamart = ((TableDatamart) _dataMartTable_1).getDatamartRef();
      DatamartSource _source = datamart.getSource();
      if ((_source instanceof DatamartEntity)) {
        DatamartSource _source_1 = datamart.getSource();
        DatamartEntity entity = ((DatamartEntity) _source_1);
        this.iterateIdMap(entity, map);
      }
    }
  }
  
  public void iterateIdMap(final DatamartEntity entity, final HashMap<String, IdAttribute> map) {
    IdAttribute prop = new IdAttribute();
    prop.collapsed = true;
    map.put(DatamartDefinitionUtil.getEntityIdAliasName(entity.getEntityRef()), prop);
    boolean _isHistorizedOrTimedependentWithParent = entity.getEntityRef().isHistorizedOrTimedependentWithParent();
    if (_isHistorizedOrTimedependentWithParent) {
      map.put(DatamartDefinitionUtil.getEntityValidAliasName(entity.getEntityRef()), prop);
      map.put(DatamartDefinitionUtil.getEntityCurrentAliasName(entity.getEntityRef()), prop);
    }
    EList<DatamartNavigation> _navigations = entity.getNavigations();
    for (final DatamartNavigation navigation : _navigations) {
      this.iterateIdMap(navigation.getDatamartEntity(), map);
    }
  }
  
  public CharSequence localeChanged(final Table table) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("if(initDone) {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("tableFilterDecorator.setLocale(locale);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("for(CellSetFilterTable table:tables) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("table.setLocale(locale);");
    _builder.newLine();
    {
      if (((this.isTable(table) && 
        ((TableDatamart) this.getDataMartTable(table)).getDatamartRef().isDescription()) && 
        (((TableDatamart) this.getDataMartTable(table)).getDatamartRef().getDescriptionValue() != null))) {
        _builder.append("\t\t");
        _builder.append("table.setDescription(dslMetadataService.translate(locale.toLanguageTag(), \"");
        TableDatamart _dataMartTable = this.getDataMartTable(table);
        String _descriptionValue = ((TableDatamart) _dataMartTable).getDatamartRef().getDescriptionValue();
        _builder.append(_descriptionValue, "\t\t");
        _builder.append("\"));");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("for(String key:attributeLookupMap.keySet()) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("attributeLookupMap.get(key).setLocale(locale);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if(dataSourceContainer != null) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("dataSourceContainer.setLocale(locale);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("layoutManager.setLabelValue(dslMetadataService.translate(locale.toLanguageTag(), \"");
    String _name = table.getName();
    _builder.append(_name, "\t");
    _builder.append("\"));");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence selectItemsFromWorkloadDto(final Table table) {
    CharSequence _xblockexpression = null;
    {
      DatamartEntity datamartEntity = ((DatamartEntity) null);
      LEntity entity = ((LEntity) null);
      String alias = ((String) null);
      String dtoFqn = ((String) null);
      LDataType keyType = ((LDataType) null);
      String keyGetter = ((String) null);
      TableDatamart _dataMartTable = this.getDataMartTable(table);
      if ((_dataMartTable instanceof TableDatamart)) {
        TableDatamart _dataMartTable_1 = this.getDataMartTable(table);
        DatamartSource _source = ((TableDatamart) _dataMartTable_1).getDatamartRef().getSource();
        boolean _matched = false;
        if (_source instanceof DatamartEntity) {
          _matched=true;
          TableDatamart _dataMartTable_2 = this.getDataMartTable(table);
          DatamartSource _source_1 = ((TableDatamart) _dataMartTable_2).getDatamartRef().getSource();
          datamartEntity = ((DatamartEntity) _source_1);
          EObject _resolve = EcoreUtil2.resolve(datamartEntity.getEntityRef(), table.eResource());
          entity = ((LEntity) _resolve);
          keyType = this._modelExtensions.getDatatype(entity.getPrimaryKeyAttribute());
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("get");
          String _firstUpper = StringExtensions.toFirstUpper(entity.getPrimaryKeyAttribute().getName());
          _builder.append(_firstUpper);
          _builder.append("()");
          keyGetter = _builder.toString();
          alias = DatamartDefinitionUtil.getEntityIdAliasName(entity).toLowerCase();
          dtoFqn = EntityUtils.getDtoFQNForLEntity(entity);
        }
      }
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("List<Integer> selections = new ArrayList<>();");
      _builder.newLine();
      _builder.append("if(taskSummary != null) {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("IDto workloadDto = taskClient.getWorkloadDto(taskSummary);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("List<IDto> operativeDtos = workLoadItem.getOperativeDtos(workloadDto);");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("if\t(operativeDtos != null) {");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Set<String> ids = new TreeSet<String>();");
      _builder.newLine();
      _builder.append("\t    ");
      _builder.append("for\t(IDto operativeDto : operativeDtos) {");
      _builder.newLine();
      _builder.append("\t    \t");
      _builder.append("ids.add(String.valueOf(((");
      _builder.append(dtoFqn, "\t    \t");
      _builder.append(")operativeDto).");
      _builder.append(keyGetter, "\t    \t");
      _builder.append("));");
      _builder.newLineIfNotEmpty();
      _builder.append("\t    ");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t    ");
      _builder.append("for\t(Object selection : dataSourceContainer.getAllItemIds()) {");
      _builder.newLine();
      _builder.append("\t        ");
      _builder.append("String idValue = dataSourceContainer.getStringValueByProperty((Integer)selection,\"");
      _builder.append(alias, "\t        ");
      _builder.append("\");");
      _builder.newLineIfNotEmpty();
      _builder.append("\t    \t");
      _builder.append("if\t(ids.contains(idValue)) {");
      _builder.newLine();
      _builder.append("\t    \t\t");
      _builder.append("selections.add((Integer)selection);");
      _builder.newLine();
      _builder.append("\t    \t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t    ");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("selectTable.setValue(selections);");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public CharSequence rowChecked(final Table table) {
    CharSequence _xifexpression = null;
    TableDatamart _dataMartTable = this.getDataMartTable(table);
    if ((_dataMartTable instanceof TableDatamart)) {
      CharSequence _switchResult = null;
      TableDatamart _dataMartTable_1 = this.getDataMartTable(table);
      DatamartSource _source = ((TableDatamart) _dataMartTable_1).getDatamartRef().getSource();
      boolean _matched = false;
      if (_source instanceof DatamartEntity) {
        _matched=true;
        CharSequence _xblockexpression = null;
        {
          TableDatamart _dataMartTable_2 = this.getDataMartTable(table);
          DatamartSource _source_1 = ((TableDatamart) _dataMartTable_2).getDatamartRef().getSource();
          DatamartEntity datamartEntity = ((DatamartEntity) _source_1);
          EObject _resolve = EcoreUtil2.resolve(datamartEntity.getEntityRef(), table.eResource());
          LEntity entity = ((LEntity) _resolve);
          String alias = DatamartDefinitionUtil.getEntityIdAliasName(entity).toLowerCase();
          LDataType keyType = this._modelExtensions.getDatatype(entity.getPrimaryKeyAttribute());
          String dtoFqn = EntityUtils.getDtoFQNForLEntity(entity);
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("IDTOService<");
          _builder.append(dtoFqn);
          _builder.append("> dtoService = (IDTOService<");
          _builder.append(dtoFqn);
          _builder.append(">) DtoServiceAccess.getService(");
          _builder.append(dtoFqn);
          _builder.append(".class);");
          _builder.newLineIfNotEmpty();
          _builder.append("String idValue = dataSourceContainer.getStringValueByProperty((Integer)selection,\"");
          _builder.append(alias);
          _builder.append("\");");
          _builder.newLineIfNotEmpty();
          _builder.append("if(taskSummary != null) {");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("if\t(selected) {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("workLoadItem.addOperativeDto(taskClient.getWorkloadDto(taskSummary), ");
          {
            boolean _equals = "int".equals(keyType.getName());
            if (_equals) {
              _builder.append("dtoService.get((int) Integer.parseInt(idValue))");
            } else {
              _builder.append("dtoService.get(idValue)");
            }
          }
          _builder.append(");");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("else {");
          _builder.newLine();
          _builder.append("\t   \t");
          _builder.append("for(IDto dtoToRemove : workLoadItem.getOperativeDtos(taskClient.getWorkloadDto(taskSummary))) {");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("if(((");
          _builder.append(dtoFqn, "\t\t\t");
          _builder.append(")dtoToRemove).getId()");
          {
            boolean _equals_1 = "int".equals(keyType.getName());
            if (_equals_1) {
              _builder.append("==(int) Integer.parseInt(idValue)");
            } else {
              _builder.append(".equals(idValue)");
            }
          }
          _builder.append(") {");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t\t\t");
          _builder.append("workLoadItem.removeOperativeDto(taskClient.getWorkloadDto(taskSummary), dtoToRemove);");
          _builder.newLine();
          _builder.append("\t\t\t\t");
          _builder.append("break;");
          _builder.newLine();
          _builder.append("\t\t\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("} else {");
          _builder.newLine();
          _builder.append("\t");
          _builder.append("if(selected) {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("SelectionStore.addSelectionToPerspectiveContext(getPart(), \"");
          _builder.append(dtoFqn, "\t\t");
          _builder.append("\", ");
          {
            boolean _equals_2 = "int".equals(keyType.getName());
            if (_equals_2) {
              _builder.append("dtoService.get((int) Integer.parseInt(idValue))");
            } else {
              _builder.append("dtoService.get(idValue)");
            }
          }
          _builder.append(");");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("} else {");
          _builder.newLine();
          _builder.append("\t\t");
          _builder.append("SelectionStore.removeSelectionFromPerspectiveContext(getPart(), \"");
          _builder.append(dtoFqn, "\t\t");
          _builder.append("\", ");
          {
            boolean _equals_3 = "int".equals(keyType.getName());
            if (_equals_3) {
              _builder.append("dtoService.get((int) Integer.parseInt(idValue))");
            } else {
              _builder.append("dtoService.get(idValue)");
            }
          }
          _builder.append(");");
          _builder.newLineIfNotEmpty();
          _builder.append("\t");
          _builder.append("}");
          _builder.newLine();
          _builder.append("}");
          _builder.newLine();
          _xblockexpression = _builder;
        }
        _switchResult = _xblockexpression;
      }
      _xifexpression = _switchResult;
    }
    return _xifexpression;
  }
  
  public void toBinderFields(final JvmDeclaredType type, final TablePackage tablePckg) {
    JvmField field = null;
    final Procedure1<JvmField> _function = (JvmField it) -> {
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("org.slf4j.LoggerFactory.getLogger(\"servicebinder\")");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setInitializer(it, _function_1);
    };
    field = this._jvmTypesBuilder.toField(tablePckg, "log", this._typeReferenceBuilder.typeRef(Logger.class), _function);
    field.setStatic(true);
    field.setVisibility(JvmVisibility.PRIVATE);
    EList<JvmMember> _members = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members, field);
    field = this._jvmTypesBuilder.toField(tablePckg, "userAccessService", this._typeReferenceBuilder.typeRef(IUserAccessService.class));
    field.setStatic(true);
    field.setVisibility(JvmVisibility.PRIVATE);
    EList<JvmMember> _members_1 = type.getMembers();
    this._jvmTypesBuilder.<JvmField>operator_add(_members_1, field);
  }
  
  public void toBinderOperations(final JvmDeclaredType type, final TablePackage tablePckg) {
    EList<JvmMember> _members = type.getMembers();
    final Procedure1<JvmOperation> _function = (JvmOperation it) -> {
      it.setVisibility(JvmVisibility.PUBLIC);
      it.setStatic(true);
      final Procedure1<ITreeAppendable> _function_1 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("return userAccessService;");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_1);
    };
    JvmOperation _method = this._jvmTypesBuilder.toMethod(tablePckg, "getUserAccessService", this._typeReferenceBuilder.typeRef(IUserAccessService.class), _function);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members, _method);
    EList<JvmMember> _members_1 = type.getMembers();
    final Procedure1<JvmOperation> _function_1 = (JvmOperation it) -> {
      JvmAnnotationReference annotationRef = this._annotationTypesBuilder.annotationRef(Reference.class);
      this._annotationExtension.addAnnAttr(annotationRef, tablePckg, "cardinality", ReferenceCardinality.MANDATORY);
      this._annotationExtension.addAnnAttr(annotationRef, tablePckg, "policy", ReferencePolicy.STATIC);
      EList<JvmAnnotationReference> _annotations = it.getAnnotations();
      this._jvmTypesBuilder.<JvmAnnotationReference>operator_add(_annotations, annotationRef);
      it.setVisibility(JvmVisibility.PUBLIC);
      it.setSynchronized(true);
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(tablePckg, "userAccessService", this._typeReferenceBuilder.typeRef(IUserAccessService.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      final Procedure1<ITreeAppendable> _function_2 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        String _firstUpper = StringExtensions.toFirstUpper(this._iQualifiedNameProvider.getFullyQualifiedName(tablePckg).getLastSegment());
        String _plus = (_firstUpper + "ServiceBinder");
        _builder.append(_plus);
        _builder.append(".userAccessService = userAccessService;");
        _builder.newLineIfNotEmpty();
        _builder.append("log.debug(\"Datamart UserAccessService bound\");");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_2);
    };
    JvmOperation _method_1 = this._jvmTypesBuilder.toMethod(tablePckg, "bindUserAccessMethod", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_1);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_1, _method_1);
    EList<JvmMember> _members_2 = type.getMembers();
    final Procedure1<JvmOperation> _function_2 = (JvmOperation it) -> {
      it.setVisibility(JvmVisibility.PUBLIC);
      it.setSynchronized(true);
      EList<JvmFormalParameter> _parameters = it.getParameters();
      JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(tablePckg, "userAccessService", this._typeReferenceBuilder.typeRef(IUserAccessService.class));
      this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
      final Procedure1<ITreeAppendable> _function_3 = (ITreeAppendable it_1) -> {
        StringConcatenation _builder = new StringConcatenation();
        String _firstUpper = StringExtensions.toFirstUpper(this._iQualifiedNameProvider.getFullyQualifiedName(tablePckg).getLastSegment());
        String _plus = (_firstUpper + "ServiceBinder");
        _builder.append(_plus);
        _builder.append(".userAccessService = null;");
        _builder.newLineIfNotEmpty();
        _builder.append("log.debug(\"Datamart UserAccessService unbound\");");
        it_1.append(_builder);
      };
      this._jvmTypesBuilder.setBody(it, _function_3);
    };
    JvmOperation _method_2 = this._jvmTypesBuilder.toMethod(tablePckg, "unbindUserAccessMethod", this._typeReferenceBuilder.typeRef(Void.TYPE), _function_2);
    this._jvmTypesBuilder.<JvmOperation>operator_add(_members_2, _method_2);
  }
  
  public void infer(final EObject pkg, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    if (pkg instanceof TablePackage) {
      _infer((TablePackage)pkg, acceptor, isPreIndexingPhase);
      return;
    } else if (pkg != null) {
      _infer(pkg, acceptor, isPreIndexingPhase);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(pkg, acceptor, isPreIndexingPhase).toString());
    }
  }
}
